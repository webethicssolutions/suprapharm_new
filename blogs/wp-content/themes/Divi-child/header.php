<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();
?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
	  <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="INSPIRO" />
    <meta name="description" content="Themeforest Template Polo">
    <!-- Document title -->
    <title>Index | Suprapharm</title>
    <!-- Stylesheets & Fonts --><link href="<?php echo get_stylesheet_directory_uri(); ?>/css/plugins.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>css/responsive.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>css/slick-theme.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>css/slick.css" rel="stylesheet">
</head>

<body>


    <!-- Body Inner -->    <div class="body-inner">


        <!-- Topbar -->
        <div id="topbar" class="topbar-fullwidth d-none d-xl-block d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="top-menu">
                            <li><a href="tel:01 55 21 70 76"><i class="fa fa-phone"></i> 01 55 21 70 76</a></li>
                            <li><a href="mailto:contact@suprapharm.fr"><i class="fa fa-envelope"></i> contact@suprapharm.fr</a></li>
							<li><i class="fa fa-map-marker-alt"></i>Address: 108 rue Marius Aufan, 92300 Levallois Perret</li>
                        </ul>
                    </div>
                    <div class="col-md-4 d-none d-sm-block">
                        <div class="social-icons social-icons-colored-hover">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Topbar -->

        <!-- Header -->
        <header id="header" data-fullwidth="true">
            <div class="header-inner">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="https://suprapharm.webethics.online/" class="logo" data-src-dark="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-supra1.png">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-supra1.png" alt="Polo Logo">
                        </a>
                    </div>
                    <!--End: Logo-->

                                        <!-- Search -->
                    <div id="search">
                        <div id="search-logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-supra1.png" alt="Polo Logo"></div>
                        <button id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i
                                class="icon-x"></i></button>
                        <form class="search-form" action="search-results-page.html" method="get">
                            <input class="form-control" name="q" type="search" placeholder="Search..."
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
                            <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                        </form>

                        
                    </div>
                    <!-- end: search -->

                    <!--Header Extras-->
                    <div class="header-extras">
                        <ul>
                                                        <li>
                                <!--search icon-->
                                <a id="btn-search" href="#"> <i class="icon-search1"></i></a>
                                <!--end: search icon-->
                            </li>
                          
                        </ul>
                    </div>
                    <!--end: Header Extras-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="index.html">C’est quoi?</a></li>
                                    <li> <a href="#">Nos services</a></li>
									<li> <a href="#">Cartes des pharmacies</a></li>
                                    <li> <a href="#">Accès membres</a></li>
                                  
                
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->
