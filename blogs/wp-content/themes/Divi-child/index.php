<?php get_header(); ?>

<div id="main-content blog">
<div class="container-fluid blog-container">
<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

if (strpos($url,'category') !== false) { ?>
<div class="et_pb_section et_pb_section_post et_pb_section_1 et_pb_with_background et_section_regular"> 
					<div class="et_pb_row et_pb_row_0 et_pb_gutters4">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h1 style="text-align: center;"><?php echo ucwords(str_ireplace('-',' ',$category_name));?></h1>
					<div class="line-white-long"></div>
					<div class="line-white-short"></div>
					</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div>
			<?php } else if (strpos($url,'author') !== false) { ?>
<div class="et_pb_section et_pb_section_post et_pb_section_1 et_pb_with_background et_section_regular"> 
					<div class="et_pb_row et_pb_row_0 et_pb_gutters4">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h1 style="text-align: center;"><?php echo $author = get_the_author();?></h1>
					<div class="line-white-long"></div>
					<div class="line-white-short"></div>
					

				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div>
			<?php } else if (strpos($url,'blog') !== false) { ?>
<div class="et_pb_section et_pb_section_post et_pb_section_1 et_pb_with_background et_section_regular">
				
				
				
				
					<div class="et_pb_row et_pb_row_0 et_pb_gutters4">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h1 style="text-align: center;">Blog</h1>
					<div class="line-white-long"></div>
					<div class="line-white-short"></div>
					
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div>
			<?php } else if (isset($_GET['s']) && trim($_GET['s']) != "") { ?>
				<div class="et_pb_section et_pb_section_post et_pb_section_1 et_pb_with_background et_section_regular"> 
					<div class="et_pb_row et_pb_row_0 et_pb_gutters4">
						<div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child"> 
							<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left"> 
								<div class="et_pb_text_inner">
									<h1 style="text-align: center;">Suprapharm</h1>
									<div class="line-white-long"></div>
									<div class="line-white-short"></div> 
									
								</div>
							</div> <!-- .et_pb_text -->
						</div> <!-- .et_pb_column --> 
					</div> <!-- .et_pb_row --> 
			</div>
			<?php
			}else{
			 echo do_shortcode('[et_pb_section global_module="289"][/et_pb_section]');
			}			?>
</div>
<section class="pt-0">
	<div class="container-fluid">
		<div id="content-area" class="clearfix">
	
			
			
			<div id="blog" class="grid-layout post-4-columns m-b-30" data-item="post-item" data-stagger="10">
		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format(); ?>

                    
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post post-item border blogpost' ); ?>>

				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_pb_post_main_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					et_divi_post_format_content();

					if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
						if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
							printf(
								'<div class="et_main_video_container">
									%1$s
								</div>',
								et_core_esc_previously( $first_video )
							);
						elseif ( ! in_array( $post_format, array( 'gallery' ) ) && 'on' === et_get_option( 'divi_thumbnails_index', 'on' ) && '' !== $thumb ) : ?>
						<?php
			$category_detail=get_the_category( $post->ID );
			
			$category_name = "";
			$category_link = "";
			
				$category_name = $category_detail[0]->name;
				$category_id   = $category_detail[0]->cat_ID;
				$category_link = get_category_link($category_id);
				/* echo "<pre>category_link ";print_r($category_link); */
			
			?> 
						 <div class="post-image">
							<a class="entry-featured-image-url" href="<?php the_permalink(); ?>">
								<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
							</a>
							<span class="post-meta-category"><a href="<?php echo $category_link;?>"><?php echo $category_name;?></a></span>
					<?php
						elseif ( 'gallery' === $post_format ) :
							et_pb_gallery_images();
						endif;
					} ?>
					</div>
<div class="post-item-wrap">
                            <div class="post-item-description">
                                
                                

                            
				<?php if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) : ?>
					<?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) : ?>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i><?php echo get_the_date();?></span>
                             <span class="post-meta-comments"><a href=""><i class="fa fa-comments-o"></i><?php echo get_comments_number();?> Comments</a></span>
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
					<?php endif; ?>

					<p><?php echo $stringCut = substr(get_the_excerpt(), 0, 80); ?></p>
					

                                <a href="<?php the_permalink(); ?>" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>
								</div>
                        </div>
				<?php endif; ?>

					</article>
					
					<!-- .et_pb_post -->
			<?php
					endwhile;
?></div>
		<div class="pagination_div">
		<?php			if ( function_exists( 'wp_pagenavi' ) )
						wp_pagenavi();
					else
						get_template_part( 'includes/navigation', 'index' );
				else :
					get_template_part( 'includes/no-results', 'index' );
				endif;
			?></div>
			
			
			<!-- #left-area -->
		</div> <!-- #content-area -->
	</div> 
	</section><!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();
