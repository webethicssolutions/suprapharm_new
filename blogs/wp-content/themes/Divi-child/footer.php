       <!-- Footer -->
       <footer id="footer">
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                            <div class="col-lg-4">
                                    <div class="widget">
                                
                                            <div class="widget-title">Suprapharm</div>
                                                <p class="mb-5">Groupement de Pharmacies Françaises <br/><a href="#">Carte des Pharmacies</a><br>
                                    </div>
                        </div> <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-4">
                                        <div class="widget">
                                            <div class="widget-title">Contact</div>
                                            <ul class="list">                                           
                                                    <li><a href="JavaScript:void(0);"><i class="fa fa-map-marker-alt"></i><span>108 rue Marius Aufan 92300 Levallois Perret</span></a></li>
                                                    <li><a href="tel:0155217076"><i class="fa fa-phone"></i><span>01 55 21 70 76</span></a></li>
                                                    <li><a href="mailto:contact@suprapharm.fr" target="_blank"><i class="fa fa-envelope"></i><span>contact@suprapharm.fr</span></a></li>
                                                </ul>
                                        </div>  
                                    </div>

                                        <div class="col-lg-4">
                                                <div class="widget">
                                                    <div class="widget-title">Actualités</div>
                                                    <ul class="list">
                                                            <li><a href="{{ url('/about_us')}}">C’est quoi?</a></li>
                                                            <li> <a href="{{ url('/services')}}">Nos Services</a></li>
                                                            <li> <a href="{{ url('/cards-of-pharmacies')}}">Cartes des pharmacies</a></li>
                                                            <li><a href="{{ url('/contact-us')}}">Contact</a></li>
                                                        </ul>
                                                </div>  
                                            </div>
                                
                                                <div class="col-lg-4">
                                                        <div class="widget">
                                                            <div class="widget-title">Instagram</div>
                                                            <ul class="list insta_pictures">                                           
                                                                    <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta1.jpg"></a></li>
                                                                    <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta2.jpg"></a></li>
                                                                    <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta3.jpg"></a></li>
																	<li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta1.jpg"></a></li>
                                                                    <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta2.jpg"></a></li>
                                                                    <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta3.jpg"></a></li>
                                                                </ul>
                                                        </div>  
                                                    </div>
                            </div>
                        </div>
                

                </div>
                </div>
            </div>
            <div class="copyright-content">
                <div class="container">
                    <div class="copyright-text text-center">© Suprapharm 2018 | 2020</div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
    <!--Plugins-->
   <?php wp_footer(); ?>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/plugins.js"></script>

    <!--Template functions-->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/functions.js"></script>
	    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js"></script>

    

	
</body>
</html>
