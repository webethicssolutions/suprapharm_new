
var thumb_width =200;
var thumb_height =200;

function preview(img, selection) { 
	//get width and height of the uploaded image.
	
	var current_width = $('#uploaded_image').find('#thumbnail').width();
	var current_height = $('#uploaded_image').find('#thumbnail').height();

	var scaleX = thumb_width / selection.width; 
	var scaleY = thumb_height / selection.height; 
	
	$('#uploaded_image').find('#thumbnail_preview').css({ 
		width: Math.round(scaleX * current_width) + 'px', 
		height: Math.round(scaleY * current_height) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
	});
	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
} 

//show and hide the loading message
function loadingmessage(msg, show_hide){
	if(show_hide=="show"){
		$('#loader').show();
		$('#progress').show().text(msg);
		$('#uploaded_image').html('');
	}else if(show_hide=="hide"){
		$('#loader').hide();
		$('#progress').text('').hide();
	}else{
		$('#loader').hide();
		$('#progress').text('').hide();
		$('#uploaded_image').html('');
	}
}

//delete the image when the delete link is clicked.
function deleteimage(large_image, thumbnail_image,restaurant_logo){ 
	loadingmessage('Please wait, deleting images...', 'show');
	$.ajax({
		type: 'POST',
		url: base_url+'/restaurant/image_uplaod_crop',
		data: 'a=delete&large_image='+large_image+'&thumbnail_image='+thumbnail_image+'&_token='+$('input[name=_token]').val(),
		cache: false,
		success: function(response){
			loadingmessage('', 'hide');
			response = unescape(response);
			
			//window.location.reload();
			var response = response.split("|");
			var responseType = response[0];
			var responseMsg = response[1];
			if(responseType=="success"){ 
				var saved_image = $("#save_image").val();
				 
				if(restaurant_logo == 'undefined'){
				$('#crop_imaged').hide();
				}else{
					if(saved_image!= ""){
						$('#crop_imaged').html('<img src="'+saved_image+'" alt="Thumbnail Image" width="60" height="60" />');
					}else{
						$('#crop_imaged').html('<img src="'+restaurant_logo+'" alt="Thumbnail Image" width="60" height="60" />');
					}
					
				}  
			}else{
				//$('#upload_status').show().html('<h1>Unexpected Error</h1><p>Please try again</p>'+response);
			}
		}
	});
}

$(document).ready(function ($) {
		$('#loader').hide();
		$('#progress').hide();
		var myUpload = $('#upload_link').upload({
		   name: 'image',
		   action: base_url+'/restaurant/image_uplaod_crop',
		   enctype: 'multipart/form-data',
		   params: {upload:'Upload',_token:$('input[name=_token]').val()},
		   autoSubmit: true,
		   onSubmit: function() {
		   		$('#upload_status').html('').hide();
				loadingmessage('Please wait, uploading file...', 'show');
		   },
		   onComplete: function(response) {
		   		loadingmessage('', 'hide');
				response = unescape(response);
				var response = response.split("|");
				var responseType = response[0];
				var responseMsg = response[1];
				if(responseType=="success"){
					var current_width = response[2];
					var current_height = response[3];
					//display message that the file has been uploaded
					 
					//put the image in the appropriate div
					$('#uploaded_image').html('<img src="'+responseMsg+'" style="float: left; margin-right: 10px;" id="thumbnail" alt="Create Thumbnail" /><div style="border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden; width:'+thumb_width+'px; height:'+thumb_height+'px;"><img src="'+responseMsg+'" style="position: relative;" id="thumbnail_preview" alt="Thumbnail Preview" /></div>');
					
					$('#crop_image').modal('show');
					$('#crop_imaged').show();
					//find the image inserted above, and allow it to be cropped
					$('#uploaded_image').find('#thumbnail').imgAreaSelect({ aspectRatio: '1:'+thumb_height/thumb_width, onSelectChange: preview }); 
					//display the hidden form
					$('#thumbnail_form').show();
				}else if(responseType=="error"){
					$('#upload_status').show().html('<h1>Error</h1><p>'+responseMsg+'</p>');
					$('#uploaded_image').html('');
					$('#thumbnail_form').hide();
				}else{
					$('#upload_status').show().html('<h1>Unexpected Error</h1><p>Please try again</p>'+response);
					$('#uploaded_image').html('');
					$('#thumbnail_form').hide();
				}
		   }
		});
	
	//create the thumbnail
	$('#save_thumb').click(function(e) {
		
		e.preventDefault();
		var x1 = $('#x1').val();
		var y1 = $('#y1').val();
		var x2 = $('#x2').val();
		var y2 = $('#y2').val();
		var w = $('#w').val();
		var h = $('#h').val();
	
		if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
			alert("You must make a selection first");
			return false;
		}else{
			//hide the selection and disable the imgareaselect plugin
			$('#uploaded_image').find('#thumbnail').imgAreaSelect({ disable: true, hide: true }); 
			loadingmessage('Please wait, saving thumbnail....', 'show');
			$.ajax({
				type: 'POST',
				url: base_url+'/restaurant/image_uplaod_crop',
				data: 'save_thumb=Save Thumbnail&x1='+x1+'&y1='+y1+'&x2='+x2+'&y2='+y2+'&w='+w+'&h='+h+'&_token='+$('input[name=_token]').val()+'&restaurant_logo='+$('#restaurant_logo').val(),
				cache: false,
				success: function(response){
					loadingmessage('', 'hide');
					response = unescape(response);
					var response = response.split("|");
					var responseType = response[0];
					var responseLargeImage = response[1];
					var responseThumbImage = response[2];
					var responseLargeImageLocation = response[3];
					var responseThumbImageLocation = response[4];
					var restaurant_logo = response[5];
					if(responseType=="success"){ 
						//load the new images
						$('#crop_imaged').html('<img src="'+responseThumbImage+'" alt="Thumbnail Image"/><br /><a href="javascript:deleteimage(\''+responseLargeImageLocation+'\', \''+responseThumbImageLocation+'\', \''+restaurant_logo+'\');">Discard Images</a>');

						 //hide the thumbnail form
						$('#crop_image').modal('hide');
						$('#crop_imaged').show();
						$('#thumbnail_form').hide();
					}else{
						$('#upload_status').show().html('<h1>Unexpected Error</h1><p>Please try again</p>'+response);
						//reactivate the imgareaselect plugin to allow another attempt.
						$('#crop_imaged').find('#thumbnail').imgAreaSelect({ aspectRatio: '1:'+ thumb_height/thumb_width, onSelectChange: preview }); 
						$('#thumbnail_form').show();
					}
				}
			});
			
			return false;
		}
	});
}); 
/************* DELETE A RESTAURANT CONFIRM BLOCK *********/
function confirm_delete(url){
   $('#myModal').modal('show')
    .one('click', '#delete', function(e) {
      window.location = url;
  }); 
}

$(document).ready(function(){
	$("#close").click(function(){
		$(".imgareaselect-outer").css('position','');
		$(".imgareaselect-outer").css('width','0px');
		$(".imgareaselect-outer").css('height','0px');
		$(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4").css('background','none'); 
	}) 
});