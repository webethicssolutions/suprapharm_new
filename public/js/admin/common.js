/* user data table */
jQuery(document).ready(function(){

		jQuery('.fltr-select').prop('disabled',true);


jQuery('.file-check').click(function(){


		if(jQuery(this).is(":checked"))
		{
			jQuery('#fltr-rw').show();
			jQuery('.fltr-select').prop('disabled',false);
		}
		else
		{
			var any=0;
			jQuery('.file-check').each(function(){
			if(jQuery(this).is(":checked"))
			{
			any=any+1;
			}
			});
			if(any>0){
				jQuery('#fltr-rw').show();
				jQuery('.fltr-select').prop('disabled',false);
			}
			else{
				jQuery('#fltr-rw').hide();
				jQuery('#check-all').prop('checked',false);
					jQuery('.fltr-select').prop('disabled',true);
			}
		}
	});

jQuery('#add-files').click(function(){
	all_ids=[];
	every=[];
	jQuery('.file-check').each(function(){
		every.push(jQuery(this).val());
		if(jQuery(this).is(":checked"))
		{
			all_ids.push(jQuery(this).val());
		}
	});
	var _cat_id = $( "#parent_category option:selected" ).val();
	var _sub_cat_id = $( "#sub_category option:selected" ).val();

	var usr_tokn = $('input[name="_token"]').val();
	 ajax_url = "/admin/save-images-to-category";
	$.ajax({
			type: "POST",
			url: ajax_url,
			data:{'_token':usr_tokn,'img_id':all_ids,'cat_id':_cat_id,'sub_cat_id':_sub_cat_id,'every':every},
			success: function (response) {
				console.log('response '+response);
				location.reload();
			}
		});
});




	$('#check-all').click(function(){
		if($(this).prop("checked") == true){
			jQuery('#fltr-rw').show();
			jQuery('.file-check').prop('checked',true);
				jQuery('.fltr-select').prop('disabled',false);

		}

		else if($(this).prop("checked") == false){
	jQuery('#fltr-rw').hide();
			jQuery('.file-check').prop('checked',false);
				jQuery('.fltr-select').prop('disabled',true);

		}
	})

	$("#parent_category").change(function(){

		if($(this).data('val')!=""){

			var value = $(this).val();
			var usr_tokn = $('input[name="_token"]').val();
			ajax_url = "/admin/category/get-sub-categories";
			 $.ajax({
				type: "POST",
				url: ajax_url,
				data:{'_token':usr_tokn,value:value},
				success: function (data) {
					 $("#sub_category").empty();
           $("#sub_category").append('<option value="">Select Sub Category</option>');
           $.each(data,function(key,value){

                   $("#sub_category").append('<option value="'+key+'">'+value+'</option>');

				});
			}
			});
			 jQuery("#sub_category").val('')
			 var sub_cat=jQuery("#sub_category").val();

			  all_imgs=[];
			 ajax_url1 = "/admin/category/selected-categories";
			 $.ajax({
				type: "POST",
				url: ajax_url1,
				data:{'_token':usr_tokn,value:value,sub_cat:sub_cat},
				success: function (data) {
					 $.each(data,function(key,value){

                  all_imgs.push(value);

				});
					 jQuery('.file-check').each(function(){
		var curr=jQuery(this).val();
		 if(jQuery.inArray(curr, all_imgs) !== -1)
		{
			jQuery(this).prop('checked',true);
		}
		else{
			jQuery(this).prop('checked',false);
		}
	});


					 }

				});
			}


	})



	$("#sub_category").change(function(){

		if($(this).data('val')!=""){
		var value=jQuery("#parent_category").val();
			var sub_cat = $(this).val();
			var usr_tokn = $('input[name="_token"]').val();

			  all_imgs=[];
			 ajax_url1 = "/admin/category/selected-categories";
			 $.ajax({
				type: "POST",
				url: ajax_url1,
				data:{'_token':usr_tokn,value:value,sub_cat:sub_cat},
				success: function (data) {
					 $.each(data,function(key,value){

                  all_imgs.push(value);

				});
					 jQuery('.file-check').each(function(){
		var curr=jQuery(this).val();
		 if(jQuery.inArray(curr, all_imgs) !== -1)
		{
			jQuery(this).prop('checked',true);
		}
		else{
			jQuery(this).prop('checked',false);
		}
	});


					 }

				});
			}


	})



	$('#cardNumber').keyup(function() {
		var foo = $(this).val().split("-").join(""); // remove hyphens
		if (foo.length > 0) {
			foo = foo.match(new RegExp('.{1,4}', 'g')).join("-");
		}
		$(this).val(foo);
	});
	$('#expiryDD').keyup(function() {
		var foo = $(this).val().split("/").join(""); // remove hyphens
		if (foo.length > 0) {
			foo = foo.match(new RegExp('.{1,2}', 'g')).join("/");
		}
		$(this).val(foo);
	});
})



$(function () {
	$("#users").DataTable({
		"order": [] ,
		'aoColumnDefs': [{
			'bSortable': false,
			'aTargets': [], /* 1st one, start by the right */
			"aaSorting": []
		}]
	});


	$("#archieve").DataTable({
		"order": [] ,
		'aoColumnDefs': [{
			'bSortable': false,
			'aTargets': [], /* 1st one, start by the right */
			"aaSorting": []
		}]
	});

	setTimeout(function(){ $('#success-msg').hide() }, 2000);


	/* Enable/Disable user status */
	$(".usr_status").change(function(){
        $(".success-msg").hide();
        $(".erorr-msg").hide();
        if($(this).data('val')!=""){
            var u_id = $(this).data('val');
            var usr_tokn = $('input[name="_token"]').val();

            var ajax_url = "";
            var ustatus = "";
            if($(this).prop("checked") == true){
                ajax_url = "/admin/user/enableuser/"+u_id;
                ustatus = "Enabled";
            }else{
                ajax_url = "/admin/user/disableuser/"+u_id;
                ustatus = "Disabled";
            }

            $.ajax({
                type: "POST",
                url: ajax_url,
                data:{'_token':usr_tokn},
                success: function (response) {
                    console.log('response '+response);
                    if(response == "success"){
                        $(".msg_sec").html('<div class="success-msg">User Status '+ustatus+' Successfully.</div>')
                    }else{
                        $(".msg_sec").html('<div class="success-msg">Something went wrong.</div>')
                    }

                    $(".msg_sec").show();
                }
            });
        }
    });

	$("#archieve .lead_status").change(function(){
		$(".success-msg").hide();
		$(".erorr-msg").hide();
		if($(this).data('val')!=""){
			var lead_id = $(this).data('lead_id');
			var status_id = $(this).val();
			var row_id  = $(this).data('row_id');

			var usr_tokn = $('input[name="_token"]').val();
			ajax_url = "changestatus";
			 $.ajax({
				type: "POST",
				url: ajax_url,
				data:{'_token':usr_tokn,'status_id':status_id,'lead_id':lead_id},
				success: function (response) {
					if(response == "success"){
						$(".msg_sec").html('<div class="success-msg">Leads Status updated Successfully.</div>');
						if(status_id != 4){
							$('tr.row_'+row_id).hide();
						}
					}else{
						$(".msg_sec").html('<div class="success-msg">Something went wrong.</div>')
					}

					$(".msg_sec").show();
				}
			});
		}
	})
	$("#users .lead_status").change(function(){
		$(".success-msg").hide();
		$(".erorr-msg").hide();
		if($(this).data('val')!=""){
			var lead_id = $(this).data('lead_id');
			var status_id = $(this).val();
			var row_id  = $(this).data('row_id');

			var usr_tokn = $('input[name="_token"]').val();
			ajax_url = "changestatus";
			 $.ajax({
				type: "POST",
				url: ajax_url,
				data:{'_token':usr_tokn,'status_id':status_id,'lead_id':lead_id},
				success: function (response) {
					if(response == "success"){
						$(".msg_sec").html('<div class="success-msg">Leads Status updated Successfully.</div>');
						if(status_id == 4){
							$('tr.row_'+row_id).hide();
						}
					}else{
						$(".msg_sec").html('<div class="success-msg">Something went wrong.</div>')
					}

					$(".msg_sec").show();
				}
			});
		}
	})
	/* End Enable/Disable user status */
	$('#status').change( function() {


		$.fn.dataTable.ext.search.push(
			function( settings, data, dataIndex ) {
				var min =  $('#status').val();
				var age =  data[3]; // use data for the age column
				if ( min == age && min != 'all')
				{
					console.log(1);
					return true;
				}
				if(min == 'all'){
					console.log(2);
					return true;
				}
				return false;
			}
		);
		var table = $('#users').DataTable();
		table.draw();

    });

	/* Enable/Disable image */
	$(".image_status").change(function(){
		$(".success-msg").hide();
		$(".erorr-msg").hide();
		if($(this).data('val')!=""){
			var u_id = $(this).data('val');
			var usr_tokn = $('input[name="_token"]').val();

			var ajax_url = "";
			var ustatus = "";
			if($(this).prop("checked") == true){
				ajax_url = "/admin/user/enableimage/"+u_id;
				ustatus = "Enabled";
			}else{
				ajax_url = "/admin/user/disableimage/"+u_id;
				ustatus = "Disabled";
			}

			$.ajax({
				type: "POST",
				url: ajax_url,
				data:{'_token':usr_tokn},
				success: function (response) {
					console.log('response '+response);
					if(response == "success"){
						$(".msg_sec").html('<div class="success-msg">Image Status '+ustatus+' Successfully.</div>')
					}else{
						$(".msg_sec").html('<div class="success-msg">Something went wrong.</div>')
					}

					$(".msg_sec").show();
				}
			});
		}
	})
	/* End Enable/Disable user status */
 });
 	/* Start delete Mass delete Promotion */

 $('#Check_all').on('click', function(e) {
         if($(this).is(':checked',true))
         {
            $(".checkbox1").prop('checked', true);
         } else {
            $(".checkbox1").prop('checked',false);
         }
        });
         $('.checkbox1').on('click',function(){
            if($('.checkbox:checked').length == $('.checkbox').length){
                $('#Check_all').prop('checked',true);
            }else{
                $('#Check_all').prop('checked',false);
            }

         });

 $('.delete-All').on('click', function(e) {

            var idsArr = [];
            $(".checkbox1:checked").each(function() {

                idsArr.push($(this).attr('data-id'));

            });
            if(idsArr.length <=0)
            {
                alert("Please select atleast one record to delete.");
            }  else {
                if(confirm("Are you sure, you want to delete the selected Magzines?")){
                    var strIds = idsArr.join(",");
					var usr_tokn = $('input[name="_token"]').val();


                    $.ajax({
                        url: '/admin/promotion/multiple-delete',
                        type: 'POST',
                        data: 'ids='+strIds+ '&_token=' + usr_tokn,
						dataType:'json',
                        success: function (data) {

                            if(data.success) {


                                $(".checkbox1:checked").each(function() {

                                    $(this).parent().parent().remove();
                                });
                            } else {
                                alert('Whoops Something went wrong!');
                            }
                        },

                    });
                }

            }

        });


  	/* End delete Mass delete Promotion */



 	$('#check_all').on('click', function(e) {
         if($(this).is(':checked',true))
         {
            $(".checkbox").prop('checked', true);
         } else {
            $(".checkbox").prop('checked',false);
         }
        });
         $('.checkbox').on('click',function(){
            if($('.checkbox:checked').length == $('.checkbox').length){
                $('#check_all').prop('checked',true);
            }else{
                $('#check_all').prop('checked',false);
            }

         });

 $('.delete-all').on('click', function(e) {

            var idsArr = [];
            $(".checkbox:checked").each(function() {

                idsArr.push($(this).attr('data-id'));

            });
            if(idsArr.length <=0)
            {
                alert("Please select atleast one record to delete.");
            }  else {
                if(confirm("Are you sure, you want to delete the selected Magzines?")){
                    var strIds = idsArr.join(",");
					var usr_tokn = $('input[name="_token"]').val();
				$.ajax({
                        url: '/admin/magzine/multiple-delete',
                        type: 'POST',
                        data: 'ids='+strIds+ '&_token=' + usr_tokn,
						dataType:'json',
                        success: function (data) {

                            if(data.success) {


                                $(".checkbox:checked").each(function() {

                                    $(this).parent().parent().remove();
                                });
                            } else {
                                alert('Whoops Something went wrong!');
                            }
                        },

                    });


                }

            }

        });
				function confirm_delete(url){
					$('#myModal').modal('show')
						.one('click', '#delete', function(e) {
						window.location = url;
					});
				}
				function upload_csv(url){
					$('#uploadcsv').modal('show')
						.one('click', '#delete', function(e) {
						window.location = url;
					});
				}




/**  Verify/Unverify User**/
function confirm_verify(url,verify_document){
	if(verify_document.trim() == ""){
		$("#veriModal iframe").hide();
		$("#veriModal .no_doc").html("No document found.");
		$("#veriModal .no_doc").show();
	}else{
		$("#veriModal .no_doc").html("");
		$("#veriModal .no_doc").hide();
		$("#veriModal iframe").attr("src",verify_document);
		$("#veriModal iframe").show();
	}

	$('#veriModal').modal('show')
		.one('click', '#verify', function(e) {
		window.location = url;
	});
}

function confirm_unverify(url,verify_document){
	if(verify_document.trim() == ""){
		$("#unveriModal iframe").hide();
		/* $("#unveriModal .no_doc").html("No document found.");
		$("#unveriModal .no_doc").show(); */
	}else{
		/* $("#unveriModal .no_doc").html("");
		$("#unveriModal .no_doc").hide(); */
		$("#unveriModal iframe").attr("src",verify_document);
		$("#unveriModal iframe").show();
	}

	$('#unveriModal').modal('show')
		.one('click', '#unverify', function(e) {
		window.location = url;
	});
}

 function replaceValidationUI( form ) {
    // Suppress the default bubbles
    form.addEventListener( "invalid", function( event ) {
        event.preventDefault();
    }, true );

    // Support Safari, iOS Safari, and the Android browser—each of which do not prevent
    // form submissions by default
    form.addEventListener( "submit", function( event ) {
        if ( !this.checkValidity() ) {
            event.preventDefault();
        }
    });

    var submitButton = form.querySelector( "button:not([type=button]), input[type=submit]" );
    submitButton.addEventListener( "click", function( event ) {
        var invalidFields = form.querySelectorAll( ":invalid" ),
            errorMessages = form.querySelectorAll( ".error-message" ),
            parent;

        // Remove any existing messages
        for ( var i = 0; i < errorMessages.length; i++ ) {
            errorMessages[ i ].parentNode.removeChild( errorMessages[ i ] );
        }

        for ( var i = 0; i < invalidFields.length; i++ ) {
            parent = invalidFields[ i ].parentNode;
            parent.insertAdjacentHTML( "beforeend", "<div class='error-message'>" +
                invalidFields[ i ].validationMessage +
                "</div>" );
        }

        // If there are errors, give focus to the first invalid field
        if ( invalidFields.length > 0 ) {
            invalidFields[ 0 ].focus();
        }
    });
}



	/* Enable/Disable magzine */
	$(".select_mag").change(function(){
		if($(this).data('val')!=""){
			var id = $(this).val();
			var mag_tokn = $('input[name="_token"]').val();

			var ajax_url = "";
			var magstatus = "";
			console.log(id);
			if($(this).prop("checked") == true){
				ajax_url = "/admin/magzine/enableimage/"+id;
				magstatus = "Enabled";
			}else{
				ajax_url = "/admin/magzine/disableimage/"+id;
				magstatus = "Disabled";
			}

			$.ajax({
				type: "POST",
				url: ajax_url,
				data:{'_token':mag_tokn},
				success: function (response) {
					console.log('response '+response);
					if(response == "success"){
						$(".msg_sec").html('')
					}else{
						$(".msg_sec").html('')
					}
					$(".msg_sec").show();
				}
			});
		}
	})


	/* Enable/Disable promotion */
	$(".select_pro").change(function(){
		if($(this).data('val')!=""){
			var id = $(this).val();
			var mag_tokn = $('input[name="_token"]').val();

			var ajax_url = "";
			var magstatus = "";
			console.log(id);
			if($(this).prop("checked") == true){
				ajax_url = "/admin/promotion/enablepromotion/"+id;
				prostatus = "Enabled";
			}else{
				ajax_url = "/admin/promotion/disablepromotion/"+id;
				prostatus = "Disabled";
			}
			$.ajax({
				type: "POST",
				url: ajax_url,
				data:{'_token':mag_tokn},
				success: function (response) {
					console.log('response '+response);
					if(response == "success"){
						$(".msg_sec").html('')
					}else{
						$(".msg_sec").html('')
					}

					$(".msg_sec").show();
				}
			});
		}
	})

// Replace the validation UI for all forms
var forms = document.querySelectorAll( "form" );
for ( var i = 0; i < forms.length; i++ ) {
    replaceValidationUI( forms[ i ] );
}
