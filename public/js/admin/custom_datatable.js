$(document).ready(function() {
		/*
		 * Find and assign actions to filters
		 */
		 $('.dtable').dataTable({
			"fnDrawCallback":function(){
				$(this).show();
			}
		}); 
		$.fn.dataTableExt.afnFiltering = new Array();
		var oControls = $('.dtable').prevAll('.dtable_custom_controls:first').find(':input[name]');
		oControls.each(function() {
			var oControl = $(this);
			console.log(oControl);
			//Add custom filters
			$.fn.dataTableExt.afnFiltering.push(function( oSettings, aData, iDataIndex ) {
				if ( !oControl.val() || !oControl.hasClass('dtable_filter') ) return true;

				for ( i=0; i<aData.length; i++ )
					if ( aData[i].indexOf(oControl.val()) != -1 )
						return true;

				return false;
			});
		});

		options = {
			"sDom"				: 'R<"H"lfr>t<"F"ip<',
			"bProcessing"		: true,
			"bJQueryUI"		: true,
			"bStateSave"		: true,
			"iDisplayLength"	: 8,
			"aLengthMenu"		: [[8, 25, 50, -1], [8, 25, 50, "All"]],
			"sPaginationType"	: "full_numbers",
			"aaSorting"			: [[ 0, "asc" ]],
			"fnDrawCallback"	: function(){
				//Without the CSS call, the table occasionally appears a little too wide
				$(this).show().css('width', '100%');
				//Don't show the filters until the table is showing
				$(this).closest('.dataTables_wrapper').prevAll('.dtable_custom_controls').show();
			},
			"fnStateSaveParams": 	function ( oSettings, sValue ) {
				//Save custom filters
				oControls.each(function() {
					if ( $(this).attr('name') )
						sValue[ $(this).attr('name') ] = $(this).val().replace('"', '"');
				});
				return sValue;
			},
			"fnStateLoadParams"	: function ( oSettings, oData ) {
				//Load custom filters
				oControls.each(function() {
					var oControl = $(this);

					$.each(oData, function(index, value) {
						if ( index == oControl.attr('name') )
							oControl.val( value );
					});
				});
				return true;
			}
		};
		/* var oTable = $('.dtable').dataTable(options); */

		/*
		 * Trigger the filters when the user interacts with them
		 */
		oControls.each(function() {
			console.log($(this));
			$(this).change(function() {
				//Redraw to apply filters
				oTable.fnDraw();
			});
		});
		
	});