$(document).ready(function(){
	$(".pos-content-showphone").click(function(){
		jQuery(this).addClass("hide"); 
		jQuery(".pos-content-phone, .pos-content-fax").removeClass("hide");
	});

	var marker;
	var lati= $("#map-lat").val();
	var lngi = $("#map-lng").val();
	var myLatLng = {lat: parseInt(lati), lng: parseInt(lngi)};

	if($('#lf_accessmap_canvas').length > 0) {
		initMap1();
	}
	function initMap1() {
		map = new google.maps.Map(document.getElementById('lf_accessmap_canvas'), {
			center: myLatLng,
			zoom: 12
		});
		marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			icon: '/assets/frontend/new/images/marker-pos.png',
		});
	}

	var input = document.getElementById('query');
    var options = {
      	componentRestrictions: {country: "fr"}
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.bindTo('bounds', map);
     
    autocomplete.addListener('place_changed', function() {
       	var place = autocomplete.getPlace();
       	var slatitude = place.geometry.location.lat();
	   	var slongitude = place.geometry.location.lng();
	   	$('.searchform-submit').attr('disabled', false);
       	$('#slat').val(slatitude);
       	$('#slng').val(slongitude);
    });

	$(".pos-content-tabs-row h2").click(function() {
		$(".pos-content-tabs-row h2").removeClass('active');
		$(this).addClass('active');
		var link = $(this).data('id');
		$('html, body').animate({
			scrollTop: $("#"+link).offset().top-100
		},500);
	});

	function showLocation(position) {
		var latitude = position.coords.latitude;
		var longitude = position.coords.longitude;
		$('#slat').val(latitude);
		$('#slng').val(longitude);
		$('#searchForm').submit();
	}

	function errorHandler(err) {
		if(err.code == 1) {
		   	alert("Error: Access is denied!");
		} else if( err.code == 2) {
		   	alert("Error: Position is unavailable!");
		}
	}

	function getLocation() {
		if(navigator.geolocation) {  
		   	var options = {timeout:60000};
		   	navigator.geolocation.getCurrentPosition(showLocation, errorHandler, options);
		} else {
		   	alert("Sorry, browser does not support geolocation!");
		}
	}

	$('.searchform-geoloc').click(function(){
		getLocation();
	})

})

