$(document).ready(function(){ 
	/* if($("#sign_up") != 'undefined'){ 
		$("#sign_up")[0].reset();	
	}   */	
	  
	show_read_more();
	/* get_post_comment_likes(); */
	  
	 /* check for new notifications after every 3 seconds */
	setInterval(function(){  
		check_notification();	
	},3000)  
	 /* end check for new notifications after every 3 seconds */
		
	/************************** Login/Signup Functionnality ****************************************/
	
	$('#login').click(function(){ 
		var token          =  $('input[name=_token]').val();
		var login_email    =  $('#login_email').val().trim();
		var login_password =  $('#login_password').val().trim();
		
		var pattern =  /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/; 
		
		$(".error").text('');  
		if(pattern.test(login_email) && login_password != ""){  
			if ($('#agree_trms').prop('checked') == false){  
				$("#agree_trms").css("outline","2px solid #f80505 !important");
				$(".btn-login").attr('disabled','disabled');
			}else{   
				$(".btn-login").removeAttr('disabled');
				$("#agree_trms").css("outline","none"); 
			}
		} 
		
		var action_type = $("#action_type").val();  
		var params = {'login_email': login_email,'login_password': login_password,'_token':token};
		if(action_type == "signup"){ 
			$.ajax({
				type: 'POST',
				url: base_url+'/signup',
				data: params,
				dataType: 'json',
				success: function (data) {
					if( data.success === true ) {
						$("#login_email").removeClass('login_errs');					 
						$("#login_password").removeClass('login_errs');
						window.location.href = base_url+'/dashboard'; 
					}else{
						$("#login_password_error").text(val)   
					}       
				},
				error: function (reject) {  }
			});
		}else{ 
			$.ajax({
				type: 'POST',
				url: base_url+'/login',
				data: params,
				dataType: 'json',
				success: function (data) {
					if( data.success === false ) {
						$.each(data.errors, function (key, val) {
							if(val == "Please fill correct credential."){
								 $("#login_password_error").text(val);
							}else{
								$("#" + key).addClass('login_errs');
							}
						});          
					}
					if( data.success === true ) { 
						$("#login_email").removeClass('login_errs');					 
						$("#login_password").removeClass('login_errs');	 
						if(data.referer!='')
							window.location.href = data.referer;
					   else
							window.location.href = base_url+'/feeds/'+data.user_id; 
							   
					}          
				},
				error: function (reject) {  }
			});
		} 
	});  
			
	$('#login_email').keyup(function(){
		var user_email = $(this).val().trim();
		var token      =  $('input[name=_token]').val();
		
		var pattern =  /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		
		if(pattern.test(user_email)){
			$("#login_email").removeClass('login_errs'); 
		}
		else{
			$("#login_email").addClass('login_errs');
			$(".btn-login").attr('disabled','disabled'); 
		}
		
		var params = {'user_email': user_email,'_token':token};
		$.ajax({
			type: 'POST',
			url: base_url+'/check_user_email',
			data: params, 
			success: function (data) { 
				$(".agree-trms").hide();
				$("#action_type").val("login"); 
				
				if ($('#agree_trms').prop('checked') == false){ 
					 $(".btn-login").attr('disabled','disabled');
				}else{
					$(".btn-login").removeAttr('disabled');
				}
				if(data == "exists"){
					$(".btn-login").text('Login');
					$(".btn-login").removeAttr('disabled'); 
				}else if(data == "not exists"){ 
					$(".btn-login").text('Sign Up');
					$(".agree-trms").show();
					$("#agree_trms").css("outline","2px solid #f80505 !important");
					$("#action_type").val("signup");
				}else{ 
					/* $("#login_password_error").text(data); */
					$(".btn-login").attr('disabled','disabled');
					$(".btn-login").text('Login / Sign Up'); 
				} 
			},
			error: function (reject) {  }
		});	 
	}); 
	
	$('#login_password').keyup(function(){
		var user_pass = $(this).val().trim();
		 
		if(user_pass != ""){
			$("#login_password").removeClass('login_errs');	  
			var action_type = $("#action_type").val();  
			if(action_type == "signup"){
				if ($('#agree_trms').prop('checked') == true){ 
					var login_email =  $('#login_email').val().trim();
					var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/; 
					if(pattern.test(login_email) && login_password != ""){ 
						$(".btn-login").removeAttr("disabled");
					}else{
						console.log(' not pattern ');
					}
				}else{ 
					$(".btn-login").attr("disabled","disabled");
				}
			}else{ 
				var login_email =  $('#login_email').val().trim();
				var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/; 
				if(pattern.test(login_email) && login_password != ""){
					$(".btn-login").removeAttr("disabled");
				}else{
					console.log(' not pattern ');
				}
			}
		}
		else{
			$(".btn-login").attr("disabled","disabled"); 
		} 
	});
			
	$('#agree_trms').change(function(){
		var login_email    =  $('#login_email').val().trim();
		var login_password =  $('#login_password').val().trim(); 
		if ($('#agree_trms').prop('checked') == true){ 
			var pattern =  /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/; 
			if(pattern.test(login_email) && login_password != ""){
				$(".btn-login").removeAttr("disabled");
			}
		}else{
			$(".btn-login").attr("disabled","disabled");
		}
	});
		
/************************** End Login/Signup Functionnality ****************************************/
			
/******************** Forgot Password **************************************/			
	$('#forgot_password').click(function(){
		var token =  $('input[name=_token]').val();
		var forgot_email =  $('#forgot_email').val();
		$(".error").text(''); 
		var params = {forgot_email: forgot_email,'_token':token};
		$.ajax({
			type: 'POST',
			url: base_url+'/forgot_password',
			data: params,
			dataType: 'json',
			success: function (data) {
				if( data.success === false ) {
					$.each(data.errors, function (key, val) { 
						$("#" + key + "_error").text(val[0]);
					});          
				}
				if( data.success === true ) { 
					$('#fwd-modal').modal('hide');
					$('#fwd-modal-msg').modal({show: true});
				}          
			},
			error: function (reject) {  }
		});
	}) ;  
 
	$("#reset_pass").click(function(){ 
		$(".msg_sec").html("");
		var reset_email = $(".reset_email").val().trim();
		var reset_token = $("input[name='_token']").val(); 
		if(reset_token){
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			emailReg.test( reset_email );
			if(reset_email == ""){
				$(".msg_sec").html("<div class='error-msg'>Please enter email.</div>");
				$(".msg_sec").show();
			}else if(!emailReg.test(reset_email)){
				$(".msg_sec").html("<div class='error-msg'>Please enter valid email.</div>");
				$(".msg_sec").show();
			}else{
				$(".msg_sec").html("");
				$(".msg_sec").hide();
				$.ajax({ 
					type: "post", 
					url: base_url+'/forgot_password',
					data:{'forgot_email':reset_email,'_token':reset_token}
				})
				.done(function(data){ 
					if(data.success){
						/* $(".msg_sec").html("<div class='success-msg'>Please check your e-mail for a temporary password reset link and make sure you set a new one right after you click it.</div>");
						$(".msg_sec").show();
						*/ 
						$(".forgot_pass #contnt").text("Please check your e-mail for a temporary password reset link and make sure you set a new one right after you click it.");
						$(".reset_email").hide();
						$(".forgot_pass .modal-footer").hide(); 
						$(".forgot_pass .modal-title").text("Message"); 
					
					}else{ 
						$(".msg_sec").html("<div class='error-msg'>"+data.errors+"</div>"); 
						$(".msg_sec").show();
					}
				});
			}
		}
	});
	
	 
	/* change password from dashboard page */ 
	$(".btn_password").click(function(){
		$(".msg_sec").hide();
		$(".success-msg").text("");
		$(".error-msg").text(""); 
		var current_password = $("#current_password").val().trim();
		var new_password     = $("#new_password").val().trim();
		var verify_password  = $("#verify_password").val().trim();
		var token            = $('input[name=_token]').val();
		var params           = {"current_password":current_password,"new_password":new_password,"verify_password":verify_password,"_token":token};
		$.ajax({
			type: 'POST',
			url: base_url+'/change_password',
			data: params,
			dataType: 'html',
			success: function (data) {
				if(data == "success"){ 
					$(".success-msg").text("Password updated successfully.");
					$(".msg_sec").show(); 
					
					$("#current_password").val('');
					$("#new_password").val('');
					$("#verify_password").val('');
					setTimeout(function(){
						$(".success-msg").text('');
						$(".msg_sec").hide();
					},3000);
				}else{
					$(".error-msg").text(data);
					$(".msg_sec").show();
				}   
			}
		}); 
	});  
	/* end change password from dashboard page */  
	
	/* update account from dashboard page */ 
	$(".btn_upd_acnt").click(function(){
		$(".msg_sec").hide();
		$(".success-msg").text("");
		$(".error-msg").text(""); 
		var country     = $("#country").val().trim();
		var username    = $("#username").val().trim(); 
		var address     = $("#address").val().trim(); 
		var description = $("#description").val().trim(); 
		var token       = $('input[name=_token]').val();
		var params      = {"country":country,"username":username,"address":address,"description":description,"_token":token};
		
		if(username == ""){
			$(".error-msg").text("Please enter username.");
			$(".msg_sec").show();
		}else if(country == ""){
			$(".error-msg").text("Please select country.");
			$(".msg_sec").show();
		}else{
			$.ajax({
				type: 'POST',
				url: base_url+'/update_account',
				data: params,
				dataType: 'html',
				success: function (data) {
					if(data == "success"){ 
						$(".success-msg").text("Profile updated successfully.");
						$(".msg_sec").show();  
						setTimeout(function(){
							$(".success-msg").text('');
							$(".msg_sec").hide();
						},3000);
					}else{
						$(".error-msg").text(data);
						$(".msg_sec").show();
					}   
				}
			});
		}		
	}); 
	
	/* end update account from dashboard page */ 
		
	$("#updateAccount #username").focusout(function(){ 
		var username = $("#username").val().trim();
		var token    = $('input[name=_token]').val();
		if(current_password != ""){
			$.ajax({
				type: 'POST',
				url: base_url+'/check_user_name',
				data: {'username' : username, '_token' : token},
				dataType: 'html',
				success: function (data) {
					if(data == "exists"){ 
						$(".error-msg").text("This username already exists, please try different one.");
						$(".msg_sec").show(); 
						setTimeout(function(){
							$(".success-msg").text('');
							$(".msg_sec").hide();
						},3000);
					}  
				}
			});
		} 
	}); 
	
	
	/* add credits from dashboard page */ 
	$(".btn_credits").click(function(){
		$(".msg_sec").hide();
		$(".success-msg").text("");
		$(".error-msg").text(""); 
		var credit_amount = $("#credit_amount").val().trim(); 
		var token    = $('input[name=_token]').val();
		var params   = {"credit_amount":credit_amount,"_token":token};
		
		if(credit_amount == ""){
			$(".error-msg").text("Please enter credit amount.");
			$(".msg_sec").show();
		}else{
			var checkNumeric = $.isNumeric(credit_amount);
			if(checkNumeric == false){
				$(".error-msg").text("Please enter valid amount.");
				$(".msg_sec").show();
			}else{
				$.ajax({
					type: 'POST',
					url: base_url+'/add_credits',
					data: params,
					dataType: 'html',
					success: function (data) {
						if(data == "success"){
							var total_credits = $(".total_credits").val();
							var final_credits = parseFloat(credit_amount) + parseFloat(total_credits);
							final_credits = "$"+final_credits.toFixed(2);
							console.log("final_credits "+final_credits+" total_credits "+total_credits+" credit_amount "+credit_amount);
							
							$(".total_credits").val(final_credits); 
							$("#credit_amount").val(''); 
							
							$(".credit_amt").prop("placeholder",final_credits+" Credits");							
							$(".success-msg").text("Credits added successfully.");
							
							$(".msg_sec").show();  
							setTimeout(function(){
								$(".success-msg").text('');
								$(".msg_sec").hide();
							},3000);
						}else{
							$(".error-msg").text(data);
							$(".msg_sec").show();
						}   
					}
				});
			}
		}		
	}); 
	
	/* end add credits from dashboard page */ 
	
	$(".nav-pills a").click(function(){
		$(".msg_sec").hide();
		$(".success-msg").text("");
		$(".error-msg").text("");  
	}); 
	
	$("#verify_profile").submit(function(event){
		event.preventDefault();
		$(".msg_sec").hide();
		$(".success-msg").text("");
		$(".error-msg").text(""); 
		var u_full_name = $("#u_full_name").val().trim(); 
		var token       = $('input[name=_token]').val();
		var params      = {"u_full_name":u_full_name,"_token":token};
		
		if(u_full_name == ""){
			$(".error-msg").text("Please enter name.");
			$(".msg_sec").show();
		}else{ 
			var formData = new FormData($("#verify_profile")[0]);
			$.ajax({
				type: 'POST',
				url: base_url+'/verify_document',
				/* data: params, */
				data: formData,
				dataType: 'html',
				processData: false,
				contentType: false,
				success: function (data) { 
					if(data == "success"){  
						$(".success-msg").text("Your document has been uploaded successfully."); 
						$(".vrfydoc").hide(); 
						$(".msg_sec").show();   
							
						$('html,body').animate({
							scrollTop: $("#profile-verification").offset().top
						}, 'slow');
						setTimeout(function(){
							$(".success-msg").text('');
							$(".msg_sec").hide();
						},3000);
					
					}else{
						$(".vrfydoc").hide();
						$(".error-msg").text(data);
						$(".msg_sec").show();
						
						$('html,body').animate({
							scrollTop: $("#profile-verification").offset().top
						}, 'slow');
						
						setTimeout(function(){
							$(".error-msg").text('');
							$(".msg_sec").hide();
						},3000);
					}   
				}
			}); 
		}		
	}); 
	
	/* Open file uploader on click on camera icon in feed page */
	$("#opn_feed_uploadr").click(function () { 
		$("#feed_image").click();
	});
	/* end Open file uploaders */
	
	/* FOR SAVE FEED */ 
	$("#post_feed").submit(function(event){ 
		event.preventDefault();
			
		var uploaded_file = $("#feed_image").val();
		/* alert($("#feed_image").val()); */
		if(uploaded_file == ""){
			$(".error-msg").text("Please upload image.");
			$(".msg_sec").show();
			
			setTimeout(function(){
				$(".error-msg").text('');
				$(".msg_sec").hide();
			},3000);
		}else{  
			$(".msg_sec").hide();
			$(".success-msg").text("");
			$(".error-msg").text(""); 
			 
			var formData = new FormData($("#post_feed")[0]);
			$('.myprogress').css('width', '0');
			formData.append('feed_image', $('#feed_image')[0].files[0]);
		
			/* $('.msg').text('Uploading in progress...'); */
			$.ajax({
				url: base_url+'/save_feed',
				data: formData,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType: 'json',
				xhr: function () {
					$('.progress').show();
					$(".upd_feed").attr("disabled","disabled"); 
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function (evt) {
						
						if (evt.lengthComputable) { 
							var percentComplete = evt.loaded / evt.total;
							percentComplete = parseInt(percentComplete * 100);
							$('.myprogress').text(percentComplete + '%');
							$('.myprogress').css('width', percentComplete + '%');
						}
					}, false);
					return xhr;
				},
				success: function (data) {
					$('.progress').hide();
					$(".upd_feed").removeAttr("disabled");
					$("#progressbar").hide();  
					if(data.content == "error"){
						$(".error-msg").text("Something went wrong! Please try again.");
						$(".msg_sec").show();
						
						setTimeout(function(){
							$(".error-msg").text('');
							$(".msg_sec").hide();
						},3000);
					}else if(data.content == "upload image"){ 
						$(".error-msg").text("Please upload image.");
						$(".msg_sec").show();
						
						setTimeout(function(){
							$(".error-msg").text('');
							$(".msg_sec").hide();
						},3000);
					}else{
						$(".success-msg").text("Your feed posted successfully."); 
						$(".msg_sec").show();   
						$("#post_feed")[0].reset();	
						$(".feeds_list").html(data.content); 
						
						if(data.total_posts){
							$(".post_count").text("Posts "+data.total_posts); 
						}
						 
						show_read_more(); 
						  
						setTimeout(function(){
							$(".success-msg").text('');
							$(".msg_sec").hide();
						},3000);
						
						//get_feed_count(); /* get feeds count */
					} 
				}
            }); 
		}
	}); 
	/* end FOR SAVE FEED */

	
	$(".unfollow_frnd").click(function(){
		var frnd_id = $(this).data('val');
		if(frnd_id != ""){
			$.ajax({
				type: 'GET',
				url: base_url+'/unfollow_friend/'+frnd_id,
				dataType: 'json',
				processData: false,
				contentType: false,
				success: function (data) { 
					if(data.code == 200){  
						window.location.reload();
					}
				}
			});	
		} 
	}) 
	
		
	/* show image on click on reveal button */
	/* $(".reveal_pic").click(function(){ 
		// alert($(this).data('val')); 
		var reveal_img = $(this).data('val');
		if(reveal_img != ""){
			$(".img-responsive.reveal-img").attr("src",reveal_img);
		} 
	}); */		
	/* end show image on click on reveal button */
	
	
	/* search usernames from search form on header */
	$(".search_user").keyup(function(){ 
		var keyword = $(this).val().trim();
		if(keyword !=""){
			var token =  $('.srch_form input[name=_token]').val(); 
			$.ajax({
				type: 'POST',
				url: base_url+'/getusersname',
				data: {'keyword':keyword,'_token':token}, 
				success: function (data) {
					/* alert(data);					 */
					if(data != "nofound"){
						$(".reslt_sec").html(data);
						$(".srch_form .btn").removeAttr("disabled");
					}else{
						$(".reslt_sec").html("No result found");
						$(".srch_form .btn").attr("disabled","disabled");
					}
				},
				error: function (reject) {  }
			});	
		}else{
			$(".reslt_sec").html("");
		}	
	});
	/* end search usernames from search form on header */
	
	/* search usernames from search form on header */
	$(".srch_form .btn").click(function(){  
		var keyword = $(".srch_form .search_user").val().trim();
		if(keyword !=""){ 
			/* $(".srch_form").submit(); */
			window.location.href=base_url+"/searched_users/"+keyword;
		}
		return false;		 
	});
	/* end search usernames from search form on header */
	
	/* search usernames from search form on header */
	$(".show_notifications").click(function(){  
		/* var token =  $('input[name=_token]').val(); */
		$.ajax({
			type: 'POST',
			url: base_url+'/show_notifications',
			data: {'_token':token}, 
			success: function (data) {
				/* alert(data);					 */
				if(data != "nofound"){
					$(".reslt_sec").html(data);
					$(".srch_form .btn").removeAttr("disabled");
				}else{
					$(".reslt_sec").html("No result found");
					$(".srch_form .btn").attr("disabled","disabled");
				}
			},
			error: function (reject) {  }
		});	 
	});
	/* end search usernames from search form on header */
	
	
	
	
	
	token_1 = $('input[name=_token]').val();
	/* profile pic uploader code */
	var croppicContaineroutputOptions = {
		uploadUrl:base_url+'/payment/uploadFile?_token='+token_1,
		cropUrl:base_url+'/payment/cropFile/?_token='+token_1, 
		outputUrlId:'cropOutput',
		uploadData:{
				"_token":$('input[name=_token]').val(),
			},
		modal:true,
		processInline:true,
		doubleZoomControls:false,
	    rotateControls: false,
		loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
		onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
		onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
		onImgDrag: function(){ console.log('onImgDrag') },
		onImgZoom: function(){ console.log('onImgZoom') },
		onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
		onAfterImgCrop:function(){ 
			
			$('#dbd-user').attr('src',$('#cropOutput').val());
			$('.croppedImg').hide();
		
		},
		
		onAfterRemoveCroppedImg:function(){  
			var explode = function(){
			$('#cropContaineroutput img').css({
					"width":"auto","height":"auto", "left":"0", "top":"0"});	
			};
			setTimeout(explode, 20);	
			console.log('onResetImgCrop');
		}, 
		onReset:function(){console.log('onResetImgCrop') },
		onError:function(errormessage){ $('#error_message').html(errormessage)}, 
	} 
	
	var cropContaineroutput = new Croppic('cropContaineroutput', croppicContaineroutputOptions);
 
	/* end profile pic uploader code */ 
}); 
  
function show_read_more(){ 
	var showChar = 300;
	var ellipsestext = "...";
	var moretext = "Show more";
	var lesstext = "Show less";
	
	var cnt = 1;
	$('.post_body').each(function() {
		var content = $(this).html(); 
		if(content.length > showChar) { 
			var c = content.substr(0, showChar);
			var h = content.substr(showChar, content.length - showChar);
			/* var random_value = Math.random(); */
			var random_value = new Date().getTime();

			random_value = random_value + cnt; 
			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="javascript:void(0)" class="morelink morelnk_'+random_value+'" onclick="morelink('+random_value+')">' + moretext + '</a></span>';

			$(this).html(html);
		} 
		
		cnt++;
	}); 
}

function morelink(random_value){ 
	var moretext = "Show more";
	var lesstext = "Show less";   
	if($(".morelnk_"+random_value).hasClass("less")) {
		$(".morelnk_"+random_value).removeClass("less");
		$(".morelnk_"+random_value).html(moretext);
	} else {
		$(".morelnk_"+random_value).addClass("less");
		$(".morelnk_"+random_value).html(lesstext);
	}
	
	$(".morelnk_"+random_value).parent().prev().toggle();
	$(".morelnk_"+random_value).prev().toggle(); 
	$(".morelnk_"+random_value).prev().css("display","block !important"); 
	
	return false;
}


function follow_user(follow_user_id,action='',follower_id='',action2=''){ 
	if(follow_user_id != ""){
		$.ajax({
			type: 'GET',
			url: base_url+'/follow_friend/'+follow_user_id+'/'+follower_id,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data) {   
				if(data.code == 200){   
					/* window.location.reload(); */
					 /* alert(action2); */
					if(action2 == "yes"){ 
						$(".following_count").html('Followings '+data.total_followings);
					}
					
					
					$(".add_user_"+follow_user_id).attr("disabled","disabled");
					$(".add_user_"+follow_user_id).addClass("flw-action");
					/* $(".add_user_"+follow_user_id).html("Following"); */
					
					//following_count(''); /* get following user's count */
					
					if(action == "refresh"){ 
						window.location.reload();
					}else{  
						$(".add_user_"+follow_user_id).hide();
						$(".add_user_"+follow_user_id).parent(".user-area").hide(); 
					}
				
					
					/* setTimeout(function(){
						if(action == "refresh"){ 
							window.location.reload();
						}else{ 
							$(".add_user_"+follow_user_id).hide();
							$(".add_user_"+follow_user_id).parent(".user-area").hide();
						}
					},3000); */
				}
			}
		});
	} 
}

function focus_comment_box(post_id){ 
	if(post_id != ""){ 
		$('html,body').animate({
			scrollTop: $(".comment_sec_"+post_id).last().offset().top
		}, 'slow');
		$(".comment_area_"+post_id).focus(); 
	} 
}

function loadMoreComments(post_id,comment_id){
	console.log('loadMoreComments');
	$(".comment_sec_"+post_id+" .vw_less_cmt").hide(); 
	if(post_id != ""){ 
		/* var page_num = parseInt($(".comment_pg_num_"+post_id).val()) + 1;  */
		var page_num = $(".comment_pg_num_"+post_id).val(); 
		/* alert(page_num); */
		if(page_num != ""){
			console.log('post_id '+post_id+' comment_id '+comment_id);
			$(".vw_all_"+comment_id).attr("disabled","disabled");
			$(".vw_all_"+comment_id).hide();
			$.ajax({
				type: 'GET',
				url: base_url+'/load_more_comments/'+post_id+'/'+page_num+'/more',
				dataType: 'html', 
				success: function (data) {  
					if(data != "error" && data != ""){  
						/* $(".comment_sec_"+post_id+" .vw_less").remove(); */
						if(page_num == 0){
							$(".comment_sec_"+post_id).html(data); 
						}else{
							$(".comment_sec_"+post_id).append(data); 
						}
						page_num = parseInt($(".comment_pg_num_"+post_id).val()) + 1
						$(".comment_pg_num_"+post_id).val(page_num); 
						$(".vw_all_"+comment_id).hide();
						$(".line_"+comment_id).hide(); 
						$(".comment_sec_"+post_id+" .less_comments").hide(); 
						
						show_read_more();
					}else{
						if(data == ""){
							$(".comment_sec_"+post_id+" .vw_more_cmt").hide(); 
						}
					}
					
					$(".vw_all_"+comment_id).removeAttr("disabled");
				}
			});
		} 
	} 
}

function lessComments(post_id,comment_id){  
	if(post_id != ""){ 
		var page_num = 0;  
		$.ajax({
			type: 'GET',
			url: base_url+'/load_more_comments/'+post_id+'/'+page_num+'/less',
			dataType: 'html', 
			success: function (data) {   
				if(data != "error" && data != ""){  
					$(".comment_sec_"+post_id).html(data);  
					/* $(".comment_sec_"+post_id).append(data);   */
					$(".comment_pg_num_"+post_id).val(page_num);  
					show_read_more();
					
					$(".comment_sec_"+post_id+" .vw_less_cmt").hide(); 
					$(".comment_sec_"+post_id+" .vw_less_cmt").last().show();

					$('html,body').stop().animate({
						scrollTop: $(".comment_sec_"+post_id).offset().top - 80
					}, 1000);
					$(".comment_area_"+post_id).focus();
				} 
			}
		}); 
	} 
}

function postComment(){ 
	if (e.which == 13) { 
		var comment = $(this).val().trim();
		var post_id = $(this).data('val');
		var _token = $("input[name=_token]").val(); 
		
		$(".comment_sec_"+post_id+ ' .line').hide(); 
		$(".comment_sec_"+post_id+ ' .vw_more_cmt').hide();  
					
		$.ajax({
			type: 'POST',
			url: base_url+'/save_comment',
			dataType: 'html', 
			data:{'comment':comment,'post_id':post_id,'_token':_token},
			success: function (data) {  
				if(data != "error" && data != ""){ 
					$(".comment_sec_"+post_id).append(data); 
					$(".comment_area").val(''); 
					getTotalCommentCounts(post_id);
				}else{
					if(data == ""){
						$(".comment_sec_"+post_id+" .vw_more_cmt").hide(); 
					}
				}
			}
		}); 
	}
}


function following_count(id=''){ 
	$.ajax({
		type: 'GET',
		url: base_url+'/get_following_count/'+id,
		dataType: 'html',
		processData: false,
		contentType: false,
		success: function (data) {  
			if(data != "error"){  
				$(".following_count").text("Followings "+data); 
			}
		}
	});	
}

function followers_count(id=''){ 
	$.ajax({
		type: 'GET',
		url: base_url+'/get_followers_count/'+id,
		dataType: 'html',
		processData: false,
		contentType: false,
		success: function (data) { 
			if(data != "error"){  
				$(".followrs_count").text("Followers "+data); 
			}
		}
	});	
}

/* get feeds count */
function get_feed_count(id=''){ 
	$.ajax({
		type: 'GET',
		url: base_url+'/get_feed_count/'+id,
		dataType: 'html',
		processData: false,
		contentType: false,
		success: function (data) {  
			if(data != "error"){  
				$(".post_count").text("Posts "+data); 
			}
		}
	});
}
/* end get feeds count */

/* get feeds count */
function get_liked_feeds_count(id=''){ 
	$.ajax({
		type: 'GET',
		url: base_url+'/total_liked_feeds/'+id,
		dataType: 'html',
		processData: false,
		contentType: false,
		success: function (data) {  
			if(data != "error"){  
				$(".likes_count").text("Likes "+data); 
			}
		}
	});
}
/* end get feeds count */

/* get user profile pic*/
function get_user_profile_pic(id=''){ 
	$.ajax({
		type: 'GET',
		url: base_url+'/get_user_profile_pic/'+id,
		dataType: 'html',
		processData: false,
		contentType: false,
		success: function (data) {  
			if(data != "error"){  
				$(".likes_count").text("Likes "+data); 
			}
		}
	});
}
/* end get user profile pic */

/* check for new notifications after every 3 seconds */
function check_notification(){ 
	$.ajax({
		type: 'GET',
		url: base_url+'/check_notification',
		dataType: 'html',
		processData: false,
		contentType: false,
		success: function (data) { 
			console.log(data);
			if(data != 0){ 
				/* $(".notify").prepend('<span class="note-no show_notifications">'+data+'</span> <span class="visible-sm visible-xs">Notification </span></a>') */
				$(".note-no").text(data);
			}
		}
	});
}
/* end check for new notifications after every 3 seconds */