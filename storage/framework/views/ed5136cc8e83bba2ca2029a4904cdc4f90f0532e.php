
<?php $__env->startSection('pageTitle', 'Subscriptions'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  
        <!-- Main content -->
        <section class="content">
        	<div class ="table-title">
                
              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body"> 
							<?php if(Session::has('success')): ?>
							 <div class="success-msg">
								<?php echo e(Session::get('success')); ?>

							 </div>
							 <?php endif; ?> 
							  <?php if(Session::has('error')): ?>
								   <div class="success-msg">
									  <?php echo e(Session::get('error')); ?>

							    	</div> 
							 <?php endif; ?>
							 
							 <div class="msg_sec"></div>
						
    							<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                     
               
                      
                    </tr>
                    </tbody>
                  </table>
                </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
                  <th>Username</th>
                  <th>Service</th>
                  <th>Amount</th>
									<th>Date</th>
                  <th>Next Billing Date</th>
                  <th>Action</th>
								  </tr>
								  </thead>

								  <?php $__currentLoopData = $subscription; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								  <tr>
                    <td>
                       <?php echo e(get_user_name($sub->user_id)); ?>

                    </td>
                    <td>
                       <?php echo e(get_service_name($sub->service_id)); ?>

                    </td>
									<td> €<?php echo e(get_service_amount($sub->service_id)); ?></td>									
								 <td> <?php echo e(date('d F, Y h:i:s',strtotime($sub->created_on))); ?></td> 
                  <td><?php
              
              $arr=explode('/',$sub->next_billing_date);
 $s = $arr[1].'/'.$arr[0].'/'.$arr[2];

$date = strtotime($s);
echo date('d F, Y ', $date);
                  ?></td>                 
                 <td class="action_links"> 
                 
                    <a href="<?php echo e(url('admin/user/payment-details/'.$sub->payment_id)); ?>" data-toggle="tooltip" data-original-title="Payment Details"><i class="fa fa-money"></i></a> </td>

								  </tr>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>
	  
		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    </div>
	
<style>
#myModal {
    padding-right: 45%;
}
</style>
    <?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>