<?php
 $currentAction = \Route::currentRouteAction();
    list($controller, $method) = explode('@', $currentAction);
    $controller = preg_replace('/.*\\\/', '', $controller);
    //echo $method;
    ?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>

    <!-- Sidebar Menu -->

    <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
      <!-- <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li> -->
      <li class="">
          <a href="#">
            <i class="fa fa-users text-aqua"></i>
            <span>Gérer adhérents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" <?php if($method=='users_details'): ?><?php echo e('menu-open'); ?><?php endif; ?>" <?php if($method=='users_details'): ?><?php echo e('style=display:block;'); ?>  <?php endif; ?>">
            <li class="<?php if($method=='users_details'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/users')); ?>">
                <i class="fa fa-user"></i>
                <span>Adhérents</span>
              </a>
            </li>

          </ul>
        </li>
        <li class="">
          <a href="#">
            <i class="fa fa-ticket text-aqua"></i>
            <span>Services</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php if($method=='plans' || $method=='add_plan'): ?><?php echo e('menu-open'); ?><?php endif; ?>" <?php if($method=='plans' || $method=='add_plan'): ?><?php echo e('style=display:block;'); ?>  <?php endif; ?>">
            <li class="<?php if($method=='plans'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/plans')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Services</span>
              </a>
            </li>
            <li class="<?php if($method=='add_plan'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/plan/add')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Ajouter nouveau</span>
              </a>
            </li>

          </ul>
        </li>


         <li class="<?php if($method=='all_subscriptions'): ?><?php echo e('active'); ?><?php endif; ?>"><a href="<?php echo e(url('admin/all-subscriptions')); ?>"><i class="fa fa-credit-card text-aqua"></i> <span>Abonnements</span></a></li>

		 <li class="">
          <a href="#">
            <i class="fa fa-credit-card text-aqua"></i>
            <span>Magazine</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php if($method=='all-images' || $method=='images/addImage'): ?><?php echo e('menu-open'); ?><?php endif; ?>" <?php if($method=='all_images' || $method=='addImage'): ?><?php echo e('style=display:block;'); ?>  <?php endif; ?>">
            <li class="<?php if($method=='all_images'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/all-images')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Magazine</span>
              </a>
            </li>
            <li class="<?php if($method=='addImage'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/images/add_image')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Ajouter nouveau</span>
              </a>
            </li>

          </ul>
        </li>
		<li class="">
          <a href="#">
            <i class="fa fa-gift text-aqua"></i>
            <span>Promotions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php if($method=='promotion' || $method=='addpromotion'): ?><?php echo e('menu-open'); ?><?php endif; ?>" <?php if($method=='promotion' || $method=='addpromotion'): ?><?php echo e('style=display:block;'); ?>  <?php endif; ?>">
            <li class="<?php if($method=='promotion'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/promotions')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Promotions</span>
              </a>
            </li>
            <li class="<?php if($method=='addpromotion'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/add_promotion')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Ajouter nouveau</span>
              </a>
            </li>

          </ul>
        </li>
        <li class="">
              <a href="#">
                <i class="fa fa-medkit text-aqua"></i>
                <span>Pharmacies</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu <?php if($method=='pharmacy' || $method=='add_pharmacy'): ?><?php echo e('menu-open'); ?><?php endif; ?>" <?php if($method=='pharmacy' || $method=='add_pharmacy'): ?><?php echo e('style=display:block;'); ?>  <?php endif; ?>">
                <li class="<?php if($method=='pharmacy'): ?><?php echo e('active'); ?><?php endif; ?>">
                  <a href="<?php echo e(url('admin/pharmacy')); ?>">
                    <i class="fa fa-ticket"></i>
                    <span>Pharmacies</span>
                  </a>
                </li>
                <li class="<?php if($method=='add_pharmacy'): ?><?php echo e('active'); ?><?php endif; ?>">
                  <a href="<?php echo e(url('admin/pharmacy/create')); ?>">
                    <i class="fa fa-ticket"></i>
                    <span>Ajouter pharmacie</span>
                  </a>
                </li>

              </ul>
            </li>





         <li class="<?php if($method=='transactions'): ?><?php echo e('active'); ?><?php endif; ?>"><a href="<?php echo e(url('admin/transactions')); ?>"><i class="fa fa-money text-aqua"></i> <span>Transactions</span></a></li>
        <li>
          <a href="#">
            <i class="fa fa-envelope text-aqua"></i>
            <span>Email</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" <?php if($method=='email_template' || $method=='new_email_template'): ?><?php echo e('menu-open'); ?><?php endif; ?>" <?php if($method=='email_template' || $method=='new_email_template'): ?><?php echo e('style=display:block;'); ?>  <?php endif; ?>">
            <li class="<?php if($method=='email_template'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/email')); ?>">
                <i class="fa fa-envelope"></i>
                <span>Email</span>
              </a>
            </li>
            <li class="<?php if($method=='new_email_template'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/email/new')); ?>">
                <i class="fa fa-envelope"></i>
                <span>Ajouter nouveau</span>
              </a>
            </li>

          </ul>
        </li>

      <li class="<?php if($method=='payline'): ?><?php echo e('active'); ?><?php endif; ?>"><a href="<?php echo e(url('admin/payline')); ?>"><i class="fa fa-credit-card text-aqua"></i> <span>Payline Configuration</span></a></li>


		<li class="">
          <a href="#">
            <i class="fa fa-ticket text-aqua"></i>
            <span>Google Drive</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php if($method=='shared_google_drive' || $method=='add_category' || $method=='add_sub_category' || $method=='google_drive_by_folderID'): ?><?php echo e('menu-open'); ?><?php endif; ?>" <?php if($method=='shared_google_drive' || $method=='add_category' || $method=='add_sub_category' || $method=='google_drive_by_folderID'): ?><?php echo e('style=display:block;'); ?>  <?php endif; ?>">
            <li class="<?php if($method=='shared_google_drive' || $method=='google_drive_by_folderID'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/shareddrive')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Current Files</span>
              </a>
            </li>

			<li class="<?php if($method=='add_category'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/category/add')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Add Category</span>
              </a>
            </li>

             <li class="<?php if($method=='add_sub_category'): ?><?php echo e('active'); ?><?php endif; ?>">
              <a href="<?php echo e(url('admin/category/add-sub')); ?>">
                <i class="fa fa-ticket"></i>
                <span>Add sub Category</span>
              </a>
            </li>
          </ul>
        </li>




    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
