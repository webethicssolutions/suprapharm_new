<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<?php if(Session::has('error')): ?>
								<div class="error-msg">
								   <?php echo e(Session::get('error')); ?>

								</div>
					        <?php endif; ?>
							<div class="title" style="margin-bottom:30px">
								<h2 >Add Promo Code</h2>
							</div>

							<?php echo e(Form::open(array('url' => 'admin/promo/save', 'method' => 'post','class'=>'promo_form form-horizontal'))); ?>

							<div class="form-group col-md-12">
								<div class="row">
									<div class="col-md-8 row col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Unique Coupon Name ')); ?>

											<?php echo e(Form::text('couponName', '' ,array('id'=>'couponName','class'=>'form-control'))); ?>

											<span class="error"> <?php echo e($errors->first('couponName')); ?> </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Discount Type')); ?>

											<select name="discount" id="discount" class="form-control">
												<option value="">Select</option>
												<option value="percent">Percent Off</option>
												<option value="amount">Amount off</option>
											</select>
										</div>
										<div class="col-md-12 col-xs-12 hide prcnt_sec">
											<?php echo e(Form::label('Percent Off')); ?>

											<?php echo e(Form::number('percentOff', '' ,array('id'=>'percentOff','class'=>'form-control','min'=>'0.1','max'=>'100','step'=>'0.01'))); ?> 
										</div> 
										<div class="col-md-12 col-xs-12 hide amount_sec">
											<?php echo e(Form::label('Amount Off')); ?>

											<?php echo e(Form::number('amountOff', '' ,array('id'=>'amountOff','class'=>'form-control','min'=>'0'))); ?>

										</div> 
										<div class="clearfix"></div>
										<div class="col-md-4 col-xs-12 field">
											<?php echo e(Form::label('Max Redemptions')); ?>

											<?php echo e(Form::number('max_redemptions', '' ,array('id'=>'max_redemptions','class'=>'form-control'))); ?> 
										</div>  
										<div class="col-md-4 col-xs-12 expire">
											<?php echo e(Form::label('Redeem by')); ?>

											<?php echo e(Form::date('redeem_by', '' ,array('id'=>'redeem_by','class'=>'form-control exp_on','min'=>$current_date))); ?>

										</div>
										<div class="col-md-4 col-xs-12 field">
											<?php echo e(Form::label('Duration')); ?>

											<select name="duration" class="form-control duration" id="duration">
												<option value="">Select</option>
												<option value="once">Once</option>
												<option value="repeating">Multi Months</option>
												<option value="forever">Forever</option>
											</select> 
											<span class="error"> <?php echo e($errors->first('duration')); ?> </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Duration in Months')); ?>

											<?php echo e(Form::text('duration_in_months', '' ,array('id'=>'duration_in_months','class'=>'form-control'))); ?>

										</div> 
										<div class="clearfix"></div>
									</div> 
								</div>
							</div>

							<div class="form-group col-md-12">
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									 <a href="<?php echo e(url('admin/promocodes')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> 
    <script>
        $(document).ready(function(){  
			$("#discount").change(function(){ 
				var selectedOpt1 = $(this).val();  
				if(selectedOpt1 == ""){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else if(selectedOpt1 == "percent"){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else{
					$(".prcnt_sec").addClass('hide');
					$(".amount_sec").removeClass('hide');
				}
			}); 
		});
    </script>

 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>