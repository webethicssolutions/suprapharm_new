<header class="main-header">
  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
<!-- Logo -->
<a href="<?php echo e(url('/admin')); ?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">  <img src="<?php echo e(url('resources/assets/frontend/images/logo.png')); ?>"></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">  <img src="<?php echo e(url('resources/assets/frontend/images/logo.png')); ?>"></span>
  </a>

    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            <img src="<?php echo e(url('assets/admin/dist/img/man.png')); ?>" class="user-image" alt="User Image">
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs"><?php echo e(Session::get('admin_user_name')); ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- The user image in the menu -->
            <li class="user-header">
              <img src="<?php echo e(url('assets/admin/dist/img/man.png')); ?>" class="img-circle" alt="User Image">
              <p>
                <?php echo e(Session::get('admin_user_name')); ?>

              </p>
            </li>
            <!-- Menu Body -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                  <a href="<?php echo e(url('admin/change-password-admin')); ?>" class="btn btn-default btn-flat">Change Password</a>

              </div>
              <!--div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div-->
              <div class="pull-right">
                <a href="<?php echo e(url('admin/logout')); ?>" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
