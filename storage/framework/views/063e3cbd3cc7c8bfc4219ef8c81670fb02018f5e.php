
<?php $__env->startSection('pageTitle', 'Subscriptions'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.user_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.member_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  
        <!-- Main content -->
        <section class="content">
        	<div class ="table-title">
                
              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body"> 
							<?php if(Session::has('success')): ?>
							 <div class="success-msg">
								<?php echo e(Session::get('success')); ?>

							 </div>
							 <?php endif; ?> 
							  <?php if(Session::has('error')): ?>
								   <div class="success-msg">
									  <?php echo e(Session::get('error')); ?>

							    	</div> 
							 <?php endif; ?>
							 
							 <div class="msg_sec"></div>
						
    							<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                     
               
                      
                    </tr>
                    </tbody>
                  </table>
                </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
                  <th>Sr No.</th>
                  <th>Name</th>
									<th>Action</th>
                  
								  </tr>
								  </thead>
                  <?php $i=1;?>
								  <?php $__currentLoopData = $files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								  <tr>
                    <td><?php echo e($i); ?> </td>
									<td><?php $arr=explode('_',$file->image_id);
                  echo $arr[count($arr)-1];?></td>									
							
                        
                 <td class="action_links"> 
                 
                    <a href="/public/uploads/Google-Drive/<?php echo e($file->image_id); ?>" data-toggle="tooltip" data-original-title="View" target="_blank"><i class="fa fa-tasks"></i></a>
                         <a href="/public/uploads/Google-Drive/<?php echo e($file->image_id); ?>" data-toggle="tooltip" data-original-title="Download" class="dwnld-file" download><i class="fa fa-download" ></i></a> 
                       </td>

								  </tr>
                  <?php $i++;?>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->

<?php if(count($sub_category)>0): ?>
        <section class="content usr-contnt"> 
      <div class="row"> 
       <div class="title col-sm-12" style="margin-bottom:30px">
                <h2>Sub Categories</h2>
              </div>
       
        <div class="col-lg-12"> 
          <div class="box box-primary folder-box">
            <?php $__currentLoopData = $sub_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="service_blk text-center">
    
    
   
  <a href="<?php echo e(url('sub-category-files/'.$cat->id)); ?>">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4><?php echo e($cat->sub_category_name); ?></h4></span>
        </a>
    


        </div>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>
	  
		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    </div>
	
<style>
#myModal {
    padding-right: 45%;
}
</style>
    <?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>