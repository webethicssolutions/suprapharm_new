<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/bootstrap.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/fontawesome.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/style.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/slick.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/slick-theme.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/fakeLoader.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/jquery-ui.css')); ?>">

<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(url('resources/assets/frontend/images/favicon.png')); ?>">

    <title>Autolove - <?php echo $__env->yieldContent('pageTitle'); ?></title>

   
   <meta name="description" content="<?php echo $__env->yieldContent('pageDescription'); ?>" />

  </head>
  <?php
  $email='';
    $currentAction = \Route::currentRouteAction();
    list($controller, $method) = explode('@', $currentAction); 
    $controller = preg_replace('/.*\\\/', '', $controller);  
   if($method=='index'){
    echo '<body id="home"> <div class="fakeLoader"></div>';
   }
   else{
    echo '<body>';
   }
    ?> 
 

  <?php echo $__env->yieldContent('content'); ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo e(url('resources/assets/frontend/js/jquery-3.2.1.min.js')); ?>"></script>
    <script src="<?php echo e(url('resources/assets/frontend/js/bootstrap.bundle.min.js')); ?>"></script> 
    <script src="<?php echo e(url('resources/assets/frontend/js/srch.js')); ?>"></script>
	<script src="<?php echo e(url('resources/assets/frontend/js/typed.js')); ?>"></script>
	<script src="<?php echo e(url('resources/assets/frontend/js/jquery.sticky-nav.js')); ?>"></script>
  <script src="<?php echo e(url('resources/assets/frontend/js/help.js')); ?>"></script>
    <script src="<?php echo e(url('resources/assets/frontend/js/slick.min.js')); ?>"></script>
     <script src="<?php echo e(url('resources/assets/frontend/js/fakeLoader.min.js')); ?>"></script>
    <script src="<?php echo e(url('resources/assets/frontend/js/jquery-ui.js')); ?>"></script>
	<script src="<?php echo e(url('resources/assets/frontend/js/floatong-emo.js')); ?>"></script>
	<script src="<?php echo e(url('resources/assets/frontend/js/custom.js')); ?>"></script>
  <script src="<?php echo e(url('resources/assets/frontend/js/match_height.js')); ?>"></script>
     

  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->



  

  <script>
//  const video = document.querySelector('video');
// video.addEventListener("progress", function() {
//   // When buffer is 1 whole video is buffered
//   if (Math.round(video.buffered.end(0)) / Math.round(video.seekable.end(0)) === 1) {
//     // Entire video is downloaded
//     alert('yes');
//  }
// }, false);

     $.fakeLoader({
                    timeToHide:1000,
                    bgColor:"#00b4d6",
                    spinner:"spinner2"
                });
     
jQuery(document).ready(function() {
  jQuery('.navbar-nav.ml-auto').stickynav();
});


 	jQuery(document).ready(function() {
	if( typeof Typed !== 'undefined' ) {
		var typed = new Typed("#heading-animated-igwqI4rhLS .sh-heading-animated-typed", {
			strings: ["Likes","Views","Followers",],
			contentType: 'html',
			typeSpeed: 0,
			loop: true,
			startDelay: 300,
			typeSpeed: 80,
			backSpeed: 20,
			backDelay: 700,
		});
	}
});


  
  jQuery(document).ready(function(){
       
        var    dheight = jQuery('html').height(),
            cbody = jQuery('.full-height').height(),
            wheight = jQuery(window).height(),
            cheight = wheight - dheight + cbody;
           
        if (wheight > dheight){
            jQuery('.full-height').height(cheight);
        }
       
        jQuery(window).resize(function(){
            wheight = jQuery(window).height();
            noscroll();
            changepush();
        });

        function noscroll(){
           if (wheight > dheight) {
                jQuery('html').addClass('noscroll');
           }

            else if (wheight <= dheight) {
                jQuery('html').removeClass('noscroll');
            }
           
            else {}

        }

        function changepush(){
           if (wheight > dheight) {
                   jQuery('.full-height').height(wheight-dheight+cbody);
           }
           
        }

  $("#update_email").keyup(function(){
  var type_email=$('#update_email').val();
  var email="<?php echo $email;?>";
  // console.log(type_email);
  // console.log(email);
if(type_email==email){
 $("#save_email").prop('disabled', true);
}
else{
 $("#save_email").prop('disabled', false);
}
});

$('.plan-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
  ]
});
    
    $('.testi').slick({
  infinite: true,
  autoplay:true,
  slidesToShow: 1,
  slidesToScroll: 1,

});
}); 

  $(function() {
  $('.card-header').matchHeight();
});
</script>
<script>
 window.intercomSettings = {
   app_id: "z5u9k013"
 };
</script>
<!-- <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/z5u9k013';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script> -->


  <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-c4bc573d-458f-4876-bb0b-113955922543"></div>


<!-- <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-b6ca5c23-11bd-4f08-b451-dab7a6a3fef7"></div> -->
  </body>
</html>