<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="main-banner">
		<video width="100%" height="684" id="home-video" autoplay muted>
			<source src="<?php echo e(url('resources/assets/frontend/videos/home.webm')); ?>" type="video/webm">
			<source src="<?php echo e(url('resources/assets/frontend/videos/home.mp4')); ?>" type="video/mp4"> 
			<source src="<?php echo e(url('resources/assets/frontend/videos/home.ogv')); ?>" type="video/ogv"> 
		</video>
		
		<div class="container"> 
			<div class="row align-items-center">
				<div class="col-12">
				<h1 class="display-5">Experience<br>R E A L</h1>
				<div class="sh-heading" id="heading-animated-igwqI4rhLS">
					<h1 class="display-5 sh-heading-content size-xxxl sh-heading-animated-content">
						
						<span class="sh-heading-animated-typed"></span>
					</h1>
				</div>				
				<p class="lead mb-5 mt-2 mt-sm-4">Subscribe, post and experience real likes from real users. Really! 😍<br>
Real followers available based on subscription option chosen.</p>	
                <div class="col-12 bnr_btn">
				<div class="button">
				<a href="#pricing" class="price-sec">Get Real Likes</a>
				</div>
				</div>				
				</div>
			</div>
		</div>	
		
	
		
	</div>
	
<div id="about">
<div class="special-bg">
<div class="graphic-sec">
	<div class="container">
		<div class="row align-items-center">
		
			<div class="col-lg-6">
			<img src="<?php echo e(url('resources/assets/frontend/images/iphone.png')); ?>" class="img-fluid">
			</div>		
			<div class="col-lg-6">
				<h2 class="title">How does AutoLove.co work?</h2>
				<p>Our servers monitor your subscribed accounts in real time. Once a new upload is
detected, the chosen engagement will be delivered to your latest posts automatically.<br>
</p>
				<p>NO bots, just real user likes. Really! 💯</p>
				<p class="notice">(all likes/views are delivered within 2-4hours after each new upload)</p>
				<div class="col-12 bnr_btn">
				<div class="button">
			
				<a href="javascript:void(0)" class="price-sec">Get Real Likes</a>
				</div>
				<div class="left_text">
				<p><span class="icon_trl"><img src="<?php echo e(url('resources/assets/frontend/images/clock.png')); ?>"></span>3 days free trial</p>
				</div>
				</div>
			</div>
			
		</div>	
	</div>
	
</div>
<div id="features" class="container icon-info-wrapper services">
        <div class="row">
		
          <div class="col-md-4 text-center mb-5 mb-md-0">
          <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/aeroplane-.png')); ?>">

            <h3 class="mt-4 mb-2">Sign-up</h3>
			<p>Subscribe to your current social media engagement needs.</p>
          </div>
          <div class="col-md-4 text-center mb-5 mb-md-0">
		 <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/idea.png')); ?>">

            <h3 class="mt-4 mb-2">Post</h3>
			<p>Upload your new photo or video to Instagram.</p>
          </div>
          <div class="col-md-4 text-center">
		<img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/power.png')); ?>">

            <h3 class="mt-4 mb-2">Enjoy</h3>
			<p>Sit back and enjoy real user likes/views automatically at organic speeds.</p>
          </div>
		  <div class="col-md-12 bdr"></div>
            <div class="col-md-4 text-center mb-5 mb-md-0">
          <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/smartphone.png')); ?>">

            <h3 class="mt-4 mb-2">Explore</h3>
			<p>Experience real user likes/views, followers based on subscription option chosen.</p>
          </div>
          <div class="col-md-4 text-center mb-5 mb-md-0">
		 <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/innovation.png')); ?>">

            <h3 class="mt-4 mb-2">Grow</h3>
			<p>Unlike illegitimate bot likes. REAL user likes/views will rank your posts and #hashtags.</p>
          </div>
          <div class="col-md-4 text-center">
		<img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/data-transfer.png')); ?>">

            <h3 class="mt-4 mb-2">Discover</h3>
			<p>Endless ways to kickstart your account. Just be yourself. Post Explore and Discover.</p>
          </div>		  
        </div>
</div>

<div id="floating-emoji">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">  
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">  
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-dark.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">  
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
  <img class="emoji" src="<?php echo e(url('resources/assets/frontend/images/heart-light.png')); ?>">
</div>	

</div>
</div>
<div>
<div class="client-reviews">
<div class="container">
<div class="row align-items-center">
	<div class="col-md-12">
<div class="review-slider">
  <div>
 		<div class="heading-bar">
			<div class="title">WHY OUR CLIENTS LOVE US?</div>
		</div> 
		
		<div class="testi">
			<div>Post and know your hashtags will be ranked! Love the service! #top9gang 💜</div>
			<div>Our followers engagement probably doubled since using this service. And we are only
			using a starter package! T$ H$ X💋</div> 
			<div>AutoLove.co backdrop video is as dope as the service. WOW! 😍</div> 
			<div>Thank you Christian so much for encouraging me a year ago to sign-up. I did not post
			daily as you recommended but still doubled my followers count in 1 year time. 20K this
			year is the next step! (250 package)</div>

			<div>Cost of a cup of coffee per day. ☕ sold! Ksenia Sukhinova</div> 
			<div>I always thought this kind of service was fake until I tried autolove.co</div> 
			<div>These guys truly rock! I have tried many other services, nothing compares to this one.
			Account has been growing just from likes packages only but I did receive the email of
			the new followers feature option that’s being released. Will definitely give it a try once
			available! Cheers automatic love team, Spencer from Florida.</div> 
			<div>Day and night difference in your followers engagement.</div> 
			<div>A 5k package 2 years and 3 sponsorships later... Totally worth it! 😎</div> 
			<div>Subscribed once and never looked back. Ron</div> 
			<div>Dude this is so 🔥 🔥 🔥 , I can’t believe how I grew to 50k without this tool. Doubled
			down in 8 month time!!! 🃏 3k package</div> 
			<div>I had an issue which has been resolved same day. Darn typo. Make sure you enter the
			correct username lol. Otherwise you end up sending someone else free likes on your
			expense like me haha😅 support was responsive and helpful. 750 package, Dave</div> 
			<div>Top service guys. I only wish I would have found you sooner. Regards from Australia 1k
			CJ</div> 
			<div>Don’t bet on anyone else, ACE of Spades for the win! ♠ ❤ Sophie</div> 
			<div>Thank you for your time and assistance with the setup for our group. Also appreciate
			your personal input and recommendation. Salute from the team. Jason</div> 
			<div>Good vibes only on this site. Glad I tried it, love the service.</div> 
			<div>AutoLove.co backdrop video is as dope as the service. WOW! 😍</div> 
			<div>VIP pricing but nothing comes close as far as quality goes. 1k likes + 3k followers per
			month. Jessica Fernandez 😘 T.A.L.T.</div> 
			<div>If you are reading this, it’s not to late to sign up 😁 but seriously. Awesome how the
			followers are spread throughout the month. As natural as possible. Dave U.K.</div> 

			<div>Perfect service for ecomm product profiles. It’s like almost guaranteed your videos go
			viral! 📦 💰 Matt K.</div> 
			<div>Automatic love is for real. Really 1 💭 💯 ❤</div> 
			<div>So thanks much. I try and service was very good. Lukasz from fiverr</div> 
	  </div>
		<div class="col-12 bnr_btn">
				<div class="button">
				<a href="#pricing" class="price-sec">Get Real Likes</a>
				</div>
				</div>
  </div>
  <div>
</div>	
</div>
</div>
</div>	
</div>
</div>
</div>
<div class="pb-5 pricing-wrap">
<div class="form-wrapper pricing-table-sec">
<div class="container">
<div class="row">
	<div class="col-12">
 <h1  id="pricing" class="text-center">Love Plan</h1>
</div>	 
<div class="col-12 mt-5">
	<div class="card-deck mb-3 text-center"> 
		<div class="plan-slider"> 
			<?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($plan->type=='L' && ($plan->post_like !=1500 && $plan->post_like !=2500 && $plan->post_like !=5000 && $plan->post_like !=10000)): ?>
				<div>
					<div class="card mb-4">
					<span></span>
					<span></span>
					<span></span>
					<span></span>			
					<div class="card-header">
						<!--<h3 class="mt-0 mb-3">Most Popular</h3> -->
						<div>
						<?php if($plan->type=='L'): ?>
							<?php echo e("Likes Per Post"); ?>

						<?php else: ?>
							<?php echo e('Likes+Followers'); ?>

						<?php endif; ?>
						</div>	
					  <h4 class="my-0"><?php echo e($plan->post_like); ?></h4>				
					</div>
					  <div class="card-body">
						<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1> 
						<ul class="list-unstyled mt-3 mb-4 planlist">
							<!---li><b>Unlimited posts</b></li---->
							<li><b>4 posts/day</b></li>
							<li>Real likes from real users </li>
							<li><b>Free views</b> on all videos </li>
							<li>Safe & secure </li>
							<li>24/7 customer support </li>		
						</ul>
						<h4 class="my-0">$<?php echo e($plan->amount); ?> <sub>/ month</sub></h4>
						<div class="prc_button"><a class="pricing_button" href="<?php echo e(url('register')); ?>/<?php echo e($plan->id); ?>">Make a payment</a></div>
					  </div>
					</div>
				</div>
				<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<div>
				<div class="card mb-4">
					<span></span>
					<span></span>
					<span></span>
					<span></span>			
					<div class="card-header">
						
						<div><?php echo e("Likes Per Post"); ?></div>	 
						<select id="all-plans" class="likes_plans">
							<?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($plan->type=='L'): ?>
									<option value="<?php echo e($plan->id); ?>" data-price="<?php echo e($plan->amount); ?>"><?php echo e($plan->post_like); ?> <?php echo e("Likes"); ?> </option>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select> 	
 
						<div><?php echo e("Drip-feed Followers Per Month"); ?></div>	 
						<select id="followers_plans" class="all-plans followers_plans">
							<option  selected="selected" data-price="0" value="">Please Selecte Followers Plan</option>
							<?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($plan->type=='F'): ?>
									<?php if($plan->post_like > 5000): ?>
										<option value="<?php echo e($plan->id); ?>" data-price="<?php echo e($plan->amount); ?>" disabled><?php echo e($plan->post_like); ?> <?php echo e('Followers'); ?> </option>
									<?php else: ?>
										<option value="<?php echo e($plan->id); ?>" data-price="<?php echo e($plan->amount); ?>"><?php echo e($plan->post_like); ?> <?php echo e('Followers'); ?> </option>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select> 							
				  </div>
				  <div class="card-body">
					<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1>

					<ul class="list-unstyled mt-3 mb-4 planlist">
					<li><b>4 posts/day</b></li>
					<li>Real likes from real users </li>
					<li><b>Free views</b> on all videos </li>
					<li>Safe & secure </li>
					<li>24/7 customer support </li>		
					</ul>
					<h4 class="my-0 dynamic-price" id="dynamic-price">$19 <sub>/ month</sub></h4>
					<div class="prc_button"><a class="pricing_button dynamic-price-link" id="dynamic-price-link" href="<?php echo e(url('register')); ?>/1">Make a payment</a></div>
				  </div>
				</div>
			</div>
  
	</div>

	</div>
		  
	<div class="btn-wrap text-center"><a class="btn" href="<?php echo e(url('help')); ?>">Frequently Asked Questions</a></div>		  


</div>	
	</div>	
 
	  

 </div>
 </div>
 </div>	 </div>	

<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>