
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container full-height">
    <div class="row h-100 align-items-center">
  <div class="col-md-12"> 
<div class="login-form">
        
        <div class="form_container">
          <div class="form-title">Log in to your dashboard</div>

          <form method="POST" id="payment-form"  action="<?php echo e(url('/check_login')); ?>">
               <?php if(Session::has('error')): ?>
          <span class="error">
                    <?php echo e(Session::get('error')); ?>

            </span>
             
                <?php endif; ?>
            <?php if(Session::has('success')): ?>
               <p style="color:green">
                    <?php echo e(Session::get('success')); ?>

               </p>
              <?php else: ?>
             
                <?php endif; ?>
            <?php echo e(csrf_field()); ?>

            <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="text"  name="email" class="form-control input_user" value="" placeholder="Email">
               <span class="error"> <?php echo e($errors->first('email')); ?></span>
            </div>
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" name="password" class="form-control input_pass" value="" placeholder="password">
               <span class="error"> <?php echo e($errors->first('password')); ?></span>
            </div>
            <div class="form-group form-group-bottom">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customControlInline" name="keep">
                <label class="custom-control-label" for="customControlInline">Keep me signed in</label>
              </div>
              <div class="forg-pass">
              <a href="<?php echo e(url('/forget_password')); ?>">Forgot Password?</a>
              </div>  
            </div>
            <input type="Submit" name="button" class="btn login_btn" value="Submit">
          </form>
        </div>
        
        
      </div>
      </div>      
      </div>      
    </div>  
      
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>