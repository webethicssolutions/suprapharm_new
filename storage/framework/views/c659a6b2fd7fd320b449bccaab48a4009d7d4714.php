<nav class="navbar navbar-expand-lg ">
	<div class="container">
      <a  class="navbar-brand" href="<?php echo e(url('/')); ?>">
	  <img src="<?php echo e(url('resources/assets/frontend/images/logo.png')); ?>">
	  </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
       <i class="fas fa-bars"></i></span>
      </button>

		  <div class="collapse navbar-collapse" id="navbar-menu">
			<ul class="navbar-nav ml-auto">
			  <li class="nav-item active">
				<a class="nav-link" href="<?php echo e(url('/#home')); ?>">Home</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/#about')); ?>">About</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/#features')); ?>">Features</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/#pricing')); ?>">Pricing</a>
			  </li> 
			  <?php if(empty(Session::get('user_id'))): ?>
			  <li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/login')); ?>">Login</a>
			  </li> 
			  <?php else: ?>
			  <?php if(Session::get('user_id') &&  Session::get('subscription_id')): ?>
			  <li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/account_settings')); ?>">Dashboard</a>
			  </li> 
			  <?php else: ?>
			  <li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/logout')); ?>">Logout</a>
			  </li> 
			  <?php endif; ?>
			  <?php endif; ?>
			</ul>			
		  </div>
      </div>
    </nav>