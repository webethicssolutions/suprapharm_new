<?php $__env->startSection('pageTitle', 'Privacy'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<div class="page container">

    
    <div id="content-transactions" class="content content-settings">

        <div class="panel panel-pad">
  <h1>Please Read the Following Terms Carefully Before Using this Service</h1>
<br><br>
              

    <p id="introduction">
       The materials on this website www.autolove.co (the "Site" “AutoLove”) are provided by AutoLove.co (“we”) as a service to its customers (“users”). This document explains the terms by which you may use our website in connection with the service (collectively the "Service"). By using this site, you agree to be legally bound by these terms. If you do not agree to them, do not use the site/service or download any materials from it.
</p>
<h4>1. Acknowledgement and Acceptance of Terms of Use</h4>
    <p>This Service is offered to you conditioned on your acceptance without modification, of the terms, conditions, and notices contained in this agreement and any operating rules, policies, and procedures that may be published from time to time on the Site by AutoLove (collectively, the "Terms"). The following Terms govern your access to and use of the Service. Your use of the Service constitutes
your agreement to all such Terms.</p>

<p>If any User is using the Service on behalf of User's employer, User represents that he/she is authorized to accept these Terms on his/her employer's behalf. Unless explicitly stated otherwise, the Terms will govern the use of any new features that augment or enhance the current Services, including the release of new resources and services. In case of any violation of these rules and regulations, AutoLove
reserves the right to seek all remedies available by law and in equity for such violations.</p> 
 
<h4>2. The Service and Use</h4>
<p>AutoLove is a social media marketing company. AutoLove provides you social media engagement, automated likes/views/followers through Instagram. This service provides convenience to users for promoting their product, brand or service through Instagram. You can also access the Instagram posts on AutoLove Site to know how many likes/views your image/content has. You are required to pay monthly subscription fees through PayPal, for the services chosen for use on your social media account. Your account will be debited every month on the same date
on which the initial payment was made. You can cancel your account before the next billing date. Service usually starts within 30 minutes of your initial payment.Fee paid will be refunded only if the service does not start within three business days. In case of any delay or other concerns, you can contact our support team bysending an email to info@autolove.co. We reserve the right, in our sole discretion,
to change fees for the Service at any time.</p>

<p>You acknowledge that AutoLove’s ability to provide the Services is dependent upon your full and timely co-operation as well as the accuracy and completeness of any specification, information and data that you provide to AutoLove. Accordingly, youshall provide AutoLove with access to and use of, all information, data and documentation reasonably required by AutoLove for the performance of the Services.</p>


<h4>3. Registration</h4>
<p>In order to use the services, you will have to visit the site and register to open your account with us. Registration can be done free of any charge.</p>

<p>You must be at least 13 years of age or older. Individual users, Businesses/ Corporate have to submit their valid email address and user name to register for using the service. Businesses, in addition to name and email address, have to furnish billing address, PayPal details. You have to select a username and password during the registration process.</p>

<p>By electing to use these services; (a) you warrant that all information you submit while registering is current, true and accurate; (b) you agree to update this information; (c) You will not create more than one personal profile: (d) If we disable your account, you will not create another one without our permission; (e) you agree not to use the Services for any unlawful purpose.</p>

<p>You also represent that you are not a person barred from receiving the Services under the laws of your country or other applicable jurisdiction. If you provide any information that is untrue, incorrect, not current or incomplete, or if AutoLove has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, AutoLove has the right to suspend or terminate your
account and refuse any and all current or future use of the Services (or any portion thereof).</p>


<h4>4. Security</h4>
<p>The password chosen by you is known only to you. You are entirely responsible for maintaining the confidentiality of your password. You agree not to use the account, username, or password of another User at any time or to disclose your password to any third party. You shall use all reasonable endeavours to ensure that the information transmitted to or from AutoLove’s servers with respect to the Services you require is secure. You are solely responsible for the activity that occurs on your account. You agree to notify AutoLove immediately if you suspect
any unauthorized use of your account or access to your password. You are solely responsible for any and all use of your account by any third party.</p>

<h4>5. Modifications</h4>
<p>AutoLove reserves the right to modify these terms. The amended Terms shall be effective immediately after they are initially posted on the Site. Your continued use of the Service after the posting of the amended Terms on the Site constitutes your affirmative: (a) acknowledgement of the Terms and its modifications; and (b) agreement to abide and be bound by the Terms, as amended. You acknowledge and agree that AutoLove shall not be liable to you or any third party in the event that AutoLove exercises its right to modify or discontinue all or part of the Service.</p>

<h4>6. License</h4>
<p>All right, title and interest (including without limitation all Intellectual Property Rights) in the AutoLove Materials shall vest in and remain vested in AutoLove and its licensors. The materials on this Site are copyrighted and any unauthorized use of any materials on this Site may violate copyright, trademark, and other laws. Subject to the terms and conditions of this Agreement, you are hereby granted a personal, non-exclusive, limited, non-transferable, freely revocable, license to use the Service. You may download the information ("Materials") found on AutoLove website specifically licensed to you or as allowed by any license terms or as per the Terms provided with individual Materials. All rights not expressly granted to you in this Agreement are reserved by AutoLove and its licensors. This is a license, not a transfer of title, and is subject to the following restrictions. You may not:</p>
<p>a. Modify the Materials or sell or rent out;</p>

<p>b.Decompile, reverse engineer, or disassemble software Materials except and
only to the extent permitted by applicable law;</p>

<p>c. Remove any copyright or other proprietary notices from the Materials;</p>

<p>d. Transfer, sub-license or distribute the site content or service to another person. You agree to prevent any unauthorized copying of the Materials.
</p>

<h4>7. User Conduct/Lawful Use</h4>
<p>You agree that use of the site/service is subject to all applicable International, National, Federal, state, and local laws and regulations. You agree to abide by these laws and are solely responsible for using the Service. You agree to use
AutoLove for lawful purposes only.</p>

<p>You agree:</p>
<p>a. not to use AutoLove for illegal purposes;</p>

<p>b. not to interfere or disrupt networks connected to AutoLove;</p>

<p>c. to comply with all regulations, policies and procedures of networks connected to AutoLove;</p>

<p>d. not to resell or transfer your access to the service to any third party;
</p>e. not to restrict any other visitor from using the service;
</p>

<p>f. to act responsibly, treat other website users with respect and not violate
their rights;</p>
<p>g. not to copy, modify, adapt, sublicense, translate, sell, reverse engineer,
decompile, or disassemble any portion of the Service or any part of the
Website;</p>

<p>h. not to harvest or collect information about Users of the service without
their express consent;</p>

<p>i. to comply with copyright laws and rules covering copyrighted data;</p>

<p>d. not to resell or transfer your access to the service to any third party;
</p>j. not to extract data which is contrary to applicable laws or third party rights including Intellectual Property Rights.
</p>

<p>You agree that - you will not distribute or facilitate distribution of any content, including but not limited to text, communications, software, images, sounds, data, or other information; you will not send / receive any message that: (1) is unlawful,threatening, abusive, harassing, defamatory, libellous, deceptive, fraudulent,invasive of another's privacy, tortuous, or otherwise violates AutoLove's rules or
policies; (2) victimizes, harasses, degrades, or intimidates an individual or group of individuals on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability; (3) infringes on any patent, trademark, trade secret, copyright, right of publicity, or other proprietary right of any party; (4) constitutes unauthorized or unsolicited advertising, junk or bulk email (also known as "spamming"); (5) contains software viruses or any other computer code, files, or programs that are designed or intended to disrupt, damage, or limit the functioning of any software, hardware, or telecommunications equipment or to damage or obtain unauthorized access to any data or other information of any third party; (6) contains site search/retrieval applications or device or process to download the contents available through the service; (7) impersonates any person or entity, including any of our employees or representatives.
</p>
<p>If it is brought to our notice that you are using the service for unlawful purposes, or infringing on any copyright, you will be advised to desist from such use. Your failure to take the required action immediately will result in a cancellation of your account and forfeiture of any fees provided to AutoLove. AutoLove also reserves the right to remove any prohibited material without warning or notification to the
User.</p>

<h4>2. Restricted Activities</h4>
<p>Your Information and your activities shall not: (a) be false, inaccurate or misleading; (b)be fraudulent; (c) infringe any third party's copyright, trademark, trade secret or other proprietary rights or rights of publicity or privacy; (d) violate any law, statute, ordinance
or regulation (including, but not limited to, those governing antidiscrimination or false advertising); (e) be defamatory, libelous, unlawfully threatening or unlawfully harassing; (f) be obscene or contain child pornography or, if otherwise adult in nature or harmful to
minors; (g) contain any viruses, Trojan horses, worms, time bombs or other computer programming routines that may damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information.</p>

<p>If you encounter such prohibited material on the Site you waive your right to any damages related to such exposure. Such material should be immediately reported to info@autolove.co.</p>

<p>In case of any violation of these Terms AutoLove reserves the right to seek all remedies available by law and in equity for such violations. As we use Instagram services for our services, you are also required to follow the policies of InstagramInc. We are in no way liable for account suspensions or any other action taken by Instagram due to violating their terms of use.</p>

<h4>8. No Resale</h4>
<p>You agree not to reproduce, duplicate, copy, sell or resell access to the Service,without the express written consent of AutoLove.</p>

<h4>9. Intellectual Property Rights</h4>
<p>For the purposes of this Agreement, "Intellectual Property Rights" means all patent rights, copyright rights, mask work rights, moral rights, rights of publicity, trademark, trade dress and service mark rights, goodwill, trade secret rights and other intellectual property rights as may now exist or hereafter come into existence, and all applications therefore and registrations, renewals and extensions thereof, under the laws of any state, country, territory or other jurisdiction.</p>

<p>You acknowledge that content, including but not limited to policy information, text, software, music, sound, photographs, video, graphics, the arrangement of text and images, commercially produced information, and other materials contained on the Site or through the Service ("Content"), is protected by copyright, trademarks, service marks, patents or other proprietary agreements and laws and you are only permitted to use the Content as expressly authorized by AutoLove .These Terms do not transfer any right, title, or interest in the Service, Site or the
Content to User, and User may not copy, reproduce, distribute, or create derivative works from this Content without express authorization by AutoLove. You agree not to use or divulge to others any information designated by AutoLove as proprietary or confidential. Any unauthorized use of any Content contained on the Site or through the Service may violate copyright laws, trademark laws, the laws of privacy and publicity, and communications regulations and statutes.</p>

<p>EXCEPT AS SPECIFICALLY PERMITTED HEREIN, NO PORTION OF THE INFORMATION ON THE SITE OR SERVICE MAY BE REPRODUCED IN ANY FORM, OR BY ANY MEANS, WITHOUT PRIOR WRITTEN PERMISSION FROM AUTOLOVE. USERS ARE NOT PERMITTED TO MODIFY, DISTRIBUTE, PUBLISH, TRANSMIT OR CREATE DERIVATIVE WORKS OF ANY MATERIAL FOUND ON THE SITE OR SERVICE, FOR ANY PUBLIC, PERSONAL OR COMMERCIAL PURPOSES</p>

<h4>10. Disclaimer/ No Warranty</h4>
<p>AutoLove will attempt to provide the Services at all times, except for limited periods for maintenance and repair. The actual service coverage, speeds, locations and quality may vary. However, the Services may be subject to unavailability for a variety of factors beyond our control including emergencies, third party service failures, transmission, equipment or network problems or limitations,interference, signal strength, and may be interrupted, limited or curtailed. Delays or omissions may occur. AutoLove will not guarantee the accuracy, completeness or reliability of the data extracted, and shall not be liable for the consequences of use of such data. We may impose usage or Services limits, suspend the Services, or block certain kinds of usage in our sole discretion to protect users or the Services.</p>

<p>THE SERVICE IS PROVIDED "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY OF ANY KIND INCLUDING WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT OF INTELLECTUAL PROPERTY OR FITNESS FOR ANY PARTICULAR PURPOSE. IN NO EVENT SHALL AUTOLOVE BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, BUSINESS INTERRUPTION, LOSS OF INFORMATION) ARISING OUT OF THE USE OF OR INABILITY TO USE THE SERVICES, OR INCURRED BY YOU ARISING OUT OF OR IN CONNECTION WITH ANY DISPUTE BETWEEN YOU AND ANY WEBSITE WHICH YOU SCRAPE, FROM WHICH YOU EXTRACT DATA OR FROM WHICH YOU COPY DATA, EVEN IF AUTOLOVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. BECAUSE SOME JURISDICTIONS PROHIBIT THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.</p>


<p>AutoLove further does not warrant the accuracy, guarantee or completeness of the information, text, graphics or other items contained within the website content. AutoLove may make changes to the content, or to the services described therein, at any time without notice. AutoLove makes no commitment to update the content.</p>
<p>The user should not rely on the information presented on this site for any purpose, and should always seek the legal advice of counsel in the appropriate jurisdiction before using the service.</p>

<center><h4>WE ARE NOT ENDORSED BY OR PART OF INSTAGRAM INC. OR FACEBOOK INC.</h4></center>
<h4>11. Limitation of Liability</h4>
<p>As a condition of use of the Service, and in consideration of the services provided by AutoLove, you agree that neither AutoLove, nor any officer, affiliate, director, shareholder, agent, contractor or employee of AutoLove will be liable to you or any third party for any direct, indirect, incidental, special, punitive, or consequential loss of profits, loss of earnings, loss of business opportunities, loss of
goodwill, loss of reputation, loss of anticipated savings, damages, expenses, or costs resulting directly or indirectly from, or otherwise arising in connection with the Service, Site or Content; including but not limited to any of the following:</p>
<h4>11. Contact</h4>
<p>a. Reliance: The use of the Service by you, including but not limited to damages resulting from or arising from your reliance on the Service, or the mistakes, omissions, interruptions, errors, defects, delays in operation, non-deliveries, mis-deliveries, transmissions, eavesdropping by third parties, or any failure of performance of the Service.</p>

<p>b. Termination: The termination of your use of the Service by AutoLove pursuant to these Terms.</p>
<p>c. Infringement: Any allegation, claim, suit, or other proceeding based upon a contention that the use of the Service or the Site by you or a third party infringes or misappropriates the copyright, patent, trademark, trade secret, confidentiality, privacy, or other intellectual property rights or contractual rights of any third party.</p>
<p>d. Force Majeure: Any delay or failure of AutoLove to perform due to government restriction, strikes, war, any natural disaster or any other
condition beyond AutoLove's control.</p>

<p>The limitations set forth in this section apply to the acts, omissions, negligence,and gross negligence of AutoLove and its affiliates, contractors, subcontractors, officers, directors, shareholders, managers, employees, and agents, which, but for this provision, would give rise to a course of action in contract, or any other legal doctrine.</p>
<p>EXCEPT AS OTHERWISE EXPRESSLY PROVIDED IN THESE TERMS, AUTOLOVE SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, SPECIAL, MULTIPLE, OR CONSEQUENTIAL DAMAGES, RESULTING FROM THE USE OR THE INABILITY TO USE AUTOLOVE SERVICES OR FOR COST OF PROCUREMENT OF
SUBSTITUTE SERVICES OR RESULTING FROM ANY SERVICES PURCHASED OR OBTAINED THROUGH THE SITE OR RESULTING FROM UNAUTHORIZED ACCESS TO DATA,
INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, USE, DATA OR OTHER INTANGIBLE PROPERTY, EVEN IF AUTOLOVE HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES. THE ENTIRE LIABILITY OF AUTOLOVE IN CONTRACT, TORT (INCLUDING, WITHOUT LIMITATION, NEGLIGENCE) OR OTHERWISE, AND YOUR EXCLUSIVE REMEDY WITH RESPECT TO THE USE OF THE SITE AND THE SERVICE OR ANY BREACH OF THIS AGREEMENT ARE LIMITED TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION. YOU HEREBY RELEASE AUTOLOVE FROM ANY AND ALL OBLIGATIONS, LIABILITIES AND CLAIMS IN EXCESS OF THIS LIMITATION.</p>

<h4>12. Indemnification</h4>
<p>
You agree to defend, indemnify and hold harmless AutoLove and its affiliates, directors, officers, employees, proprietors, independent contractors, consultants, partners, shareholders, representatives, customers, agents, predecessors, successors, and permitted assigns from and against any claim, suit, demand, loss, damage, expense (including reasonable attorneys' fees and costs) or liability that may result from, arise out of or relate to: (a) acts or omissions by you arising out of or in connection with this Agreement; (b) intentional or negligent violations byyou of any applicable laws or governmental regulation, (c) contractual relations between you and a third party; or (d) infringement of intellectual property rights including, but not limited to, rights relating to patent and copyright.</p>
<p>
User hereby indemnifies and holds harmless AutoLove, its stockholders, officers, directors, employees and agents from any and all loss, cost, damage, expense, or liability relating to or arising out of the transmission, reception, and/or content of information of whatever nature transmitted or received by User.</p>


<h4>13. Termination</h4>
<p>
You agree that we may terminate your right to use our Services if the information that you provided initially to register for our Services or subsequently modified, contains false or misleading information, or conceals or omits any information we would likely consider material. Additionally, AutoLove reserves the right to deny provision of service to any user.</p>
<p>
Any attempt to undermine or cause harm to AutoLove’s server or its customers, is strictly prohibited and will be subject to automatic account termination. This includes Spamming, transmission of - malware, viruses, trojan horses; or by linking to sites and files that contain or distribute malware, viruses and trojan horses.</p>
<p>AutoLove may terminate user’s account and forfeit any fee at any time without prior notice, if you are in breach of the terms of this Agreement. AutoLove will be the sole arbiter as to what constitutes a violation of the Agreement.</p>



<h4>14. Confidentiality</h4>
<p>
Through use of the Service and/or Site, you and AutoLove may disclose or make available to each other, Confidential Information in connection with the activities contemplated herein. Both the parties agree that during the Term of this Agreement and thereafter (a) they shall provide at a minimum, the same care to avoid disclosure or unauthorized use of confidential information as is provided to provide its own similar information, but in no event less than a reasonable standard of care; (b) they will use Confidential Information belonging to the other
solely for the purposes of this Agreement and (c) they will not disclose Confidential Information belonging to the other party, to any third party (other than its employees and/or consultants reasonably requiring such Confidential Information for purposes of this Agreement who are bound by obligations of nondisclosure and limited use at least as stringent as those contained herein) without the express prior written consent of the other party. Each party will promptly return to the disclosing Party upon request any Confidential Information of the other party.AutoLove will not monitor, edit, disclose, sell, rent, license, exchange, or releaseany of your personally identifiable information without your explicit consent unless AutoLove has reason to believe that disclosing this information is necessary: (a) to conform to the edicts of law or comply with legal process served on AutoLove; (b) to prevent injury to or interference with (either intentionally or unintentionally) AutoLove rights or property, other AutoLove users, or anyone else that could be harmed by such activities; or (c) to act under exigent circumstances to protect the personal safety of the users or the public. If AutoLove transfers assets or has any change in control, user information may be transferred to the entity that acquires such assets or control of AutoLove.</p>
<p>
For purposes of this Agreement, "Confidential Information" means, with respect to AutoLove, any and all information in written, representational, electronic, verbal or other form relating directly or indirectly to the present or potential business, operation or financial condition of AutoLove (including, but not limited to, information identified as being proprietary and/or confidential, pricing, marketing plans, customer and supplier lists, service data, and any information which might reasonably presumed to be proprietary or confidential in nature) excluding any such information which: (i) is known to the public (through no act or omission of User in violation of this Agreement); (ii) is lawfully acquired by User from an independent source having no obligation to maintain the confidentiality of such information; (iii) was known to User prior to its disclosure under this Agreement; (iv) was independently developed by the User; or (v) is required to be disclosed by governmental or judicial order.</p>



<h4>15. Miscellaneous Provisions</h4>
<p>
a. Assignment: You may not assign this Agreement, to any third party, without AutoLove's prior written consent. Any assignment without such consent will make the assignment/transfer null and void. AutoLove may assign or transfer these Terms, at its sole discretion, without restriction, to provide the agreed Services to you. Subject to the foregoing, these Terms will bind and inure to the benefit of the parties, their successors and permitted assigns.</p>
<p>b. Notice: Any notice required to be given or otherwise given pursuant to this Agreement by the parties concerned, shall be in writing and shall be sent through email, or hand delivered, mailed by express mail, Government mail service, or sent by recognized courier service to the address mentioned in the website in case of AutoLove and to the address provided by the user at the time of registration.</p>
<p>c. Entire Agreement: These Terms and any modifications hereto constitute the entire agreement between the parties with regard to the subject matterhereof and supersede all prior understandings and agreements, whether written or oral, as to such subject matter. Nothing contained in these Terms shall be deemed to constitute either party as the agent or representative of the other party, or both parties as joint ventures or partners for any purpose.</p>
<p>d. Severability: In the event that any provision of the Terms shall, in whole or in part, be determined to be invalid, unenforceable or void for any reason, such determination shall affect only the portion of such provision determined to be invalid, unenforceable or void, and shall not affect in any way the remainder of such provision or any other provision of the Terms.</p>
<p>e. No Waiver: AutoLove’s failure to act with respect to a breach by you or others, does not waive its right to act with respect to subsequent or similar breaches.</p>

<h4>16. Governing Law and Venue</h4>
<p> These Terms shall be governed by and construed in accordance with the Laws of Florida, USA, without regard to its choice of law provisions. In the event of any conflict between foreign laws, rules and regulations and those of Florida, USA, the laws rules and regulations of Florida, USA shall govern. The United Nations Convention on Contracts for the International Sale of Goods shall not apply to
these Terms.</p>
<p> Any dispute, controversy or claim arising out of or relating to this Agreement, or the breach termination or invalidity thereof, shall be settled by arbitration in accordance with the Rules of American Arbitration Association as at present in force and as may be amended. The place of arbitration shall be ______________, Florida. Arbitration shall be conducted in English language.</p>
<h4>17. Contact</h4>
<p>
If you have any complaints or require any clarifications regarding these Terms, please submit your complaints or queries via an email to info@autolove.co.</p>
            <div class="panel-content">
                
            </div>

        </div>
    </div>
</div>     
<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>     
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>