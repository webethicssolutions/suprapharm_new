<?php echo $__env->make('frontend.common.header-new', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!--about-content-start-->
        <!-- Inspiro Slider -->
        <div id="slider" class="inspiro-slider innerbanner">
            <!-- Slide 2 -->
            <div class="slide background-overlay-dark kenburns" style="background-image:url('/assets/frontend/new/images/office.jpg');">
                <div class="container">
                    <div class="slide-captions text-center text-light">
                        <!-- Captions -->
                        <h1>Prestations de service</h1>
						
                        <!-- end: Captions -->
                    </div>
                </div>
            </div>
            <!-- end: Slide 2 -->

        </div>
        <!--end: Inspiro Slider -->

        <section class="contactsection">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 contactinformation">
                        <p>Ensemble, nous nous donnons les moyens de developer les outils pour rendre le meilleur service possible à nos clients et simplifier la gestion du back office. Nous proposons des solutions autour de 4 axes majeurs : les achats, la communication, la vente et la fidélisation sans oublier le back office et le réglementaire</p>
                    </div>
                </div>
             </div>
        </section>

<section>
                <!-- Blog -->
				<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/service1.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">La centrale</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Plus de 70 laboratoires et prestataires 
                    Conditions max sans se stocker

Les négociations sont réalisées par des titulaires spécialisés dans les achats

100% des négos obtenues avant mi février

Envois automatiques limités

les Rfa ne transitent pas par le groupement mais sont directement versées au pharmacien

Plan trade annuel pour faciliter vos achats</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/esnseigne.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">L’enseigne : la façade
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Une enseigne valorisante, clairement identifiable tout en respectant l’identité de la pharmacie et du pharmacien.

Des solutions économiques avec des éléments qui s'emboîtent dans l’existant

Des coverings pour un impact maximal à faible coût</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/magasin_suprapharm.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">L’enseigne : le concept intérieur
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>4 univers - une zone de confidentialité distincte

Une triple signalétique pour un repérage rapide des rayons

du mobilier moderne pour un entretien facile

des linéaires totalement modulables et adaptables à votre espace avec siganlétique aimantée interchangeable

Des tarifs négociés avec des entreprises de couverture nationale</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/magasine.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">La com
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Le mag : catalogue thématique bimensuel intégrant offres produits - conseils santé - jeux concours

L'animation du point de vente : stop rayons et affiches en rapport avec les offres du mag

Les coverings : personnalisables à la demande</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/pharmacie_virtuelle.png')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Le digital
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>offre site web+application mobile spécifique

mise à jour automatique produits, prix et stocks avec votre logiciel de gestion

procédure ansm simplifiée

envoi d'ordo via smartphone

Live chat

livraison express (1H)</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/marque-propre.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Les marques propres
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Nous développons des produits exclusifs à très forte rotation

Qualité pharmaceutique et tolerance optimale

Prix inférieur au prix le plus bas du marché sans stocker

Commande par pharmaml ou plateforme pour une livraison en 48h</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/caddie-avec.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">La plate-forme d'achat
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Disponible en pharmaml

Commande en petite quantité

Livraison 48h via grossiste

Produits marques propres

Marques leaders à prix canon</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/garde-ordos.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Les goodies
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Gardes ordonnances

porte carte vitale

blouses, badges

cadeaux clients

bacs soldeurs</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/iMac-Template.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">L'intranet</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Ressources téléchargeables

Conseils Santé

Process métier - Suivi des salariés

Contacts fournisseurs et prestataires

Conditions commerciales

outil de création d'affiches et flyers</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/besoins3.png')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">La formation
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Nous travaillons avec des organismes agréés OPCA PL et DPC pour nos formations et celles de nos équipes. Les dossiers d’agréments sont pris en charge directement par l’organisme.

</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/marlboro4.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Le merchandising
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Nous intervenons dans les missions suivantes :

définition des espaces avant travaux

optimisation de l’existant

étude de rentabilité linéaire

suivi merchandising mensuel</p>
</div></div>
				</div>
					</div>
					<div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/juridique.png')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Le back-office
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Conseils juridiques : droits du commerce, droits du travail, transactions

Conseils ordinaux : nous vous accompagnons dans vos démarches de nature ordinale

Leasing et emprunts : partenariats privilégiés pour des contrats sans fausses notes</p>
</div></div>
				</div>
					</div>
                    <div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/magasin_suprapharm.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Bien  acheter
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Analyse statistique -  stratégie d’achat - 110 accords - produits exclusifs - centrale d’achat
</p>
</div></div>
				</div>
					</div>
                    <div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/magasin_suprapharm.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Bien communiquer
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Enseigne - catalogue et animation rayons -  Facebook

</p>
</div></div>
				</div>
					</div>
                    <div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/magasin_suprapharm.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Bien vendre
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Merchandising - formation - site internet - trade 400 opérations commerciales
</p>
</div></div>
				</div>
					</div>
                    <div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/magasin_suprapharm.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Fidéliser
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Carte de fidélité- formations -  scanordo - livraison express - MAD online - matériel médical pour médecins et infirmières

</p>
</div></div>
				</div>
					</div>
                    <div class="col-lg-4 col-md-6 serviceblkdiv">
					<div class="serviceblk">
						<div class="serviceimgwrap">
							<img src="<?php echo e(url('assets/frontend/new/images/magasin_suprapharm.jpg')); ?>">
						</div>
						<div class="sc_services_item_info">
		<div class="sc_services_item_header">
			<h6 class="sc_services_item_title"><a href="#">Back office et réglementaires 
</a></h6>
		</div>
					<div class="sc_services_item_content"><p>Fiches process - Rgpd - Mediateur de la consommation - suivi salarié - comite d’entreprise - conciergerie prestataires

</p>
</div></div>
				</div>
					</div>
				</div>
				</div>
                <!-- end: Blog -->
</section>



        <!-- Footer -->
   <?php echo $__env->make('frontend.common.footer-new', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
    <!--Plugins-->
    


    <script type="text/javascript">
        var tpj = jQuery;

        var revapi21;
        tpj(document).ready(function() {
            if (tpj("#rev_slider_21_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_21_1");
            } else {
                revapi21 = tpj("#rev_slider_21_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/plugins/revolution/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        onHoverStop: "off",
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [868, 768, 960, 720],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 700,
                        levels: [2, 6, 10, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51, 55],
                        type: "mouse",
                        disable_onmobile: "on"
                    },
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

    </script>

</body>

</html>
