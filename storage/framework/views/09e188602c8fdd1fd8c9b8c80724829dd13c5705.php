
<?php $__env->startSection('pageTitle', 'Add User'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<?php if(Session::has('error')): ?>
								<div class="error-msg">
								   <?php echo e(Session::get('error')); ?>

								</div>
					        <?php endif; ?>
							<div class ="col-sm-12 user_profile" style="margin-bottom:30px">
								<h2 >Add New User</h2>
							</div>
							
							<?php echo e(Form::open(array('url' => 'admin/user/admin_save_user', 'method' => 'post','class'=>'profile form-horizontal','enctype'=>'multipart/form-data'))); ?>

							<div class="form-group col-md-12">
								<div class="row"> 
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('First Name')); ?>

											<?php echo e(Form::text('first_name','',array('class'=>'form-control','placeholder'=>'First Name'))); ?>

											<span class="error"> <?php echo e($errors->first('first_name')); ?> </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Last Name')); ?>

											<?php echo e(Form::text('last_name','',array('class'=>'form-control','placeholder'=>'Last Name'))); ?>

											<span class="error"> <?php echo e($errors->first('last_name')); ?> </span>
										</div>  

										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Username')); ?>

											<?php echo e(Form::text('username','',array('class'=>'form-control','placeholder'=>'First Name'))); ?>

											<span class="error"> <?php echo e($errors->first('username')); ?> </span>
										</div> 
										

										<div class="clearfix"></div>
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Email')); ?>

											<?php echo e(Form::text('email','',array('id'=>'user_email','class'=>'form-control','placeholder'=>'Email'))); ?>

											<span class="error"> <?php echo e($errors->first('email')); ?> </span>
										</div>
										<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Password')); ?>

												<?php echo e(Form::password('password',array('class'=>'form-control','placeholder'=>'Password'))); ?>

												<span class="error" >  <?php echo e($errors->first('password')); ?> </span>
										</div>
										
										<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Gender')); ?>

												<select class="form-control" name="gender">
                   								<option disabled="disabled">Select</option>
	                                    		<option value="male">Male</option>
	                                    		<option value="female">Female</option>
	                                    		<option value="other">Other</option>
	                                          
	                                          </select> 
										</div>
									<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Date Of Birth')); ?>

												<?php echo e(Form::date('dob','',array('class'=>'form-control','placeholder'=>'Date of Birth'))); ?>

												<span class="error" >  <?php echo e($errors->first('dob')); ?> </span>
										</div>
										
										<div class="clearfix"></div>
										
										 																
									</div>
								
								</div>
							</div>  
								
							<div class="col-md-12"> 
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Add User" type="submit">
									 <a href="<?php echo e(url('admin/users')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>			
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- <script>
jQuery(document).ready(function(){ 
	jQuery(".profile").submit(function(e){
		var company_name    = jQuery("#company_name").val().trim();
		var company_number  = jQuery("#company_number").val().trim();
		var user_email      = jQuery("#user_email").val().trim();
		var contact_phone   = jQuery("#contact_phone").val().trim(); 
		
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		   
		if(company_name == ""){ 
			jQuery("#company_name").addClass("error_block"); 
			jQuery("#company_number").removeClass("error_block");  
			jQuery("#contact_phone").removeClass("error_block"); 
			jQuery("#user_email").removeClass("error_block"); 
		}else if(company_number == ""){ 
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").addClass("error_block"); 
			jQuery("#user_email").removeClass("error_block"); 
			jQuery("#contact_phone").removeClass("error_block"); 
		}else if(user_email == ""){  
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").removeClass("error_block"); 
			jQuery("#user_email").addClass("error_block");   
			jQuery("#contact_phone").removeClass("error_block");  
		}else if(user_email != "" && !emailReg.test( user_email )){ 
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").removeClass("error_block"); 
			jQuery("#user_email").addClass("error_block");   
			jQuery("#contact_phone").removeClass("error_block");  
		}else if(contact_phone == ""){    
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").removeClass("error_block"); 
			jQuery("#user_email").removeClass("error_block");  
			jQuery("#contact_phone").addClass("error_block"); 
		}else{ 
			return true;
		}
		
		e.preventDefault();
	});
})
</script> -->
 <?php $__env->stopSection(); ?>
 
 
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>