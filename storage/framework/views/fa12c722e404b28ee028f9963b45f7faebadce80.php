<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
// define('PAYMENT_ENVIRONMENT','SANDOBX');
// define('PAYPAL_URL','https://www.sandbox.paypal.com/cgi-bin/webscr');
// define('PAYPAL_MERCHANT','pathcodertest@gmail.com');


// define('CURRENCY','USD');
// define('PAYPAL_RETURN_URL','https://autolove.co/return_url');
// define('CANCEL_URL','https://autolove.co/');
// define('PAYPAL_NOTIFICATION_URL','https://autolove.co/ipn');

/*get name visitor's country */
$ip = $_SERVER['REMOTE_ADDR'];
$dataArray = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
$current_country = $dataArray->geoplugin_countryName;
/*end get name visitor's country */

?>
<div id="create-account" class="content content-checkout">
    <div class="panel panel-pad panel-left">
        <div class="summary">
            <!--- div class="summary-toggle">
                <div>
                    <img class="menu" src="/assets/img/dash/v1/menu.svg">
                    <a href="#">Show summary</a>
                </div>
                <div>$17.00</div>
            </div ---->
			<?php if(Session::has('success')): ?>
                <div class="success-msg">
                   <?php echo e(Session::get('success')); ?>

                </div>
            <?php endif; ?>
            <div class="summary-content">
                <table class="summary-table">
                    <thead class="hidden-xs hidden-sm">
                        <tr>
                            <th class="title" colspan="2">Order summary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span class="icon">
                                    <img src="<?php echo e(url('resources/assets/frontend/images/heartbeat.gif')); ?>">
                                </span>
                                <span style="margin-left:10px;">
                                    <?php echo e($like); ?> <?php if($type=='L'): ?>
										<?php echo e("Likes Per Post"); ?>

									  <?php else: ?>
										<?php echo e("Likes Per Post"); ?> + <?php echo e($followers); ?> <?php echo e('Followers'); ?> 
										<?php endif; ?> <!-- <em class="sub">/ post</em> -->
								 	<br>
                                    <a href="<?php echo e(url('/#pricing')); ?>">Change</a>
                                </span>
                            </td>
                            <td>&nbsp;<span> <?php echo e($price); ?> / mo</span></td>
                        </tr>
                      
                        <tr class="subtotal" style="display: none">
                            <td>
                                <div>Subtotal</div>
                                <div class="subtotal-prorated" style="display: none">Prorate Adj.</div>
                                <div class="subtotal-promo" style="display: none">Discount <span class="subtotal-promo-code"></span> <a href="#" class="clear"></a></div>
                            </td>
                            <td>
                                <div>$<span class="subtotal-amount"><?php echo e($price); ?></span></div>
                                <div class="subtotal-prorated" style="display: none">- $<span class="subtotal-discount">NaN</span></div>
                                <div class="subtotal-promo" style="display: none">- $<span class="subtotal-discount">NaN</span></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total today</th>
                            <th>$<span class="total"><?php echo e($price); ?></span></th>
                        </tr> 
                    </tfoot>
                </table> 
				<table class="coupn_table"> 
                    <thead>
						<th> 
							<label for="coupn-code"><span class="placeholder">Coupon code</span></label>
                        </th> 
                    </thead>
                    <tbody> 
						<tr> 
							<td>
								<div class="input-group">
									<input id="coupn_cde" type="text" name="coupn_cde" autocomplete="off" class="form-control" placeholder="Coupon Code" aria-label="Coupon Code"> 
								</div> 
							</td>
							<td>
								<div class="input-group">
								   <a class="btn btn-success btn-block" id="apply_coupon">Apply</a>
								</div>
							</td> 
                        </tr> 
						<tr> 
							<td colspan="2">
								<span id="apply_coupon_err" class="error text-left"></span>
							</td> 
                        </tr>
                    </tbody> 
                </table>
                <div class="summary-features hidden-xs hidden-sm">
                    <ul>
                        <li>Real likes from real people</li>
                        <li>Dynamic and natural delivery</li>
                        <li>Completely automatic</li>
                        <li>Safe and secure</li>
                        <li>Cancel anytime</li>
                    </ul>
                </div>
            </div>
        </div>    </div>
    <div class="panel panel-pad panel-right">
      
      <!-- <?php echo e(Form::open(array('url' => '/paypal', 'method' => 'post'))); ?> -->
     <form class="w3-container w3-display-middle w3-card-4 " method="POST" id="regsiter_form" action="#">
  <?php echo e(csrf_field()); ?>

        <div class="form checkout-form">
            <div class="user-info" id="user-details">
            <div class="form-header">
                <div class="title">Let's get started&nbsp;&nbsp;<span class="icon"></span></div>
                <p class="statement">
                    Create an account now to access your personalized dashboard after the checkout and connect your Instagram. 
                </p>
            </div>
            <hr>
            <div class="account-info">
              <div class="form-group">
                  <div class="form-group-header">
                      <span class="sub-title">Create account</span>
                  </div>
                  <div class="input-group input-group-email validation-group">
                      <input id="email" type="text" name="email" class="form-control placeholder-shown" placeholder="Email address" aria-label="Email address" value="<?php echo e($email); ?>"" required="">
                      <label for="email"><span class="placeholder">Email address</span></label>
                      <span class="validation-message required">Please enter an email address</span>
                    
                      <span id="email_error" class="error"> <?php echo e($errors->first('email')); ?> 
                      
                    </span>
                  </div>
                  <div class="input-group input-group-username validation-group">
                      <input id="username" type="text" name="username" class="form-control placeholder-shown" placeholder="Create username of login" aria-label="Create username of login" value="<?php echo e($name); ?>" required="">
                      <label for="username"><span class="placeholder">Create username for login</span></label>
                      <span class="validation-message required">Please enter a username</span>
                      <span class="validation-message invalid">Username is invalid</span>
                     
                      <span id="username_error" class="error"> <?php echo e($errors->first('username')); ?> 
                      
                    </span>

                  </div>
                  <div class="input-group input-group-password validation-group">
                      <input id="password" type="password" name="password" class="form-control placeholder-shown mask" placeholder="Password" aria-label="Password" required="">
                      <label for="password"><span class="placeholder">Password</span></label>
                      <span class="validation-message required">Please enter a password</span>
                    
                      <span id="password_error" class="error"> <?php echo e($errors->first('password')); ?> 
                      
                    </span>

                  </div>
                  <div class="input-group input-group-confirm-password validation-group">
                      <input id="confirm-password" type="password" name="password_confirmation" class="form-control placeholder-shown" placeholder="Confirm password" aria-label="Confirm password" required="">
                      <label for="confirm-password"><span class="placeholder">Confirm password</span></label>
                      <span class="validation-message required">Please confirm the password</span>
                      
                      <span id="password_confirmation_error" class="error"> <?php echo e($errors->first('password_confirmation')); ?> 
                      
                    </span>

                  </div>
              </div>      <hr>
              <div class="form-group">
                  <div class="form-group-header">
                      <span class="sub-title">Add Instagram account</span>
                      <span class="sub-title-info">
                          <span class="icon icon-lock"></span>
                          <span>SECURE</span>
                          <span class="icon icon-help" data-toggle="tooltip" data-placement="top" title="" data-original-title="No Instagram password required. To get started enter your Instagram username so we know where to start delivering likes."></span>
                      </span>

                  </div>
                  <div class="input-group input-search input-group-igusername validation-group">

                      <input id="insta_username" type="text" name="insta_username" class="form-control placeholder-shown" placeholder="Instagram username" aria-label="Instagram username" value="<?php echo e($insta_username); ?>" required="" >
                      
                      <span class="icon left"></span>
                      <label for="igusername"><span class="placeholder">Instagram username</span> <span class="validation-message"></span></label>
                         <span id="insta_username_error" class="error"> <?php echo e($errors->first('insta_username')); ?> 
                      
                    </span>

                      <div class="input-search-dropdown">
                          <ul class="input-search-dropdown-menu"></ul>
                          <span class="input-search-selection">
                              <img class="profile" src="">
                              <span class="name input-search-item-info">
                                  <span class="username input-search-item-title"></span>
                                  <span class="fullname input-search-item-subtitle hidden-xs hidden-sm"></span>
                              </span>
                              <span class="followers input-search-item-info">
                                  <span class="count input-search-item-title"></span>
                                  <span class="input-search-item-subtitle hidden-xs hidden-sm">followers</span>
                              </span>
                              <a href="#" class="close"></a>
                          </span>
                          <div class="input-search-item-template" style="display:none">
                              <a href="#" class="input-search-item">
                                  <img class="profile" src="">
                                  <span class="name input-search-item-info">
                                      <span class="username input-search-item-title"></span>
                                      <span class="fullname input-search-item-subtitle hidden-xs hidden-sm"></span>
                                  </span>
                                  <span class="followers input-search-item-info">
                                      <span class="count input-search-item-title"></span>
                                      <span class="input-search-item-subtitle hidden-xs hidden-sm">followers</span>
                                  </span>
                              </a>
                          </div>
                      </div>
                      <span class="loading" style="display: none"></span>
                      <span class="validation-message required">Please add a valid Instagram account</span>
                      <span class="validation-message invalid">Instagram account is invalid.</span>
                      <span class="validation-message conflict">Instagram account is set to Private. Change it to public and try again.</span>
                  </div>
              </div>    </div>
              <input type="hidden" id="subscription" name="subscription" value="<?php echo e($id); ?>">
              <input type="hidden" id="plan2id" name="plan2id" value="<?php echo e($planid); ?>">
             
            <button  id="register_btn" class="btn btn-success btn-block mx">Next<span class="loading" style="display:none;"><img src="<?php echo e(url('resources/assets/frontend/images/load.gif')); ?>"></span></button>
</div>
          <div class="billing-info" style="display:none;" id="billing-details">
          	<input type="hidden" name="custom" value="" id="custom">
              <input type="hidden" name="stripe_plan_id" value="<?php echo e($stripe_plan_id); ?>" id="stripe_plan_id">
              <input type="hidden" name="stripe_plan2_id" value="<?php echo e($stripe_plan2); ?>" id="stripe_plan2_id">
                <div class="form-group">
                    <div class="form-group-header">
                        <span class="sub-title">Pay with credit / debit card</span>
                        <div class="billing-features">
                            <div class="billing-feature"><span class="icon"><img src="/assets/img/moneyback.svg"></span><span class="message"><em data-toggle="tooltip" title="" data-original-title="If for any reason you are not satisfied with our service within 30 days of signing up, contact us for a full refund - no questions asked.">30 day money back guarantee</em></span></div>
                            <div class="billing-feature"><span class="icon"><img src="/assets/img/cancel-anytime.svg"></span><span class="message">Cancel anytime</span></div>
                        </div>        <!-- <span class="sub-title-info card-list">
                            <span class="card card-visa"></span>
                            <span class="card card-mastercard"></span>
                            <span class="card card-discover"></span>
                            <span class="card card-amex"></span>
                        </span> -->
                    </div>
                    <div class="input-group input-group-number validation-group">
                        <input id="cardNumber" type="tel" name="cardNumber" class="form-control placeholder-shown" placeholder="Card number" aria-label="Card number" required="">
                        <label for="card-number"><span class="placeholder">Card number</span></label>
                        <span class="icon icon-lock"></span>
                        <span class="validation-message required">Please enter a card number</span>
                        <span class="validation-message invalid">Card number is invalid</span>
                         <span id="cardNumber_error" class="error"> <?php echo e($errors->first('cardNumber')); ?>   </span>

                      
                    </div>
                    <div class="input-group input-group-name validation-group">
                        <input id="cardName" type="text" name="cardName" value="" class="form-control" placeholder="Cardholder name" aria-label="Cardholder name" required="">
                        <label for="card-name"><span class="placeholder">Cardholder name</span></label>
                        <span class="validation-message required">Please enter a cardholder name</span>
                        <span class="validation-message invalid">Cardholder name is invalid</span>
                           <span id="cardName_error" class="error"> <?php echo e($errors->first('cardName')); ?>   </span>

                    </div>
                    <div class="input-group input-select input-group-month validation-group">
                        <select id="cardMonth" name="cardMonth" class="form-control" placeholder="Month" aria-label="Month" required="">
                            <option value="" disabled="" selected="" hidden=""></option>
                            <option value="01" selected="">Jan 01</option>
                            <option value="02">Feb 02</option>
                            <option value="03">Mar 03</option>
                            <option value="04">Apr 04</option>
                            <option value="05">May 05</option>
                            <option value="06">Jun 06</option>
                            <option value="07">Jul 07</option>
                            <option value="08">Aug 08</option>
                            <option value="09">Sep 09</option>
                            <option value="10">Oct 10</option>
                            <option value="11">Nov 11</option>
                            <option value="12">Dec 12</option>
                        </select>
                        <label for="card-month"><span class="placeholder">Month</span></label>
                        <span class="validation-message required">Please enter a month</span>
                            <span id="cardMonth_error" class="error"> <?php echo e($errors->first('cardMonth')); ?> </span>
                    </div>
                    <div class="input-group input-select input-group-year validation-group">
                        <select id="cardYear" name="cardYear" class="form-control" placeholder="Year" aria-label="Year" required="">
                            <option value="" disabled="" selected="" hidden=""></option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021" selected="">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                            <option value="2028">2028</option>
                            <option value="2029">2029</option>
                        </select>
                        <label for="card-year"><span class="placeholder">Year</span> <span class="validation-message"></span></label>
                        <span class="validation-message required">Please enter a year</span>
                        <span id="cardYear_error" class="error"> <?php echo e($errors->first('cardYear')); ?>   </span>
                    </div>
                    <div class="input-group input-group-code validation-group">
                        <input id="cardCode" type="password" name="cardCode" autocomplete="off" class="form-control placeholder-shown mask" placeholder="CVV/CVC" aria-label="CVV/CVC" pattern="[0-9]*" maxlength="4" inputmode="numeric" required=""> 
                        <label for="card-code"><span class="placeholder">CVV/CVC</span></label>
                        <span class="validation-message required">Please enter a CVV/CVC code</span>
                        <span class="validation-message invalid">CVV/CVC code is invalid</span>
                          <span id="cardCode_error" class="error"> <?php echo e($errors->first('cardCode')); ?>   </span>
                    </div>  
                </div>
                <hr>
                <div class="form-group">
                    <div class="form-group-header">
                        <span class="sub-title">Billing address</span>
                    </div>
                    <div class="input-group input-select validation-group input-group-country">
                        <select id="billCountry" name="billCountry" class="form-control" placeholder="Country" aria-label="Country" required="">
                            <option value="" disabled="" hidden=""></option>
                            <option value="US" <?php if($current_country == "United States"){ echo "selected";}?>>United States</option>
                            <option value="AR" <?php if($current_country == "Argentina"){ echo "selected";}?>>Argentina</option>
                            <option value="AU" <?php if($current_country == "Australia"){ echo "selected";}?>>Australia</option>
                            <option value="BR" <?php if($current_country == "Brazil"){ echo "selected";}?>>Brazil</option>
                            <option value="CA" <?php if($current_country == "Canada"){ echo "selected";}?>>Canada</option>
                            <option value="CO" <?php if($current_country == "Colombia"){ echo "selected";}?>>Colombia</option>
                            <option value="HR" <?php if($current_country == "Croatia"){ echo "selected";}?>>Croatia</option>
                            <option value="DK" <?php if($current_country == "Denmark"){ echo "selected";}?>>Denmark</option>
                            <option value="FR" <?php if($current_country == "France"){ echo "selected";}?>>France</option>
                            <option value="DE" <?php if($current_country == "Germany"){ echo "selected";}?>>Germany</option>
                            <option value="IN" <?php if($current_country == "India"){ echo "selected";}?>>India</option>
                            <option value="ID" <?php if($current_country == "Indonesia"){ echo "selected";}?>>Indonesia</option>
                            <option value="IL" <?php if($current_country == "Israel"){ echo "selected";}?>>Israel</option>
                            <option value="IT" <?php if($current_country == "Italy"){ echo "selected";}?>>Italy</option>
                            <option value="JP" <?php if($current_country == "Japan"){ echo "selected";}?>>Japan</option>
                            <option value="LU" <?php if($current_country == "Luxembourg"){ echo "selected";}?>>Luxembourg</option>
                            <option value="MX" <?php if($current_country == "Mexico"){ echo "selected";}?>>Mexico</option>
                            <option value="NZ" <?php if($current_country == "New Zealand"){ echo "selected";}?>>New Zealand</option>
                            <option value="NO" <?php if($current_country == "Norway"){ echo "selected";}?>>Norway</option>
                            <option value="PR" <?php if($current_country == "Puerto Rico"){ echo "selected";}?>>Puerto Rico</option> 
                            <option value="RU" <?php if($current_country == "Russia"){ echo "selected";}?>>Russia</option>
                            <option value="SG" <?php if($current_country == "Singapore"){ echo "selected";}?>>Singapore</option>
                            <option value="ZA" <?php if($current_country == "South Africa"){ echo "selected";}?>>South Africa</option>
                            <option value="ES" <?php if($current_country == "Spain"){ echo "selected";}?>>Spain</option>
                            <option value="SE" <?php if($current_country == "Sweden"){ echo "selected";}?>>Sweden</option>
                            <option value="CH" <?php if($current_country == "Switzerland"){ echo "selected";}?>>Switzerland</option>
                            <option value="AE" <?php if($current_country == "United Arab Emirates"){ echo "selected";}?>>United Arab Emirates</option>
                            <option value="GB" <?php if($current_country == "United Kingdom"){ echo "selected";}?>>United Kingdom</option>
                        </select>
                        <label for="bill-country"><span class="placeholder">Country</span></label>
                        <span class="validation-message required">Please enter a country</span>

                          <span id="billCountry_error" class="error"> <?php echo e($errors->first('billCountry')); ?>   </span>
                    </div>
                    <div class="input-group input-group-address validation-group">
                        <input id="billAddress1" type="text" name="billAddress1" class="form-control" placeholder="Street Address" aria-label="Street Address" data-default="test" required="">
                        <label for="bill-address"><span class="placeholder">Address line 1</span></label>
                        <span class="validation-message required">Please enter an address</span>
                           <span id="billAddress1_error" class="error"> <?php echo e($errors->first('billAddress1')); ?>   </span>
                    </div>
                    <div class="input-group input-group-address2">
                        <input id="billAddress2" type="text" name="billAddress2" class="form-control placeholder-shown" placeholder="Apartment, suite, etc. (optional)" aria-label="Apartment, suite, etc. (optional)" data-default="">
                        <label for="bill-address2"><span class="placeholder">Address line 2</span></label>
                           <!---span id="billAddress2_error" class="error"> <?php echo e($errors->first('billAddress2')); ?>   </span--->
                    </div>
                    <div class="input-group input-group-city validation-group">
                        <input id="billCity" type="text" name="billCity" class="form-control" placeholder="City" aria-label="City" value="" data-default="test" required="">
                        <label for="bill-city"><span class="placeholder">City</span></label>
                        <span class="validation-message required">Please enter a city/town</span>
                         <span id="billCity_error" class="error"> <?php echo e($errors->first('billCity')); ?>   </span>
                    </div>
                    <div class="input-group input-select input-group-state validation-group hidden">
                         <input id="state" type="text" name="state"  placeholder="state" class="form-control placeholder-shown" data-default="">
                        <label for="state"><span class="placeholder">State</span></label>
                        <span class="validation-message required">Please enter a state</span>
                        <span id="state_error" class="error"> <?php echo e($errors->first('state')); ?>   </span>
                    </div>

                    <div class="input-group input-group-zip validation-group">
                        <input id="billZip" type="text" name="billZip" class="form-control" placeholder="Zip Code" aria-label="Zip Code" data-default="160059" required="">
                        <label for="bill-zip"><span class="placeholder">Postal code</span></label>
                        <span class="validation-message required">Please enter a zip/postal code</span>
                            <span id="billZip_error" class="error"> <?php echo e($errors->first('billZip')); ?>   </span>
                    </div>
					<?php if($current_country == "Puerto Rico"): ?>				
						<div class="notic mb-1"><em>(Note: Restricted, contact support.)</em></div>
					<?php endif; ?>
                </div>
				
				<input id="coupn_code" type="hidden" name="coupn_code"> 
				
                <button type="submit" class="btn btn-success btn-block btn-submit-cc mx" data-payment="card" data-saved="false" data-setup="subscription" id="pay_btn">Confirm &amp; Pay<span class="loading" style="display:none;"><img src="<?php echo e(url('resources/assets/frontend/images/load.gif')); ?>"></span></button>
				
                 </div>

          </form>
        <!--   <?php echo e(Form::close()); ?> -->
        </div>    </div>
</div>

<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<style>
.icon img {
  border-radius: 50px !important;
  height: 40px !important;
  width: 40px;
  margin-top: -7px !important;
}
.loading img {

   height: 25px !important;
   float: none;
   position: absolute;
   right: 0;
   top: -25px;

}
.loading {

   width: 100%;
   position: relative;

}
</style>

 <?php $__env->stopSection(); ?>
 
 
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>