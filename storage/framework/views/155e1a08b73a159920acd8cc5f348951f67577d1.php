
<?php $__env->startSection('pageTitle', 'Flyer Tool'); ?>

<?php $__env->startSection('content'); ?>

<style>
.create-np .row {
    height: auto;
}
.col-md-5.left-cont {
    padding: 50px 40px;
    color: #fff;
    font-weight: normal;
    font-family: 'Poppins', sans-serif !important;
   -webkit-box-shadow: 3px 0 10px 0 rgba(0, 0, 0, 0.3);
   box-shadow: 3px 0 10px 0 rgba(0, 0, 0, 0.3);
   background: #00b7ea;
   background: -o-linear-gradient(top, #00b7ea 0%,#009ec3 100%);
   background: -webkit-gradient(linear, left top, left bottom, from(#00b7ea), to(#009ec3));
   background: -o-linear-gradient(top, #00b7ea 0%, #009ec3 100%);
   background: linear-gradient(to bottom, #00b7ea 0%, #009ec3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00b7ea', endColorstr='#009ec3',GradientType=0 );
    margin-right: 30px;
    min-height: 100vh;
    overflow-y: auto;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
}
.left-cont label {
  font-size: 16px;
  font-weight: 100;
  margin-bottom: 8px;
  color: #fff !important;
}
.left-cont .form-control {
  height: 45px;
  border-radius: 0px;
  border: 0px;
  font-size: 14px;
  font-weight: 100 !important;
  margin: 0px;
  font-family: 'Poppins', sans-serif;
}
.create-np {
    border-top: 1px solid #0285a8;
    margin: 10px 0;
    padding: 10px 0px;
}
.main-heading::after {
    width: 100px;
    height: 1px;
    background-color: #fff;
    content: " ";
    display: inline-block;
    vertical-align: super;
    margin-left: 10px;
}
.main-heading {
    font-weight: 100;
    margin: 0px;
    padding: 10px 0;
    font-size: 20px;
}

.create-np .btn {
    border-radius: 0px;
    background-color: #fff;
    height: 45px;
    padding-left: 15px;
}
.create-np .filter-option-inner-inner {
    font-size: 14px;
    color: #505050;
    padding-top: 3px;
}
.flex-btn {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    width: 100%;
    -webkit-box-pack: space-evenly;
        -ms-flex-pack: space-evenly;
            justify-content: space-evenly;
    margin-top: 0;
	margin-bottom: 0px;
}

.flex-btn .btn {
  width: 100%;
  display: inline-block;
  background-color: #034757 !important;
  padding: 14px 10px !important;
  margin: 0 0 !important;
  font-size: 16px;
  font-weight: 100;
  text-decoration: none;
  text-transform: uppercase;
  border-radius: 0px;
  height: 50px;
    -webkit-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
.flex-btn .btn:nth-child(2) {
  margin: 0px 10px !important;
}
.flex-btn .btn:hover {
    background-color: #06323c !important;
	color:#fff !important;
}
.create-np .open .btn {
    height: auto !important;
}
.dropup .btn {
    height: auto !important;
}
.btn-row {
    display: inline-block;
    margin-top: 20px;
}
#loader {
    position: absolute;
    top: 19%;
    left: 50%;
    transform: translate(-50% ,-19%);
}
.fieldrow{display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;}
.fieldcolumn{-ms-flex-preferred-size: 0;
    flex-basis: 0;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;padding: 0px 10px;
    max-width: 100%;}

@media(max-width:1500px)
{
.col-md-6.left-cont {
   padding: 20px 20px;
}
.left-cont label {
    font-size: 14px;
	margin-bottom: 8px;
}
.main-heading {
    font-size: 22px;
	padding: 0 0 10px 0;
}
.left-cont .form-control {
    height: 40px;
}

.create-np .btn {
	height: 40px;
	font-size: 12px;
}

.create-np .filter-option-inner-inner {
    font-size: 12px;
    padding-top: 0;
}
.flex-btn .btn
{
	font-size: 14px;
}
.create-np .form-group.col-sm-4 {
    width: 100%;
    padding-bottom: 10px;
}
#loader {
    position: absolute;
    top: 12%;
    left: 50%;
    transform: translate(-50% ,-12%);
}
}


@media(max-width:1300px)
{
.create-np .form-group.col-sm-4, .create-np .form-group.col-sm-6 {
    padding: 0px 6px;
}


}

@media(max-width:1199px)
{
#loader {
    position: absolute;
    top: 25%;
    left: 50%;
    transform: translate(-50% ,-25%);
}

}

@media(max-width:991px)
{
.col-md-5.left-cont {
	margin-right: 0px;
	margin-bottom: 30px;
}
#loader {
    position: absolute;
    top: 20%;
    left: 50%;
    transform: translate(-50% ,-20%);
}
}

</style>



<!--  Select Product -->
<?php //$prdocuts =  get_products($CIP,$token);

?>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="" aria-hidden="false">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="background: #1e90ff;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

         </div>
         <div class="modal-body">
           <p id="succ_msg" style="height: 20px;color:Green;"></p>
          <label>Template Name</label>
   <input type="text" name="flyer_template" class="form-control" id="flyer_template" placeholder="Template name" >
   <p id="temp_err" style="height: 20px;color:red;"></p>

    <button type="button" class="btn btn-primary" id="save-flyer-template">Submit</button>
         </div>
         <div class="modal-footer">


             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

 <div class="container2">
 <div class="containerrow">
  <div class="col-md-5 left-cont">

<?php  // If token is expired or not matched
   if($prdocuts['error']!=''){ ?>
    <div class="row">

        <div class="form-group col-sm-6"  style="margin-top:100px;">

        <label for="uname">Please Enter Token Name:</label>
         <textarea name="token_value" cols="80" rows="10" class="form-control" id="token_value"> </textarea>
          <p style="color:red"> <?php echo $prdocuts['error']; ?></p>
        </div>


       </div>

       <div class="row">

        <div class="form-group col-sm-6" >
         <button type="submit" name="submit" value="search" class="btn btn-primary" id="token_save">Save Token</button>
        </div>

       </div>


 <?php   }else { ?>



  <!-- <button type="submit" name="submit" value="search" class="btn btn-primary">Submit</button> -->
     <!-- Product Info Change ---->
   <!--div id="update_box" style="display:none"-->

   	<div class="row">
	  		<div class="loader-wrap"> <img id="loader" src="ajax-loader.gif" style="width:30px;height:30px;display:none;"></div>
		<div class="form-group col-sm-6 pb-1" >
			<p style="color:red"> <?php echo $prdocuts['error']; ?></p>
			<label for="uname">Choisir un produit</label>
			<select name="product" class="form-control" id="product">
				<option value="">Choisir un produit</option><?php
					if(count($prdocuts['data'])){
						foreach($prdocuts['data'] as $key=>$value){?>
							<option value="<?php echo $value->id;  ?>"><?php echo $value->name_medicament;  ?></option>
						<?php }
					}  ?>
			</select>
		</div>

   </div>




	<div class="create-np">

	<div class="row">
	 <div class="col-sm-12">
	  <h1 class="main-heading"> Créer un nouveau produit </h1>
	 </div>
	</div>

	<div class="row">

		<div class="form-group col-sm-6">
			<label for="uname">Choisir modèle</label>
			<select name="" class="form-control" id="saved_template">
			  <option value="">Select...</option>
				<?php if(count($templates)>0): ?>
				<?php $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($temp->id); ?>"><?php echo e($temp->template_name); ?></option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
			</select>
		</div>
	</div>

	<div class="row">

		<div class="form-group col-sm-6" >
			<label for="uname">Titre:</label>
			<input type="text" name="main_title" class="form-control" id="main_title">
		</div>

        <div class="form-group col-sm-6" >
			<label for="uname">Nom du produit:</label>
			<input type="text" name="product_name" class="form-control" id="product_name">
        </div>
   </div>
   <div class="row">
        <div class="form-group col-sm-6" >
			<label for="uname">Prix:</label>
			<input type="text" name="product_price" class="form-control" id="product_price">
			<p class="error" style="color:red" id="price_error"> </p>
        </div>

		<div class="form-group col-sm-6">
			<label for="uname">Format:<span style="color:red">*</span></label>
			<select name="paper_size" class="form-control" id="paper_size">
				<option value="a4">A4</option>
				<option value="a4multi">A4 Multi Size</option>
				<option value="a5">A5</option>
			</select>
		</div>
    </div>
		<div class="row">
			<div class="form-group col-sm-6" >
				<label for="uname">Prix barré:</label>
				<input type="text" name="oldprice" class="form-control" id="oldprice">
				<p class="error" style="color:red" id="oldprice"> </p>
			</div>
			<div class="form-group col-sm-6" >
				<label for="uname">Opération:</label>
				<input type="text" name="offertext" class="form-control" id="offertext">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-6" >
				<label for="view">View:</label>
				<select name="view_option" class="form-control" id="view_option">
				<option value="Portrait">Portrait</option>
				<option value="Landscape">Landscape</option>
				</select>
			</div>
		    <div class="form-group col-sm-6" >		
			</div>			
		</div>
  </div>



  <div class="create-np">
  	<div class="row">
	 <div class="col-sm-12 pb-1">
	  <h1 class="main-heading"> Choisir opération </h1>
	 </div>
	</div>
    <div class="row fieldrow">
		<div class="form-group fieldcolumn">
			<label for="uname">Choisir image 1:</label>
			<select name="mu_images" class="form-control selectpicker" id="choose-img">
				<option value="">Select...</option>
				<?php $__currentLoopData = $files1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if($value != "." && $value != ".."): ?>
					<?php $explode=explode('.', $value);?>
						<option value="/public/uploads/flyer/1/<?php echo e($value); ?>" data-content="<span class='option-ttl'><?php echo e($explode[0]); ?></span><img src='/public/uploads/flyer/1/<?php echo e($value); ?>' style='width:50px'>"><?php echo e($value); ?></option>
					<?php endif; ?>
			   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		  </select>
		</div>
		<div class="form-group fieldcolumn" id="scnd-img-dd" style="display: block;">
			<label for="uname">Choisir Image 2:</label>
			<select name="mu_images" class="form-control selectpicker" id="choose-img2">
				<option value="">Select...</option>
				<?php $__currentLoopData = $files2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if($value != "." && $value != ".."): ?>
					<?php $explode=explode('.', $value);?>
						<option value="/public/uploads/flyer/2/<?php echo e($value); ?>" data-content="<span class='option-ttl'><?php echo e($explode[0]); ?></span><img src='/public/uploads/flyer/2/<?php echo e($value); ?>' style='width:50px'>"><?php echo e($value); ?></option>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</select>
		</div>

		<div class="form-group fieldcolumn">
			<label for="uname">Position de l'image</label>
			<select name="img_pos" class="form-control" id="offr_img_pos">
				<option value="right">Right</option>
				<option value="left">Left</option>
		  </select>
		</div>
    </div>
</div>


	<div class="row btn-row ">
		<div class="form-group col-sm-12 flex-btn" >
			<button type="submit" name="submit" value="search" class="btn btn-primary" id="update_product">Mettre à jour </button>
			<a class="btn" id="btn-Convert-Html2Image" href="javascript:void(0);"><i class="fa fa-download"></i>Télécharger</a>
			<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal1" style="margin-top: 10px;margin-left: 5px;">Sauvegarder</button>
		</div>
	</div>
<!--/div-->
</div>


	<div class="col-md-6">
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="form-group col-sm-10" style="display:none" id="preview">
				<h3 style="font-weight:400">Preview :</h3>
				<div id="html-content-holder" ></div>
				<div id="previewImage"> </div>
			</div>
			<div class="col-sm-1"></div>
			<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $final_res): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="pre-content"><span>Welcome <?php echo e($final_res->first_name); ?> <?php echo e($final_res->last_name); ?></span></div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
		<div class="preview-loader"><span>Loading...</span></div>
	</div>
	</div>
   <?php } ?>

</div>

    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">


<style>
.containerrow {
    display: flex;
    flex-wrap: wrap;
}
	.preview-loader{
		display:none;
		position: absolute;
		left: -25px;
		background: #ccc;
		width: 100%;
		height: 100%;
		top: 0;
		width: 57.5vw;
	}
	.preview-loader span{
		display: block;
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%);
		color:#000;
		font-size: 25px;
	}
	#previewImage canvas {
		max-height: 800px;
	}
	.left-cont .row {
    height: auto;
}
	.pre-content{
		width: 57.5vw;
		min-height: 100%;
		background: #ccc;
		padding: 39px;
		color:#000;
		font-size: 30px;
		text-transform: capitalize;
		font-weight: 300;
		position: absolute;
		top: 0;
		left: -25px;
	}
	.pre-content span{
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%);
	}
	.container2 {
		width: 100%;
	   /*  display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-wrap: wrap;
			flex-wrap: wrap; */
	}
	.containerrow {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
}
	#preview {
	  /*  margin-top: 60px;*/
		margin-left: -80px;
	}

	#myModal {
		padding-right: 45%;
	}
	.text,.filter-option-inner-inner {

		display: -webkit-box !important;

		display: -ms-flexbox !important;

		display: flex !important;
		width: 100%;
		-webkit-box-pack: justify;
			-ms-flex-pack: justify;
				justify-content: space-between;
		-ms-flex-wrap: wrap;
			flex-wrap: wrap;

	}
	.option-ttl {

		margin-top: 2%;

	}
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script>
$(document).ready(function(){


  /* $("#choose-img").change(function(){
       
    var img=$(this).val();
    if(img!=''){
		
      $('#scnd-img-dd').show();
    }
    else{
		 
     $('#scnd-img-dd').hide();
    }
  }); */

    $("#product").change(function(){

		$('#saved_template').val('');
		var product = $(this).val();
		var oldprice = $('#oldprice').val();
		var offertext = $('#offertext').val();
		var _token =  $('input[name=_token]').val();
		$.ajax({
			method: "POST",
			url: "/flyer-ajax",
			data: { product: product,action:'show_detail',_token:_token,oldprice:oldprice,offertext:offertext}
			,beforeSend: function(){
				$(".preview-loader").fadeIn("fast");
				setTimeout(function(){
					$(".pre-content").css("visibility","hidden");
				}, 2000);
				$('#loader').show();
			}
			,success: function( data ) {
			   //alert(msg.redirect);
			  setTimeout(function(){ $(".preview-loader").fadeOut("slow"); }, 2000);

			  if(data.redirect){

				location.reload();
			  }
			  else{
				  //console.log(data);

				$("#html-content-holder").html(data);
				$('#loader').hide();
				//Seto old value and create canvas
				//var paper_size= $('#paper_size').val();
				$("#paper_size").trigger('change');
				//Set paper size
				//paperformat_size(paper_size);
				// Set Canvas
				//canvas_function();
			  }
			}

		}) ;
	}) ;

//When Change paper format from dropdown


var paper_size= $('#paper_size').val('a4');
$("#paper_size").change(function(){
  var offr_img=$('#choose-img').val();
 var offr_img2=$('#choose-img2').val();
 var offr_img_class=$('#offr_img_pos').val();

  var paper_size= $('#paper_size').val();
  //Get Old value from Template
  var template_main_title = $('#template_main_title').text();
  var new_price = $('#new_price').data('new_price');
  var new_price_dec = $('#new_price_dec').text();
  var product_name1 = $('#product_name1').text();
  var image_name = $('#image_name').attr('src');
  var old_price = $('#old_price').text();
  var oldprice = $('#oldprice').val();
  var offertext = $('#offertext').val();

  //console.log(oldprice)
  $('#loader').show();
  var _token =  $('input[name=_token]').val();

  // Ajax for update template value paper size .
  $.ajax({
    method: "POST",
    url: "/flyer-ajax",
    data: { template_main_title: template_main_title,new_price:new_price,new_price_dec:new_price_dec,product_name1:product_name1,image_name:image_name,paper_size:paper_size,old_price:old_price,oldprice:oldprice,action:'a4multi',_token:_token,'offr_img':offr_img,'offr_img2':offr_img2,'offr_img_class':offr_img_class,offertext:offertext},
	beforeSend: function(){
		$(".preview-loader").fadeIn("fast");

	},
	success: function( msg ){
      $("#html-content-holder").html(msg);
      $('#loader').hide();
      //Seto old value and create canvas
      var paper_size= $('#paper_size').val();
      if(paper_size=='a5'){
       $('.productdetails').removeClass('productdetailsA4');
      }
      //Set paper size
      paperformat_size(paper_size);   
      // Set Canvas
	  setTimeout(function(){ $(".preview-loader").fadeOut("slow"); }, 2000);
      canvas_function();
    }
});
});

/* ********************************
*
Save product information as template
*
* ******************************** */

  $('#saved_template').change(function(){
    var id=$(this).val();
     var _token =  $('input[name=_token]').val();
        // Ajax to get product template
    $.ajax({
      method: "POST",
      url: "/get-flyer-template",
      data: { _token:_token,id:id}
    }).done(function(response) {
       $('#product').val(response.data.product_id);
      $('#update_box').show();
      $('#image_name').attr('src',response.data.image);
      $('#main_title').val(response.data.title);
      $('#product_name').val(response.data.name);
      $('#product_price').val(response.data.price);
      $('#paper_size').val(response.data.size);
      $('#offr_img_pos').val(response.data.image_pos);
      $('#choose-img').val(response.data.image1);
       $('#choose-img2').val(response.data.image2);
    $('#update_product').trigger('click');
        });
  });



/* ********************************
*
Save product information as template
*
* ******************************** */
$('#save-flyer-template').click(function(){
$('#temp_err').html('');
$('#succ_msg').html('');
var template_name=$('#flyer_template').val();
if(template_name==''){
  $('#temp_err').html('Please type the Template name.')
return false;
}else{



  var prod_id=$('#product').val();
var offr_img2='';
    var offr_img=$('#choose-img').val();
   var offr_img2=$('#choose-img2').val();
 var offr_img_class=$('#offr_img_pos').val();
      // Get New value from form fields
    var product_name = $('#product_name').val();
    var product_price = $('#product_price').val();
    var main_title = $('#main_title').val();

    //Get old vlaue from template
    var image_name= $('#image_name').attr('src');
    var old_price= $('#old_price').text();
    var oldprice= $('#oldprice').val();

    // if set name in field
    if(main_title!=''){
      $('.template_main_title').html(main_title);
    }else{
      // old value from template
      main_title= $('#template_main_title').text();
    }

    // Prduct name update
    if(product_name!=''){
      $('.product_name1').html(product_name);
    }else{
      // old value from template
      product_name= $('#product_name1').text();
    }
    if(product_price!=''){
      $('.new_price').html(product_price);
    }else{
      new_price= $('#new_price').data('new_price');
      new_price_dec= $('#new_price_dec').text();
      var arr_new = new_price_dec.split('€');
      product_price = new_price+'.'+arr_new[1];
    }
     //price validation
     var regex = /^\d*(\.\d{2})?$/;
     if(product_price!=''){
     if(regex.test(product_price)){
      }else{
      $('#price_error').html('Please enter correct price format');
      return false;
      }
      }
    var paper_size= $('#paper_size').val();
    var _token =  $('input[name=_token]').val();
        // Ajax to get product template
    $.ajax({
      method: "POST",
      url: "/save-flyer-template",
      data: { template_name:template_name,product_id:prod_id,product_name: product_name,product_price:product_price,image_name:image_name,old_price:old_price,oldprice:oldprice,main_title:main_title,paper_size:paper_size,_token:_token,'offr_img':offr_img,'offr_img2':offr_img2,'offr_img_class':offr_img_class}
    }).done(function( ) {

   $('#flyer_template').val('');
$('#succ_msg').html('The template has been saved.');
setTimeout(function(){  $('#myModal1').modal('toggle');$('#succ_msg').html(''); }, 3000);
        });
  }
    })



/* ********************************
*
Update product information
*
* ******************************** */
$('#update_product').click(function(){

var offr_img2='';
    var offr_img=$('#choose-img').val();
   var offr_img2=$('#choose-img2').val();
 var offr_img_class=$('#offr_img_pos').val();
      // Get New value from form fields
    var product_name = $('#product_name').val();
    var product_price = $('#product_price').val();
    var main_title = $('#main_title').val();

    //Get old vlaue from template
    var image_name= $('#image_name').attr('src');
	
    var old_price= $('#old_price').text();
    var oldprice= $('#oldprice').val();
    var offertext= $('#offertext').val();
    
    // if set name in field
    if(main_title!=''){
      $('.template_main_title').html(main_title);
    }else{
      // old value from template
      main_title= $('#template_main_title').text();
    }

    // Prduct name update
    if(product_name!=''){
      $('.product_name1').html(product_name);
    }else{
      // old value from template
      product_name= $('#product_name1').text();
    }
    if(product_price!=''){
      $('.new_price').html(product_price);
    }else{
      new_price= $('#new_price').data('new_price');
      new_price_dec= $('#new_price_dec').text();
      var arr_new = new_price_dec.split('€');
      product_price = new_price+'.'+arr_new[1];
    }
     //price validation
     var regex = /^\d*(\.\d{2})?$/;
     if(product_price!=''){
     if(regex.test(product_price)){
      }else{
      $('#price_error').html('Please enter correct price format');
      return false;
      }
      }
    var paper_size= $('#paper_size').val();
    var _token =  $('input[name=_token]').val();
        // Ajax to get product template
    $.ajax({
      method: "POST",
      url: "flyer-ajax",
      data: { product_name: product_name,product_price:product_price,image_name:image_name,old_price:old_price,oldprice:oldprice,main_title:main_title,paper_size:paper_size,action:'update_product',_token:_token,'offr_img':offr_img,'offr_img2':offr_img2,'offr_img_class':offr_img_class,'offertext':offertext}
    }).done(function( msg ) {

        $("#html-content-holder").html(msg);
        if(offr_img && offr_img2) {
          $('.productdetails, .multibox').addClass('two-offers');
        }
        if(offr_img && !offr_img2) {
          $('.productdetails, .multibox').addClass('one-offers');
        }
        
        $('#loader').hide();
        //Seto old value and create canvas

        // Paper size
        var paper_size= $('#paper_size').val();
        //alert(paper_size)
        paperformat_size(paper_size);
        canvas_function();

        });
    })

/* ********************************
Save Token
* ******************************** */
$('#token_save').click(function(){
   var _token =  $('input[name=_token]').val();
     var token_value = $('#token_value').val();
    $.ajax({
      method: "POST",
      url: "/flyer-ajax",
      data: { token: token_value,action:'token_save',_token:_token}
    }).done(function( token ) {
           location.reload();
        });
});

  // Chnage paprer size image setting
  function paperformat_size(paper_size){
      // paper size is a4
     // alert(paper_size);
     if(paper_size=='a4'){

           //  Main Title
           $('.productdetails').addClass('productdetailsA4');
           var MAIN_TITLE = $('.template_main_title').text();
           var FONT_SIZE_MAIN_TITLE = '50px';
            // if(MAIN_TITLE.length< 16){
              // FONT_SIZE_MAIN_TITLE = '70px';
            // }else if(MAIN_TITLE.length >16 && MAIN_TITLE.length < 20){
              // FONT_SIZE_MAIN_TITLE = '60px';
            // }
            // else if(MAIN_TITLE.length >20 && MAIN_TITLE.length < 30){
              // FONT_SIZE_MAIN_TITLE = '55px';
            // }
            // else if(MAIN_TITLE.length >30 && MAIN_TITLE.length < 70){
              // FONT_SIZE_MAIN_TITLE = '45px';
            // }

           // PRODUCT_NMAE
           var PRODUCT_NAME = $('.product_name1').text();
           var FONT_SIZE_PRODUCT_NAME = '30px';
           // if(PRODUCT_NAME.length >40 && PRODUCT_NAME.length <60){
              // FONT_SIZE_PRODUCT_NAME = '25px';
            // }else if(PRODUCT_NAME.length >60){
              // FONT_SIZE_PRODUCT_NAME = '20px';
            // }

           // Font Size
            var offr_temp = $('.new_price').data('temp');
            var NEW_PRICE = $('.new_price').data('new_price');
            price_length = NEW_PRICE.toString().length;
            if(offr_temp=='offer'){
               if(price_length===1)
               {
            font_size ='30vw';
             h3width ='45%';
             h3top='50px';
           }
           else if(price_length ===2)
           {
            font_size ='28vw';
             h3width ='75%';
             h3top='50px';
          }
           else if(price_length===3)
           {
             h3width ='60%';
            font_size ='22vw';
            h3top='100px';
          }
           else if(price_length===4)
           {
             h3width ='60%';
            font_size ='16vw';
            h3top='100px';
 }
            $('#html-content-holder').css('height','1100px');
            $('#html-content-holder').css('width','794px');
            $('.template_main_title').css('font-size','50px');
            $('.product_name1').css('font-size',FONT_SIZE_PRODUCT_NAME);
            $('.new_price').css('font-size',font_size);
            $('.price_h3').css('width',h3width);
             $('.price_h3').css('top',h3top);
            }else{
           if(price_length===1)
            font_size ='28vw';
           else if(price_length ===2)
            font_size ='26vw';
           else if(price_length===3)
            font_size ='20vw';
           else if(price_length===4)
            font_size ='16vw';

            $('#html-content-holder').css('height','1123px');
            $('#html-content-holder').css('width','794px');
            $('.template_main_title').css('font-size',FONT_SIZE_MAIN_TITLE);
            $('.product_name1').css('font-size',FONT_SIZE_PRODUCT_NAME);
            $('.new_price').css('font-size',font_size);
          }

            //-------------Template view------------------//
		
		   var view_option = $('#view_option').val();
		   if(view_option=='Landscape'){
		  	  $('.productdetails').addClass('landscapeA4');

					if(price_length===1)
					font_size ='24vw';
					else if(price_length ===2)
					font_size ='20vw';
					else if(price_length===3)
					font_size ='12vw';
					else if(price_length===4)
					font_size ='9vw';

					var FONT_SIZE_PRODUCT_NAME_LANDSCAPE = '30px';
					$('#html-content-holder').css('height','500px');
					$('#html-content-holder').css('width','1050px');
					$('.product_name1').css('font-size',FONT_SIZE_PRODUCT_NAME_LANDSCAPE);
					$('.new_price').css('font-size',font_size);
		   }else{
		  	  $('.productdetails').addClass('portraitA4');	
		   }

       }

             if(paper_size=='a4multi'){

				$('#html-content-holder').css('height','1000px');
				$('#html-content-holder').css('width','1148px');
       }


     // paper size is a5
     if(paper_size=='a5'){
         //  Main Title
         var MAIN_TITLE = $('.template_main_title').text();

         $('.productdetails').removeClass('productdetailsA4');
         //Length
         title_count = MAIN_TITLE.length;
         var FONT_SIZE_MAIN_TITLE = '30px';
          // if(title_count < 16){
            // FONT_SIZE_MAIN_TITLE = '65px';
          // }else if(title_count >16 && title_count< 20){
            // FONT_SIZE_MAIN_TITLE = '60px';
          // }
          // else if(title_count >20 && title_count< 30){
            // FONT_SIZE_MAIN_TITLE = '55px';
          // }
          // else if(title_count >30 && title_count < 70){
            // FONT_SIZE_MAIN_TITLE = '45px';
          // }

         // PRODUCT_NMAE
         var PRODUCT_NAME = $('.product_name1').text();
        //product name length
          product_length =  PRODUCT_NAME.length;
         var FONT_SIZE_PRODUCT_NAME = '25px';
         // if(product_length >40 && product_length <60){
            // FONT_SIZE_PRODUCT_NAME = '18px';
          // }else if(product_length >60){
            // FONT_SIZE_PRODUCT_NAME = '15px';
          // }
          var offr_temp = $('.new_price').data('temp');
         // Font Size
        var NEW_PRICE = $('.new_price').data('new_price');
            price_length = NEW_PRICE.toString().length;
        if(offr_temp=='offer'){
            $('.productdetails').addClass('productdetails_A5');
               if(price_length===1)
               {
            font_size ='20vw';
             h3width ='50%';
             h3top='530px';
           }
           else if(price_length ===2)
           {
            font_size ='16vw';
             h3width ='5%';
             h3top='530px';
          }
           else if(price_length===3)
           {
             h3width ='5%';
            font_size ='12vw';
            h3top='600px';
          }
           else if(price_length===4)
           {
             h3width ='5%';
            font_size ='8vw';
            h3top='600px';

 }
            $('#html-content-holder').css('height','930px');
            $('#html-content-holder').css('width','559px');
            $('.template_main_title').css('font-size','30px');
            $('.product_name1').css('font-size',FONT_SIZE_PRODUCT_NAME);
            $('.new_price').css('font-size',font_size);
            $('.price_h3').css('text-align','center');
            $('.price_h3').css('width',h3width);
              //$('.price_h3').css('height','761px');
             $('.price_h3').css('top',h3top);
            }else{
         if(price_length===1)
          font_size ='20vw';
         else if(price_length ===2)
          font_size ='18vw';
         else if(price_length===3)
          font_size ='15vw';
         else if(price_length===4)
          font_size ='12vw';

                  //set image height width
         // $('#html-content-holder').css('height','794px');
          $('#html-content-holder').css('height','930px');
          $('#html-content-holder').css('width','559px');
          $('.template_main_title').css('font-size',FONT_SIZE_MAIN_TITLE);
          $('.product_name1').css('font-size',FONT_SIZE_PRODUCT_NAME);
          $('.new_price').css('font-size',font_size);
        }
     }

   }

// Set Canvas
 function canvas_function (){
    $("#previewImage").html('');
    $("#html-content-holder").show();
    $("#update_box").css('display','block');
    $("#preview").css('display','block');
    //old value from template
    new_price = $('#new_price').data('new_price');
    new_price_dec = $('#new_price_dec').text();
    main_title = $('#template_main_title').text();
    var arr_new = new_price_dec.split('€');
	//console.log(arr_new);
    product_price = new_price+'.'+arr_new[1];
    product_name= $('#product_name1').text();
    $('#product_name').val(product_name);
    $('#product_price').val(product_price);
    $('#main_title').val(main_title);
    $(".o_price .strike").css("transform","skewY(10deg)");
    //Convert html to canvas
    var element = $("#html-content-holder");
     var getCanvas; // global variable
     html2canvas(element, {
        onrendered: function (canvas) {
         $("#previewImage").append(canvas);
          getCanvas = canvas;
          $("#html-content-holder").hide();
         },
		allowTaint: false,
		useCORS: false,
		removeContainer: false
     });
    //Download Image
    $("#btn-Convert-Html2Image").on('click', function () {
      //var image_path = $('#image_name').attr('src');
      var product_name = $('#product_name1').text();
      var imgageData = getCanvas.toDataURL("image/jpg");
      // Now browser starts downloading it instead of just showing it
      var newData = imgageData.replace(/^data:image\/jpg/, "data:application/octet-stream");

      $("#btn-Convert-Html2Image").attr("download", product_name+".jpg").attr("href", newData);
      /* $.ajax({
          method: "POST",
          url: "ajax.php",
          data: {action:'delete_img',image_path :image_path}
        }).done(function( msg ) {}) */
    });

   }

      
});
</script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.flyer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>