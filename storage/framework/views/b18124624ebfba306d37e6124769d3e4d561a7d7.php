
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container full-height">
    <div class="row h-100 align-items-center">
  <div class="col-md-12"> 
<div class="login-form">
        
        <div class="form_container">
          <div class="form-title">Reset Password</div>
             <?php if(Session::has('error')): ?>
          <span class="error">
                    <?php echo e(Session::get('error')); ?>

            </span>
             
                <?php endif; ?>
  <?php if(!$flag): ?>
          <form method="POST" id="payment-form"  action="<?php echo e(url('/save-reset-password')); ?>">
           
            <?php echo e(csrf_field()); ?>

          
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" name="password" class="form-control input_pass" value="" placeholder="password">
               <span class="error"> <?php echo e($errors->first('password')); ?></span>
            </div>
              <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="password"  name="password_confirmation" class="form-control input_user" value="" placeholder="Confirm password">
               <span class="error"> <?php echo e($errors->first('password_confirmation')); ?></span>
            </div>
            <?php if($token !='' ): ?>
                <input name="password_token"  value="<?php echo e($token); ?>" type="hidden">
                <?php endif; ?>
                <input name="email"  value="<?php echo e($email); ?>" type="hidden">
            <input type="Submit" name="button" class="btn login_btn" value="Submit">
          </form>
           <?php else: ?>
             <h3 style="color:red"> Your Link has been expired. </h3> 

             <?php endif; ?>
        </div>
        
        
      </div>
      </div>      
      </div>      
    </div>  
      <?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>