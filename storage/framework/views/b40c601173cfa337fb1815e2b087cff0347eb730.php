<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>

    <!-- Sidebar Menu -->
    
    <ul class="sidebar-menu"> 
      <!-- Optionally, you can add icons to the links -->
      <!-- <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li> -->
      <li><a href="<?php echo e(url('admin/email')); ?>"><i class="fa fa-envelope"></i> <span>Email Template</span></a></li>
      <li><a href="<?php echo e(url('admin/promocodes')); ?>"><i class="fa fa-ticket"></i> <span>Promo Codes</span></a></li>
    </ul> 
  </section>
  <!-- /.sidebar -->
</aside>
