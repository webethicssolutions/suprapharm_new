<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  
        <!-- Main content -->
        <section class="content">
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body">
							<?php if(Session::has('success')): ?>
							 <div class="success-msg">
								<?php echo e(Session::get('success')); ?>

							 </div>
							 <?php endif; ?>
							  <?php if(Session::has('error')): ?>
								   <div class="success-msg">
									  <?php echo e(Session::get('error')); ?>

							    	</div>
							 <?php endif; ?>

							 <div class="msg_sec"></div>

							   <div class="dtable_custom_controls">
								 <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
								  <tbody><tr>
									<td><a href="<?php echo e(url('admin/promo/add')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Add Promo Code</a></td>
									
								  </tr>
								  </tbody>
								</table>
							  </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th>Sr.No</th>
									<th>Coupon Name</th>
									<th>Percent Off</th>
									<th>Amount Off</th>
									<th>Duration</th>   
									<th>Max Redumption</th>   
									<th>Redeem By</th>   
									<th>Duration in months</th>   
									<th>Discount</th>   
									<th>Created On</th>   
									<th>Action</th>
								  </tr>
								  </thead>
									<?php $i=1; ?>
								  <?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								  <tr> 
									<td><?php echo $i; ?></td>
									<td> <?php echo e($data->couponName); ?></td>
									<td> <?php echo e($data->percent_off); ?></td> 
									<td> <?php echo e($data->amount_off); ?></td> 
									<td> <?php echo e(ucwords($data->duration)); ?></td> 
									<td> <?php echo e($data->max_redemptions); ?></td> 
									<td> <?php echo e(date('d F, y h:i:s',$data->redeem_by)); ?></td> 
									<td> <?php echo e($data->duration_in_months); ?></td> 
									<td> <?php echo e(ucwords($data->discount)); ?></td> 
									<td> <?php echo e(date('d F, y h:i:s',strtotime($data->created))); ?></td> 
									<td class="action_links"> 
										<!--a href ="<?php echo e(url('admin/promo/edit')); ?>/<?php echo e($data->id); ?>" title="Edit"><i class="fa fa-pencil"></i>Edit</a--->
										<a href ="javascript:void(0)"  data-target="#myModal" onclick="confirm_delete('<?php echo e(url('admin/delete/promo')); ?>/<?php echo e($data->id); ?>')" title="Delete"><i class="fa fa-trash-o"></i>Delete</a>
									</td>
								  </tr>
									<?php  $i++; ?>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    </div>
	<!-- Modal -->
<div class="modal fade confirm_del" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>