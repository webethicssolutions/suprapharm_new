<!-- Global site tag (gtag.js) - Google Analytics -->

<footer>
<?php   
$currentAction = \Route::currentRouteAction();
list($controller, $method) = explode('@', $currentAction);
$method = preg_replace('/.*\\\/', '', $method);  
if($method != "index"){ ?>
	<style>
	.eawc-root-layout-component {
		display: none;
	}
	</style>
<?php } ?> 
	<style>
	
	/*.eawc-user-image-image.jsx-3478511027 {
    background-image: url("https://pbs.twimg.com/profile_images/934096266669314048/D7h2tl-Z.jpg") !important;
}*/
	</style>
<div class="container">
<div class="row align-items-end">
<div class="col-lg-4 col-md-6">
		<div class="footer-column">
			<h2 class="footer-main-title">Company</h2>
			<ul>
				<li><a href="<?php echo e(url('/privacy')); ?>">Privacy policy</a>
				</li>
				<li><a href="<?php echo e(url('/terms')); ?>">Terms & conditions</a>
				</li>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-lg-4 col-md-6">
		<div class="footer-column lgoo pricing-table-sec">
			

			<h2 class="footer-main-title">Our Contacts</h2>
			<p><a href="#" target="_blank">9 Lorem Ipsum, Lorem Ipsum<br></a>
			Tel. <a href="tel:1234 567 8900">1234 567 8900</a>
			<a href="mailto:info@sharepharm.com"> info@info@sharepharm.com</a></p>
		</div>
	
	</div>
	<div class="col-lg-4 col-md-6">
		<div class="footer-column">
			<h2 class="footer-main-title">Contact us</h2>
			<div class="btn-wrap text-center"><a class="btn" href="#">Contact us</a></div>
			<br>
		</div>
	</div>
</div>
<div class="row footer-bottom">
	<div class="col">
		<p class="copyright-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		<p class="copyright-text">
			© Copyright 2019. All Rights Reserved
		</p>
	</div>
</div>
</div>		
</footer>