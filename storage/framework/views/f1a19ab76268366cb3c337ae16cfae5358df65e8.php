<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143249426-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-143249426-1');
</script>
<footer>
<?php   
$currentAction = \Route::currentRouteAction();
list($controller, $method) = explode('@', $currentAction);
$method = preg_replace('/.*\\\/', '', $method);  
if($method != "index"){ ?>
	<style>
	.eawc-root-layout-component {
		display: none;
	}
	</style>
<?php } ?> 
	<style>
	
	/*.eawc-user-image-image.jsx-3478511027 {
    background-image: url("https://pbs.twimg.com/profile_images/934096266669314048/D7h2tl-Z.jpg") !important;
}*/
	</style>
<div class="container">
<div class="row align-items-end">
<div class="col-lg-4 col-md-6">
		<div class="footer-column">
			<h2 class="footer-main-title">Company</h2>
			<ul>
				<li><a href="<?php echo e(url('/privacy')); ?>">Privacy policy</a>
				</li>
				<li><a href="<?php echo e(url('/terms')); ?>">Terms & conditions</a>
				</li>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-lg-4 col-md-6">
		<div class="footer-column lgoo pricing-table-sec">
			<img src="<?php echo e(url('resources/assets/frontend/images/img-3.png')); ?>" width="125px" height="100px">
			<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=qI7P1cAQprQIQ6AiYEDscgisXMqjOzhRkl0WnX6H2dYOyO3qeMUplCiF7PNh"></script></span>
		</div>
	</div>
	<div class="col-lg-4 col-md-6">
		<div class="footer-column">
			<h2 class="footer-main-title">Contact us</h2>
			<div class="btn-wrap text-center"><a class="btn" href="#">Contact us</a></div>
			<br>
		</div>
	</div>
</div>
<div class="row footer-bottom">
	<div class="col">
		<p class="copyright-text">Not endorsed or certified by Instagram. All Instagram TM logos and trademarks displayed on this page are property of Instagram Inc.</p>
		<p class="copyright-text">
			© Copyright 2019. All Rights Reserved
		</p>
	</div>
</div>
</div>		
</footer>