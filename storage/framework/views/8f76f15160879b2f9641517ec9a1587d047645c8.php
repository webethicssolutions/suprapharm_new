
<?php $__env->startSection('pageTitle', 'Billing'); ?>
<?php $__env->startSection('pageDescription', 'This is Billing page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
      
        <li class="active"><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li class=""><a href="<?php echo e(url('account-settings')); ?>">Account settings</a></li>
       <!--  <li><a href="<?php echo e(url('help')); ?>">FAQ</a></li> -->
       
    </ul>
</div>
</div>

<div class="page container">

    <!-- <div id="content-payment-methods" class="content content-settings">

        <div class="panel panel-pad">

            <h1>Billing</h1>
            
            <div class="panel-content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Payment method</th>
                            <th>Cardholder name</th>
                            <th>Expires</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="payment-method">
                                <span class="cc-number">
                                    <img class="cc-brand" src="<?php echo e(url('resources/assets/frontend/images/card-mastercard.svg')); ?>">
                                    <span class="cc-mask">••••</span>
                                    <span class="cc-digits">9988</span>
                                    <span class="bullet"></span>
                                    <span class="cc-expiry">01 / 2021</span>
                                </span>
                            </td>
                            <td class="cc-name">
                                <span class="cc-name">Christian Gal</span>
                            </td>
                            <td class="actions">
                                <a href="#" class="btn remove-btn">
                                    <i class="fa fa-times" aria-hidden="true"></i> <span>Remove card</span>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        
    </div> -->

    <div id="content-transactions" class="content content-settings">

        <div class="panel panel-pad">

            <h1>Transaction history</h1>

            <div class="panel-content">
			<div class="table-responsive">
                <table class="table">
                    <thead class="">
                        <tr>
                            <th>Date</th>
                            <th>Username</th>
                            <th>Description</th>
                            <th>Payment method</th>
                            <th class="right">Total</th>
                        </tr>
                    </thead>
                    <tbody>



  <?php $__currentLoopData = $billing; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                          
                            <td class="trans-date">
                                <div class="date">
                                    
                                        <?php $date = explode(" ", $bill->created_on);
                                        $timestamp = strtotime($date[0]);
?>


                                 <a target="_blank" href="<?php echo e(url('pay_detail')); ?>/<?php echo e($bill->transaction_id); ?>"><?php echo e(date('M d, Y', $timestamp)); ?></a>
                                    
                                </div>
                            </td>
                            <td class="trans-sub ">
                                                           <?php echo e($bill->insta_username); ?>

                            </td>
                            <td class="trans-desc ">
                                                        
                            </td>
                            <td class="payment-method ">
                                                            <span class="cc-number">
                                                                <!-- <img class="cc-brand" src="<?php echo e(url('resources/assets/frontend/images/card-mastercard.svg')); ?>"> -->
                                                                <span class="cc-digits">Stripe</span>
                                                            </span>
                            </td>
                            <td class="trans-amount right">
                                <div class="total">
                                    <?php echo e($bill->amount); ?> <?php echo e($bill->currency_code); ?>

                                </div>

                            </td>
                        </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
            </div>

        </div>
    </div>
</div>       
<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>