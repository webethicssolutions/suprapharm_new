
<?php $__env->startSection('pageTitle', 'Login'); ?>
<?php $__env->startSection('content'); ?>
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo e(url('/admin')); ?>">Suprapharm Login</a>
        </div>
        <!--/.nav-collapse -->
      </div>
    </nav>
	<div class="login_area">
<div class="login-box">
  <div class="login-logo">
    <a href="#">Login</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
  	 <?php if(Session::has('error')): ?>
  			<p class="alert alert-error"><?php echo e(Session::get('error')); ?></p>
  	 <?php endif; ?>
	    <?php echo e(Form::open(array('url' => 'admin/checklogin', 'method' => 'post'))); ?>

      	   <div class="form-group has-feedback">
           		<?php echo e(Form::text('email', '',array('class'=>'form-control','placeholder'=>'Email'))); ?>

              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      		   <span class="error" >  <?php echo e($errors->first('email')); ?> </span>
            </div>
            <div class="form-group has-feedback">
               <?php echo e(Form::password('password',array('class'=>'form-control','placeholder'=>'Password'))); ?>

                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        		   <span class="error" >  <?php echo e($errors->first('password')); ?> </span>
            </div>
            <div class="row">
                      <div class="col-xs-12">
              			<?php echo e(Form::submit('Sign In',array('class'=>'btn btn-primary btn-block btn-flat'))); ?>

                      </div>
            </div>
	<?php echo e(Form::close()); ?>

  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>