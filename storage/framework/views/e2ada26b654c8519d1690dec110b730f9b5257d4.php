
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
define('PAYMENT_ENVIRONMENT','SANDOBX');
define('PAYPAL_URL','https://www.sandbox.paypal.com/cgi-bin/webscr');
define('PAYPAL_MERCHANT','dev.harshvardhan@gmail.com');


define('CURRENCY','USD');
define('PAYPAL_RETURN_URL','https://dev.webethics.online/autolove/return_url');
define('CANCEL_URL','https://dev.webethics.online/autolove/cancel_url');
define('PAYPAL_NOTIFICATION_URL','https://dev.webethics.online/autolove/notify_url');



?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
        <li class=""><a href="<?php echo e(url('account_settings')); ?>">Account settings</a></li>
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class="active"><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li><a href="<?php echo e(url('help')); ?>">Help center</a></li>
        <li class=""><a href="<?php echo e(url('insta')); ?>">Instagram feeds</a></li>
        <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
    </ul>
</div>
</div>

<div class="page container subscription-page">

    <div id="content-subs" class="content content-settings">

        <div class="panel panel-pad">

            <h1>Subscriptions</h1>

            <div class="panel-content">
                <table class="table">
                    <thead class="hidden-xs hidden-sm">
                        <tr>
                            <th>Instagram username</th>
                            <th>Subscription</th>
                            <th>Next billing date</th>
                          <!--   <th></th> -->
                        </tr>
                    </thead>
                    <tbody>
                          <?php $__currentLoopData = $subscription; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td class="user-sub active">
                                <div class="profile">
                                    <div class="avatar">
                                        <img src="<?php echo e(url('resources/assets/frontend/images/user-img.png')); ?>">
                                    </div>
                                    <div class="info">
                                        <a href="#" class="username"><?php echo e($sub->insta_username); ?></a>

                                                                            </div>
                                </div>
                            </td>
                            <td class="sub-info hidden-xs hidden-sm">
                                <div><em><?php echo e(App\Helpers\Helper::get_plan_like($sub->subscription_id)); ?></em> </div>
                                <div class="light"><?php echo e($sub->amount); ?> <?php echo e($sub->currency_code); ?></div>
                            </td>
                            <td class="billing-info hidden-xs hidden-sm">
                                <div><em><?php $date = explode(" ", $sub->created_on);
                                        $timestamp =  strtotime( "+1 month",strtotime($date[0]));
                                       
?>


                                    <?php echo e(date('M d, Y', $timestamp)); ?></em></div>
                                                            <div class="light">    <!-- <img src="<?php echo e(url('resources/assets/frontend/images/card-mastercard.svg')); ?>"> --><span>Paypal</span></div>
                                                                </td>
                           <!--  <td class="actions">
                                <a href="#" class="btn manage-btn">
                                    <i class="fa fa-cog" aria-hidden="true"></i> Manage</span>
                                </a>
                            </td> -->
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        
            <div class="add">
                <a class="btn add-acct" data-target="#addUserSubModal" data-toggle="modal">
                    <img src="<?php echo e(url('resources/assets/frontend/images/plus.svg')); ?>" width="12" height="12">
                    <span>Add account</span>
                </a>
            </div>
            
        </div>
    </div>

</div>

            
<div id="addUserSubModal" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon">
                        <img src="<?php echo e(url('resources/assets/frontend/images/gram.svg')); ?>">
                    </span>
                    <div class="title">Add new Instagram Account</div>
                    <p class="step_1">Type in Instagram username you want to add and proceed to selecting a subscription.</p>
                    <p class="step_2" style="display:none;">Please select your subscription.</p>
                </div>

                <div class="av-notify"></div>

                <form id="add_user" class="typeUnlock" method="post" action="<?php echo PAYPAL_URL;?>">
                     <input type="hidden" value="<?php echo e(Session::get('user_id')); ?>" name="uid" id=uid>
                         <input type="hidden" value="<?php echo PAYPAL_MERCHANT;?>" name="business">

  <input type="hidden" value="_xclick-subscriptions" name="cmd">

  <input type="hidden" value="1" name="no_note">

  <input type="hidden" value="1" name="no_hipping">

  <input type="hidden" value="currency_code" name="USD">

  <input type="hidden" value="country" name="IN">


<input type="hidden" value="item_name" name="sample Item">
<input type="hidden" value="" name="a3" id="price">
<input type="hidden" value="1" name="p3">
<input type="hidden" value='M' name='t3'>
<input type="hidden" value="1" name="src">
<input type="hidden" value="1" name="sra">
<input type="hidden" value="2" name="rm">

<input type="hidden" value="" name="custom" id="custom">

<input type="hidden" value="<?php echo PAYPAL_RETURN_URL;?>" name="return">
<input type="hidden" value="<?php echo CANCEL_URL;?>" name="cancel_return">
<input type="hidden" value="<?php echo PAYPAL_NOTIFICATION_URL;?>" name="notify_url">
<input type="hidden" value="1" name="upload">
<input type="hidden" value="0" name="address_verride">

      <!-- <?php echo e(Form::open(array('url' => '/paypal', 'method' => 'post'))); ?> -->
     <!--  <form class="w3-container w3-display-middle w3-card-4 " method="POST" id="payment-form"  action="/autolove/paypal"> -->
  <?php echo e(csrf_field()); ?>

                    <div class="input-group" id="sub_username">
                        <span class="icon icon-ig"></span>
                        <input id="add_username" type="text" name="iguser" class="form-control" placeholder="Instagram username" aria-placeholder="Instagram username">
                    </div>
                        <div class="input-group" id="sub_plans" style=display:none;>
                            <?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<label class="check mx">
            <input name="subscription" type="radio" value="<?php echo e($plan->id); ?>" data-p="<?php echo e($plan->amount); ?> " class="plan_rd">
            <span class="sub-info"><em><?php echo e($plan->post_like); ?></em> <?php if($plan->type=='L'): ?><?php echo e('Likes /Post'); ?><?php else: ?><?php echo e("Like+Followers"); ?><?php endif; ?> <span class="dull pull-right">$<?php echo e($plan->amount); ?> / mo</span></span>
        </label><br>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </div>

                    <div class="btn-group vertical">

                        <button class="btn btn-green btn-block" disabled="" id="btn-nxt">Next<span class="loading"></span></button>
                         <button class="btn btn-green btn-block"  disabled="" id="btn-nxt1" style="display:none;">Pay<span class="loading" ></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>