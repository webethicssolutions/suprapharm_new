
<?php $__env->startSection('pageTitle', 'User details'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  
        <!-- Main content -->
			<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                      <td><a href="<?php echo e(url('admin/user/adduser')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Add User</a></td>


                    </tr>
                    </tbody>
                  </table>
                </div>
        <section class="content">
        	<div class ="table-title">

              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body">
						<!--	<?php if(Session::has('success')): ?>
							 <div class="success-msg">
								<?php echo e(Session::get('success')); ?>

							 </div>
							 <?php endif; ?>
							  <?php if(Session::has('error')): ?>
								   <div class="success-msg">
									  <?php echo e(Session::get('error')); ?>

							    	</div>
							 <?php endif; ?>

							 <div class="msg_sec"></div>-->


							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
                  <th>Full Name</th>
                  <th>Username</th>
									<th>Email</th>

                  <th>Status</th>
                  <!--<th>Gender</th>
                  <th>Date of birth</th>
                  <th>Registred On</th>-->
                  <th>Action</th>
								  </tr>
								  </thead>

								  <?php $__currentLoopData = $user_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								  <tr>
                    <td><?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?></td>
									<td><?php

                     echo $user->username;
                   ?></td>
									<td><?php echo e($user->email); ?></td>


									<td><label class="switch">
											<?php if($user->status == "enable"): ?>
												<input class="usr_status" id="on_off" data-val="<?php echo e($user->id); ?>" checked="checked" type="checkbox">
											<?php else: ?>
												<input class="usr_status" id="on_off1" data-val="<?php echo e($user->id); ?>" type="checkbox">
											<?php endif; ?>
											<span class="slider round"></span>
										</label>
									</td>
                 <!--<td><?php echo e($user->gender); ?></td>
                  <td><?php echo e($user->dob); ?></td>
                  <td>
                  <?php $date = explode(" ", $user->modified_on);

                               $timestamp = strtotime($date[0]);
                        ?>
                           <?php echo e(date('d M Y', $timestamp)); ?> <?php echo e($date[1]); ?>

                  </td>-->
									<td class="action_links">
                   <!--  <a href ="javascript:void(0)" title="manage"><i class="fa fa-tasks"></i></a> -->
                   <!-- <a href ="<?php echo e(url('admin/user/manage')); ?>/<?php echo e($user->id); ?>" target="_blank" title="manage"><i class="fa fa-tasks"></i>Manage</a> -->
										<a href ="<?php echo e(url('admin/user/edit')); ?>/<?php echo e($user->id); ?>" data-toggle="tooltip" data-original-title="Edit" ><i class="fa fa-pencil"></i></a>
										<a href ="javascript:void(0)"  data-target="#myModal" data-toggle="tooltip" data-original-title="Delete" onclick="confirm_delete('<?php echo e(url('admin/deleteuser')); ?>/<?php echo e($user->id); ?>')" ><i class="fa fa-trash-o"></i></a>
										  <a href ="<?php echo e(url('admin/user/manage')); ?>/<?php echo e($user->id); ?>" data-toggle="tooltip" data-original-title="Manage User" target="_blank"><i class="fa fa-tasks"></i></a>  
                      <a href ="<?php echo e(url('admin/user/user-services')); ?>/<?php echo e($user->id); ?>" data-toggle="tooltip" data-original-title="User subscriptions" ><i class="fa fa-external-link" aria-hidden="true"></i></a>
									</td>
								  </tr>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    </div>
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<script type="text/javascript">
$(".usr_status").change(function(){

        $(".success-msg").hide();
        $(".erorr-msg").hide();
        if($(this).data('val')!=""){
            var u_id = $(this).data('val');
            var usr_tokn = $('input[name="_token"]').val();

            var ajax_url = "";
            var ustatus = "";
            if($(this).prop("checked") == true){
                ajax_url = "/admin/user/enableuser/"+u_id;
                ustatus = "Enabled";
            }else{
                ajax_url = "/admin/user/disableuser/"+u_id;
                ustatus = "Disabled";
            }

            $.ajax({
                type: "POST",
                url: ajax_url,
                data:{'_token':usr_tokn},
                success: function (response) {
                    console.log('response '+response);
                    if(response == "success"){
                        $(".msg_sec").html('<div class="success-msg">User Status '+ustatus+' Successfully.</div>')
                    }else{
                        $(".msg_sec").html('<div class="success-msg">Something went wrong.</div>')
                    }

                    $(".msg_sec").show();
                }
            });
        }
    });
</script>
<style>
#myModal {
    padding-right: 45%;
}
</style>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>