<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php $__currentLoopData = $pay_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div class="page container">
    <div class="pay-header">
    <h1>Autolove</h1>
</div>
    <div id="content-transactions" class="content content-settings">
        
        <div class="panel panel-pad">
            <h1>Transaction Detail:-</h1>
            <div class="panel-content table-responsive">
                <table class="table">
                    <thead class="">
                       <tr>
                        <td>
                            Customer<br>
                            <b><?php echo e($detail->username); ?><br>
                            <?php echo e($detail->email); ?></b>
                        </td>
                        <td></td>
                        <td>
                           <span>Payment method</span><br>    
                         <span><b>Stripe</b></span>
                               
                        </td>
                        <td class="right"><span>Transaction id</span><br>    
                         <span><b><?php echo e($detail->transaction_id); ?></b></span>
                               
                        </td>

                        </tr>
                        <tr>
                            <th>DATE</th>
                            <th>DESCRIPTION</th>
                            <th>SERVICE PERIOD</th>
                            <th class="right">AMOUNT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="trans-date">
                                <div class="date"> 
									<?php 
										$date = explode(" ", $detail->created_on);
										$timestamp = strtotime($date[0]); 
									?>

									<?php echo e(date('M d, Y', $timestamp)); ?> 
                                </div>
                            </td>
                            
                            <td class="trans-desc "><span>Autolove Subscription</span><br><?php echo e(App\Helpers\Helper::get_plan_like($detail->subscription_id)); ?> 
                                                        
                            </td>
                            <td><?php echo "From " .date('M d, Y', $timestamp)." - Till ".  date('M d, Y', strtotime('+1 month', $timestamp)); ?>

                            </td>
                            <td class="trans-amount right">
                                <div class="total">
                                    <?php echo e($detail->currency_code); ?> <?php echo e($detail->amount); ?>

									<?php if(isset($invoice_details['discount']['coupon'])): ?>
										 ( <?php echo e('Coupon Applied:'); ?> <?php echo e($invoice_details['discount']['coupon']['id']); ?>, 
									
										<?php if(isset($invoice_details['discount']['coupon']['amount_off'])): ?>
											<?php echo e('FLAT'); ?> <?php echo e($invoice_details['discount']['coupon']['amount_off']/100); ?> Discount)
										<?php endif; ?>
										
										<?php if(isset($invoice_details['discount']['coupon']['percent_off'])): ?>
											<?php echo e('FLAT'); ?> <?php echo e($invoice_details['discount']['coupon']['percent_off']); ?>% Discount)
										<?php endif; ?>
									<?php endif; ?>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td><b>Total Paid</b>
                            </td>
                             <td> </td>
                              <td> </td>
                            <td class="trans-amount right">
                                <div class="total"><b> 
									<?php if($invoice_details['amount_paid']): ?>
										 <?php echo e($invoice_details['amount_paid']/100); ?> <?php echo e($invoice_details['currency']); ?>

									<?php else: ?>	
										 <?php echo e($detail->amount); ?> <?php echo e($detail->currency_code); ?>

									<?php endif; ?>
                                </b></div>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>        
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>