<!-- Footer -->
       <footer id="footer">
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                            <div class="col-lg-4">
                                    <div class="widget">

                                            <div class="widget-title">Suprapharm</div>
                                                <p class="mb-5">Groupement de Pharmacies Françaises <br/><a href="#">Carte des Pharmacies</a><br>
                                    </div>
                        </div> <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-4">
                                        <div class="widget">
                                            <div class="widget-title">Contact</div>
                                            <ul class="list">
                                                    <li><a href="JavaScript:void(0);"><i class="fa fa-map-marker-alt"></i><span>108 rue Marius Aufan 92300 Levallois Perret</span></a></li>
                                                    <li><a href="tel:0155217076"><i class="fa fa-phone"></i><span>01 55 21 70 76</a></span></li>
                                                    <li><a href="mailto:contact@suprapharm.fr" target="_blank"><i class="fa fa-envelope"></i><span>contact@suprapharm.fr</span></a></li>
                                                </ul>
                                        </div>
                                    </div>

                                        <div class="col-lg-4">
                                                <div class="widget">
                                                    <div class="widget-title">Actualités</div>
                                                    <ul class="list">
                                                            <li><a href="<?php echo e(url('/about_us')); ?>">C’est quoi?</a></li>
                                                            <li> <a href="<?php echo e(url('/services')); ?>">Nos Services</a></li>
                                                            <li> <a href="<?php echo e(url('/cards-of-pharmacies')); ?>">Cartes des pharmacies</a></li>
                                                            <li><a href="<?php echo e(url('/contact-us')); ?>">Contact</a></li>
                                                           <!-- <li><a href="#">Widgets</a></li>
                                                            <li><a href="#">Footers</a></li>-->
                                                        </ul>
                                                </div>
                                            </div>

                                                <div class="col-lg-4">
                                                        <div class="widget">
                                                            <div class="widget-title">Instagram</div>
                                                            <ul class="list insta_pictures">
                                                                    <li><a href="#"><img src="<?php echo e(url('/assets/frontend/new/images/insta1.jpg')); ?>"></a></li>
                                                                    <li><a href="#"><img src="<?php echo e(url('/assets/frontend/new/images/insta2.jpg')); ?>"></a></li>
                                                                    <li><a href="#"><img src="<?php echo e(url('/assets/frontend/new/images/insta3.jpg')); ?>"></a></li>
																	<li><a href="#"><img src="<?php echo e(url('/assets/frontend/new/images/insta1.jpg')); ?>"></a></li>
                                                                    <li><a href="#"><img src="<?php echo e(url('/assets/frontend/new/images/insta2.jpg')); ?>"></a></li>
                                                                    <li><a href="#"><img src="<?php echo e(url('/assets/frontend/new/images/insta3.jpg')); ?>"></a></li>
                                                                </ul>
                                                        </div>
                                                    </div>
                            </div>
                        </div>


                </div>
                </div>
            </div>
            <div class="copyright-content">
                <div class="container">
                    <div class="copyright-text text-center">© Suprapharm 2018 | 2020</div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="fas fa-arrow-up"></i><i class="fas fa-arrow-up"></i></a>
    <!--Plugins-->
    <script src="<?php echo e(url('/assets/frontend/new/js/jquery.js')); ?>"></script>
    <script src="<?php echo e(url('/assets/frontend/new/js/plugins.js')); ?>"></script>

    <!--Template functions-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmByayAiEvKG4MdveNZZ2v5nEPbwFz6I8&language=en&libraries=places,geometry"></script>
    <!--<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">-->
    <script src="<?php echo e(url('/assets/frontend/new/js/functions.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('/assets/frontend/new/js/jquery.themepunch.tools.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('/assets/frontend/new/js/jquery.themepunch.revolution.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('/assets/frontend/new/js/revolution.extension.layeranimation.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('/assets/frontend/new/js/revolution.extension.parallax.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('/assets/frontend/new/js/revolution.extension.slideanims.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('/assets/frontend/new/js/slick.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('/assets/frontend/new/js/custom.js')); ?>"></script>
    <script type="text/javascript">
        var tpj = jQuery;

        var revapi21;
        tpj(document).ready(function() {
            if (tpj("#rev_slider_21_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_21_1");
            } else {
                revapi21 = tpj("#rev_slider_21_1").show().revolution({
					
					sliderLayout: 'fullwidth', 
                    jsFileLocation: "assets/frontend/new/js/plugins/revolution/js/",
                    dottedOverlay: "none",
					delay: 3000,
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1024, 1024, 778, 480],
                    gridheight: [400, 400, 600, 520], 
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 1000,
                        levels: [10, 6, 10, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51, 55],
                        type: "mouse",
                        disable_onmobile: "on"
                    },
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/
jQuery('.similar-slider').slick({
slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  arrow:false,
  dots:false,
  autoplaySpeed: 2000,


 	  responsive: [
		{
		  breakpoint: 1200,
		  settings: {
  slidesToShow: 1,
  slidesToScroll: 1,
		  }
		},
		{
		  breakpoint: 992,
		  settings: {
  slidesToShow: 2,
  slidesToScroll: 2,
		  }
		},
		{
		  breakpoint: 641,
		  settings: {
  slidesToShow: 1,
  slidesToScroll: 1,
    arrows:false,
 dots: true,
		  }
		}
	  ]


});
    </script>

</body>

</html>
