<?php echo $__env->make('frontend.common.header-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Body content -->
	<div class="pos-background relative print-container search-detail">
		<div class="bg-white">
			<div class="container">
				<div class="pos-background relative print-container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 pos-content-print">
							<h1 class="h2"><?php echo e($search_data->pharmacy); ?></h1> 
							<div class="pos-onscreen"></div>
							<div class="pos-content-phone-wrapper row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<address itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="pos-content-address">
										<div itemprop="streetAddress">
											<?php echo e($search_data->address); ?>

										</div>
										<div>
											<span itemprop="addressLocality"><?php echo e($search_data->city); ?></span>, <span itemprop="addressRegion"></span> <span itemprop="postalCode"><?php echo e($search_data->cp); ?></span>
										</div>
									</address>
									
									<button class="btn btn-primary pos-content-showphone hidden-print">Voir le n° de téléphone</button>
									
									<div class="pos-content-phone bold hide">
										<span class="pos-content-phone-label"><em class="fa fa-phone"></em></span>
										<span itemprop="telephone"><?php echo e($search_data->phone); ?> </span>
										<span class="pos-content-phone-comment small"></span>
									</div>
									<div class="pos-content-fax bold hide">
										<span class="pos-content-fax-label"><em class="fa fa-fax"></em></span>
										<span itemprop="faxNumber"><?php echo e($search_data->phone); ?></span>
									</div>
									<div class="pos-content-url small-margin-top bold">
										<a href="<?php echo e($search_data->url); ?>" id="lf-buy-online" target="_blank" class="btn btn-primary small-margin-bottom">
											<em class="fa fa-shopping-cart"></em>
											Acheter sur cette pharmacie en ligne
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 pos-content-print pos-content-print">
							<?php if($search_data->profile): ?>
								<img src="/images/admin/pharmacy/<?php echo e($search_data->profile); ?>" alt="<?php echo e($search_data->name); ?>"/>
							<?php else: ?>
								<img src="/assets/frontend/new/images/enseigne-suprapharm.jpg" alt="<?php echo e($search_data->name); ?>"/>
							<?php endif; ?>
						</div>
					</div>
					<?php 
						$days = unserialize($search_data->timing);
					 ?>
					<?php if($search_data->timing && count($days) > 0): ?>
					<div class="pos-hours" id="lf_openinghours">
						<div class="lf_openinghoursdays">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<h2 class="pos-hours-title title h4 margin-top">Horaires d'ouverture </h2>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

									<div class="hidden-print">
										<div class="title h5 title h5 text-right margin-top no-margin-top-xs small-margin-bottom">Votre magasin est <span class="pos-hours-closed color-red" style="display: none;">fermé</span><span class="pos-hours-open color-green">ouvert</span></div>
									</div>
								</div>
							</div>
							
							
							<ul class="pos-hours-list lf_days row row-no-padding">
								<li class="col-md-custom col-sm-3 col-xs-6 days <?php echo e((date('l') == 'Monday')? 'today' : ''); ?>">
									<ul>
										<li class="day text-center">lundi</li>
										<li class="time text-center">
											<!-- de 09:00 à 20:00 -->
											<div class="time-value">
												<?php echo e($days['monday']); ?>

											</div>
										</li>
									</ul>
								</li>

								<li class="col-md-custom col-sm-3 col-xs-6 days <?php echo e((date('l') == 'Tuesday')? 'today' : ''); ?>">
									<ul>
										<li class="day text-center">mardi</li>
										<li class="time text-center">
											<!-- de 09:00 à 20:00 -->
											<div class="time-value">
												<?php echo e($days['tuesday']); ?>

											</div>
										</li>
									</ul>
								</li>

								<li class="col-md-custom col-sm-3 col-xs-6 days <?php echo e((date('l') == 'Wednesday')? 'today' : ''); ?>">
									<ul>
										<li class="day text-center">mercredi</li>
										<li class="time text-center">
											<!-- de 09:00 à 20:00 -->
											<div class="time-value">
												<?php echo e($days['wednesday']); ?>

											</div>
										</li>
									</ul>
								</li>

								<li class="col-md-custom col-sm-3 col-xs-6 days <?php echo e((date('l') == 'Thursday')? 'today' : ''); ?>">
									<ul>
										<li class="day text-center">jeudi</li>
										<li class="time text-center">
											<!-- de 09:00 à 20:00 -->
											<div class="time-value">
												<?php echo e($days['thursday']); ?>

											</div>
										</li>
									</ul>
								</li>

								<li class="col-md-custom col-sm-3 col-xs-6 days <?php echo e((date('l') == 'Friday')? 'today' : ''); ?>">
									<ul>
										<li class="day text-center">vendredi</li>
										<li class="time text-center">
											<!-- de 09:00 à 20:00 -->
											<div class="time-value">
												<?php echo e($days['friday']); ?>

											</div>
										</li>
									</ul>
								</li>

								<li class="col-md-custom col-sm-3 col-xs-6 days <?php echo e((date('l') == 'Saturday')? 'today' : ''); ?>">
									<ul>
										<li class="day text-center">samedi</li>
										<li class="time text-center">
											<!-- de 09:00 à 20:00 -->
											<div class="time-value">
												<?php echo e($days['saturday']); ?>

											</div>
										</li>
									</ul>
								</li>

								<li class="col-md-custom col-sm-3 col-xs-6 days <?php echo e((date('l') == 'Sunday')? 'today' : ''); ?>">
									<ul>
										<li class="day text-center">dimanche</li>
										<li class="time text-center">
											<div class="time-value">
												<?php echo e($days['sunday']); ?>

											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="bg-primary">
		<div class="hidden-print">
			<div class="pos-content-tabs">
				<div class="container">
					<div class="row row-no-padding pos-content-tabs-row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2 class="title h4 pos-content-tabs-title col-sm-3 col-xs-5 active" data-id="tabs-presentation">Présentation</h2>
							<h2 class="title h4 pos-content-tabs-title col-sm-3 col-xs-5" data-id="tabs-acces">S'y rendre</h2>
							<div class="dropdown col-xs-2 visible-xs-block">
								<div class="title h4 pos-content-tabs-title dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="dropdownTabs" role="menu"><em class="fa fa-bars"></em></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-white">
		<div class="pos-scrolling" style="padding-top: 54px;">
			<?php if($search_data->description): ?>
			<div class="container hidden-print" id="tabs-presentation">
				<div class="pos-background">
					<div class="row pos-presentation">
						<div class="col-xs-12 pos-presentation-description vcenter">
							<h2 class="title h3 no-margin-top">Présentation</h2>
							<div class="text-justify">
							<?php echo $search_data->description; ?>

							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="container" id="tabs-acces">
				<div class="pos-background print-container">
					<div class="row pos-access">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2 class="title h3">S'y rendre</h2>
						</div>
						<div class="col-md-12 col-xs-12 pos-print-map">
							<div class="relative">
								<div id="lf_accessmap_canvas" class="pos-access-map" aria-hidden="true" style="position: relative; overflow: hidden;">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="map-lat" value="<?php echo e($search_data->latitude); ?>" />
<input type="hidden" id="map-lng" value="<?php echo e($search_data->longitude); ?>" />
<!-- End Body content -->
<!-- Footer -->
  <?php echo $__env->make('frontend.common.footernew', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- end: Footer -->


