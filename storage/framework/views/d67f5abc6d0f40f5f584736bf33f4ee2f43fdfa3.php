<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="INSPIRO" />
    <meta name="description" content="Themeforest Template Polo">
    <!-- Document title -->
    <title>Suprapharm <?php echo $__env->yieldContent('pageTitle'); ?></title>
    <!-- Stylesheets & Fonts --><link href="<?php echo e(url('assets/frontend/new/css/plugins.css')); ?>" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="icon" href="<?php echo e(url('assets/frontend/new/images/favicon-supra1.png')); ?>" type="image/gif" sizes="16x16">
    <link href="<?php echo e(url('assets/frontend/new/css/style.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('assets/frontend/new/css/responsive.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('assets/frontend/new/css/slick-theme.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('assets/frontend/new/css/slick.css')); ?>" rel="stylesheet">
	</head>
<body>


    <!-- Body Inner -->    <div class="body-inner">

		
        <!-- Header -->
        <header id="header" data-fullwidth="true" class="header_new">
			<div class="container">
				<div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
				</div>
				<div class="top-logo">
					<div class="button red thin">
						<a class="open-contact-us" data-pharmacist="1">Vous êtes pharmacien ?</a>
					</div>
					<div class="logo">
						<div>
							<a href="<?php echo e(url('/')); ?>">
								<img alt="logo pharmavie" src="<?php echo e(url('assets/frontend/new/images/logo-supra1.png')); ?>">
							</a>
						</div>
					</div>
					
					<div class="directions">
						<div class="voile"></div>
						<div class="links">
							<div>
								<a href="<?php echo e(url('/search')); ?>">Achetez en ligne</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="mainMenu" class="menubar">
				<div class="container">
					<div>
						<a href="<?php echo e(url('/vous-êtes-pharmacien/nos-services')); ?>">NOS SERVICES</a>
					</div>
					<div>
						<a href="<?php echo e(url('/vous-êtes-pharmacien/installation-help')); ?>">Aide à l installation</a>
					</div>
					<div>
						<a class="s1" href="<?php echo e(url('/admin/login')); ?>" target='_blank'><span>CONNEXION</span></a>
					</div>
				</div> 
			</div>
        </header>
		<!--Navigation-->
		<div id="mainMenu" class="light menuhide">
			<div class="container">
				<nav>
					<ul>
				
				
						<li> <a href='<?php echo e(url('/vous-êtes-pharmacien/nos-services')); ?>'>NOS SERVICES</a></li>
						<li> <a href='<?php echo e(url('/vous-êtes-pharmacien/installation-help')); ?>'>Aide à l'installation</a></li>      
						<li> <a class="s1" href='<?php echo e(url('/admin/login')); ?>' target='_blank'>CONNEXION</a></li>

					 


					</ul>
				</nav>
			</div>
		</div>
		<!--end: Navigation-->
                
        
        <!-- end: Header -->
