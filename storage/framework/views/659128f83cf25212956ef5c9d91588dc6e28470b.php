<?php $__env->startSection('pageTitle', 'Subscription'); ?>
<?php $__env->startSection('pageDescription', 'This is Subscription page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
     
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class="active"><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li class=""><a href="<?php echo e(url('account-settings')); ?>">Account settings</a></li>
        <li><a href="<?php echo e(url('help')); ?>">FAQ</a></li>
        
    </ul>
</div>
</div>

<div class="page container subscription-page">
    <div id="content-subs" class="content content-settings">
        <div class="panel panel-pad">

            <h1>Subscriptions</h1>

            <div class="panel-content">
			<div class="table-responsive">
                <table class="table">
                    <thead class="hidden-xs hidden-sm">
                        <tr>
                            <th>Instagram username</th>
                            <th>Subscription</th>
                            <th>Next billing date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1;?>
                          <?php $__currentLoopData = $subscription; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td class="user-sub <?php if($sub->status=='active'){echo 'active';}?>">
                                <div class="profile">
									<?php 
									  $next_billing_date = "";
									  if(isset($sub->next_billing_date)){
										$next_billing_date = $sub->next_billing_date;
										$date = $sub->next_billing_date;
									  }else{
										 $_date = explode(" ", $sub->created_on);
										 $next_billing_date = $_date[0];
										 $timestamp =  strtotime( "+1 month",strtotime($date[0]));
										 $next_billing_date = date('M d, Y',$timestamp);  
										 $date = date('M d, Y',$timestamp);  
									  }
									   
									   	$_date1 = date('Y-m-d',strtotime($next_billing_date));  
										$current_date = strtotime(date("Y-m-d"));  
										$date2        = strtotime($_date1);
										
										$datediff = $current_date - $date2;  
										$datediff2 = abs($datediff / (60 * 60 * 24));
									?>
			
			
                                    <?php if($result_data!='nouser'): ?>
										<div class="avatar <?php if($datediff2 <= 0){ echo "expire";}else{ echo "deactive";}?>">
											<img src="<?php echo e($sub->profile_pic); ?>" id="pic_<?php echo e($i); ?>">
										</div>
									<?php endif; ?>

                                    <div class="info">
                                        <a href="#" class="username" id="name_<?php echo e($i); ?>"><?php echo e($sub->insta_username); ?></a> 
                                    </div>
                                </div>
                            </td>
                            <td class="sub-info hidden-xs hidden-sm">
                                <div><em><?php echo e(App\Helpers\Helper::get_plan_like($sub->subscription_id)); ?></em> </div>
                                <div class="light"><?php echo e(App\Helpers\Helper::get_plan_amount($sub->subscription_id)); ?></div>
                            </td>
                            <td class="billing-info hidden-xs hidden-sm">
                                <div><em style="color:red;">
									<?php //$date = explode(" ", $sub->created_on);
									//$timestamp = ; //strtotime( "+1 month",strtotime($date[0]));
									  //echo date('M d, Y', strtotime($sub->next_billing_date));  
									  /* if(isset($sub->next_billing_date)){
										$date=$sub->next_billing_date;
									  }                       
									  else{
										 $date = explode(" ", $sub->created_on);
										  $timestamp =  strtotime( "+1 month",strtotime($date[0]));
										 $date=date('M d, Y',$timestamp);  
									  } */
									?>


                                    <?php echo e(date('M d, Y', strtotime($date))); ?></em></div>
                                                            <div class="light">    <!-- <img src="<?php echo e(url('resources/assets/frontend/images/card-mastercard.svg')); ?>"> --><span>Stripe</span></div>
                                                                </td>
                            <td class="actions">
                                <a href="javascript:void(0);" class="btn manage-btn"  cureent-id="<?php echo e($sub->subscription_id); ?>" sub-id="<?php echo e($sub->subscriber_id); ?>" click-id="<?php echo e($i); ?>" status="<?php echo e($sub->status); ?>" cus-id="<?php echo e($sub->customer_id); ?>" typ="<?php echo e(App\Helpers\Helper::get_plan_type($sub->subscription_id)); ?>">
                                    <i class="fa fa-cog" aria-hidden="true"></i> Manage</span>
                                </a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
            </div>
        
            <div class="add">
                <a class="btn add-acct" data-target="#addUserSubModal" data-toggle="modal">
                    <img src="<?php echo e(url('resources/assets/frontend/images/plus.svg')); ?>" width="12" height="12">
                    <span>Add account</span>
                </a>
            </div>
            
        </div>
    </div>

</div>






            
<div id="addUserSubModal" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon">
                        <img src="<?php echo e(url('resources/assets/frontend/images/gram.png')); ?>" style="height:50px !important;">
                    </span>
                    <div class="title">Add new Instagram Account</div>
                    <p class="step_1">Type in Instagram username you want to add and proceed to selecting a subscription.</p>
                    <p class="step_2" style="display:none;">Please select your subscription.</p>
                </div>

                <div class="av-notify"></div>

               <form id="add_user" class="typeUnlock" method="POST" action="#">
					<input type="hidden" value="<?php echo e(Session::get('user_id')); ?>" name="uid" id="uid">
                   <input type="hidden" name="_token1" value="<?php echo e(csrf_token()); ?>" />
                    <span id="name_error" style="color:red;"></span>
                    <div class="input-group" id="sub_username">
                        <span class="icon icon-ig"></span>
                        <input id="add_username" type="text" name="iguser" class="form-control" placeholder="Instagram username" aria-placeholder="Instagram username">

                    </div>
					
					<div class="input-group" id="sub_plans1" style="display:none;">
                        <?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($plan->type=='L'): ?>
                            <label class="check mx">
                                <input name="subscription" type="radio" value="<?php echo e($plan->id); ?>" data-p="<?php echo e($plan->amount); ?> " class="plan_rd">
                                <span class="sub-info"><em><?php echo e($plan->post_like); ?></em> <?php if($plan->type=='L'): ?><?php echo e('Likes /Post'); ?><?php else: ?><?php echo e("Like+Followers"); ?><?php endif; ?> <span class="dull pull-right">$<?php echo e($plan->amount); ?> / mo</span></span>
                            </label><br>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="text-center w-100 mb-2 mt-2"><?php echo e("Drip-feed Followers Per Month"); ?></div>   
                        <select id="fol_plans" class="all-plans followers_plans">
                            <option  selected="selected" data-price="0" value="0">Please Selecte Followers Plan</option>
                            <?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($plan->type=='F'): ?>
                                    <?php if($plan->post_like > 5000): ?>
                                        <option value="<?php echo e($plan->id); ?>" data-price="<?php echo e($plan->amount); ?>" disabled><?php echo e($plan->post_like); ?> <?php echo e('Followers'); ?> </option>
                                    <?php else: ?>
                                        <option value="<?php echo e($plan->id); ?>" data-price="<?php echo e($plan->amount); ?>"><?php echo e($plan->post_like); ?> <?php echo e('Followers'); ?> </option>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>           
                    </div>
					<input type="hidden" id="custom" value="">
                    <div class="btn-group vertical">
                        <button class="btn btn-green btn-block" disabled="" id="btn-nxt">Next<span class="loading" style="display:none;"><img src="https://autolove.co/resources/assets/frontend/images/load.gif" style="height: 20px;width: 20px;float: right;"></span></button>
                        <button class="btn btn-green btn-block" disabled="" style="display:none;" id="btn-nxt1">Pay<span class="loading" ></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

 <div id="payment-success" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <?php if(isset($_COOKIE['autolove_payment_img'])) 
            { 
             $img= $_COOKIE['autolove_payment_img'];
          } 
else{
   $img= "{{ url('resources/assets/frontend/images/gram.png')}}";
}
          ?>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon">
                        <img src="<?php echo e($img); ?>">
                    </span>
                    <div class="title">Thank you for your payment</div>
                   <p>Please allow our system up to 30 minutes for account configuration.</p>
                </div>

                

              
            </div>
        </div>
    </div>
</div>




<div id="manageplan" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon" id="mng-pop-img">
                        <img src="<?php echo e(url('resources/assets/frontend/images/gram.png')); ?>" style="height:50px;">
                    </span>
                    <div class="instaname-name"></div>
                    <br>
                    <div id="manage-active">
                    <div class="title">Manage subscription</div>
                    <p>After the subscription plan is changed, the changes to likes being sent are instant. You will be billed for a new subscription plan.</p>
              
               

                <div class="av-notify"></div>

                <form id="add_user" class="typeUnlock" method="post" action="#">
                    
                   
                        <div class="input-group" id="sub_plans">
                            <?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<label class="check mx upgrade <?php if($plan->type=='L'): ?><?php echo e('onlyy-l'); ?><?php else: ?><?php echo e('both-lf'); ?><?php endif; ?>">
            <input name="subscription" type="radio" value="<?php echo e($plan->id); ?>" data-p="<?php echo e($plan->amount); ?> " class="plan_rd">
            <span class="sub-info"><em><?php echo e($plan->post_like); ?></em> <?php if($plan->type=='L'): ?><?php echo e('Likes /Post'); ?><?php else: ?><?php echo e("Followers"); ?><?php endif; ?> <span class="dull pull-right">$<?php echo e($plan->amount); ?> / mo</span></span>
        </label>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </div>
<input type="hidden" name="custom1" value="" id="custom1">
<input type="hidden" name="custom2" value="" id="custom2">
<input type="hidden" name="custom3" value="" id="custom3">
                    <div class="btn-group vertical">

                         <button class="btn btn-green btn-block"  disabled="" id="btn-nxt2">Pay<span class="loading" ></span></button>

                    </div>
                   
                </form>
                  <div class="btn-group vertical" style="margin-top:20px;">

                        

                          <button class="btn btn-red btn-block"  id="cancel-plan" data-id="" cus-id="">Cancel Subscription</button>
                    </div>
                     </div>
                     <div id="manage-cancel">
                     <div class="btn-group vertical" style="margin-top:20px;">

<form action="/reactive-subscription" method="POST">

                    <input name="user-sub" type="hidden" value="" id="active_sub_id">    
                     <input name="user-cus" type="hidden" value="" id="active_cus_id">    
                    <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
                    
                    <div class="btn-group vertical">     
            <button type="submit" class="btn btn-block" style="background:#00bdc8;color:white;">Reactivate Now<span class="loading"></span></button>
                     
                    </div>
                </form>
                    </div>
                     </div>
            </div>
        </div>
    </div>
</div>
</div>












<div id="cancelModal" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
<a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon" id="mng-pop-img1">
                        <img class="profile" src="">
                    </span>
                    <div class="sub-title"></div>
                    <div class="title">
                        Cancel subscription
                    </div>
                    <p>Are you sure you want to cancel your subscription?</p>

                    <p class="text-left">Please let us know why you're cancelling or how we can improve!</p>
                </div>

                <form action="/cancel-subscription" method="POST">
                    <input name="user-sub" type="hidden" value="" id="cancel_sub_id">    
                     <input name="user-cus" type="hidden" value="" id="cancel_cus_id">    
                    <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
                    <div class="input-group input-select validation-group">
                        <select name="reason" class="form-control" required="">
                            <option value="">Select a reason for cancelling</option>
                            <option value="slow">Likes are delivered too slowly</option>
                            <option value="fake">Quality of accounts liking my posts are poor</option>
                            <option value="price">Prices are too high</option>
                            <option value="trial">I'm just trying out the service</option>
                            <option value="change">I'm changing my plan</option>
                            <option value="switch">I'm moving to a different service</option>
                            <option value="other">Other</option>
                        </select>
                       
                    </div>
                    
                    <div class="input-group input-textarea validation-group">
                        <textarea class="form-control placeholder-shown" name="comment" placeholder="Please enter any comments or suggestions on how we may improve" rows="3" required=""></textarea>
                    
                    </div>

                    <div class="btn-group vertical">     
                        <button type="submit" class="btn btn-red btn-block">Cancel Now<span class="loading"></span></button>
                     
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php 
// setcookie("autolove_payment",'success', time() + 30);
if(isset($_COOKIE['autolove_payment']))
  {?>

<script type="text/javascript">
  jQuery(document).ready(function(){
    


    var pay="<?php echo $_COOKIE['autolove_payment'];?>";
   // alert(pay);
    jQuery('#payment-success').modal('show');
    jQuery('#payment-success').css('margin-top','12%');
    jQuery('.icon > img').css('height','55px');
  });
</script>
<?php }
else {
?>

<?php } ?>

<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('.manage-btn').click(function(){
		var click_id=jQuery(this).attr('click-id');
		var current=jQuery(this).attr('cureent-id');
		var sub_id=jQuery(this).attr('sub-id');
        var cus_id=jQuery(this).attr('cus-id');
		var status=jQuery(this).attr('status');
        var typ=jQuery(this).attr('typ');
		//alert(click_id);
		var img=jQuery('#pic_'+click_id).attr('src');
		var name=jQuery('#name_'+click_id).html();
		 var subscription_id=jQuery(this).attr('sub-id');
		 jQuery('#agreement_id').val(subscription_id);
		 
         if(typ=='ony-l')
         {

            jQuery('.onlyy-l').show();
            jQuery('.both-lf').hide();
         }
          if(typ=='lik+fo')
         {
            jQuery('.onlyy-l').hide();
            jQuery('.both-lf').show();
         }
	 jQuery('#custom3').val(sub_id);
		$(".upgrade input:radio").each(function(){
		
		var radio_value = $(this).val();

		if(current==radio_value){
			jQuery(this).prop("checked", true);
		}
																																																																																																																			
        });
   
    if(status=='active'){
    jQuery('#manage-active').show();   
     jQuery('#manage-cancel').hide();
    jQuery('#cancel-plan').show();
    }else{
    jQuery('#active_sub_id').val(sub_id);
    jQuery('#active_cus_id').val(cus_id); 

    jQuery('#manage-active').hide(); 
     jQuery('#manage-cancel').show();  
    jQuery('#cancel-plan').hide();
    
    }

    jQuery('#mng-pop-img > img').attr('src',img);
    jQuery('.instaname-name').html(name);
    jQuery('#cancel-plan').attr('data-id',sub_id)
     jQuery('#cancel-plan').attr('cus-id',cus_id)
    $('#manageplan').modal('show');
    });
     
    $(".upgrade input:radio").change(function() {
     var price=jQuery(".upgrade input[name='subscription']:checked").attr('data-p');
   //alert(price);
   jQuery('#price1').val(price);
    var sub_id=jQuery(this).val();
	
    var name=jQuery('.instaname-name').text();
  var uid=jQuery('#uid').val();
   var custom1= sub_id;
   var custom2= name;
   jQuery('#custom1').val(custom1);
   jQuery('#custom2').val(custom2);
   
     jQuery('#btn-nxt2').prop('disabled', false );
 
   });  


jQuery('#cancel-plan').click(function(){
   
 var img=jQuery('#mng-pop-img > img').attr('src');
    $('#manageplan').modal('hide');
     jQuery('#mng-pop-img1 > img').attr('src',img);
    var id=jQuery(this).attr('data-id');
    var cus_id=jQuery(this).attr('cus-id');

    jQuery('#cancel_sub_id').val(id);
    jQuery('#cancel_cus_id').val(cus_id);

jQuery('#cancelModal').modal('show');
})
});

   
</script>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>