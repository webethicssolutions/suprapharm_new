
<?php $__env->startSection('pageTitle', 'FAQ'); ?>
<?php $__env->startSection('pageDescription', 'This is FAQ page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php if(Session::get('user_id') != ''): ?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu"> 
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li class=""><a href="<?php echo e(url('account-settings')); ?>">Account settings</a></li>
        <li class="active"><a href="<?php echo e(url('help')); ?>">FAQ</a></li> 
    </ul>
</div>
</div>
<?php endif; ?> 

<div class="box-a-main">
<div class="box-a-wrap">
<div class="box-b-main page-knowledge_base/dashboard page-knowledge_base/dashboard-index">
<div class="box-b-wrap center-panel">
<div class="box-c-knowledge-base-dashboard">
<div class="box-c-dashboard-header">

<div class="box-c-dashboard-header-wrap">
<h1>How can we help?</h1>
<div class="box-c-search">
<form class="search-wide formtastic" action="<?php echo e(url('/search')); ?>" accept-charset="UTF-8" data-remote="true" method="post"><input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input id="kb-search" name="search" placeholder="Enter a question, keyword or topic..." required >

</form>

</div>
</div>
</div>
<div class="help-desc-dsply">

</div>
<div class="faq-section">
<?php $__currentLoopData = $top_help; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $view): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="help-desc-dsply">
    <a class="help_title" href="javascript:void(0);"><?php echo e($view->help_title); ?></a>
    <div class="help_description" style="display: none;"><?php echo e($view->help_description); ?></div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>


</div>

</div>
</div>
</div>
</div>
<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>