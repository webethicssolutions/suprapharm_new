
<?php $__env->startSection('pageTitle', 'Image Details'); ?>
<?php $__env->startSection('style'); ?>
<style>
#uploadcsv
{
	padding-right:45% !important;
}
#myModal
{
	padding-right:45% !important;
}
#myModal1
{
	padding-right:45% !important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  

        <!-- Main content -->
        <div class="dtable_custom_controls">

         <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
          <tbody><tr>
            <td><a href="<?php echo e(url('admin/pharmacy/create')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="" type="submit">Add Pharmacy</a>
            <a href="javascript:void(0)"  data-target="#myModal1" data-toggle="tooltip" onclick="upload_csv('<?php echo e(url('admin/uploadcsv')); ?>')" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Import</a></td>



          </tr>
          </tbody>
        </table>
      </div>
        <section class="content">
        	<div class ="table-title">

              </div>
          <div class="row">
            	<div class="col-lg-12">
                <div class="box box-primary">
                  <!-- /.box-header -->
                  <div class="box-body">
      							<?php if(Session::has('success')): ?>
      							 <div class="success-msg">
      								<?php echo e(Session::get('success')); ?>

      							 </div>
      							 <?php endif; ?>
      							  <?php if(Session::has('error')): ?>
      								   <div class="success-msg">
      									  <?php echo e(Session::get('error')); ?>

      							    	</div>
      							 <?php endif; ?>

      							 <div class="msg_sec"></div>


      							  <div class="table-responsive">
      								<table id="images" class="table table-bordered table-striped">
      								  <thead>
      								  <tr>
                        <th>Holder</th>
                        <th>CIP</th>
                        <th>Pharmacy</th>
                        <th>Address</th>
                        <th>CP</th>

                        <th>City</th>
                        <th>Date of Entry</th>
                        <th>URL</th>

                        <!--<th>Gender</th>
                        <th>Date of birth</th>
                        <th>Registred On</th>-->
                        <th>Action</th>
      								  </tr>
      								  </thead>

      								  <?php $__currentLoopData = $pharmacy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$pharm_acy): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      								  <tr>
                          <td><?php echo e($pharm_acy->name); ?></td>
                          <td><?php echo e($pharm_acy->cip); ?></td>
                          <td><?php echo e($pharm_acy->pharmacy); ?></td>
                          <td><?php echo e($pharm_acy->address); ?></td>
                          <td><?php echo e($pharm_acy->cp); ?></td>
                          <td><?php echo e($pharm_acy->city); ?></td>
                          <td><?php echo e(date('d F, Y h:i:s',strtotime($pharm_acy->created_at))); ?></td>
                          <td><?php echo e($pharm_acy->url ?? "N/A"); ?></td>

      									<td class="action_links">

                          <a href ="<?php echo e(url('admin/pharmacy/edit')); ?>/<?php echo e($pharm_acy->id); ?>" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
      										<a href ="javascript:void(0)"  data-target="#myModal" data-toggle="tooltip" onclick="confirm_delete('<?php echo e(url('admin/pharmacy/delete')); ?>/<?php echo e($pharm_acy->id); ?>')" data-original-title="Delete"  ><i class="fa fa-trash-o"></i></a>

      									</td>
      								  </tr>
      								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      								</table>
      							  </div>
                    </div>
              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    </div>
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="uploadcsv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        </div>
        <div class="modal-body">
     <h4 class="modal-title" id="myModalLabel">Upload Csv</h4>
        </div>
        <form action="<?php echo e(url('admin/importcsv')); ?>" method="POST" enctype="multipart/form-data">
   <?php echo e(csrf_field()); ?>

     <div class="modal-footer">
          <input type="file" name="import_file" class = "form-control" required>
          <span class="error"> <?php echo e($errors->first('import_file')); ?> </span>
          <input type="submit" name="button" value="Upload">
        </div>
      </form>
     </div>
  </div>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>