
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
        <li class="active"><a href="<?php echo e(url('account_settings')); ?>">Account settings</a></li>
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li><a href="<?php echo e(url('help')); ?>">Help center</a></li>
        <li class=""><a href="<?php echo e(url('insta')); ?>">Instagram feeds</a></li>
        <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
    </ul>
</div>
</div>


<div class="page container">

    <div id="content-account" class="content content-settings">

        <div class="panel panel-pad">

            <div class="panel-content">
                <hr class="lg">

            <h1>Account settings</h1>
  <?php if(Session::has('error')): ?>
          <span class="error">
                    <?php echo e(Session::get('error')); ?>

            </span>
             
                <?php endif; ?>
            <?php if(Session::has('success')): ?>
               <p style="color:green">
                    <?php echo e(Session::get('success')); ?>

               </p>
              <?php else: ?>
             
                <?php endif; ?>
                <form method="post" action="<?php echo e(url('update_email')); ?>" class="form typeUnlock">
                   
            <?php echo e(csrf_field()); ?>

                   <div class="form-title">Account email</div>
                    <div class="form-group">
                        <div class="input-group">
                            <input id="update_email" name="email" type="email" class="form-control" value="<?php echo e($email); ?>" placeholder="Email address" required="">
                        <?php echo e($errors->first('email')); ?>

                        </div>
                    </div>
                    <input id="save_email" type="submit" class="btn btn-green mx" value="Save email" disabled>
                </form>

                <hr class="lg">

                <form method="post" action="<?php echo e(url('update_password')); ?>" class="form typeUnlock">
               
            <?php echo e(csrf_field()); ?>

                  
                    <div class="form-title">Change password</div>
                    <div class="form-group">
                        <div class="input-group">
                            <input id="currentPassword" name="old_password" type="password" class="form-control" placeholder="Current password" required="" min="">
                            <?php echo e($errors->first('old_passowrd')); ?>

                        </div>
                        <div class="input-group">
                            <input id="newPassword" name="password" type="password" class="form-control placeholder-shown" placeholder="New password" required="">
                            <?php echo e($errors->first('password')); ?>

                        </div>
                        <div class="input-group">
                            <input id="confirmPassword" name="password_confirmation" type="password" class="form-control placeholder-shown" placeholder="Confirm new password" required="">
                             <?php echo e($errors->first('password_confirmation')); ?>

                        </div>
                    </div>
                    <input type="submit" class="btn btn-green mx" value="Save password">
                </form>
            </div>

        </div>
    </div>

</div>

 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>