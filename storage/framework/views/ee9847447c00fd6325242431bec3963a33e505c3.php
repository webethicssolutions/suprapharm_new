
<?php $__env->startSection('pageTitle', 'Payline Configuration'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<?php if(Session::has('error')): ?>
								<div class="error-msg">
								   <?php echo e(Session::get('error')); ?>

								</div>
					        <?php endif; ?>
							<div class="title col-sm-12" style="margin-bottom:30px">
								<h2 >Payline Configuration</h2>
							</div>

							<?php echo e(Form::open(array('url' => 'admin/payline/save', 'method' => 'post','class'=>'promo_form form-horizontal'))); ?>

							<div class="form-group col-md-12">
								<div class="row">
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Merchant ID')); ?>

											<?php echo e(Form::text('merchant_id', $mer_id ,array('id'=>'couponName','class'=>'form-control'))); ?>

											<span class="error"> <?php echo e($errors->first('merchant_id')); ?> </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Access Key ')); ?>

											<?php echo e(Form::text('access_key', $acc_key ,array('id'=>'couponName','class'=>'form-control'))); ?>

											<span class="error"> <?php echo e($errors->first('access_key')); ?> </span>
										</div> 
										
										
								</div>
							</div>
							<div class="col-md-12">
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									 <!-- <a href="<?php echo e(url('admin/plans')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a> -->
								</div>
							</div>
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>