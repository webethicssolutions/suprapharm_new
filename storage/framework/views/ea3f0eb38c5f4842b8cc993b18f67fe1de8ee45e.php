
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div  class="main-banner">
		<iframe width="100%" height="684"  src="https://www.youtube.com/embed/rAAcNk2fi7o?controls=0&showinfo=0&rel=0&autoplay=1&mute=1&loop=1" frameborder="0" allowfullscreen></iframe>
				
		<div class="container">

			<div class="row">
				<div class="col-12">
				<h1 class="display-5 line-1 anim-typewriter" >Real Likes,Views,Followers.</h1>
				<p class="lead mb-5 mt-2 mt-sm-4">Lorem ipsum dolor sit amet incididunt ut labore <br> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>	
                <div class="col-12 bnr_btn">
				<div class="button">
				<a href="#pricing ">Get Real Likes</a>
				</div>
				<div class="left_text">
				<p><span class="icon_trl"><img src="<?php echo e(url('resources/assets/frontend/images/clock_img.png')); ?>"></span>30 days free trail</p>
				</div>
				</div>				
				</div>
			</div>
		</div>	
	</div>
	
<div id="about">
<div class="special-bg">
<div class="graphic-sec">
	<div class="container">
		<div class="row align-items-center">
		
			<div class="col-lg-6">
			<img src="<?php echo e(url('resources/assets/frontend/images/iphone.png')); ?>" class="img-fluid">
			</div>		
			<div class="col-lg-6">
				<h2 class="title">How Dose Autometically Viral Work</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has since the 1500s.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has since the 1500s.</p>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged remaining essentially unchanged.</p>
				<div class="col-12 bnr_btn">
				<div class="button">
				<a href="#pricing ">Get Real Likes</a>
				</div>
				<div class="left_text">
				<p><span class="icon_trl"><img src="<?php echo e(url('resources/assets/frontend/images/clock.png')); ?>"></span>30 days free trail</p>
				</div>
				</div>
			</div>
			
		</div>	
	</div>				
</div>
<div id="features" class="container icon-info-wrapper services">
        <div class="row">
		
          <div class="col-md-4 text-center mb-5 mb-md-0">
          <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/aeroplane-.png')); ?>">

            <h3 class="mt-4 mb-2">Lorem Ipsum</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p>
          </div>
          <div class="col-md-4 text-center mb-5 mb-md-0">
		 <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/idea.png')); ?>">

            <h3 class="mt-4 mb-2">Lorem Ipsum</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p>
          </div>
          <div class="col-md-4 text-center">
		<img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/power.png')); ?>">

            <h3 class="mt-4 mb-2">Lorem Ipsum</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p>
          </div>
		  <div class="col-md-12 bdr"></div>
            <div class="col-md-4 text-center mb-5 mb-md-0">
          <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/smartphone.png')); ?>">

            <h3 class="mt-4 mb-2">Lorem Ipsum</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p>
          </div>
          <div class="col-md-4 text-center mb-5 mb-md-0">
		 <img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/power.png')); ?>">

            <h3 class="mt-4 mb-2">Lorem Ipsum</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p>
          </div>
          <div class="col-md-4 text-center">
		<img class="img-fluid" src="<?php echo e(url('resources/assets/frontend/images/data-transfer.png')); ?>">

            <h3 class="mt-4 mb-2">Lorem Ipsum</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p>
          </div>		  
        </div>
</div>
</div>
</div>
<div>
<div class="client-reviews">
<div class="container">
<div class="row align-items-center">
	<div class="col-md-12">
<div class="review-slider">
  <div>
 		<div class="heading-bar">
		<div class="title">Why Our Clients Love Us</div>
		</div>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> Lorem Ipsum has been the industry's standard dummy text. Lorem Ipsum is simply dummy <br> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
		<div class="col-12 bnr_btn">
				<div class="button">
				<a href="#pricing ">Get Real Likes</a>
				</div>
				</div>
  </div>
  <div>
</div>	
</div>
</div>
</div>	
</div>
</div>
</div>
<div class="pb-5 pricing-wrap">
<div class="form-wrapper pricing-table-sec">
<div class="container">
<div class="row">
	<div class="col-12">
 <h1  id="pricing" class="text-center">Love Plan</h1>
</div>	 
<div class="col-12 mt-5">
		  <div class="card-deck mb-3 text-center">


<div class="plan-slider">
  
			<?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <div>			<div class="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div class="card-header">
			<!--   <h3 class="mt-0 mb-3">Most Popular</h3> -->
				<div><?php if($plan->type=='L'): ?>
					<?php echo e("Likes Per Post"); ?>

					<?php else: ?>
					<?php echo e('Likes+Followers'); ?>

<?php endif; ?>
				</div>	
                  <h4 class="my-0"><?php echo e($plan->post_like); ?></h4>				
			  </div>
			  <div class="card-body">
				<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1>
					
<ul class="list-unstyled mt-3 mb-4">
					<li><b>Unlimited posts</b></li>
					<li>Real likes from real users </li>
					<li><b>Free views</b> on all videos </li>
					<li>Safe & secure </li>
					<li>24/7 customer support </li>		
				</ul>
				<h4 class="my-0">$<?php echo e($plan->amount); ?> <sub>/ month</sub></h4>
				<div class="prc_button"><a class="pricing_button" href="<?php echo e(url('register')); ?>/<?php echo e($plan->id); ?>">Make a payment</a></div>
			  </div>
			</div></div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  <!-- <div><div class="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div class="card-header">
				<div>Link Per Post</div>	
                  <h4 class="my-0">500</h4>						
			  </div>
			  <div class="card-body">
				<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1>
<ul class="list-unstyled mt-3 mb-4">
				<li><b>Unlimited posts</b></li>
					<li>Real likes from real users </li>
					<li><b>Free views</b> on all videos </li>
					<li>Safe & secure </li>
					<li>24/7 customer support </li>		
				</ul>
				<h4 class="my-0">$48.00 <sub>/ month</sub></h4>
				<div class="prc_button"><a class="pricing_button" href="<?php echo e(url('register/3')); ?>">Make a payment</a></div>
			  </div>
			</div></div>


  <div><div class="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div class="card-header">
				<div>Link Per Post</div>	
                  <h4 class="my-0">1000</h4>						
			  </div>
			  <div class="card-body">
				<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1>
<ul class="list-unstyled mt-3 mb-4">
				<li><b>Unlimited posts</b></li>
					<li>Real likes from real users </li>
					<li><b>Free views</b> on all videos </li>
					<li>Safe & secure </li>
					<li>24/7 customer support </li>		
				</ul>
				<h4 class="my-0">$75.00 <sub>/ month</sub></h4>
				<div class="prc_button"><a class="pricing_button" href="<?php echo e(url('register/4')); ?>">Make a payment</a></div>
			  </div>
			</div></div>
  <div><div class="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div class="card-header">
				<div>Link Per Post</div>	
                  <h4 class="my-0">1500</h4>						
			  </div>
			  <div class="card-body">
				<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1>
<ul class="list-unstyled mt-3 mb-4">
				<li><b>Unlimited posts</b></li>
					<li>Real likes from real users </li>
					<li><b>Free views</b> on all videos </li>
					<li>Safe & secure </li>
					<li>24/7 customer support </li>		
				</ul>
				<h4 class="my-0">$99.00 <sub>/ month</sub></h4>
				<div class="prc_button"><a class="pricing_button" href="<?php echo e(url('register/5')); ?>">Make a payment</a></div>
			  </div>
			</div></div>
  <div><div class="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div class="card-header">
				<div>Link Per Post</div>	
                  <h4 class="my-0">3000</h4>						
			  </div>
			  <div class="card-body">
				<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1>
<ul class="list-unstyled mt-3 mb-4">
				<li><b>Unlimited posts</b></li>
					<li>Real likes from real users </li>
					<li><b>Free views</b> on all videos </li>
					<li>Safe & secure </li>
					<li>24/7 customer support </li>		
				</ul>
				<h4 class="my-0">$189.00 <sub>/ month</sub></h4>
				<div class="prc_button"><a class="pricing_button" href="<?php echo e(url('register/6')); ?>">Make a payment</a></div>
			  </div>
			</div></div>
 -->
</div>

	

			
		  </div>
		  
<div class="btn-wrap text-center"><a class="btn" href="#">Frequently Asked Questions</a></div>		  


</div>	
	</div>	
 
	  

 </div>
 </div>
 </div>	 </div>	
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>