
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
        <li class=""><a href="<?php echo e(url('account_settings')); ?>">Account settings</a></li>
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li><a href="<?php echo e(url('help')); ?>">Help center</a></li>
        <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
    </ul>
</div>
</div>

<div class="box-b-wrap center-panel">
<div class="box-c-knowledge-base-dashboard">
<div class="box-c-dashboard-header">
<div class="box-c-dashboard-header-wrap">
<h1>
How can we help?</h1>
<div class="box-c-search">

<form class="search-wide formtastic" action="<?php echo e(url('/search')); ?>" accept-charset="UTF-8" data-remote="true" method="post"><input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input id="kb-search" name="search" value="<?php if(isset($search_error)) {echo $search_error;} else {echo $search_data;}?>" required >
</form>
</div>
</div>
</div>

 
        
<?php if(isset($search_data)): ?>
<div class="help-desc-dsply">
<center><h3 class="text-info">Search result for <?php echo e($search_data); ?></h3></center>
</div>
<?php $__currentLoopData = $search_all; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$search): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div class="help-desc-dsply">
    <a class="help_title" href="#"><?php echo e($search->help_title); ?></a>
    <div class="help_description" style="display: none;"><?php echo e($search->help_description); ?></div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php endif; ?>

<?php if(isset($search_error)): ?>
<div class="help-desc-dsply">
<center><h3 class="text-info">Search result for <?php echo e($search_error); ?></h3>
<br><h4 class="text-danger">Your search returned no matches.</h4>

<h5>Suggestions:-</h5>
->Try different keywords.<br>
->Try more general keywords.
</center>
</div>
<?php endif; ?>
</div>
</div>


 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>