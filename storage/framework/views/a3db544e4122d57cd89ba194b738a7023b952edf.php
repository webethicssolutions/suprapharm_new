<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/bootstrap.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/fontawesome.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/style.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/slick.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('resources/assets/frontend/css/slick-theme.css')); ?>">



    <title>Autolove - <?php echo $__env->yieldContent('pageTitle'); ?></title>

   
   <meta name="description" content="<?php echo $__env->yieldContent('pageDescription'); ?>" />

  </head>
  <?php
  $email='';
    $currentAction = \Route::currentRouteAction();
    list($controller, $method) = explode('@', $currentAction); 
    $controller = preg_replace('/.*\\\/', '', $controller);  
   if($method=='index'){
    echo '<body id="home">';
   }
   else{
    echo '<body>';
   }
    ?> 
  

  <?php echo $__env->yieldContent('content'); ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo e(url('resources/assets/frontend/js/jquery-3.2.1.min.js')); ?>"></script>
    <script src="<?php echo e(url('resources/assets/frontend/js/bootstrap.bundle.min.js')); ?>"></script>
	<script src="<?php echo e(url('resources/assets/frontend/js/custom.js')); ?>"></script>
  <script src="<?php echo e(url('resources/assets/frontend/js/help.js')); ?>"></script>
    <script src="<?php echo e(url('resources/assets/frontend/js/slick.min.js')); ?>"></script>




  

  <script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});

  
  jQuery(document).ready(function(){
       
        var    dheight = jQuery('html').height(),
            cbody = jQuery('.full-height').height(),
            wheight = jQuery(window).height(),
            cheight = wheight - dheight + cbody;
           
        if (wheight > dheight){
            jQuery('.full-height').height(cheight);
        }
       
        jQuery(window).resize(function(){
            wheight = jQuery(window).height();
            noscroll();
            changepush();
        });

        function noscroll(){
           if (wheight > dheight) {
                jQuery('html').addClass('noscroll');
           }

            else if (wheight <= dheight) {
                jQuery('html').removeClass('noscroll');
            }
           
            else {}

        }

        function changepush(){
           if (wheight > dheight) {
                   jQuery('.full-height').height(wheight-dheight+cbody);
           }
           
        }

  $("#update_email").keyup(function(){
  var type_email=$('#update_email').val();
  var email="<?php echo $email;?>";
  // console.log(type_email);
  // console.log(email);
if(type_email==email){
 $("#save_email").prop('disabled', true);
}
else{
 $("#save_email").prop('disabled', false);
}
});

$('.plan-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});
    
}); 
</script>
  </body>
</html>