
<?php $__env->startSection('pageTitle', 'Services'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  
        <!-- Main content -->
		   <div class="dtable_custom_controls">
								 <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
								  <tbody><tr>
									<td><a href="<?php echo e(url('admin/plan/add')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Ajouter nouveau service</a></td>
									
								  </tr>
								  </tbody>
								</table>
							  </div>
        <section class="content">
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body">
							<?php if(Session::has('success')): ?>
							 <div class="success-msg">
								<?php echo e(Session::get('success')); ?>

							 </div>
							 <?php endif; ?>
							  <?php if(Session::has('error')): ?>
								   <div class="success-msg">
									  <?php echo e(Session::get('error')); ?>

							    	</div>
							 <?php endif; ?>

							 <div class="msg_sec"></div>

							
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th>Sr.No</th>
									<th>Title</th>
									<th>Amount</th>
									<!--<th>Description</th>-->
									<th>Created On</th>   
									<th>Action</th>
								  </tr>
								  </thead>
									<?php $i=1; ?>
								  <?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								  <tr> 
									<td><?php echo $i; ?></td>
									<td> <?php echo e($data->title); ?></td>
									<td> €<?php echo e($data->amount); ?></td> 
									<!--<td> <?php echo e($data->description); ?></td>-->
									<td> <?php echo e(date('d F, y h:i:s',strtotime($data->created_on))); ?></td> 
									<td class="action_links"> 
										<a href ="<?php echo e(url('admin/plan/edit')); ?>/<?php echo e($data->id); ?>" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
										<a href ="javascript:void(0)"  data-target="#myModal" onclick="confirm_delete('<?php echo e(url('admin/plan/delete')); ?>/<?php echo e($data->id); ?>')" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
									</td>
								  </tr>
									<?php  $i++; ?>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col 
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    </div>
	<!-- Modal -->
<div class="modal fade confirm_del" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>