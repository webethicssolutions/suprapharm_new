
<?php $__env->startSection('pageTitle', 'Services'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  
        <!-- Main content -->
        <section class="content">
          <div class="row">
            	<div class="col-lg-12">
            		<div class="box box-primary folder-box">
            			 <?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
            			 <?php if($file['mimeType']=='application/vnd.google-apps.folder'): ?>
							  <div class="service_blk text-center">
	  
        <a href="/admin/drive-folder/<?php echo e($file['id']); ?>">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4><?php echo e($file['name']); ?></h4></span>
        </a>
	  
	 


<!-- <div class="prc_button"><a class="pricing_button" href="https://suprapharm.webethics.online/category-files/1">View Files</a></div> -->
				</div>
				<?php endif; ?>
	  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	   	
	   				</div>
              		  <div class="box box-primary">

                          <!-- /.box-header -->
                          <div class="box-body">
							<?php if(Session::has('success')): ?>
							 <div class="success-msg">
								<?php echo e(Session::get('success')); ?>

							 </div>
							 <?php endif; ?>
							  <?php if(Session::has('error')): ?>
								   <div class="success-msg">
									  <?php echo e(Session::get('error')); ?>

							    	</div>
							 <?php endif; ?>
				<input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
							 <div class="msg_sec"></div>
							 <!--input type="checkbox" id="check-all"/>Check All</input-->
							 <div class="row">
							 	<div id="fltr-rw1">
							 <div class=" col-md-5">
								<div class="form-group">
									<select name="category" id="parent_category" class="form-control fltr-select">
										<option value=''>Select Parent Category</option>
									<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->category_name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
								</div>
							</div>	
							<div class=" col-md-5">
								<div class="form-group">
									<select name="sub_category" id="sub_category" class="form-control fltr-select">
									<option value=''>Select Parent Category First</option>
									</select> 
								</div>
							</div>
							<div class=" col-md-2">
								<div class="form-group">
									<button id="add-files" class="btn btn-primary fltr-select">Save</button>
								</div>
							 </div>
							  </div>
							   </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th> <input type="checkbox" id="check-all"/></input></th>
									<th>Title</th>
									<th>ID</th>
									<th>File Type</th>   
									<th>Assigned Categories</th> 
								  </tr>
								  </thead>
									<?php $i=1; 
									/* echo "<pre>";
									print_r($results);
									die; */
									?>
								  <?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
								  <?php if($file['mimeType']!='application/vnd.google-apps.folder'): ?>
								  <tr> 
								  	
									<td ><input type="checkbox" value="<?php echo e($file['id']); ?>_<?php echo e($file['name']); ?>" class="file-check"></td>
									<td> <?php echo e($file['name']); ?></td>
									<td><?php echo e($file['id']); ?> </td> 
									<td><?php echo e($file['mimeType']); ?> </td> 
									<td><?php echo e(get_file_categories($file['id'].'_'.$file['name'] )); ?> </td> 
									
									
								  </tr>
									<?php  $i++; ?>
									<?php endif; ?>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col 
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		
    </div>

<style>
#fltr-rw{
	display: none;
}
.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
	 background-color: "" !important; 
}
.glyphicon 
{
	top:0px !important;
display: table-cell !important;
	}
</style>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>