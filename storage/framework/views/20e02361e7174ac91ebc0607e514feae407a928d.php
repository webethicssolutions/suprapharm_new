<!-- Footer -->
       <footer id="footer" class="newfooter">
            <div class="footer-content">
                <div class="container">
                    <div class="row">
						<div class="footerlogo_div">
							<ul class="newfooter_content">
								<li class="footer_copyright">© Suprapharm 2018 | 2020</li>
								<li><a href="#">C’est quoi?</a></li>
								<li><a href="#">Nos Services</a></li>
								<li><a href="#">Cartes des pharmacies</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
							<ul class="newfootersocial_media">
								<li class="social-facebook"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-twitter"><a href="https://twitter.com/suprapharmgroup" target="_blank"><i class="fab fa-twitter"></i></a></li>
							</ul>
						</div>
                </div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="fas fa-arrow-up"></i><i class="fas fa-arrow-up"></i></a>
    <!--Plugins-->
    <script src="<?php echo e(url('assets/frontend/new/js/jquery.js')); ?>"></script>
    <script src="<?php echo e(url('assets/frontend/new/js/plugins.js')); ?>"></script>

    <!--Template functions-->
    <script src="<?php echo e(url('assets/frontend/new/js/functions.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('assets/frontend/new/js/jquery.themepunch.tools.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/frontend/new/js/jquery.themepunch.revolution.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/frontend/new/js/revolution.extension.layeranimation.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/frontend/new/js/revolution.extension.parallax.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/frontend/new/js/revolution.extension.slideanims.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('assets/frontend/new/js/slick.min.js')); ?>"></script>

    <script type="text/javascript">
        var tpj = jQuery;

        var revapi21;
        tpj(document).ready(function() {
            if (tpj("#rev_slider_21_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_21_1");
            } else {
                revapi21 = tpj("#rev_slider_21_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "assets/frontend/new/js/plugins/revolution/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        onHoverStop: "off",
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [868, 768, 960, 720],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 1000,
                        levels: [10, 6, 10, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51, 55],
                        type: "mouse",
                        disable_onmobile: "on"
                    },
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/
jQuery('.similar-slider').slick({
slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  arrow:false,
  dots:false,
  autoplaySpeed: 2000,


 	  responsive: [
		{
		  breakpoint: 1200,
		  settings: {
  slidesToShow: 3,
  slidesToScroll: 3,
		  }
		},
		{
		  breakpoint: 992,
		  settings: {
  slidesToShow: 2,
  slidesToScroll: 2,
		  }
		},
		{
		  breakpoint: 641,
		  settings: {
  slidesToShow: 1,
  slidesToScroll: 1,
    arrows:false,
 dots: true,
		  }
		}
	  ]


});
    </script>

</body>

</html>
