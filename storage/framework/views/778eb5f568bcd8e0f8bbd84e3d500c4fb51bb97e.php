<?php echo $__env->make('frontend.common.headernew', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style>
#map {

    width: 100%;

    height: 400px;

}
#reset{
  display:none;
}
</style>
<div class="hero-wrapper">

               <!--============ Page Title =========================================================================-->
             <!-- <div class="map-section">
                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5661875.706410286!2d-2.434900432556539!3d46.139041211913174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd54a02933785731%3A0x6bfd3f96c747d9f7!2sFrance!5e0!3m2!1sen!2sin!4v1576233595930!5m2!1sen!2sin" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                   <!--end container-->
             <!--  </div>-->
             <div class="map-section">
             <div id="map"></div>
</div>

               <!--============ End Page Title =====================================================================-->
               <!--============ Hero Form ==========================================================================-->
               <form class="hero-form form" novalidate="novalidate">
                   <div class="container">
                       <!--Main Form-->
                       <div class="main-search-form">
           <div class="mainsearchformdiv">
                           <div class="form-row">
                               <div class="col-sm-10">
                                   <div class="form-group">
                                     <input name="keyword" type="text" class="form-control" id="autocomplete" placeholder="Enter your address" value="">

                                   </div>
                                   <!--end form-group-->
                               </div>
                               <!--end col-sm-9-->

                               <div class="col-sm-2">
                                   <button type="button" class="btn btn-primary width-100" id="search_btn">Search</button>
                               </div>
                               <!--end col-sm-9-->
                           </div>
             </div>
                           <!--end form-row-->
                       </div>
                       <!--end main-search-form-->
                   </div>
                   <!--end container-->
               </form>
               <!--============ End Hero Form ======================================================================-->
               <!--end background-->
           </div>
<section>
    <!-- Blog -->
       <div class="container">
       <div class="row supraPharmacy" id="main">
         <?php echo $__env->make('frontend.common.list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


       </div>
       <div class="row">
       <div class="col-sm-12 text-center mt-4" id="remove-row">
     <button  class="btn btn-outline" id="loadmore"  data-id="<?php echo e($lastId ?? 0); ?>">Load More</button>
     <button  class="btn btn-outline" id="reset" onClick="window.location.reload()">Reset</button>

     <!--	<a href="https://suprapharm.webethics.online/blogs" class="btn btn-outline btndiv"><span>Afficher plus
</span></a>-->
       </div>
       </div>
       </div>
               <!-- end: Blog -->
</section>



       <!-- Footer -->
  <?php echo $__env->make('frontend.common.footernew', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       <!-- end: Footer -->
   </div>
   <!-- end: Body Inner -->
   <!-- Scroll top -->
   <a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
   <!--Plugins-->
   <script src="js/jquery.js"></script>
   <script src="js/plugins.js"></script>
   <!--Template functions-->
   <script src="js/functions.js"></script>
     <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
   <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
   <script type="text/javascript" src="js/revolution.extension.layeranimation.min.js"></script>
   <script type="text/javascript" src="js/revolution.extension.parallax.min.js"></script>
   <script type="text/javascript" src="js/revolution.extension.slideanims.min.js"></script>
   <script>
   var map,infowindow,marker;
   var slat,slog;
   function initMap() {
     map = new google.maps.Map(document.getElementById('map'), {
       center: {lat: 46.227638, lng: 2.213749000000007},
       zoom: 13
     });
     var input = document.getElementById('autocomplete');
   //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
     var autocomplete = new google.maps.places.Autocomplete(input);
     autocomplete.bindTo('bounds', map);
     infowindow = new google.maps.InfoWindow();
     marker = new google.maps.Marker({
         map: map,
         anchorPoint: new google.maps.Point(0, -29)
     });
     autocomplete.addListener('place_changed', function() {
       var place = autocomplete.getPlace();
       console.log(place.geometry.location);

       var slatitude = place.geometry.location.lat();
       var slongitude = place.geometry.location.lng();

       console.log(slatitude);

       $('#search_btn').click();
        /* infowindow.close();
         marker.setVisible(true);
         var place = autocomplete.getPlace();

         if (place.geometry.viewport) {
             map.fitBounds(place.geometry.viewport);
         } else {
             map.setCenter(place.geometry.location);
             map.setZoom(17);
         }
         marker.setIcon(({
             url: place.icon,
             size: new google.maps.Size(71, 71),
             origin: new google.maps.Point(0, 0),
             anchor: new google.maps.Point(17, 34),
             scaledSize: new google.maps.Size(35, 35)
         }));
         marker.setPosition(place.geometry.location);
         marker.setVisible(true);
         var address = '';
         if (place.address_components) {
             address = [
               (place.address_components[0] && place.address_components[0].short_name || ''),
               (place.address_components[1] && place.address_components[1].short_name || ''),
               (place.address_components[2] && place.address_components[2].short_name || '')
             ].join(' ');
         }
         infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
         infowindow.open(map, marker);*/
     });
   }

 $(document).ready(function(){
     $(document).on('click','#search_btn',function(){
       initMap();
      $value=$('#autocomplete').val();
         $.ajax({
         type : 'get',
         url : '<?php echo e(url("/filter-pharmacies")); ?>',
         dataType : "json",
         data:{'search':$value},
         success:function(data){
           console.log(data.data);
           $('.supraPharmacy').html(data.html);
           var locations = data.data;
           for (i = 0; i < locations.length; i++) {
             if(i==0){
               map = new google.maps.Map(document.getElementById('map'), {
                 center: {lat:locations[i]['latitude'], lng: locations[i]['longitude']},
                 zoom: 9
               });
             }
             //console.log(locations[i]['latitude']+' '+locations[i]['longitude']);
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']),
              map: map,
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                 infowindow.setContent('<div><strong>' + locations[i]['name'] + '</strong><br>' + locations[i]['address']);
                infowindow.open(map, marker);
              }
            })(marker, i));
          }
           $("#loadmore").hide();
           $("#reset").show();
         }
       });
     });

     $(document).on('click','#loadmore',function(){
     var id = $(this).data('id');
     $("#loadmore").html("Loading");
         $.ajax({
             url : '<?php echo e(url("/load-pharmacies")); ?>',
             method : "POST",
             data : {id:id, _token:"<?php echo e(csrf_token()); ?>"},
             dataType : "json",
             success : function (data)
             {
               if(data.html != '')
                {
                    $('.supraPharmacy').append(data.html);
                    $('#loadmore').data('id',data.lastId);
                    $('#loadmore').html('load more');
                }
                else
                {
                    $('#loadmore').html("No More Data");
                    $("#loadmore").attr("disabled", true);
                }
             }
         });
     });

   });
       var tpj = jQuery;
       var revapi21;
       tpj(document).ready(function() {
           if (tpj("#rev_slider_21_1").revolution == undefined) {
               revslider_showDoubleJqueryError("#rev_slider_21_1");
           } else {
               revapi21 = tpj("#rev_slider_21_1").show().revolution({
                   sliderType: "standard",
                   jsFileLocation: "js/plugins/revolution/js/",
                   sliderLayout: "fullscreen",
                   dottedOverlay: "none",
                   delay: 9000,
                   navigation: {
                       onHoverStop: "off",
                   },
                   responsiveLevels: [1240, 1024, 778, 480],
                   visibilityLevels: [1240, 1024, 778, 480],
                   gridwidth: [1240, 1024, 778, 480],
                   gridheight: [868, 768, 960, 720],
                   lazyType: "none",
                   parallax: {
                       type: "mouse",
                       origo: "slidercenter",
                       speed: 700,
                       levels: [2, 6, 10, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51, 55],
                       type: "mouse",
                       disable_onmobile: "on"
                   },
                   shadow: 0,
                   spinner: "spinner0",
                   stopLoop: "off",
                   stopAfterLoops: -1,
                   stopAtSlide: -1,
                   shuffle: "off",
                   autoHeight: "off",
                   fullScreenAutoWidth: "off",
                   fullScreenAlignForce: "off",
                   fullScreenOffsetContainer: "",
                   fullScreenOffset: "",
                   disableProgressBar: "on",
                   hideThumbsOnMobile: "off",
                   hideSliderAtLimit: 0,
                   hideCaptionAtLimit: 0,
                   hideAllCaptionAtLilmit: 0,
                   debugMode: false,
                   fallbacks: {
                       simplifyAll: "off",
                       nextSlideOnWindowFocus: "off",
                       disableFocusListener: false,
                   }
               });
           }
       }); /*ready*/

   </script>

   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmByayAiEvKG4MdveNZZ2v5nEPbwFz6I8&language=en&libraries=places,geometry&callback=initMap"></script>



</body>

</html>
