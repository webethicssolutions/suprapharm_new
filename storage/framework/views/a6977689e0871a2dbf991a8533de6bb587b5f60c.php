
<?php $__env->startSection('pageTitle', 'Add New Service'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<?php if(Session::has('error')): ?>
								<div class="error-msg">
								   <?php echo e(Session::get('error')); ?>

								</div>
					        <?php endif; ?>
							<div class="title col-sm-12" style="margin-bottom:30px">
								<h2 >Add New Category</h2>
							</div>

							<?php echo e(Form::open(array('url' => 'admin/category/save', 'method' => 'post','class'=>'promo_form form-horizontal'))); ?>

							<div class="form-group col-md-12">
								<div class="row">
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Category Name')); ?>

											<?php echo e(Form::text('category_name', '' ,array('id'=>'couponName','class'=>'form-control'))); ?>

											<span class="error"> <?php echo e($errors->first('category_name')); ?> </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Sub Category (Optional)')); ?>

											<?php echo e(Form::text('sub_category', '' ,array('id'=>'couponName','class'=>'form-control'))); ?>

											<span class="error"> <?php echo e($errors->first('sub_category')); ?> </span>
										</div> 
										
										
								</div>
							</div>
							<div class="col-md-12">
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									<!--  <a href="<?php echo e(url('admin/plans')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a> -->
								</div>
							</div>
								  <?php echo e(Form::close()); ?>


					</div>
					 
				</div>
			</div>
		</div>
	</div>
	</section>
	<section class="content usr-contnt">
	 <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body">
						

							 <div class="msg_sec"></div>

							   <div class="dtable_custom_controls">
								 <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
								  <tbody><tr>
									
									
								  </tr>
								  </tbody>
								</table>
							  </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th>Sr.No</th>
									<th>Title</th>
									<th>Action</th>
								  </tr>
								  </thead>
									<?php $i=1; ?>
								 <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								  <tr> 
									<td><?php echo $i; ?></td>
									<td><?php echo e($cat->category_name); ?></td>
									<td class="action_links"> 
										<a href="/admin/category/edit/<?php echo e($cat->id); ?>" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
										<a href="javascript:void(0)" data-target="#myModal" onclick="confirm_delete('/admin/category/delete/<?php echo e($cat->id); ?>')" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
									</td>
								  </tr>
									<?php  $i++; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
							        </div>
                    </div>

            </div>
</section>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> 
    <script>
        $(document).ready(function(){  
			$("#discount").change(function(){ 
				var selectedOpt1 = $(this).val();  
				if(selectedOpt1 == ""){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else if(selectedOpt1 == "percent"){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else{
					$(".prcnt_sec").addClass('hide');
					$(".amount_sec").removeClass('hide');
				}
			}); 
		});
    </script>
<style>
.modal-dialog {
    margin-left: -240px !important;
}
</style>
 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>