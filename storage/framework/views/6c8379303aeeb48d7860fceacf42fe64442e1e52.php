
<?php $__env->startSection('pageTitle', 'Services'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.user_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.member_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				 <?php if(Session::has('success')): ?>
     <div class="success-msg">
       <?php echo e(Session::get('success')); ?>

     </div>
     <?php endif; ?>
     <?php if(Session::has('error')): ?>
     <div class="error-msg">
       <?php echo e(Session::get('error')); ?>

     </div>
     <?php endif; ?>
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary">
						<?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	  <div class="col-md-4 col-lg-4 service_blk text-center">
	  	<div class="services">
	  	<h2><?php echo e($service->title); ?></h2>
	  <ul class="list-unstyled mt-3 mb-4 planlist">
						
							<?php $desc=explode(',',$service->description);?>
							<?php $__currentLoopData = $desc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $des): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li><?php echo e($des); ?></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								
						</ul>
<h4 class="my-0">€<?php echo e($service->amount); ?> <sub>/ month</sub></h4>
<?php if(in_array($service->id,$subs_arr)): ?>
 <div class="prc_button"><a class="pricing_button" href="<?php echo e(url('cancel-subscription/'.$service->id)); ?>">Cancel Subscription</a></div>
<!-- <div class="prc_button"><a class="pricing_button" href="<?php echo e(url('cancel-service/'.$service->id)); ?>">Cancel Subscription</a></div> -->
<?php else: ?>
<div class="prc_button"><a class="pricing_button" href="<?php echo e(url('buy-service/'.$service->id)); ?>">Make a payment</a></div>
<?php endif; ?>					</div></div>
	   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
	</d0iv>
</section>


 <?php $__env->stopSection(); ?>
 
 
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>