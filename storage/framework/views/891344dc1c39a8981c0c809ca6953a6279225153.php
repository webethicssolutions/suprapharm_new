
<?php $__env->startSection('pageTitle', 'Services'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.user_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.member_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				 <?php if(Session::has('success')): ?>
     <div class="success-msg">
       <?php echo e(Session::get('success')); ?>

     </div>
     <?php endif; ?>
     <?php if(Session::has('error')): ?>
     <div class="error-msg">
       <?php echo e(Session::get('error')); ?>

     </div>
     <?php endif; ?>
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary folder-box">
						<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	  <div class="service_blk text-center">
	  
        <a href="<?php echo e(url('category-files/'.$cat->id)); ?>">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4><?php echo e($cat->category_name); ?></h4></span>
        </a>
	  
	 


<!-- <div class="prc_button"><a class="pricing_button" href="<?php echo e(url('category-files/'.$cat->id)); ?>">View Files</a></div> -->
				</div>
	   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
	</div>
</section>


 <?php $__env->stopSection(); ?>
 
 
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>