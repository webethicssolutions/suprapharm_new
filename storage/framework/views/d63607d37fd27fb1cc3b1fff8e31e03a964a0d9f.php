
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
      	<?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">

				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
					        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class ="user_profile" style="margin-bottom:30px">
								<h2 ><?php echo e($data->title); ?></h2>
							</div>

							<?php echo e(Form::open(array('url' => 'admin/email/update', 'method' => 'post','class'=>'profile form-horizontal','enctype'=>'multipart/form-data'))); ?>



							<div class="form-group col-md-12">
								<div class="row">
									<div class="col-md-8 row col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Subject')); ?>

											<?php echo e(Form::text('subject',$data->subject,array('id'=>'subject','class'=>'form-control','placeholder'=>''))); ?>

											<span class="error"> <?php echo e($errors->first('subject')); ?> </span>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Description')); ?>

											<?php echo e(Form::textarea('description',$data->description,array('class'=>'form-control','placeholder'=>''))); ?>

											<span class="error"> <?php echo e($errors->first('description')); ?> </span>

										</div>

										<div class="clearfix"></div>




									</div>

								</div>
							</div>


							<div class="form-group col-md-12">
								 <div class="sign-up-btn ">
									<input type="hidden" value="<?php echo e($data->id); ?>" name="email_id" id="email_id" >
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Update" type="submit">
									 <a href="<?php echo e(url('admin/email')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<!-- <script src="<?php echo e(asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')); ?>"></script> -->
    <script>
        CKEDITOR.replace( 'description',{
    allowedContent: true
} );
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>