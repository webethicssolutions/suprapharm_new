<nav class="navbar navbar-expand-lg ">
	<div class="container">
	<div class="content">
      <a  class="navbar-brand" href="<?php echo e(url('/')); ?>">
     <img src="<?php echo e(url('resources/assets/frontend/images/logo.png')); ?>">
	  </a>
	  <?php if(empty(Session::get('user_id'))): ?>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
       <i class="fas fa-bars"></i></span>
      </button>
	    <?php endif; ?>
	  <?php if(Session::get('user_id') &&  Session::get('subscription_id')): ?>
	  
	  <div class="navbar-right">
                <div class="navbar-user dropdown overlay">
    
           
                    <a href="#" data-toggle="dropdown" aria-expanded="false"><span class="username"><?php echo e(Session::get('user_name')); ?></span></a>
    
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo e(url('/account-settings')); ?>">Account settings</a></li>
                        <li><a href="<?php echo e(url('/billing')); ?>">Billing</a></li>
                        <li><a href="<?php echo e(url('/subscription')); ?>">Subscriptions</a></li>
                      <!--   <li><a href="<?php echo e(url('/help')); ?>">Help center</a></li> -->
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo e(url('/logout')); ?>">Log out<img src="<?php echo e(url('resources/assets/frontend/images/logout.svg')); ?>"></a></li>
                    </ul>
    
                </div>
            </div>
	  

 <?php endif; ?>
 <?php   $currentAction = \Route::currentRouteAction();
    list($controller, $method) = explode('@', $currentAction); 
    $controller = preg_replace('/.*\\\/', '', $controller);  
    ?>
   
 <?php if(empty(Session::get('user_id'))): ?>
			<?php if($method=='index'): ?>
			<div class="collapse navbar-collapse" id="navbar-menu">
			<ul class="navbar-nav ml-auto">

			<li class="nav-item">
			<a class="nav-link active" href="#home">Home</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="#about">About</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="#features">Features</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="#pricing">Pricing</a>
			</li> 
</ul>
			<ul class="navbar-nav navbar-right">
			<li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/login')); ?>">Login</a>
			  </li> 
			  </ul>
</div>
			<?php else: ?>
			<div class="collapse navbar-collapse" id="navbar-menu">
			<ul class="navbar-nav ml-auto">

			<li class="nav-item">
			<a class="nav-link active" href="<?php echo e(url('/#home')); ?>">Home</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="<?php echo e(url('/#about')); ?>">About</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="<?php echo e(url('/#features')); ?>">Features</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="<?php echo e(url('/#pricing')); ?>">Pricing</a>
			</li> 
			</ul>
						<ul class="navbar-nav navbar-right">
			<li class="nav-item">
				<a class="nav-link" href="<?php echo e(url('/login')); ?>">Login</a>
			  </li> 
			  </ul>

</div>
			<?php endif; ?>

<?php endif; ?>		
      </div>
      </div>
    </nav>