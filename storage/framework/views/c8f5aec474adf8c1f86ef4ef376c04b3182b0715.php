 <?php $__env->startSection('title', 'Moodframe Admin: Change Password Admin'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->
        <section class="content">

<div class="bottom">
<div class="container">

<div class="row">

<div class="col-md-8 col-md-offset-2">

<div class="panel-default">
<div class="panel-body">
	 <?php if(Session::has('success')): ?>
     <div class="success-msg">
       <?php echo e(Session::get('success')); ?>

     </div>
     <?php endif; ?>
     <?php if(Session::has('error')): ?>
     <div class="error-msg">
       <?php echo e(Session::get('error')); ?>

     </div>
     <?php endif; ?>
					<h2>Change Password</h2>
					<?php echo e(Form::open(array('url' => 'admin/password-change-admin', 'method' => 'post','class'=>'profile','enctype'=>'multipart/form-data'))); ?>


					<div class="form-group">
							<?php echo e(Form::label('Old Password')); ?>

							<?php echo e(Form::password('old_password',array('id'=>'admin_old_password','class'=>'form-control','placeholder'=>'Old Password'))); ?>

							<span class="error" id="admin_old_password_error" >  <?php echo e($errors->first('old_password')); ?> </span>
					</div>
					<div class="form-group">
							<?php echo e(Form::label('New Password')); ?>

							<?php echo e(Form::password('password',array('class'=>'form-control','placeholder'=>'Password'))); ?>

							<span class="error" >  <?php echo e($errors->first('password')); ?> </span>
					</div>
					<div class="form-group">
							<?php echo e(Form::label('Confirm Password')); ?>

							<?php echo e(Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Confirm Password'))); ?>

							<span class="error" >  <?php echo e($errors->first('password_confirmation')); ?> </span>
					</div>
					<div class="sign-up-btn ">
							<!-- <input name="login" class="loginmodal-submit" id="change_password_admin" value="Update" type="submit"> -->
							<input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Update" type="submit">
					 </div>
 		  <?php echo e(Form::close()); ?>

</div>
</div>
</div>
</div>
</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>