
<?php $__env->startSection('pageTitle', 'Edit Service'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<?php if(Session::has('error')): ?>
								<div class="error-msg">
								   <?php echo e(Session::get('error')); ?>

								</div>
					        <?php endif; ?>
							<div class="title" style="margin-bottom:30px">
								<h2 >Update Service</h2>
							</div>

							<?php echo e(Form::open(array('url' => 'admin/plan/update', 'method' => 'post','class'=>'promo_form form-horizontal'))); ?>

							<div class="form-group col-md-12">
								<div class="row">
									<?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<div class="col-md-8 row col-xs-12">
											<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Title')); ?>

												<?php echo e(Form::text('title', $data->title, array('id'=>'couponName','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('title')); ?> </span>
											</div> 
											<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Amount')); ?>

												<?php echo e(Form::text('amount', $data->amount, array('id'=>'couponName','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('amount')); ?> </span>
											</div> 
											<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Description (Comma Seprated)')); ?>

											<textarea name="description" placeholder="Description" class='form-control'><?php echo e($data->description); ?></textarea>
											<span class="error"> <?php echo e($errors->first('description')); ?> </span>
										</div> 
											
											 
											<div class="clearfix"></div>
											<input name="plan_id" type="hidden" value="<?php echo e($data->id); ?>" />
										</div> 
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</div>
							</div>

							<div class="form-group col-md-12">
								 <div class="sign-up-btn "> 
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									 <a href="<?php echo e(url('admin/plans')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> 
    <script>
        $(document).ready(function(){  
			$("#discount").change(function(){ 
				var selectedOpt1 = $(this).val();  
				if(selectedOpt1 == ""){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else if(selectedOpt1 == "percent"){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else{
					$(".prcnt_sec").addClass('hide');
					$(".amount_sec").removeClass('hide');
				}
			}); 
		});
    </script>

 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>