
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
        <li class=""><a href="<?php echo e(url('account_settings')); ?>">Account settings</a></li>
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li class=""><a href="<?php echo e(url('help')); ?>">Help center</a></li>
        <li class="active"><a href="<?php echo e(url('insta')); ?>">Instagram feeds</a></li>
        <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
    </ul>
</div>
</div>
<div id="dashboard" data-user-subscription="115178">

    <div id="userSubNav" class="topbar active">
        <div class="user-sub insta-user-sub container active">
            <div class="profile">
                <span class="avatar">

                    <?php $__currentLoopData = $result_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($key==0): ?>
                 
                   
            <img src="<?php echo e($value->user->profile_picture); ?>"> 
                    <div class="user-sub-list">
                        <a href="#"><span class="username"><?php echo e($value->user->full_name); ?></span></a>
                    </div>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>			
                </span>
                <span class="info">
                    <a href="#" class="subscription">
                        <i class="fa fa-cog" aria-hidden="true"></i><span>Manage</span>
                        

                    </a>
                </span>
            </div>
        </div>

    </div>

    <div class="page container">
        <div class="content">
            <div id="postContainer">
                
                <div class="post-overlay"></div>

               

                <?php $__currentLoopData = $result_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <div id="media_<?php echo e($data->id); ?>" class="post-col" data-id="<?php echo e($data->id); ?>" data-type="IMAGE">
                    <div class="post">
                        <div class="av-notify"></div>
                       
                        <div class="image" style="background-image: url(<?php echo e($data->images->standard_resolution->url); ?>)"></div>
                        
                        <div class="post-counts counts-desktop">
                            <div class="center-counts">
                                <div class="post-count post-count-likes"><img src="<?php echo e(url('resources/assets/frontend/images/post-likes-white.svg')); ?>" width="13" height="12"><span class="count"><?php echo e($data->likes->count); ?></span></div>

                                <div class="post-count post-count-comments"><img src="<?php echo e(url('resources/assets/frontend/images/post-comments-white.png')); ?>" width="13" height="12"><span class="count"><?php echo e($data->comments->count); ?></span></div>
                            </div>
                        </div>
        
        
     
                    </div>
<!-- 
                    <div class="post-modal post-add-form">
                        <div class="modal-content">
                            <h3>Send extra likes</h3>
                            <form method="post" action="" novalidate="">
                                <div class="input-group validation-group likes-input">

                                    <input id="likes-input-<?php echo e($data->id); ?>" type="tel" name="likes" class="form-control placeholder-shown" placeholder="0" pattern="^[0-9]+(?:,[0-9]{3})*$" inputmode="numeric" autocomplete="off" data-bonus="true">
                                    <label for="likes-input-<?php echo e($data->id); ?>">likes</label>
                                    <span class="icon"></span>
                                </div>
                                <div class="summary">
                                    <table class="table">
                                        <tbody>
                                            <tr class="available">
                                                <td>Available credits</td>
                                                <td class="user-credits credits validation-group">13</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="total">
                                                <td>Total cost</td>
                                                <th class="credits">0</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="btn-group validation-group">
                                    <button type="button" class="btn btn-grey btn-credits" data-toggle="modal" data-target="#creditsModal">Buy credits</button>
                                    <button type="submit" class="btn btn-green" disabled="">Send now<span class="loading"></span></button>
                                </div>
                            </form>
                            <button class="btn btn-close"></button>
                        </div>
                    </div> -->
                </div>
        
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</div>
        
        </div>
    </div>

</div>

 
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>