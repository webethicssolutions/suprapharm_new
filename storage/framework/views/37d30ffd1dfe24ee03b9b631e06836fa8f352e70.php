<!DOCTYPE html>
<html lang='en'><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(url('resources/assets/frontend/images/favicon.png')); ?>">
    <title>Autolove-Admin</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo e(url('assets/admin/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/admin/bootstrap/css/font-awesome.min.css')); ?>">

     <link rel="stylesheet" href="<?php echo e(url('assets/admin/plugins/datatables/dataTables.bootstrap.css')); ?> ">
    <link rel="stylesheet" href="<?php echo e(url('assets/admin/dist/css/AdminLTE.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(url('assets/admin/dist/css/skins/skin-blue.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('css/admin/style.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('css/admin/tokenize2.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('css/admin/bootstrap-toggle.css')); ?>">
 <script src="<?php echo e(url('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js')); ?>"></script>
	 <script src="<?php echo e(url('assets/admin/bootstrap/js/bootstrap.min.js')); ?>"></script>
     <script> var base_url1 = '<?php echo e(url('/admin')); ?>' ;</script>
     <script> var base_url = '<?php echo e(url('')); ?>' ;</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition login-page hold-transition skin-blue sidebar-mini">
        <div class='container-fluid'>
            <div class='row'>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
	   

 
   
   
    <script src="<?php echo e(url('assets/admin/dist/js/app.min.js')); ?>"></script>
     <script src="<?php echo e(url('assets/admin/dist/js/bootstrap-filestyle.min.js')); ?>"></script>
     <script src="<?php echo e(url('assets/admin/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
     <script src="<?php echo e(url('assets/admin/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
     <!---  For CK EDitor -->
     <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	    <script src="<?php echo e(url('js/admin/tokenize2.js')); ?>"></script>
     <script src="<?php echo e(url('js/admin/common.js')); ?>"></script>
     <script src="<?php echo e(url('js/admin/bootstrap-toggle.js')); ?>"></script>
	  
	  <!--script src="<?php echo e(url('resources/assets/frontend/js/bootstrap.min.js')); ?>"></script-->
      <script src="<?php echo e(url('resources/assets/frontend/js/particles.js')); ?>"></script>
      <script src="<?php echo e(url('resources/assets/frontend/js/app.js')); ?>"></script>
	  <!-- <script src="https://use.fontawesome.com/da3840b714.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">  
	   
	  <script>
	 jQuery('#imageUpload').change(function(){			
			readImgUrlAndPreview(this);
			function readImgUrlAndPreview(input){
				 if (input.files && input.files[0]) {
			            var reader = new FileReader();
			            reader.onload = function (e) {			            	
			               jQuery('#imagePreview').attr('src', e.target.result);
							}
			          };
			          reader.readAsDataURL(input.files[0]);
			     }	
		});
			

  function ConfirmDelete()
  {
  var x = confirm("Are you sure you want to delete?");
  if (x)
    return true;
  else
    return false;
  }
		

	  </script>
    </body>
</html>
