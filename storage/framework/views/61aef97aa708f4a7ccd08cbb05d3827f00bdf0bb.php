<?php $__env->startSection('pageTitle', 'Privacy'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="page container"> 
    <div id="content-transactions" class="content content-settings">

        <div class="panel panel-pad">
		<h1>Our commitment to Privacy Policy</h1>
	<br><br>
				  
	<p id="introduction">
			Your privacy is important to us. AutoLoveCo (“we”, “us”) owns and operates the website www.autolove.co (“Site”). AutoLoveCo promotes your products, brands and services through Instagram. We are committed to safeguarding your privacy. This Privacy Policy applies to the Site and governs collection of your information and usage.The following policy explains how we deal with the information collected from you, your personal information and options available to you in regard to the policies followed by us. By creating an account with AutoLove or using our service, you consent to the practices described in this policy. The term "you" or "You" or “User” or “Users” shall refer to any person or entity, who views, uses, accesses, browses or submits any content or material to the Site.
	</p>
	<h4>1. Collection of information</h4>
    <p>
	We may collect, store and use both personal and non-personal information. We will not share, sell, or trade this information to third parties in ways different from what is disclosed in this Privacy Policy. We use various technologies to collect information from your device and about your activities on our Service. We automatically collect information from your browser or device when you visit our Service. This information could include your IP address, device ID and type, your browser type and language, the operating system used by your device, access times, your device’s geographic location and the referring website address. We may obtain both personal and non-personal information about you from other similar businesses, business partners/affiliates and other third parties.
	</p>
	<p>We collect the following kinds of data/information:</p> 
	 <p>(a) personally identifiable information, such as your e-mail address, user name, billing address or telephone number, payment information for our recurring/subscription based services through PayPal merchant, when you register on our website and access it or contact us;</p>   

	 <p>(b) anonymous demographic information, which is not unique to you, such as your ZIP code and country. We may also collect the following information: dates when connected to our service; information relating to any transactions carried out between you and us on or in relation to this Site.</p>    

	<p>We use Google Analytics to collect information. Google Analytics are cookies used to collect information about how visitors use our site. We use the information to compile reports and to help us improve our site. The cookies collect information in an anonymous form, including the number of visitors to our site, where visitors have come to our site from and the pages they visited. To find out more about Google Analytics, please view Google Privacy Policy and Cookies & Google Analytics.</p>

	<p>We collect your PayPal details. We use Web Hooks to link our Site with PayPal Merchant.</p>
	<h4>1.1. Personal information and Confidentiality</h4>
	<p>Personal information means any information that may be used to identify the User or his company including, but not limited to, User name, telephone number, e-mail address, postal address, occupation and/or industry. We may collect, store and use the following kinds of personal information:</p>

	<p>(a) Information that you provide while signing up for our services, during telephone call or live chat, or by filling in forms on our website when you register for use, like, details of your name, address, telephone number, email, credit card and bank account.</p>

	<p>(b) Once you register you may provide other information about yourself by connecting with, for example, your photo, current city, hometown, family, relationships, networks, activities, interests, and places. In some cases we may ask for additional information for security reasons or to provide specific services to you.</p>

	<p>By giving personal information, you give AutoLove consent to collect and use it in accordance with this agreement.</p>
	<h4>1.2. Information disclosure and sharing</h4>
	<p>When you register as a user, your profile can be viewed by other users of the Site/Service.If you log in using your Social media/ Instagram account or Email ID, you authorize us to access and collect your Social media/ Instagram account information. Other users, in addition to your profile information, will also be able to view information you have provided to us directly or through Social media/ Instagram. While, User’s personal information is never shared outside the scope of AutoLove’s business without prior permission, however User’s personal information may be stored and/or shared with supporting companies/services as required by us. However, these service providers are not
	permitted to share or use such information for any purpose other than the purpose for
	which it is shared.</p>
	<h4>1.3. Data retention and Security of your personal data</h4>
	<p>We keep your information only as long as we need it for legitimate business purposes and as permitted by applicable legal requirements. If you close your account, we will retain certain data for analytical purposes and recordkeeping integrity, as well as to prevent fraud, enforce our Terms of Use, take actions we deem necessary to protect the integrity of our Service or our users, or take other actions otherwise permitted by law.</p>

	<p>We may transfer data that we collect from you to our main server for processing and storing. By submitting your personal data, you agree to this transfer, storing or processing.We will take reasonable technical and organizational precautions to prevent the loss,misuse or alteration of your personal information. However, you are responsible for keeping your password and user details confidential. We will not sell, transfer, or use the information relating to the terminated account of an individual in any way. We will not ask you for your password. Please do not share your pass word with others. Change your pass word regularly.</p>

	<p>The transmission of information via the internet is not completely secure and therefore we cannot guarantee the security of data sent to us electronically and transmission of such data is therefore entirely at your own risk.</p>
	<h4>1.4. Notification of Changes</h4>
	<p>AutoLove reserves the right to update/amend this Policy from time to time without prior notice. At times, we may notify you by posting a prominent announcement on the Site/our web pages/Instagram. We may also notify you of changes to our privacy policy by email.</p>

	<h4>1.5. Your rights</h4>
	<p>You may instruct us to provide you with any personal information we hold about you.</p>

	<h4>1.6. Control of Your Password</h4>
	<p>You are responsible for all actions taken with your User ID and password, including fees charged to your account. Therefore we do not recommend that you disclose your(ACCOUNT) password to any third parties. If you choose to share your User ID and
	password or your personal information with third parties, you are responsible for all actions taken with your account and therefore you should review that third party's privacy policy. If you lose control of your password, you may lose substantial control over your personal information and may be subject to legally binding actions taken on your behalf.</p>


	<h4>1.7. Updating information</h4>
	<p>You can review and inform us to update your personal information. Applicable privacy laws may allow you to request the correction of errors or omissions in your personal information that is in our custody or under our control. The same will be corrected/updated within a reasonable time.</p>

	<h4>2. Restricted Activities</h4>
	<p>Your Information and your activities shall not: (a) be false, inaccurate or misleading; (b)be fraudulent; (c) infringe any third party's copyright, trademark, trade secret or other proprietary rights or rights of publicity or privacy; (d) violate any law, statute, ordinance
	or regulation (including, but not limited to, those governing antidiscrimination or false advertising); (e) be defamatory, libelous, unlawfully threatening or unlawfully harassing; (f) be obscene or contain child pornography or, if otherwise adult in nature or harmful to
	minors; (g) contain any viruses, Trojan horses, worms, time bombs or other computer programming routines that may damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information.</p>


	<h4>3. License</h4>
	<p>You agree to grant us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub licensable (through multiple tiers) right to exercise the copyright, publicity, and database rights you have in Your Information, in any media now known or not currently known, with
	respect to Your Information, solely to enable AutoLove to use the information you supply us with, so that we are not violating any rights you might have in that information.AutoLove will use Your Information only in accordance with our Privacy Policy.</p>

	<h4>4. Our Use of Your Information</h4>
	<p>We use your personal information to facilitate the services you request. We use your personal information available with us and other information we obtain from your current and past activities on the Site to: ensure that you receive the services that you had signed
	up for; resolve disputes; identify and troubleshoot problems; collect fees owed; inform you about offers, products, services, and updates; customize your experience; detect and protect us against error, fraud and other criminal activity; enforce our User Agreement; and as otherwise described to you at the time of collection. At times, we may look across multiple users to identify problems or resolve disputes. We may compare and review your personal information for errors, omissions and for accuracy.</p>

	<p>You agree that we may use personal information about you to improve our marketing and promotional efforts, to analyze site usage, improve our content and product offerings, and customize the Site's content, layout, and services. These uses improve the Site and better tailor it to meet your needs, so as to provide you with a smooth, efficient, safe and customized experience while using the Site.</p>

	<p>You agree that we may use your personal information to contact you and deliver information to you that, in some cases, are targeted to your interests, such as targeted banner advertisements, administrative notices, product offerings, and communications relevant to your use of the Site. By accepting the User Agreement and Privacy Policy, you expressly agree to receive this information.</p>

	<h4>5. Advertisers</h4>
	<p>We may allow service providers, advertising companies and ad networks, and other third parties to display advertisements on our Service and elsewhere. These companies may use tracking technologies, such as cookies or web beacons, to collect information about users who view or interact with their advertisements. We aggregate (gather up data across all user accounts) personal information and disclose such information in a non-personally identifiable manner to advertisers and other third parties for their marketing and promotional purposes. However, in these situations, we do not disclose to these entities any information that could be used to identify you personally. Certain information, such as
	your name, password, etc, are not disclosed to these third parties in a personally identifiable manner without your explicit consent. Websites of Advertisers have their own privacy policies which you should check. AutoLove does not accept any responsibility or liability for their policies whatsoever, or the consequences of visiting such sites through the links in our Site if any, as AutoLove has no control over them.</p>
	<h4>6. Legal Requests</h4>
	<p>AutoLove cooperates with law enforcement inquiries, as well as other third parties to enforce laws, such as: intellectual property rights, fraud and other rights, to help protect you from bad actors. Therefore, in response to a verified request by law enforcement or other government officials relating to a criminal investigation or alleged illegal activity,we can (and you authorize us to) disclose your name, city, state, telephone number, email address, User ID history, fraud complaints without a subpoena. Without limiting the above,
	in an effort to respect your privacy and our ability to keep the community free from bad actors, we will not otherwise disclose your personal information to law enforcement or other government officials without a subpoena, court order or substantially similar legal
	procedure, except when we believe in good faith that the disclosure of information is necessary to: prevent imminent physical harm or financial loss; or report suspected illegal activity. Further, we can (and you authorize us to) disclose your name, address, city, postal code, country, phone number, email, and company name to other participants under confidentiality agreement, as we in our sole discretion believe necessary or appropriate in connection with an investigation of fraud, intellectual property infringement, piracy, or other unlawful activity. Again, Due to the existing regulatory environment, we cannot ensure that all of your private communications and other
	personal information will never be disclosed in ways not otherwise described in this Privacy Policy. By way of example (without limiting the foregoing), we may be forced to disclose personal information to the government or third parties under certain circumstances, third parties may unlawfully intercept or access transmissions or private communications, or users may abuse or misuse your personal information that they collect from the Site. Further, any transmission over internet is not fully secure. Therefore,although we use industry standard practices to protect your privacy, we do not promise, that your personal information or private communications will always remain private.</p>

	<h4>7. Children’s privacy</h4>
	<p>If you are under 13 years of age, you are not permitted to access this website. We do not collect or maintain personally identifiable data from children below 13 years of age. If we notice or it is brought to our notice that such information is collected, we will ensure to
	delete such information and terminate such accounts. A parent or legal guardian of a child under 13 years, who has become member of this website, may contact us at info@autolove.co to have that child’s account terminated and information deleted.</p>
	<h4>8. 'Do Not Track' Law</h4>
	<p>AutoLove website does not currently respond to a Do Not Track ("DNT") or similar signal as it awaits the results of efforts by the policy and legal community to determine the meaning of DNT and the proper way to respond. AutoLove does not alter its practices when it receives a "Do Not Track" signal from a visitor's browser.</p>

	<p>Users can also set their browser with the privacy preference of “Do Not Track” (DNT) when they do not want certain information about their webpage visits collected over time and across websites or online services to be disclosed to anyone. We also provide “opt-out”
	link to enable you to keep your information private.</p>

	<h4>9. Communication Policy</h4>
	<p>AutoLove may from time to time send you messages regarding the service and to advise you of updates, notices regarding your account, and new services via e-mail, SMS, or direct message into your message box on your personal profile. You may at any time exercise your option not to receive such notifications or “Opt-Out” of such messages. Our Service may also deliver notifications to your Instagram account or phone or mobile device. You can disable these notifications by changing the settings on your account/
	device.</p>
	<p></p>

	<h4>10. Notification of Changes to this Privacy Policy</h4>
	<p>We may amend this Privacy Policy at any time by posting the amended terms on the Site.All amended terms shall automatically be effective 30 days after they are initially posted on the Site. If you have objections to the Privacy Policy, you should not access or use the
	Site.</p>
	<h4>11. Contact</h4>
	<p>If you have any questions about this privacy policy or our treatment of your personal data,please send email to us at info@autolove.co.</p> 
            <div class="panel-content"></div>
        </div>
    </div>
</div>     
<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>     
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>