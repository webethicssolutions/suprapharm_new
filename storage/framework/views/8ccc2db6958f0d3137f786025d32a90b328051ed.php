
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
define('PAYMENT_ENVIRONMENT','SANDOBX');
define('PAYPAL_URL','https://www.sandbox.paypal.com/cgi-bin/webscr');
define('PAYPAL_MERCHANT','dev.harshvardhan@gmail.com');


define('CURRENCY','USD');
define('PAYPAL_RETURN_URL','https://dev.webethics.online/autolove/return_url');
define('CANCEL_URL','https://dev.webethics.online/autolove/cancel_url');
define('PAYPAL_NOTIFICATION_URL','https://dev.webethics.online/autolove/notify_url');



?>
<div id="create-account" class="content content-checkout">
    <div class="panel panel-pad panel-right">
        <div class="summary">
            <!--- div class="summary-toggle">
                <div>
                    <img class="menu" src="/assets/img/dash/v1/menu.svg">
                    <a href="#">Show summary</a>
                </div>
                <div>$17.00</div>
            </div ---->
<?php if(Session::has('success')): ?>
                <div class="success-msg">
                   <?php echo e(Session::get('success')); ?>

                </div>
                  <?php endif; ?>
            <div class="summary-content">
                <table class="summary-table">
                    <thead class="hidden-xs hidden-sm">
                        <tr>
                            <th class="title" colspan="2">Order summary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span class="icon">
                                    <img src="<?php echo e(url('resources/assets/frontend/images/heart.svg')); ?>">
                                </span>
                                <span>
                                    <?php echo e($like); ?> <?php if($type=='L'): ?>
          <?php echo e("Like Per Post"); ?>

          <?php else: ?>
          <?php echo e('Likes+Followers'); ?>

<?php endif; ?> <!-- <em class="sub">/ post</em> --><br>
                                    <a href="<?php echo e(url('/')); ?>">Change</a>
                                </span>
                            </td>
                            <td><span><?php echo e($price); ?> / mo</span></td>
                        </tr>
                        <!-- <tr class="promo">
                            <td colspan="2">
                                <form novalidate="">
                                    <div class="form-group">
                                        <div class="input-group validation-group">
                                            <input type="text" name="code" class="form-control placeholder-shown" placeholder="Promo code" aria-label="Promo code" required="">
                                            <span class="loading" style="display: none"></span>
                                            <span class="validation-message required">Please enter a promo code</span>
                                            <span class="validation-message invalid">Invalid promo code</span>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-blue btn-grey-disabled" arial-label="Apply" disabled="">Apply</button>
                                </form>
                            </td>
                        </tr> -->
                        <tr class="subtotal" style="display: none">
                            <td>
                                <div>Subtotal</div>
                                <div class="subtotal-prorated" style="display: none">Prorate Adj.</div>
                                <div class="subtotal-promo" style="display: none">Discount <span class="subtotal-promo-code"></span> <a href="#" class="clear"></a></div>
                            </td>
                            <td>
                                <div>$<span class="subtotal-amount"><?php echo e($price); ?></span></div>
                                <div class="subtotal-prorated" style="display: none">- $<span class="subtotal-discount">NaN</span></div>
                                <div class="subtotal-promo" style="display: none">- $<span class="subtotal-discount">NaN</span></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total today</th>
                            <th>$<span class="total"><?php echo e($price); ?></span></th>
                        </tr>
                    </tfoot>
                </table>
                <div class="summary-features hidden-xs hidden-sm">
                    <ul>
                        <li>Real likes from real people</li>
                        <li>Automatic like delivery</li>
                        <li>30-day money back guarantee</li>
                        <li>Cancel anytime</li>
                    </ul>
                </div>
            </div>
        </div>    </div>
    <div class="panel panel-pad panel-left">
      <form action="<?php echo PAYPAL_URL;?>" name="form1" method="post" id="regsiter_form">
      <input type="hidden" value="<?php echo PAYPAL_MERCHANT;?>" name="business">

  <input type="hidden" value="_xclick-subscriptions" name="cmd">

  <input type="hidden" value="1" name="no_note">

  <input type="hidden" value="1" name="no_hipping">

  <input type="hidden" value="currency_code" name="USD">

  <input type="hidden" value="country" name="IN">


<input type="hidden" value="item_name" name="sample Item">
<input type="hidden" value="<?php echo e($price); ?>" name="a3">
<input type="hidden" value="1" name="p3">
<input type="hidden" value='M' name='t3'>
<input type="hidden" value="1" name="src">
<input type="hidden" value="1" name="sra">
<input type="hidden" value="2" name="rm">

<input type="hidden" value="" name="custom">

<input type="hidden" value="<?php echo PAYPAL_RETURN_URL;?>" name="return">
<input type="hidden" value="<?php echo CANCEL_URL;?>" name="cancel_return">
<input type="hidden" value="<?php echo PAYPAL_NOTIFICATION_URL;?>" name="notify_url">
<input type="hidden" value="1" name="upload">
<input type="hidden" value="0" name="address_verride">

      <!-- <?php echo e(Form::open(array('url' => '/paypal', 'method' => 'post'))); ?> -->
     <!--  <form class="w3-container w3-display-middle w3-card-4 " method="POST" id="payment-form"  action="/autolove/paypal"> -->
  <?php echo e(csrf_field()); ?>

        <div class="form checkout-form">
            
            <div class="form-header">
                <div class="title">Let's get started&nbsp;&nbsp;<span class="icon"></span></div>
                <p class="statement">
                    Create an account now to access your personalized dashboard after the checkout and connect your Instagram. 
                </p>
            </div>
            <hr>
            <div class="account-info">
              <div class="form-group">
                  <div class="form-group-header">
                      <span class="sub-title">Create account</span>
                  </div>
                  <div class="input-group input-group-email validation-group">
                      <input id="email" type="text" name="email" class="form-control placeholder-shown" placeholder="Email address" aria-label="Email address" value="<?php echo e($email); ?>"" required="">
                      <label for="email"><span class="placeholder">Email address</span></label>
                      <span class="validation-message required">Please enter an email address</span>
                    
                      <span id="email_error" class="error"> <?php echo e($errors->first('email')); ?> 
                      
                    </span>
                  </div>
                  <div class="input-group input-group-username validation-group">
                      <input id="username" type="text" name="username" class="form-control placeholder-shown" placeholder="Create username of login" aria-label="Create username of login" value="<?php echo e($name); ?>" required="">
                      <label for="username"><span class="placeholder">Create username for login</span></label>
                      <span class="validation-message required">Please enter a username</span>
                      <span class="validation-message invalid">Username is invalid</span>
                     
                      <span id="username_error" class="error"> <?php echo e($errors->first('username')); ?> 
                      
                    </span>

                  </div>
                  <div class="input-group input-group-password validation-group">
                      <input id="password" type="password" name="password" class="form-control placeholder-shown mask" placeholder="Password" aria-label="Password" required="">
                      <label for="password"><span class="placeholder">Password</span></label>
                      <span class="validation-message required">Please enter a password</span>
                    
                      <span id="password_error" class="error"> <?php echo e($errors->first('password')); ?> 
                      
                    </span>

                  </div>
                  <div class="input-group input-group-confirm-password validation-group">
                      <input id="confirm-password" type="password" name="password_confirmation" class="form-control placeholder-shown" placeholder="Confirm password" aria-label="Confirm password" required="">
                      <label for="confirm-password"><span class="placeholder">Confirm password</span></label>
                      <span class="validation-message required">Please confirm the password</span>
                      
                      <span id="password_confirmation_error" class="error"> <?php echo e($errors->first('password_confirmation')); ?> 
                      
                    </span>

                  </div>
              </div>      <hr>
              <div class="form-group">
                  <div class="form-group-header">
                      <span class="sub-title">Add Instagram account</span>
                      <span class="sub-title-info">
                          <span class="icon icon-lock"></span>
                          <span>SECURE</span>
                          <span class="icon icon-help" data-toggle="tooltip" data-placement="top" title="" data-original-title="No Instagram password required. To get started enter your Instagram username so we know where to start delivering likes."></span>
                      </span>

                  </div>
                  <div class="input-group input-search input-group-igusername validation-group">
                      <input id="insta_username" type="text" name="insta_username" autocomplete="off" class="form-control placeholder-shown" placeholder="Instagram username" aria-label="Instagram username" value="<?php echo e($insta_username); ?>" required="">
                      <span class="icon left"></span>
                      <label for="igusername"><span class="placeholder">Instagram username</span> <span class="validation-message"></span></label>
                         <span id="insta_username_error" class="error"> <?php echo e($errors->first('insta_username')); ?> 
                      
                    </span>

                      <div class="input-search-dropdown">
                          <ul class="input-search-dropdown-menu"></ul>
                          <span class="input-search-selection">
                              <img class="profile" src="">
                              <span class="name input-search-item-info">
                                  <span class="username input-search-item-title"></span>
                                  <span class="fullname input-search-item-subtitle hidden-xs hidden-sm"></span>
                              </span>
                              <span class="followers input-search-item-info">
                                  <span class="count input-search-item-title"></span>
                                  <span class="input-search-item-subtitle hidden-xs hidden-sm">followers</span>
                              </span>
                              <a href="#" class="close"></a>
                          </span>
                          <div class="input-search-item-template" style="display:none">
                              <a href="#" class="input-search-item">
                                  <img class="profile" src="">
                                  <span class="name input-search-item-info">
                                      <span class="username input-search-item-title"></span>
                                      <span class="fullname input-search-item-subtitle hidden-xs hidden-sm"></span>
                                  </span>
                                  <span class="followers input-search-item-info">
                                      <span class="count input-search-item-title"></span>
                                      <span class="input-search-item-subtitle hidden-xs hidden-sm">followers</span>
                                  </span>
                              </a>
                          </div>
                      </div>
                      <span class="loading" style="display: none"></span>
                      <span class="validation-message required">Please add a valid Instagram account</span>
                      <span class="validation-message invalid">Instagram account is invalid.</span>
                      <span class="validation-message conflict">Instagram account is set to Private. Change it to public and try again.</span>
                  </div>
              </div>    </div>
              <input type="hidden"  id="subscription" name="subscription" value="<?php echo e($id); ?>">
             
            <button  id="register_btn" class="btn btn-success btn-block mx">Next<span class="loading"></span></button>
          
          </form>
        <!--   <?php echo e(Form::close()); ?> -->
        </div>    </div>
</div>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>