<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<?php if(Session::has('error')): ?>
								<div class="error-msg">
								   <?php echo e(Session::get('error')); ?>

								</div>
					        <?php endif; ?>
							<div class ="user_profile" style="margin-bottom:30px">
								<h2 >Add New Email Template</h2>
							</div>

							<?php echo e(Form::open(array('url' => 'admin/email/add', 'method' => 'post','class'=>'profile form-horizontal','enctype'=>'multipart/form-data'))); ?>

							<div class="form-group col-md-12">
								<div class="row">
									<div class="col-md-8 row col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Title')); ?>

											<?php echo e(Form::text('title', '' ,array('id'=>'title','class'=>'form-control','placeholder'=>''))); ?>

											<span class="error"> <?php echo e($errors->first('title')); ?> </span>
										</div>
										<div class="clearfix"></div>

										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Subject')); ?>

											<?php echo e(Form::text('subject', '' ,array('id'=>'subject','class'=>'form-control','placeholder'=>''))); ?>

											<span class="error"> <?php echo e($errors->first('subject')); ?> </span>
										</div>

										
										<div class="clearfix"></div>

										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Message Body')); ?>

											<?php echo e(Form::textarea('description','',array('id'=>'title','class'=>'form-control','placeholder'=>''))); ?>

											<span class="error"> <?php echo e($errors->first('description')); ?> </span>
										</div>


									</div>

								</div>
							</div>

							<div class="form-group col-md-12">
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									 <a href="<?php echo e(url('admin/email')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'description',{
		    allowedContent: true
		} );
    </script>

 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>