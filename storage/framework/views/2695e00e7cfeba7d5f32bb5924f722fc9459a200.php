
<?php $__env->startSection('pageTitle', 'Services'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.user_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.member_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  
        <!-- Main content -->
        <section class="content">
          <div class="row">
            	<div class="col-lg-12">
            		<div class="box box-primary folder-box">
            			 <?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
            			 <?php if($file['mimeType']=='application/vnd.google-apps.folder'): ?>
							  <div class="service_blk text-center">
	  
        <a href="/drive-folder/<?php echo e($file['id']); ?>">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4><?php echo e($file['name']); ?></h4></span>
        </a>
	  
	 


<!-- <div class="prc_button"><a class="pricing_button" href="https://suprapharm.webethics.online/category-files/1">View Files</a></div> -->
				</div>
				<?php endif; ?>
	  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	   	
	   				</div>
              		  <div class="box box-primary">

                          <!-- /.box-header -->
                          <div class="box-body">
							
			
					
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th>Sr No.</th>
									<th>Title</th>
									<!-- <th>ID</th>
									<th>File Type</th>   
									<th>Assigned Categories</th>  -->
									<th>Action</th>
								  </tr>
								  </thead>
									<?php $i=1; 
									/* echo "<pre>";
									print_r($results);
									die; */
									?>
								  <?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
								  <?php if($file['mimeType']!='application/vnd.google-apps.folder'): ?>
								  <tr> 
								  	
									<td ><?php echo e($i); ?></td>
									<td> <?php echo e($file['name']); ?></td>
									<!-- <td><?php echo e($file['id']); ?> </td> 
									<td><?php echo e($file['mimeType']); ?> </td> 
									<td><?php echo e(get_file_categories($file['id'].'_'.$file['name'] )); ?> </td>  -->
									<td class="action_links"> 
                 
                    <a href="/public/uploads/Google-Drive/<?php echo e($file['id'].'_'.$file['name']); ?>" data-toggle="tooltip" data-original-title="View" target="_blank"><i class="fa fa-tasks"></i></a>
                         <a href="/public/uploads/Google-Drive/<?php echo e($file['id'].'_'.$file['name']); ?>" data-toggle="tooltip" data-original-title="Download" class="dwnld-file" download><i class="fa fa-download" ></i></a> 
                       </td>
									
								  </tr>
									<?php  $i++; ?>
									<?php endif; ?>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col 
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		
    </div>

<style>
#fltr-rw{
	display: none;
}
.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
	 background-color: "" !important; 
}
.glyphicon 
{
	top:0px !important;
display: table-cell !important;
	}
</style>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>