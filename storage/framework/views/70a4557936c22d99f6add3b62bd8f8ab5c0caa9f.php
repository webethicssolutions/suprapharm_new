
<?php $__env->startSection('pageTitle', 'Edit User'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<div class ="col-sm-12 user_profile" style="margin-bottom:30px">
								<h2 >User Profile</h2>
							</div>
							
							<?php echo e(Form::open(array('url' => 'admin/user/admin_user_edit', 'method' => 'post','class'=>'profile form-horizontal','enctype'=>'multipart/form-data'))); ?>

							 
							<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="form-group col-md-12">
								<div class="row"> 
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('First Name')); ?>

											<?php echo e(Form::text('first_name', $user_data->first_name ,array('class'=>'form-control','placeholder'=>''))); ?>

											<span class="error"> <?php echo e($errors->first('first_name')); ?> </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Last Name')); ?>

											<?php echo e(Form::text('last_name', $user_data->last_name ,array('class'=>'form-control','placeholder'=>''))); ?>

											<span class="error"> <?php echo e($errors->first('last_name')); ?> </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Username')); ?>

											<?php echo e(Form::text('username', $user_data->username ,array('class'=>'form-control','placeholder'=>'','readonly' => 'true'))); ?>

											<span class="error"> <?php echo e($errors->first('username')); ?> </span>
										</div> 
										
										
										<div class="clearfix"></div>
										<div class="col-md-12 col-xs-12 field">
											<?php echo e(Form::label('Email')); ?>

											<?php echo e(Form::text('email',$user_data->email,array('class'=>'form-control','placeholder'=>'','readonly' => 'true'))); ?>

											<span class="error"> <?php echo e($errors->first('email')); ?> </span>
										</div>
										

										<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Gender')); ?>

												<select class="form-control" name="gender">
                   								<option disabled="disabled">Select</option>
	                                    		<option value="male" <?php if($user_data->gender=='male'): ?><?php echo e('selected'); ?><?php endif; ?>>Male</option>
	                                    		<option value="female" <?php if($user_data->gender=='female'): ?><?php echo e('selected'); ?><?php endif; ?>>Female</option>
	                                    		<option value="other" <?php if($user_data->gender=='other'): ?><?php echo e('selected'); ?><?php endif; ?>>Other</option>
	                                          
	                                          </select> 
										</div>
									<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Date Of Birth')); ?>

												<?php echo e(Form::date('dob',$user_data->dob,array('class'=>'form-control','placeholder'=>'Date of Birth'))); ?>

												<span class="error" >  <?php echo e($errors->first('dob')); ?> </span>
										</div>
										
										<div class="clearfix"></div>
										
										
									
																		 																
									</div>
								
								</div>
							</div> 
							
								
							<div class="col-md-12"> 
								 <div class="sign-up-btn ">
									<input type="hidden" value="<?php echo e($user_data->id); ?>" name="user_edit_id" id="user_edit_id" >
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Update" type="submit">
									 <a href="<?php echo e(url('admin/users')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>			
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>