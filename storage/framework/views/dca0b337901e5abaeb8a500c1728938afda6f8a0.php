<?php $__env->startSection('pageTitle', 'Instagram Feeds'); ?>
<?php $__env->startSection('pageDescription', 'This is Instagram Feeds page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
define('PAYMENT_ENVIRONMENT','SANDOBX');
define('PAYPAL_URL','https://www.sandbox.paypal.com/cgi-bin/webscr');
define('PAYPAL_MERCHANT','dev.harshvardhan@gmail.com');

define('CURRENCY','USD');
define('PAYPAL_RETURN_URL','https://autolove.co/return_url');
define('CANCEL_URL','https://autolove.co/');
define('PAYPAL_NOTIFICATION_URL','https://autolove.co/ipn');




?>
<!-- <div class="sub-navigation">/
<div class="navbar tabbed">
    <ul class="menu">
         <li class="active"><a href="<?php echo e(url('insta')); ?>">Instagram feeds</a></li>
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li class=""><a href="<?php echo e(url('account-settings')); ?>">Account settings</a></li>
        <li><a href="<?php echo e(url('help')); ?>">FAQ</a></li>
        <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
    </ul>
</div>
</div> -->
<?php if($result_data=='nouser'){
echo '<p class="invalid-username">Please confirm Your Instagram Username is Valid.</p>';
}
else{
    ?>
<div id="dashboard" class="insta_posts">

    <div id="userSubNav" class="topbar active">
     <!--    <div class="user-sub container active insta-user-sub">  -->
        <div class="user-sub container <?php if($status=='active'){echo 'active';}?>">
            <div class="profile">
                <span class="avatar">

                 
                 
                   
            <img src="<?php echo e($result_data->profile_pic_url); ?>"> 
                    <div class="user-sub-list ">
                        <a href="https://instagram.com/<?php echo e($result_data->username); ?>" target="_blank"><span class="username"></span></a>
                    </div>
            
              		
                </span>
                <span class="info">
                    <div class="user-sub-list dropdown overlay" style="width:80%;">
                        <a href="#" data-toggle="dropdown"><span class="username"><?php echo '@';?><?php echo e($result_data->username); ?></span></a>
                        <ul class="dropdown-menu">

                           <!--  <li class="user-sub active">
        <a href="<?php echo e(url('/insta')); ?>" class="profile current">
            <div class="avatar">
                <img src="<?php echo e($result_data->profile_pic_url); ?>">
            </div>
            <div class="info">
                <span class="username"><?php echo e($result_data->full_name); ?></span>
            </div>
        </a>
    </li> -->
<?php foreach($insta_sub_user as $user){

$url1='/instagram/'.$user->insta_username.'/'.$user->subscriber_id;
?>
    <li role="separator" class="divider"></li>
    <li class="user-sub <?php if($user->status=='active') {echo 'active';}?>">
        <a href="<?php echo $url1;?>" class="profile current">
            <div class="avatar">
                <img src="<?php echo e($user->profile_pic); ?>">
            </div>
            <div class="info">
                <span class="username"><?php echo e($user->insta_username); ?></span>
                <?php $type=App\Helpers\Helper::get_plan_type($user->subscription_id);?>
                <p><?php if($type=='lik+fo'): ?> <?php echo e('Followers'); ?><?php else: ?><?php echo e('Only Likes'); ?><?php endif; ?></p>
            </div>
        </a>
    </li>
    <?php }?>
                            
                            
                            <li class="add">
                                <a href="#" class="btn btn-green" data-toggle="modal" data-target="#addUserSubModal">
                                    <img src="<?php echo e(url('resources/assets/frontend/images/plus.svg')); ?>"><span>Add new account</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php 
                    foreach($result as $data){
                         // print_r($data);
                         // die;
                        $current_id=$data->subscription_id;
                        $sub_id=$data->subscriber_id;
                        $status=$data->status;
                        $cus_id=$data->customer_id;
                    }
                    ?>
                    <a href="javascript:void(0);" class="subscription btn manage-btn" cureent-id="<?php echo e($current_id); ?>" sub-id="<?php echo e($sub_id); ?>" status="<?php echo e($status); ?>"  cus-id="<?php echo e($cus_id); ?>" typ="<?php echo e(App\Helpers\Helper::get_plan_type($current_id)); ?>">
                        <i class="fa fa-cog" aria-hidden="true"></i><span>Manage</span>
                    </a>
                </span>
            </div>
        </div>

    </div>

    <div class="page container">
        <div class="content">
            <div id="postContainer">
                
                <div class="post-overlay"></div>

              <?php  

            if(count($result_data->edge_owner_to_timeline_media->edges)==0){
				echo '<p class="not-public">Please confirm your Instagram account is public.</p>';
            }
            else{ 
				foreach($result_data->edge_owner_to_timeline_media->edges as $data){

                if($data->node->is_video==1)
                {
                $img=$data->node->thumbnail_src;
                $count=$data->node->video_view_count;
                $img_src= url('resources/assets/frontend/images/video-view.png');
                
                }
                else{
                $img=$data->node->display_url;
                $count=$data->node->edge_liked_by->count;
                $img_src= url('resources/assets/frontend/images/post-likes-white.svg');
                }



               ?>
          <div id="media_<?php echo e($data->node->id); ?>" class="post-col" data-id="<?php echo e($data->node->id); ?>" data-type="IMAGE">
                    <div class="post">
                        <div class="av-notify"></div>
                       
                        <div class="image" style="background-image: url(<?php echo e($img); ?>)"></div>
                        
                        <div class="post-counts counts-desktop">
                            <div class="center-counts">

                                <div class="post-count post-count-likes"><img src=<?php echo e($img_src); ?> width="13" height="12"><span class="count"><?php echo e($count); ?></span></div>

                                <div class="post-count post-count-comments"><img src="<?php echo e(url('resources/assets/frontend/images/post-comments-white.png')); ?>" width="13" height="12"><span class="count"><?php echo e($data->node->edge_media_to_comment->count); ?></span></div>
                            </div>
                        </div>
        
        
     
                    </div>

                </div>
       <?php } ?>
	   
       <div class="view-all-btn pt-4">
			<a href="https://instagram.com/<?php echo e($result_data->username); ?>" target="_blank">View All</a>
		</div>
<?php
   }
?>
<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" />
		</div>
        
        </div>
    </div>

</div>


<div id="manageplan" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon" id="mng-pop-img">
                        <img src="<?php echo e($result_data->profile_pic_url); ?>" style="height:50px;">
                    </span>
                    <div class="instaname-name"></div>
                    <br>
                    <div id="manage-active">
                    <div class="title">Manage subscription</div>
                    <p>After the subscription plan is changed, the changes to likes being sent are instant. You will be billed for a new subscription plan.</p>
              
               

                <div class="av-notify"></div>

                <form id="add_user" class="typeUnlock" method="post" action="#"> 
					<div class="input-group" id="sub_plans">
					<?php /*
						@foreach($plans as $plan)
							<label class="check mx upgrade">
								<input name="subscription" type="radio" value="{{$plan->id}}" data-p="{{$plan->amount}} " class="plan_rd">
								<span class="sub-info"><em>{{$plan->post_like}}</em> @if($plan->type=='L'){{'Likes /Post'}}@else{{"Like+Followers"}}@endif <span class="dull pull-right">${{$plan->amount}} / mo</span></span>
							</label><br>
						@endforeach
						*/
						//echo "result<pre>";print_r($result);

						?>
						
						<?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							
							<label class="check mx upgrade <?php if($plan->type=='L'): ?><?php echo e('onlyy-l'); ?><?php else: ?><?php echo e('both-lf'); ?><?php endif; ?>"">
								<input name="subscription" type="radio" value="<?php echo e($plan->id); ?>" data-p="<?php echo e($plan->amount); ?> " class="plan_rd">
								<span class="sub-info"><em><?php echo e($plan->post_like); ?></em><?php if($plan->type=='L'): ?> <?php echo e('Likes /Post'); ?> <?php else: ?> <?php echo e('Followers'); ?> <?php endif; ?> <span class="dull pull-right">$<?php echo e($plan->amount); ?> / mo</span></span>
							</label>
							
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			    	 </div>
					<input type="hidden" name="custom1" value="" id="custom1">
					<input type="hidden" name="custom2" value="" id="custom2">
                    <input type="hidden" name="custom3" value="" id="custom3">
                    <div class="btn-group vertical">

                         <button class="btn btn-green btn-block"  disabled="" id="btn-nxt2">Pay<span class="loading" ></span></button>

                    </div>
                   
                </form>
                  <div class="btn-group vertical" style="margin-top:20px;">

                        

                          <button class="btn btn-red btn-block"  id="cancel-plan" data-id="" cus-id="">Cancel Subscription</button>
                    </div>
                     </div>
                     <div id="manage-cancel">
                     <div class="btn-group vertical" style="margin-top:20px;">

<form action="/reactive-subscription" method="POST">

                    <input name="user-sub" type="hidden" value="" id="active_sub_id">    
                     <input name="user-cus" type="hidden" value="" id="active_cus_id">    
                    <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
                    
                    <div class="btn-group vertical">     
            <button type="submit" class="btn btn-block" style="background:#00bdc8;color:white;">Reactivate Now<span class="loading"></span></button>
                     
                    </div>
                </form>
                    </div>
                     </div>
            </div>
        </div>
    </div>
</div>
</div>


<div id="cancelModal" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
<a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon" id="mng-pop-img1">
                        <img class="profile" src="">
                    </span>
                    <div class="sub-title"></div>
                    <div class="title">
                        Cancel subscription
                    </div>
                    <p>Are you sure you want to cancel your subscription?</p>

                    <p class="text-left">Please let us know why you're cancelling or how we can improve!</p>
                </div>

                <form action="/cancel-subscription" method="POST">
                    <input name="user-sub" type="hidden" value="" id="cancel_sub_id">    
                     <input name="user-cus" type="hidden" value="" id="cancel_cus_id">    
                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="input-group input-select validation-group">
                        <select name="reason" class="form-control" required="">
                            <option value="">Select a reason for cancelling</option>
                            <option value="slow">Likes are delivered too slowly</option>
                            <option value="fake">Quality of accounts liking my posts are poor</option>
                            <option value="price">Prices are too high</option>
                            <option value="trial">I'm just trying out the service</option>
                            <option value="change">I'm changing my plan</option>
                            <option value="switch">I'm moving to a different service</option>
                            <option value="other">Other</option>
                        </select>
                       
                    </div>
                    
                    <div class="input-group input-textarea validation-group">
                        <textarea class="form-control placeholder-shown" name="comment" placeholder="Please enter any comments or suggestions on how we may improve" rows="3" required=""></textarea>
                    
                    </div>

                    <div class="btn-group vertical">     
                        <button type="submit" class="btn btn-red btn-block">Cancel Now<span class="loading"></span></button>
                     
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>







<div id="addUserSubModal" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon">
                        <img src="<?php echo e(url('resources/assets/frontend/images/gram.png')); ?>" style="height:50px;">
                    </span>
                    <div class="title">Add new Instagram Account</div>
                    <p class="step_1">Type in Instagram username you want to add and proceed to selecting a subscription.</p>
                    <p class="step_2" style="display:none;">Please select your subscription.</p>
                </div>

                <div class="av-notify"></div>

                <form id="add_user" class="typeUnlock" method="POST" action="#">
					<input type="hidden" value="<?php echo e(Session::get('user_id')); ?>" name="uid" id="uid">
                    <?php echo e(csrf_field()); ?>

                       <span id="name_error" style="color:red;"></span>
                    <div class="input-group" id="sub_username">
                        <span class="icon icon-ig"></span>
                        <input id="add_username" type="text" name="iguser" class="form-control" placeholder="Instagram username" aria-placeholder="Instagram username">
                    </div>
					
					<div class="input-group" id="sub_plans1" style="display:none;">
						<?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($plan->type=='L'): ?>
							<label class="check mx">
								<input name="subscription" type="radio" value="<?php echo e($plan->id); ?>" data-p="<?php echo e($plan->amount); ?> " class="plan_rd">
								<span class="sub-info"><em><?php echo e($plan->post_like); ?></em> <?php if($plan->type=='L'): ?><?php echo e('Likes /Post'); ?><?php else: ?><?php echo e("Like+Followers"); ?><?php endif; ?> <span class="dull pull-right">$<?php echo e($plan->amount); ?> / mo</span></span>
							</label><br>
                            <?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="text-center w-100 mb-2 mt-2"><?php echo e("Drip-feed Followers Per Month"); ?></div>   
                        <select id="fol_plans" class="all-plans followers_plans">
                            <option  selected="selected" data-price="0" value="0">Please Selecte Followers Plan</option>
                            <?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($plan->type=='F'): ?>
                                    <?php if($plan->post_like > 5000): ?>
                                        <option value="<?php echo e($plan->id); ?>" data-price="<?php echo e($plan->amount); ?>" disabled><?php echo e($plan->post_like); ?> <?php echo e('Followers'); ?> </option>
                                    <?php else: ?>
                                        <option value="<?php echo e($plan->id); ?>" data-price="<?php echo e($plan->amount); ?>"><?php echo e($plan->post_like); ?> <?php echo e('Followers'); ?> </option>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>           
					</div>

					<input type="hidden" id="custom" value="">
                    <div class="btn-group vertical">
                        <button class="btn btn-green btn-block" disabled="" id="btn-nxt">Next<span class="loading" style="display:none;"><img src="https://autolove.co/resources/assets/frontend/images/load.gif" style="height: 20px;width: 20px;float: right;"></span></button>
                        <button class="btn btn-green btn-block" disabled="" style="display:none;" id="btn-nxt1">Pay<span class="loading" ></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php } ?>
    <script src="https://autolove.co/resources/assets/frontend/js/jquery-3.2.1.min.js"></script>
  <script>
jQuery(document).ready(function(){
      jQuery('.manage-btn').click(function(){
        //var click_id=jQuery(this).attr('click-id');
        var current=jQuery(this).attr('cureent-id');
        var sub_id=jQuery(this).attr('sub-id');
        var cus_id=jQuery(this).attr('cus-id');
        var status=jQuery(this).attr('status');
        //alert(click_id);
       // var img=jQuery('#pic_'+click_id).attr('src');
        //var name=jQuery('#name_'+click_id).html();
         var subscription_id=jQuery(this).attr('sub-id');
         jQuery('#agreement_id').val(subscription_id);
          var typ=jQuery(this).attr('typ');
      if(typ=='ony-l')
         {

            jQuery('.onlyy-l').show();
            jQuery('.both-lf').hide();
         }
          if(typ=='lik+fo')
         {
            jQuery('.onlyy-l').hide();
            jQuery('.both-lf').show();
         }
          jQuery('#custom3').val(sub_id);
        $(".upgrade input:radio").each(function(){
        
        var radio_value = $(this).val();

        if(current==radio_value){
            jQuery(this).prop("checked", true);
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        });
   
    if(status=='active'){
    jQuery('#manage-active').show();   
     jQuery('#manage-cancel').hide();
    jQuery('#cancel-plan').show();
    }else{
    jQuery('#active_sub_id').val(sub_id);
    jQuery('#active_cus_id').val(cus_id); 

    jQuery('#manage-active').hide(); 
     jQuery('#manage-cancel').show();  
    jQuery('#cancel-plan').hide();
    
    }

    //jQuery('#mng-pop-img > img').attr('src',img);
    jQuery('.instaname-name').html(name);
    jQuery('#cancel-plan').attr('data-id',sub_id)
     jQuery('#cancel-plan').attr('cus-id',cus_id)
    $('#manageplan').modal('show');
    });
     
    $(".upgrade input:radio").change(function() {
     var price=jQuery(".upgrade input[name='subscription']:checked").attr('data-p');
   //alert(price);
   jQuery('#price1').val(price);
    var sub_id=jQuery(this).val();
    
    var name=jQuery('.instaname-name').text();
  var uid=jQuery('#uid').val();
   var custom1= sub_id;
   var custom2= name;
   jQuery('#custom1').val(custom1);
   jQuery('#custom2').val(custom2);
   
     jQuery('#btn-nxt2').prop('disabled', false );
 
   });  

jQuery('#cancel-plan').click(function(){
   
 var img=jQuery('#mng-pop-img > img').attr('src');
    $('#manageplan').modal('hide');
     jQuery('#mng-pop-img1 > img').attr('src',img);
    var id=jQuery(this).attr('data-id');
    var cus_id=jQuery(this).attr('cus-id');

    jQuery('#cancel_sub_id').val(id);
    jQuery('#cancel_cus_id').val(cus_id);

jQuery('#cancelModal').modal('show');
})







	var name="<?php echo $username;?>"
	var data = new FormData();
    data.append('insta_name', name);
	var _token =  $('input[name=_token]').val();
	data.append("_token", _token);
    $.ajax({
        url: '/save-user-insta-feed',
        data:data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function (data) {
    
		}
    });
});
</script>
 <?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>