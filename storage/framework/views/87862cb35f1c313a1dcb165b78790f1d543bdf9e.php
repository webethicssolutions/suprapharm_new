<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container full-height">
    <div class="row h-100 align-items-center">
  <div class="col-md-12"> 
<div class="login-form">
        
        <div class="form_container">
          <div class="form-title">Log in to your dashboard</div>

          <form method="POST" id="payment-form"  action="<?php echo e(url('/check-login')); ?>">
            <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
               <?php if(Session::has('error')): ?>
          <span class="error">
                    <?php echo e(Session::get('error')); ?>

            </span>
             
                <?php endif; ?>
            <?php if(Session::has('success')): ?>
               <p style="color:green">
                    <?php echo e(Session::get('success')); ?>

               </p>
              <?php else: ?>
             
                <?php endif; ?>
           <!--  <?php echo e(csrf_field()); ?> -->
            <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="text"  name="email" class="form-control input_user" value="<?php echo e($email); ?>" placeholder="Email/Username">
               <span class="error"> <?php echo e($errors->first('email')); ?></span>
            </div>
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" name="password" class="form-control input_pass" value="<?php echo e($password); ?>" placeholder="password">
               <span class="error"> <?php echo e($errors->first('password')); ?></span>
            </div>
            <div class="form-group form-group-bottom">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customControlInline" name="keep" <?php if($checked=='on'): ?><?php echo e('checked'); ?><?php endif; ?>>
                <label class="custom-control-label" for="customControlInline">Keep me signed in</label>
              </div>
              <div class="forg-pass">
              <a href="<?php echo e(url('/forget-password')); ?>">Forgot Password?</a>
              </div>  
            </div>
            <input type="Submit" name="button" class="btn login_btn" value="Submit">
          </form>
   
        </div>
        
        
      </div>
      </div>      
      </div>      
    </div>  
   
     <div id="payment-success" class="modal in" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                    <span>×</span>

                </a>
            </div>
            <?php if(isset($_COOKIE['autolove_payment_img'])) 
            { 
             $img= $_COOKIE['autolove_payment_img'];
          } 
else{
   $img= "{{ url('resources/assets/frontend/images/gram.svg')}}";
}
          ?>
            <div class="modal-body">

                <div class="modal-top">
                    <span class="icon">
                        <img src="<?php echo e($img); ?>">
                    </span>
                    <div class="title">Thank you for your payment</div>
                   <p>Please allow our system up to 30 minutes for account configuration.</p>
                </div>

                

              
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('frontend.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
// setcookie("autolove_payment",'success', time() + 30);
if(isset($_COOKIE['autolove_payment']))
  {?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    var pay="<?php echo $_COOKIE['autolove_payment'];?>";
   // alert(pay);
    jQuery('#payment-success').modal('show');
    jQuery('#payment-success').css('margin-top','12%');
    jQuery('.icon > img').css('height','55px');
  });
</script>
<?php }
else {
?>

<?php } ?>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>