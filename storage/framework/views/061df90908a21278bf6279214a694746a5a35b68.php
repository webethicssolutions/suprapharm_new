
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
        <li class=""><a href="<?php echo e(url('account_settings')); ?>">Account settings</a></li>
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li class="active"><a href="<?php echo e(url('help')); ?>">Help center</a></li>
        <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
    </ul>
</div>
</div>
<div class="box-b-wrap center-panel">
<div class="box-c-knowledge-base-dashboard">
<div class="box-c-dashboard-header">
<div class="box-c-dashboard-header-wrap">
<h1>
How can we help?</h1>
<div class="box-c-search">

<form class="search-wide formtastic" action="<?php echo e(url('/search')); ?>" accept-charset="UTF-8" data-remote="true" method="post"><input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input id="kb-search" name="search" placeholder="Enter a question, keyword or topic..." required >
</form>
</div>
</div>
</div>

<div class="help-desc-dsply">
<?php $__currentLoopData = $description_help; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $desc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<h2 class="text-info">
<?php echo e($desc->help_title); ?>

</h2>

<?php echo e($desc->help_description); ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
</div>
</div>


 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>