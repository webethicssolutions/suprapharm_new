
<?php $__env->startSection('pageTitle', 'Home'); ?>
<?php $__env->startSection('pageDescription', 'This is home page meta description'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
        <li class=""><a href="<?php echo e(url('account_settings')); ?>">Account settings</a></li>
        <li class=""><a href="<?php echo e(url('billing')); ?>">Billing</a></li>
        <li class=""><a href="<?php echo e(url('subscription')); ?>">Subscriptions</a></li>
        <li class="active"><a href="<?php echo e(url('help')); ?>">Help center</a></li>
        <li class=""><a href="<?php echo e(url('insta')); ?>">Instagram feeds</a></li>
        <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
    </ul>
</div>
</div>


<div class="box-a-main">
<div class="box-a-wrap">
<div class="box-b-main page-knowledge_base/dashboard page-knowledge_base/dashboard-index">
<div class="box-b-wrap center-panel">
<div class="box-c-knowledge-base-dashboard">
<div class="box-c-dashboard-header">
<div class="box-c-dashboard-header-wrap">
<h1>How can we help?</h1>
<div class="box-c-search">
<form class="search-wide formtastic" action="<?php echo e(url('/search')); ?>" accept-charset="UTF-8" data-remote="true" method="post"><input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input id="kb-search" name="search" placeholder="Enter a question, keyword or topic..." required >

</form>

</div>
</div>
</div>
<div class="box-c-categories">
<div class="left-col column">

<div class="category">
<h2>
<a href="#">Top Questions</a>
</h2>

<ol class="topics">
<?php $__currentLoopData = $top_help; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $top): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<li class="topic">
<a href="<?php echo e(url('/help')); ?>/<?php echo e($top->id); ?>"><?php echo e($top->help_title); ?></a>
</li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ol>
<a class="view-all" href="<?php echo e(url('/help-categories')); ?>/<?php echo e($top->help_categories); ?>">View all</a>
</div>
<div class="category">
<h2>
<a href="#">Managing Your Account</a>
</h2>
<ol class="topics">
<?php $__currentLoopData = $managing_help; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $managing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<li class="topic">
<a href="<?php echo e(url('/help')); ?>/<?php echo e($managing->id); ?>"><?php echo e($managing->help_title); ?></a>
</li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ol>
<a class="view-all" href="<?php echo e(url('/help-categories')); ?>/<?php echo e($managing->help_categories); ?>">View all</a>
</div>

</div>
<div class="right-col column">
<div class="category">
<h2>
<a href="#">Additional FAQs</a>
</h2>
<ol class="topics">
<?php $__currentLoopData = $additional_help; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $additional): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<li class="topic">
<a href="<?php echo e(url('/help')); ?>/<?php echo e($additional->id); ?>"><?php echo e($additional->help_title); ?></a>
</li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ol>
<a class="view-all" href="<?php echo e(url('/help-categories')); ?>/<?php echo e($additional->help_categories); ?>">View all</a>
</div>
<div class="category">
<h2>
<a href="#">Billing &amp; Subscription</a>
</h2>
<ol class="topics">
<?php $__currentLoopData = $billing_help; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $billing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
<li class="topic">
<a href="<?php echo e(url('/help')); ?>/<?php echo e($billing->id); ?>"><?php echo e($billing->help_title); ?></a>
</li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ol>
<a class="view-all" href="<?php echo e(url('/help-categories')); ?>/<?php echo e($billing->help_categories); ?>">View all</a>
</div>

</div>
</div>

</div>

</div>
</div>
</div>
</div>

 <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>