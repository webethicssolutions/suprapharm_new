
<?php $__env->startSection('pageTitle', 'Edit Service'); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
      <!-- Main Header -->
     <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php echo $__env->make('admin.common.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('admin.common.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <?php echo $__env->make('admin.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main content -->
	
		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							<?php if(Session::has('success')): ?>
								<div class="success-msg">
								   <?php echo e(Session::get('success')); ?>

								</div>
					        <?php endif; ?>
							<?php if(Session::has('error')): ?>
								<div class="error-msg">
								   <?php echo e(Session::get('error')); ?>

								</div>
					        <?php endif; ?>
							<div class="title" style="margin-bottom:30px">
								<h2 >Update Pharmacy</h2>
							</div>

							<?php echo e(Form::open(array('url' => 'admin/pharmacy/update', 'method' => 'post','class'=>'promo_form form-horizontal','enctype'=>'multipart/form-data'))); ?>

							<div class="form-group col-md-8">
								<div class="row">
									<?php $__currentLoopData = $pharmacy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<div class="col-md-12 row col-xs-12">
											<div class="col-md-8 col-xs-12 field">
												<?php echo e(Form::label('Holder')); ?>

												<?php echo e(Form::text('name', $data->name, array('id'=>'name','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('name')); ?> </span>
											</div>
                      <div class="col-md-4 col-xs-12 field">

                        <?php echo e(Form::label('Select Image')); ?>

                        
							<?php echo e(Form::file('profile',  array('id'=>'profile','class'=>'form-control'))); ?>


							<span class="error"> <?php echo e($errors->first('profile')); ?> </span>
							<?php if(isset($data->profile) && !empty($data->profile)): ?>
								<img src="<?php echo e(url('/images/admin/pharmacy')); ?>/<?php echo e($data->profile); ?>" height="50" width="75" style="margin-top: 5px;">
							<?php endif; ?>
											</div>
                      <div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Address')); ?>

												<?php echo e(Form::text('address', $data->address, array('id'=>'autocomplete','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('address')); ?> </span>
                        <span class="error"> <?php echo e($errors->first('lati')); ?> </span>


											</div>
                      <div class="col-md-6 col-xs-12 field">
                        <label>Latitude</label>
                      <input type="input" class="form-control" name="lati" id="latitude" value="<?php echo e($data->latitude); ?>" readonly>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>Longitude</label>
                      <input type="input" class="form-control" name="longi" id="longitude" value="<?php echo e($data->longitude); ?>" readonly>
                    </div>
											<div class="col-md-6 col-xs-12 field">
												<?php echo e(Form::label('City')); ?>

												<?php echo e(Form::text('city', $data->city, array('id'=>'city','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('city')); ?> </span>
											</div>
                      <div class="col-md-6 col-xs-12 field">
												<?php echo e(Form::label('CIP')); ?>

												<?php echo e(Form::text('cip', $data->cip, array('id'=>'cip','class'=>'form-control','disabled'))); ?>

												<span class="error"> <?php echo e($errors->first('cip')); ?> </span>
											</div>
                      <div class="col-md-6 col-xs-12 field">
												<?php echo e(Form::label('CP')); ?>

												<?php echo e(Form::text('cp', $data->cp, array('id'=>'cp','class'=>'form-control','disabled'))); ?>

												<span class="error"> <?php echo e($errors->first('cp')); ?> </span>
											</div>
											<div class="col-md-6 col-xs-12 field">
												<?php echo e(Form::label('Pharmacy')); ?>

												<?php echo e(Form::text('pharmacy', $data->pharmacy, array('id'=>'pharmacy','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('city')); ?> </span>
											</div>
											
											<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Phone')); ?>

												<?php echo e(Form::text('phone', $data->phone, array('id'=>'phone','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('phone')); ?> </span>
											</div>
						<div class="col-md-12 col-xs-12 field">
												<?php echo e(Form::label('Description')); ?>

												<?php echo e(Form::textarea('description', $data->description, array('id'=>'description','class'=>'form-control'))); ?>

												<span class="error"> <?php echo e($errors->first('description')); ?> </span>
											</div>
						<?php 
							$days = unserialize($data->timing);
						 ?>
						<div class="col-md-12 col-xs-12 field">
											<table class="table">
												<thead class="thead-light">
													<tr>
														<th scope="col">Lundi</th>
														<th scope="col">Mardi</th>
														<th scope="col">Mercredi</th>
														<th scope="col">Jeudi</th>
														<th scope="col">Vendredi</th>
														<th scope="col">Samedi</th>
														<th scope="col">Dimanche</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															<?php echo e(Form::text('monday', $days['monday'], array('id'=>'monday','class'=>'form-control'))); ?>

															<span class="error"> <?php echo e($errors->first('monday')); ?> </span>
														</td>
														<td>
															<?php echo e(Form::text('tuesday', $days['tuesday'], array('id'=>'tuesday','class'=>'form-control'))); ?>

															<span class="error"> <?php echo e($errors->first('tuesday')); ?> </span>
														</td>
														<td>
															<?php echo e(Form::text('wednesday', $days['wednesday'], array('id'=>'wednesday','class'=>'form-control'))); ?>

															<span class="error"> <?php echo e($errors->first('wednesday')); ?> </span>
														</td>
														<td>
															<?php echo e(Form::text('thursday', $days['thursday'], array('id'=>'thursday','class'=>'form-control'))); ?>

															<span class="error"> <?php echo e($errors->first('thursday')); ?> </span>
														</td>
														<td>
															<?php echo e(Form::text('friday', $days['friday'], array('id'=>'friday','class'=>'form-control'))); ?>

															<span class="error"> <?php echo e($errors->first('friday')); ?> </span>
														</td>
														<td>
															<?php echo e(Form::text('saturday', $days['saturday'], array('id'=>'saturday','class'=>'form-control'))); ?>

															<span class="error"> <?php echo e($errors->first('saturday')); ?> </span>
														</td>
														<td>
															<?php echo e(Form::text('sunday', $days['sunday'], array('id'=>'sunday','class'=>'form-control'))); ?>

															<span class="error"> <?php echo e($errors->first('sunday')); ?> </span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

											<div class="clearfix"></div>
											<input name="id" type="hidden" value="<?php echo e($data->id); ?>" />
										</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</div>
							</div>

							<div class="form-group col-md-12">
								 <div class="sign-up-btn ">
									 <input name="update_pharmacy" class="loginmodal-submit btn btn-primary" id="pharmacy_update" value="Update" type="submit">
									 <a href="<?php echo e(url('admin/pharmacy')); ?>" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
								  <?php echo e(Form::close()); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmByayAiEvKG4MdveNZZ2v5nEPbwFz6I8&language=en&libraries=places,geometry&callback=initMap"></script>
    <script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
		var options = {
          componentRestrictions: {country: "fr"}
        };
		var places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);
		google.maps.event.addListener(places, 'place_changed', function () {
			var place = places.getPlace();
			var address = place.formatted_address;
			var latitude = place.geometry.location.lat();
			var longitude = place.geometry.location.lng();
			var mesg = "Address: " + address;

			$('#latitude').val(latitude);
				$('#longitude').val(longitude);
		});
	});
</script>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<!-- <script src="https://suprapharm.webethics.online/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> -->
    <script>
        CKEDITOR.replace( 'description',{
    allowedContent: true
} );
    </script>

 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>