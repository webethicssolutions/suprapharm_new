@include('frontend.common.header-new')
 <!-- REVOLUTION SLIDER -->

<section class="contactsection pharmacist-page">
            <div class="container">
			<h1 class="block-title text-center">
				Pour mériter votre confiance, <b>Pharmavie s'engage	</b><br><span>Un seul objectif : mieux répondre à vos besoins !</span>
			</h1>
			<div class="page-block icons-grid">
				<div>
					<div class="icon">
						<img alt="icone conseiller" src="{{ url('assets/frontend/new/images/conseiller.png')}}">
					</div>
					<div class="title">
						Mieux vous connaître, mieux
						vous conseiller
					</div>
					<div>
						Parce que chaque client est
						unique, nos conseils le sont
						aussi
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone accueillir" src="{{ url('assets/frontend/new/images/accueillir.png')}}">
					</div>
					<div class="title">
						Savoir accueillir et écouter
					</div>
					<div>
						Avec soin et convivialité, pour
						bien comprendre vos attentes
						et mieux vous satisfaire
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone respecter" src="{{ url('assets/frontend/new/images/Respecter.png')}}">
					</div>
					<div class="title">
							Respecter la confidentialité des échanges
					</div>
					<div>
						et protéger vos informations personnelles
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone innover" src="{{ url('assets/frontend/new/images/innover.png')}}">
					</div>
					<div class="title">
						Innover dans nos prestations et
						produits
					</div>
					<div>
						La santé évolue rapidement,
						nous aussi
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone rester disponible" src="{{ url('assets/frontend/new/images/rester-disponibles.png')}}">
					</div>
					<div class="title">
						Rester disponible et
						joingnable
					</div>
					<div>
						Vous donner la possibilité
						d'organiser à distance
						entretiens, dépistages,
						demandes de conseils
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone vie moins chere" src="{{ url('assets/frontend/new/images/vie-moins-chere.png')}}">
					</div>
					<div class="title">
						Contribuer à une vie moins
						chère
					</div>
					<div>
						Avec nos produits PharmaVie,
						nos marques exclusives et nos
						très nombreuses offres
						promotionnelles
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone recompenser" src="{{ url('assets/frontend/new/images/recompenser.png')}}">
					</div>
					<div class="title">
						Récompenser votre fidélité
					</div>
					<div>
						Vous proposer chaque jour de
						nouveaux avantages et un
						service renforcé.
						Pour en savoir plus sur notre programme PharmaVie AFFINITY,
						<a href="/affinity">cliquez ici</a>
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone controler" src="{{ url('assets/frontend/new/images/controler.png')}}">
					</div>
					<div class="title">
						Contrôler rigoureusement la
						qualité
					</div>
					<div>
						De nos produits et nos services
						pour votre sécurité et votre
						satisfaction
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone expertise" src="{{ url('assets/frontend/new/images/expertise.png')}}">
					</div>
					<div class="title">
						Avoir un haut niveau
						d'expertise
					</div>
					<div>
						Grâce à la formation continue
						des équipes en Pharmacie, pour
						vous conseiller et vous orienter
						dans le circuit de santé
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone acteur de sante sociale" src="{{ url('assets/frontend/new/images/acteur-de-sante-sociale.png')}}">
					</div>
					<div class="title">
						Être acteur de santé sociale
					</div>
					<div>
						En soutenant des associations
						caritatives et des actions de
						santé publique
					</div>
				</div>
				<div>
					<div class="icon">
						<img alt="icone en ligne" src="{{ url('assets/frontend/new/images/en-ligne.png')}}">
					</div>
					<div class="title">
						Être disponible en ligne
					</div>
					<div>
						En vous proposant des services
						sur les sites internet de nos
						pharmacies
					</div>
				</div>
			</div>
			<div class="contactbg">
                <div class="row">
				                    <div class="col-lg-5 contactform">
                    {{ Form::open(array('url' => '', 'id' => 'contact_form','method' => 'post','enctype'=>'multipart/form-data')) }}

                         <div class="row">
                             <div class="form-group col-md-6">
                                 <label for="name">Name</label>
                                 <input type="text" aria-required="true" name="name" id="name" class="form-control required name" placeholder="Enter your Name" value="{{ old('name', isset($request) ? $request->name : '') }}">
                                 
                                 <span style="color:red;" class="error" id="name-error" >  {{ $errors->first('name')  }} </span>
                               

                             </div>
                             <div class="form-group col-md-6">
                                 <label for="email">Email</label>
                                 <input type="email" aria-required="true" name="email" id="email" class="form-control required email" placeholder="Enter your Email">
                                 <span style="color:red;" class="error" id="email-error" >  {{ $errors->first('email')  }} </span>
                                 
                             </div>
                         </div>
                         <div class="row">
                             <div class="form-group col-md-12">
                                 <label for="subject">Your Subject</label>
                                 <input type="text" name="subject" id="subject" class="form-control required" placeholder="Subject...">
                                 <span style="color:red;" class="error" id="subject-error" >  {{ $errors->first('subject')  }} </span>
                             </div>
                         </div>   <div class="form-group">
                                 <label for="message">Message</label>
                                 <textarea type="text" name="message" rows="5" id="message" class="form-control required" placeholder="Enter your Message"></textarea>
                                 <span style="color:red;" class="error" id="message-error" >  {{ $errors->first('message')  }} </span>

                             </div>
                         <button class="btn" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Send message</button>
                         
                         {{ Form::close() }}
                         <div class="alert alert-success contact-success d-none"></div>
                       
                    </div>
                    <div class="col-lg-7 contactinformation">
                        <h3 class="text-uppercase">Avec supra, j’ai  tout d’une grande</h3>
                        <p>Mes clients bénéficient des meilleurs prix du marché sans dégrader ma marge  grâce à des conditions d’achats maximales.
                        </br>Et 400 opérations négociées à l’année.</p>
                        <p>Avec le pack com, je communique efficacement sur mes services et offres spéciales.</p>
                        <p>Mes outils statistiques et le service Merchandising optimisent mon offre produits pour répondre au mieux aux attentes de mes clients.</p>
                        <p>Grâce aux formations prises en charge et fiches de process, j’améliore mes connaissances et mon fonctionnement pour toujours mieux garantir la bonne santé de mes patients.</p>
                        <p>Je récompense la fidélité de mes clients avec ma supra carte et je renforce mes relations avec les médecins et infirmières via mon catalogue online de petit matériel médical.</p>
                        <p>Grâce à l’application Suprapharm, je garde un lien avec mes clients pour qu’ils puissent commander mes produits à distance ou m’adresser une ordonnance. Je leur laisse le choix du click&Collect ou de la livraison express. Ma communication est instantanément relayée sur les réseaux sociaux via l’application.</p>
                        <p>En suivant la stratégie de Suprapharm, j’ai augmenté mon chiffre de 33% en 2ans.</br>
                        En profitant de conditions d’achat de Suprapharm, j’ai gagné deux  points de marge.
                        Grâce aux produits exclusifs, je fidélise ma clientèle en proposant du haut de gamme made  in France à petit prix
                        </p>
                        <p>Je faisait beaucoup d’offres promotionnelles canons mais mes clients ne le savaient pas. Avec le système de catalogues et d’affichages, j’ai enfin une communication dynamique. Mes clients sont bien informés de mes offres spéciales..</p>
                        </div>
                    </div>
					</div>
</br>


                </div>
            </div>
</section>



        <!-- Footer -->
@include('frontend.common.footer-new')
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
    
    

    <script type="text/javascript">
    var tpj = jQuery;
    var revapi21;
    tpj(document).ready(function() {
    tpj(document).on('click', '#form-submit', function(e) {
    e.preventDefault();
    $('.error').html("");
    var postData = new FormData($("#contact_form")[0]);
    $.ajax({
        type:'POST',
        url:'/send-email',
        processData: false,
        contentType: false,
        data : postData,
    success:function(data){
            console.log(data);
            //var data = JSON.parse(data);
            console.log(data.msg);
    if(data.status == 'success') {
        //$('.success-msg').show();
        $('.contact-success').html(data.msg).removeClass('d-none');
        $("#contact_form")[0].reset();
        setTimeout(function(){ $('.contact-success').html('').addClass('d-none'); }, 2000);
    } 
},
    error: function( json )
      { console.log(json);
          if(json.status == 422) {
              var errors = json.responseJSON;
              //console.log(errors);
              //console.log(errors);
              $.each(errors, function (key, value) {
               // console.log(key);
                  $('#'+key+'-error').html(value);
              });
          }
          else{
            alert(json['msg']);
          }
      }
                               
   });
})

            
            if (tpj("#rev_slider_21_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_21_1");
            } else {
                revapi21 = tpj("#rev_slider_21_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/plugins/revolution/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        onHoverStop: "off",
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [868, 768, 960, 720],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 700,
                        levels: [2, 6, 10, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51, 55],
                        type: "mouse",
                        disable_onmobile: "on"
                    },
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

    </script>
<script type="text/javascript">
      $(document).ready( function() {
  $('#deletesuccess').delay(3000).animate({opacity:0},3000);
  // $('#deletesuccess').delay(3000).fadeOut();
	// $('#deletesuccess').animate({opacity:"1"},{duration:3000}, function(){
   // $(this).css('opacity','0');
// });
      });
    </script>
</body>

</html>
