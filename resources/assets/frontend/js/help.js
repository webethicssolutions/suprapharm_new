var $this;
$('.help_title').click(function() {
    $this = $(this);
    if ( $this.hasClass('revealed') ){
        $('.help_description').slideUp();
        $('.help_title').removeClass('revealed');
    }else{
        $('.help_description').slideUp();
        $('.help_title').removeClass('revealed');
        $this.parent().find('.help_description').slideDown();
        $this.addClass('revealed');
    }
});