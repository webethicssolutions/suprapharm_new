@extends('admin.layouts.master')
@section('pageTitle', 'Payline Configuration')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							@if(Session::has('error'))
								<div class="error-msg">
								   {{Session::get('error')}}
								</div>
					        @endif
							<div class="title col-sm-12" style="margin-bottom:30px">
								<h2 >Payline Configuration</h2>
							</div>

							{{ Form::open(array('url' => 'admin/payline/save', 'method' => 'post','class'=>'promo_form form-horizontal')) }}
							<div class="form-group col-md-12">
								<div class="row">
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Merchant ID') }}
											{{ Form::text('merchant_id', $mer_id ,array('id'=>'couponName','class'=>'form-control')) }}
											<span class="error"> {{ $errors->first('merchant_id') }} </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Access Key ') }}
											{{ Form::text('access_key', $acc_key ,array('id'=>'couponName','class'=>'form-control')) }}
											<span class="error"> {{ $errors->first('access_key') }} </span>
										</div> 
										
										
								</div>
							</div>
							<div class="col-md-12">
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									 <!-- <a href="{{url('admin/plans')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a> -->
								</div>
							</div>
								  {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

 @stop
