@extends('admin.layouts.master')
@section('pageTitle', 'Transactions')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="min-height: 850px;">
	  {{--- @include('admin.common.breadcrumb') ----}}
        <!-- Main content -->
        <section class="content">
        	<div class ="table-title">
                
              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body"> 
							@if(Session::has('success'))
							 <div class="success-msg">
								{{ Session::get('success') }}
							 </div>
							 @endif 
							  @if(Session::has('error'))
								   <div class="success-msg">
									  {{ Session::get('error') }}
							    	</div> 
							 @endif
							 
							 <div class="msg_sec"></div>
						
    							<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                      
                      
                    </tr>
                    </tbody>
                  </table>
                </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th>Full Name</th>
									<th>Service Title</th>
                  <th>Amount</th>  
									<th>Transaction ID</th> 
                  <th>Date</th>
									
								  </tr>
								  </thead>
                  @foreach($transactions as $trans)
                  <tr>
                    <td>{{ucwords(get_user_name($trans->user_id))}}</td>
                    <td>{{get_service_name($trans->service_id)}}</td>
                    <td>€{{$trans->amount}}</td>
                    <td>{{$trans->transaction_id}}</td>
                    <td>{{$trans->created_on}}</td>
                  </tr>
							@endforeach
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>
	  
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            
         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
$(".usr_status").change(function(){
		
        $(".success-msg").hide();
        $(".erorr-msg").hide();
        if($(this).data('val')!=""){
            var u_id = $(this).data('val');
            var usr_tokn = $('input[name="_token"]').val();
            
            var ajax_url = "";
            var ustatus = "";
            if($(this).prop("checked") == true){
                ajax_url = "/admin/user/enableuser/"+u_id;
                ustatus = "Enabled";
            }else{
                ajax_url = "/admin/user/disableuser/"+u_id;
                ustatus = "Disabled";
            }
            
            $.ajax({
                type: "POST",
                url: ajax_url,
                data:{'_token':usr_tokn},
                success: function (response) {
                    console.log('response '+response);
                    if(response == "success"){
                        $(".msg_sec").html('<div class="success-msg">User Status '+ustatus+' Successfully.</div>')
                    }else{
                        $(".msg_sec").html('<div class="success-msg">Something went wrong.</div>')
                    }
                    
                    $(".msg_sec").show();
                }
            });
        }
    });
</script>
    @stop 