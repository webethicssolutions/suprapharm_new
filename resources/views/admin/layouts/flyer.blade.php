<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" type="image/x-icon" href="{{ url('resources/assets/frontend/images/favicon.ico')}}">
		<title>Suprapharm - @yield('pageTitle')</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>

		<style>
			@import url('https://fonts.googleapis.com/css?family=Poppins:400,600,700&display=swap');

			body {
			  font-family: 'Poppins', sans-serif;
			  font-weight: 400;
			  color:#303030;
			  font-size: 16px;  
			  line-height:1.5;
			  margin: 0;
			}

			*, ::after, ::before {
				box-sizing: border-box;
			}

			h1,h2,h3,h4,h5,h6{
			font-weight: 700;
			margin-top:0;
			margin-bottom:20px; 
			}
			 /* Style buttons */
			.btn {
			  background-color: DodgerBlue;
			  border: none;
			  color: white;
			  padding: 12px 30px;
			  cursor: pointer;
			  font-size: 20px;
			}

			/* Darker background on mouse-over */
			.btn:hover {
			  background-color: RoyalBlue;
			} 
		</style>
	</head>
  

	<body class="hold-transition login-page hold-transition skin-blue sidebar-mini">
		<div class='container-fluid'>
			<div class='row'>
				@yield('content')
			</div>
		</div>
	</body>
</html>