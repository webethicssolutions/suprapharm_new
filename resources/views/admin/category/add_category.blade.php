@extends('admin.layouts.master')
@section('pageTitle', 'Add New Service')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							@if(Session::has('error'))
								<div class="error-msg">
								   {{Session::get('error')}}
								</div>
					        @endif
							<div class="title col-sm-12" style="margin-bottom:30px">
								<h2 >Add New Category</h2>
							</div>

							{{ Form::open(array('url' => 'admin/category/save', 'method' => 'post','class'=>'promo_form form-horizontal')) }}
							<div class="form-group col-md-12">
								<div class="row">
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Category Name') }}
											{{ Form::text('category_name', '' ,array('id'=>'couponName','class'=>'form-control')) }}
											<span class="error"> {{ $errors->first('category_name') }} </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Sub Category (Optional)') }}
											{{ Form::text('sub_category', '' ,array('id'=>'couponName','class'=>'form-control')) }}
											<span class="error"> {{ $errors->first('sub_category') }} </span>
										</div> 
										
										
								</div>
							</div>
							<div class="col-md-12">
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									<!--  <a href="{{url('admin/plans')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a> -->
								</div>
							</div>
								  {{ Form::close() }}

					</div>
					 
				</div>
			</div>
		</div>
	</div>
	</section>
	<section class="content usr-contnt">
	 <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body">
						

							 <div class="msg_sec"></div>

							   <div class="dtable_custom_controls">
								 <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
								  <tbody><tr>
									
									
								  </tr>
								  </tbody>
								</table>
							  </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th>Sr.No</th>
									<th>Title</th>
									<th>Action</th>
								  </tr>
								  </thead>
									<?php $i=1; ?>
								 @foreach($categories as $cat)
								  <tr> 
									<td><?php echo $i; ?></td>
									<td>{{$cat->category_name}}</td>
									<td class="action_links"> 
										<a href="/admin/category/edit/{{$cat->id}}" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
										<a href="javascript:void(0)" data-target="#myModal" onclick="confirm_delete('/admin/category/delete/{{$cat->id}}')" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
									</td>
								  </tr>
									<?php  $i++; ?>
								@endforeach
								</table>
							  </div>
							        </div>
                    </div>

            </div>
</section>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> 
    <script>
        $(document).ready(function(){  
			$("#discount").change(function(){ 
				var selectedOpt1 = $(this).val();  
				if(selectedOpt1 == ""){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else if(selectedOpt1 == "percent"){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else{
					$(".prcnt_sec").addClass('hide');
					$(".amount_sec").removeClass('hide');
				}
			}); 
		});
    </script>
<style>
.modal-dialog {
    margin-left: -240px !important;
}
</style>
 @stop
