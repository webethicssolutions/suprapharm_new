@extends('admin.layouts.master')
@section('pageTitle', 'Add New Service')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							@if(Session::has('error'))
								<div class="error-msg">
								   {{Session::get('error')}}
								</div>
					        @endif
							<div class="title col-sm-12" style="margin-bottom:30px">
								<h2 >Add New Pharmacy</h2>
							</div>

							{{ Form::open(array('url' => 'admin/pharmacy/save', 'method' => 'post','class'=>'promo_form form-horizontal','enctype'=>'multipart/form-data')) }}
							<div class="form-group col-md-8">
								<div class="row">
									<div class="col-md-12 col-xs-12">

                    <div class="col-md-8 col-xs-12 field">
                      <label>Holer Name</label>
                      <input type="text" class="form-control" name="name">
                      <span class="error"> {{ $errors->first('name') }} </span>
                    </div>
                    <div class="col-md-4 col-xs-12 field">
                      <label>Select Image</label>
                      <input type="file"  name="profile" class="form-control">
                      <span class="error"> {{ $errors->first('profile') }} </span>
                    </div>
                    <div class="col-md-12 col-xs-12 field">
                      <label>Address</label>
                      <input type="text" class="form-control" name="address" id="autocomplete">
                      <span class="error"> {{ $errors->first('address') }} </span>
                      <span class="error"> {{ $errors->first('lati') }} </span>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>Latitude</label>
                      <input type="input" class="form-control" name="lati" id="latitude" readonly>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>Longitude</label>
                      <input type="input" class="form-control" name="longi" id="longitude" readonly>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>City</label>
                      <input type="text" class="form-control" name="city">
                      <span class="error"> {{ $errors->first('city') }} </span>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>CIP</label>
                      <input type="text" class="form-control" name="cip">
                      <span class="error"> {{ $errors->first('cip') }} </span>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>CP</label>
                      <input type="text" class="form-control" name="cp">
                      <span class="error"> {{ $errors->first('cp') }} </span>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>Pharmacy</label>
                      <input type="text" class="form-control" name="pharmacy">
                      <span class="error"> {{ $errors->first('pharmacy') }} </span>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>URL</label>
                      <input type="text" class="form-control" name="url">
                      <span class="error"> {{ $errors->first('url') }} </span>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>Phone</label>
                      <input type="text" class="form-control" name="phone">
                      <span class="error"> {{ $errors->first('phone') }} </span>
                    </div>
                    <div class="col-md-12 col-xs-12 field">
                      <label>Description</label>
                      <textarea class="form-control" name="description"></textarea>
                      <span class="error"> {{ $errors->first('description') }} </span>
                    </div>

                    <div class="col-md-12 col-xs-12 field">
											<table class="table">
												<thead class="thead-light">
													<tr>
														<th scope="col">Lundi</th>
														<th scope="col">Mardi</th>
														<th scope="col">Mercredi</th>
														<th scope="col">Jeudi</th>
														<th scope="col">Vendredi</th>
														<th scope="col">Samedi</th>
														<th scope="col">Dimanche</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
                              <input type="text" class="form-control" name="monday">
															<span class="error"> {{ $errors->first('monday') }} </span>
														</td>
														<td>
                              <input type="text" class="form-control" name="tuesday">
															<span class="error"> {{ $errors->first('tuesday') }} </span>
														</td>
														<td>
                              <input type="text" class="form-control" name="wednesday">
															<span class="error"> {{ $errors->first('wednesday') }} </span>
														</td>
														<td>
                              <input type="text" class="form-control" name="thursday">
															<span class="error"> {{ $errors->first('thursday') }} </span>
														</td>
														<td>
                              <input type="text" class="form-control" name="friday">
															<span class="error"> {{ $errors->first('friday') }} </span>
														</td>
														<td>
                              <input type="text" class="form-control" name="saturday">
															<span class="error"> {{ $errors->first('saturday') }} </span>
														</td>
														<td>
                             <input type="text" class="form-control" name="sunday">
															<span class="error"> {{ $errors->first('sunday') }} </span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
                  </div>

								</div>
							</div>
							<div class="col-md-12">
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									 <a href="{{url('admin/pharmacy')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
								  {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
  {{-- Google Map Script --}}
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmByayAiEvKG4MdveNZZ2v5nEPbwFz6I8&language=en&libraries=places,geometry&callback=initMap"></script>
    <script type="text/javascript">
      google.maps.event.addDomListener(window, 'load', function () {
        var options = {
          componentRestrictions: {country: "fr"}
        };
            var places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var mesg = "Address: " + address;
                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
                console.log(mesg);

            });
        });
    </script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<!-- <script src="https://suprapharm.webethics.online/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> -->
    <script>
        CKEDITOR.replace( 'description',{
            allowedContent: true
        } );
    </script>

 @stop
