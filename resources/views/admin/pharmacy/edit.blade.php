@extends('admin.layouts.master')
@section('pageTitle', 'Edit Service')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->
	
		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							@if(Session::has('error'))
								<div class="error-msg">
								   {{Session::get('error')}}
								</div>
					        @endif
							<div class="title" style="margin-bottom:30px">
								<h2 >Update Pharmacy</h2>
							</div>

							{{ Form::open(array('url' => 'admin/pharmacy/update', 'method' => 'post','class'=>'promo_form form-horizontal','enctype'=>'multipart/form-data')) }}
							<div class="form-group col-md-8">
								<div class="row">
									@foreach($pharmacy as $data)
										<div class="col-md-12 row col-xs-12">
											<div class="col-md-8 col-xs-12 field">
												{{ Form::label('Holder') }}
												{{ Form::text('name', $data->name, array('id'=>'name','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('name') }} </span>
											</div>
                      <div class="col-md-4 col-xs-12 field">

                        {{ Form::label('Select Image') }}
                        
							{{ Form::file('profile',  array('id'=>'profile','class'=>'form-control')) }}

							<span class="error"> {{ $errors->first('profile') }} </span>
							@if(isset($data->profile) && !empty($data->profile))
								<img src="{{url('/images/admin/pharmacy')}}/{{$data->profile}}" height="50" width="75" style="margin-top: 5px;">
							@endif
											</div>
                      <div class="col-md-12 col-xs-12 field">
												{{ Form::label('Address') }}
												{{ Form::text('address', $data->address, array('id'=>'autocomplete','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('address') }} </span>
                        <span class="error"> {{ $errors->first('lati') }} </span>


											</div>
                      <div class="col-md-6 col-xs-12 field">
                        <label>Latitude</label>
                      <input type="input" class="form-control" name="lati" id="latitude" value="{{$data->latitude}}" readonly>
                    </div>
                    <div class="col-md-6 col-xs-12 field">
                      <label>Longitude</label>
                      <input type="input" class="form-control" name="longi" id="longitude" value="{{$data->longitude}}" readonly>
                    </div>
											<div class="col-md-6 col-xs-12 field">
												{{ Form::label('City') }}
												{{ Form::text('city', $data->city, array('id'=>'city','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('city') }} </span>
											</div>
                      <div class="col-md-6 col-xs-12 field">
												{{ Form::label('CIP') }}
												{{ Form::text('cip', $data->cip, array('id'=>'cip','class'=>'form-control','disabled')) }}
												<span class="error"> {{ $errors->first('cip') }} </span>
											</div>
                      <div class="col-md-6 col-xs-12 field">
												{{ Form::label('CP') }}
												{{ Form::text('cp', $data->cp, array('id'=>'cp','class'=>'form-control','disabled')) }}
												<span class="error"> {{ $errors->first('cp') }} </span>
											</div>
											<div class="col-md-6 col-xs-12 field">
												{{ Form::label('Pharmacy') }}
												{{ Form::text('pharmacy', $data->pharmacy, array('id'=>'pharmacy','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('city') }} </span>
											</div>
											
											<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Phone') }}
												{{ Form::text('phone', $data->phone, array('id'=>'phone','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('phone') }} </span>
											</div>
						<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Description') }}
												{{ Form::textarea('description', $data->description, array('id'=>'description','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('description') }} </span>
											</div>
						@php
							$days = unserialize($data->timing);
						@endphp
						<div class="col-md-12 col-xs-12 field">
											<table class="table">
												<thead class="thead-light">
													<tr>
														<th scope="col">Lundi</th>
														<th scope="col">Mardi</th>
														<th scope="col">Mercredi</th>
														<th scope="col">Jeudi</th>
														<th scope="col">Vendredi</th>
														<th scope="col">Samedi</th>
														<th scope="col">Dimanche</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															{{ Form::text('monday', $days['monday'], array('id'=>'monday','class'=>'form-control')) }}
															<span class="error"> {{ $errors->first('monday') }} </span>
														</td>
														<td>
															{{ Form::text('tuesday', $days['tuesday'], array('id'=>'tuesday','class'=>'form-control')) }}
															<span class="error"> {{ $errors->first('tuesday') }} </span>
														</td>
														<td>
															{{ Form::text('wednesday', $days['wednesday'], array('id'=>'wednesday','class'=>'form-control')) }}
															<span class="error"> {{ $errors->first('wednesday') }} </span>
														</td>
														<td>
															{{ Form::text('thursday', $days['thursday'], array('id'=>'thursday','class'=>'form-control')) }}
															<span class="error"> {{ $errors->first('thursday') }} </span>
														</td>
														<td>
															{{ Form::text('friday', $days['friday'], array('id'=>'friday','class'=>'form-control')) }}
															<span class="error"> {{ $errors->first('friday') }} </span>
														</td>
														<td>
															{{ Form::text('saturday', $days['saturday'], array('id'=>'saturday','class'=>'form-control')) }}
															<span class="error"> {{ $errors->first('saturday') }} </span>
														</td>
														<td>
															{{ Form::text('sunday', $days['sunday'], array('id'=>'sunday','class'=>'form-control')) }}
															<span class="error"> {{ $errors->first('sunday') }} </span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

											<div class="clearfix"></div>
											<input name="id" type="hidden" value="{{$data->id}}" />
										</div>
									@endforeach
								</div>
							</div>

							<div class="form-group col-md-12">
								 <div class="sign-up-btn ">
									 <input name="update_pharmacy" class="loginmodal-submit btn btn-primary" id="pharmacy_update" value="Update" type="submit">
									 <a href="{{url('admin/pharmacy')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
								  {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
{{-- Google Map Script --}}
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmByayAiEvKG4MdveNZZ2v5nEPbwFz6I8&language=en&libraries=places,geometry&callback=initMap"></script>
    <script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
		var options = {
          componentRestrictions: {country: "fr"}
        };
		var places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);
		google.maps.event.addListener(places, 'place_changed', function () {
			var place = places.getPlace();
			var address = place.formatted_address;
			var latitude = place.geometry.location.lat();
			var longitude = place.geometry.location.lng();
			var mesg = "Address: " + address;

			$('#latitude').val(latitude);
				$('#longitude').val(longitude);
		});
	});
</script>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<!-- <script src="https://suprapharm.webethics.online/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> -->
    <script>
        CKEDITOR.replace( 'description',{
    allowedContent: true
} );
    </script>

 @stop
