@extends('admin.layouts.master')
@section('pageTitle', 'Image Details')
@section('style')
<style>
#uploadcsv
{
	padding-right:45% !important;
}
#myModal
{
	padding-right:45% !important;
}
#myModal1
{
	padding-right:45% !important;
}
</style>
@endsection
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  {{--- @include('admin.common.breadcrumb') ----}}

        <!-- Main content -->
        <div class="dtable_custom_controls">

         <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
          <tbody><tr>
            <td><a href="{{url('admin/pharmacy/create')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="" type="submit">Add Pharmacy</a>
            <a href="javascript:void(0)"  data-target="#myModal1" data-toggle="tooltip" onclick="upload_csv('{{url('admin/uploadcsv')}}')" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Import</a></td>



          </tr>
          </tbody>
        </table>
      </div>
        <section class="content">
        	<div class ="table-title">

              </div>
          <div class="row">
            	<div class="col-lg-12">
                <div class="box box-primary">
                  <!-- /.box-header -->
                  <div class="box-body">
      							@if(Session::has('success'))
      							 <div class="success-msg">
      								{{ Session::get('success') }}
      							 </div>
      							 @endif
      							  @if(Session::has('error'))
      								   <div class="success-msg">
      									  {{ Session::get('error') }}
      							    	</div>
      							 @endif

      							 <div class="msg_sec"></div>


      							  <div class="table-responsive">
      								<table id="images" class="table table-bordered table-striped">
      								  <thead>
      								  <tr>
                        <th>Holder</th>
                        <th>CIP</th>
                        <th>Pharmacy</th>
                        <th>Address</th>
                        <th>CP</th>

                        <th>City</th>
                        <th>Date of Entry</th>
                        <th>URL</th>

                        <!--<th>Gender</th>
                        <th>Date of birth</th>
                        <th>Registred On</th>-->
                        <th>Action</th>
      								  </tr>
      								  </thead>

      								  @foreach($pharmacy as $key=>$pharm_acy)
      								  <tr>
                          <td>{{ $pharm_acy->name }}</td>
                          <td>{{ $pharm_acy->cip }}</td>
                          <td>{{ $pharm_acy->pharmacy }}</td>
                          <td>{{ $pharm_acy->address }}</td>
                          <td>{{ $pharm_acy->cp }}</td>
                          <td>{{ $pharm_acy->city }}</td>
                          <td>{{ date('d F, Y h:i:s',strtotime($pharm_acy->created_at)) }}</td>
                          <td>{{ $pharm_acy->url ?? "N/A" }}</td>

      									<td class="action_links">

                          <a href ="{{url('admin/pharmacy/edit')}}/{{$pharm_acy->id}}" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
      										<a href ="javascript:void(0)"  data-target="#myModal" data-toggle="tooltip" onclick="confirm_delete('{{url('admin/pharmacy/delete')}}/{{$pharm_acy->id}}')" data-original-title="Delete"  ><i class="fa fa-trash-o"></i></a>

      									</td>
      								  </tr>
      								  @endforeach
      								</table>
      							  </div>
                    </div>
              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="uploadcsv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        </div>
        <div class="modal-body">
     <h4 class="modal-title" id="myModalLabel">Upload Csv</h4>
        </div>
        <form action="{{ url('admin/importcsv') }}" method="POST" enctype="multipart/form-data">
   {{ csrf_field() }}
     <div class="modal-footer">
          <input type="file" name="import_file" class = "form-control" required>
          <span class="error"> {{ $errors->first('import_file')  }} </span>
          <input type="submit" name="button" value="Upload">
        </div>
      </form>
     </div>
  </div>

</div>
@stop
