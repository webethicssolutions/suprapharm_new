@extends('admin.layouts.master')
@section('pageTitle', 'Image Details')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  {{--- @include('admin.common.breadcrumb') ----}}
        <!-- Main content -->
		<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                      <td><a href="{{url('admin/add_promotion')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Add Promotion</a>
 <input type="submit"  class="btn btn-danger delete-All" Value="Delete">
</td>


                    </tr>
                    </tbody>
                  </table>
                </div>
        <section class="content">
        	<div class ="table-title">

              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body">
							@if(Session::has('success'))
							 <div class="success-msg">
								{{ Session::get('success') }}
							 </div>
							 @endif
							  @if(Session::has('error'))
								   <div class="success-msg">
									  {{ Session::get('error') }}
							    	</div>
							 @endif

							 <div class="msg_sec"></div>


							  <div class="table-responsive">
								<table id="images" class="table table-bordered table-striped">
								  <thead>
								  <tr>
								  <th><input type="checkbox" id="Check_all"> Select All</th>
								   <th>Image</th>


                  <!--<th>Gender</th>
                  <th>Date of birth</th>
                  <th>Registred On</th>-->
                  <th>Action</th>
								  </tr>
								  </thead>

								  @foreach($promotions as $key=>$promotion)
								  <tr>
															  <td><input type="checkbox" class="checkbox1" data-id="{{$promotion->id}}"></td>

                    <td><img src="{{url('public/images/admin/images', $promotion->name) }}" alt="Any alt text" width="150" height="130"/>
</td>












									<td class="action_links">
                    <label class="switch">							@if($promotion->status == "enable")
								  <input type="checkbox" id="on_off" name="select_mag" value='{{$promotion->id}}' class="select_pro" checked>
								@else
								  <input type="checkbox" id="on_off1" name="select_mag" value='{{$promotion->id}}' class="select_pro">
								@endif
							<span class="slider round"></span>
						</label>


										<a href ="javascript:void(0)"  data-target="#myModal" data-toggle="tooltip" data-original-title="Delete" onclick="confirm_delete('{{url('admin/deletepromotion')}}/{{$promotion->id}}')" ><i class="fa fa-trash-o"></i></a>

									</td>
								  </tr>
								  @endforeach
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade empty_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Please select atleast one magzine!</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<style>
#myModal
{
	padding-right:45% !important;
}
#myModal1
{
	padding-right:45% !important;
}
</style>

    @stop
