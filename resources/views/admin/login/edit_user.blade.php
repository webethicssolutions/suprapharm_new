@extends('admin.layouts.master')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

					<div class="bottom">
					<div class="container">

					<div class="row">

							<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->

					<div class="col-md-7 col-md-offset-2">

					<div class="panel-default">
					<div class="panel-body">
						 @if(Session::has('success'))
					                 <div class="success-msg">
					                   {{Session::get('success')}}
					                 </div>
					                 @endif
										<div class ="user_profile" style="margin-bottom:30px">
											<h2 >User Profile</h2>
											<a href="{{ url('admin/change-password')}}" id="change_password" class="btn btn-primary">change password</a>
					         </div>
												<?php
												$user_id = Session::get('user_id');
												 foreach($user_data as $key=>$post){
                           							/*echo "<pre>";
                           							print_r($user_data);die;*/
									            if($post->image!=='' && $post->image!==NULL){
												    	$image = $post->image;
															$username = $post->username;
													// echo	$post->about;die;


															$email = $post->email;
														//echo $image;

												     $pic = url('/storage/uploads/users/'.$user_id.'/'.$image);
												}
												else{
												    $pic = 'images/user-profile.png';
													$username = $post->username;
													$email = $post->email;
												}
                    
											}
											?>
												{{ Form::open(array('url' => 'admin/user-edit', 'method' => 'post','class'=>'profile','enctype'=>'multipart/form-data')) }}


												
												  <div class="preview-img">
													  
													@if($post->image)
												  <img src="{{ url('storage/app/public/uploads/users/'.$user_id.'/'.$post->image) }}" id="imagePreview" alt="Preview Image" class="center-block"/>
													
													  @else
													       <img class="img-responsive" src="{{ url('resources/assets/frontend/images/noprofile.gif') }}" alt="logo" id="imagePreview" alt="Preview Image" class="center-block"/>
													  @endif
												  </div>
												  <input type="file" name="image" id="imageUpload" class="hide"/> 
												  <label for="imageUpload" class="btn btn-primary">Change Profile Picture</label>		  
												 </div>
												<!--div class="form-group">
												{{ Form::label('Username') }}
												{{ Form::text('username', $username ,array('class'=>'form-control','placeholder'=>'','disabled' => 'disabled')) }}
												</div-->
												<div class="form-group">
												{{ Form::label('Email') }}
												{{ Form::text('email', $email ,array('class'=>'form-control','placeholder'=>'','disabled' => 'disabled')) }}
												</div>
												<div class="form-group">
														{{ Form::label('First Name') }}
														{{ Form::text('firstname',ucwords($post->first_name),array('class'=>'form-control','placeholder'=>'First Name')) }}
														<span class="error">  {{ $errors->first('firstname')  }} </span>
												</div>
												<div class="form-group">
														{{ Form::label('Last Name') }}
														{{ Form::text('lastname',ucwords($post->last_name),array('class'=>'form-control','placeholder'=>'Last Name')) }}
														<span class="error">  {{ $errors->first('lastname')  }} </span>
												</div>
												<div class="form-group">
														{{ Form::label('Phone no.') }}
														{{ Form::text('phone',ucwords($post->phone),array('class'=>'form-control','placeholder'=>'Phone')) }}
														<span class="error">  {{ $errors->first('phone')  }} </span>
												</div>
												<div class="form-group">
														{{ Form::label('City') }}
														{{ Form::text('city',ucwords($post->city),array('class'=>'form-control','placeholder'=>'City')) }}
														<span class="error">  {{ $errors->first('city')  }} </span>
												</div>
												<div class="form-group">
														{{ Form::label('State') }}
														{{ Form::text('state',ucwords($post->state),array('class'=>'form-control','placeholder'=>'State')) }}
														<span class="error">  {{ $errors->first('state')  }} </span>
												</div>
												<div class="form-group">
														{{ Form::label('Zip') }}
														{{ Form::text('zip',ucwords($post->zip),array('class'=>'form-control','placeholder'=>'Zip')) }}
														<span class="error">  {{ $errors->first('zip')  }} </span>
												</div>
												<div class="form-group">
														{{ Form::label('Occupation') }}
														{{ Form::text('occupation',ucwords($post->occupation),array('class'=>'form-control','placeholder'=>'Occupation')) }}
														<span class="error">  {{ $errors->first('occupation')  }} </span>
												</div>
												<?php
														 if($post->gender == 'male'){$male ='true';}else {$male='';}
														 if($post->gender == 'female'){$female ='true';}else {$female='';}
														 ?>
												<label class="radio-inline">
															{{ Form::radio('gender', 'male',$male, ['class' => '']) }}
															Male
														</label>
														<label class="radio-inline">
															{{ Form::radio('gender', 'female',$female, ['class' => '']) }}
															Female
														</label>
												
													    <div class="clearfix"></div>
												    	 <input type="hidden" name="id" value="<?php echo $post->id; ?>">

															 <div class="sign-up-btn ">
																 <input name="login" class="btn btn-primary" id="profile_update" value="Update" type="submit">
														  	</div>
													  {{ Form::close() }}
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>

<script>
	$('#imageUpload').change(function(){			
	readImgUrlAndPreview(this);
	function readImgUrlAndPreview(input){
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {			            	
		$('#imagePreview').attr('src', e.target.result);

	}
	};
	reader.readAsDataURL(input.files[0]);
	}	
	});
</script>
    @stop
