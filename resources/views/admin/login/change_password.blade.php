@extends('admin.layouts.master')
 @section('title', 'Moodframe Admin: Change Password')
  @section('breadcrumb', 'Change Password')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->
        <section class="content">

<div class="bottom">
<div class="container">

<div class="row">

<div class="col-md-8 col-md-offset-2">

<div class="panel-default">
<div class="panel-body">
	<?php $user_id = Session::get('user_id');?>
	 @if(Session::has('success'))
                 <div class="success-msg">
                   {{Session::get('success')}}
                 </div>
                 @endif
					<h2>Change Password</h2>
					{{ Form::open(array('url' => 'admin/password-user', 'method' => 'post','class'=>'profile','enctype'=>'multipart/form-data')) }}
					<div class="form-group">
							{{ Form::label('New Password') }}
							{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
							<span class="error" >  {{ $errors->first('password')  }} </span>
					</div>
					<div class="form-group">
							{{ Form::label('Confirm Password') }}
							{{ Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Password')) }}
							<span class="error" >  {{ $errors->first('password_confirmation')  }} </span>
					</div>
					<div class="sign-up-btn ">

							<input name="login" class="btn btn-primary" id="profile_update" value="Update" type="submit">
							<a href="{{url('admin/useredit')}}/{{$user_id}}" id="change_password" class="btn btn-primary">Back</a>
					 </div>
 		  {{ Form::close() }}
</div>
</div>
</div>
</div>
</div>
</div>

@endsection
