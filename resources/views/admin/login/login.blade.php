@extends('admin.layouts.master')
@section('pageTitle', 'Login')
@section('content')
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url('/admin')}}">Suprapharm Login</a>
        </div>
        <!--/.nav-collapse -->
      </div>
    </nav>
	<div class="login_area">
<div class="login-box">
  <div class="login-logo">
    <a href="#">Login</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
  	 @if(Session::has('error'))
  			<p class="alert alert-error">{{ Session::get('error') }}</p>
  	 @endif
	    {{ Form::open(array('url' => 'admin/checklogin', 'method' => 'post')) }}
      	   <div class="form-group has-feedback">
           		{{ Form::text('email', '',array('class'=>'form-control','placeholder'=>'Email')) }}
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      		   <span class="error" >  {{ $errors->first('email')  }} </span>
            </div>
            <div class="form-group has-feedback">
               {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        		   <span class="error" >  {{ $errors->first('password')  }} </span>
            </div>
            <div class="row">
                      <div class="col-xs-12">
              			{{ Form::submit('Sign In',array('class'=>'btn btn-primary btn-block btn-flat')) }}
                      </div>
            </div>
	{{ Form::close() }}
  </div>
@stop