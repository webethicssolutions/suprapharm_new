@extends('admin.layouts.master')
@section('pageTitle', 'Subscriptions')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.user_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.member_sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  {{--- @include('admin.common.breadcrumb') ----}}
        <!-- Main content -->
        <section class="content">
        	<div class ="table-title">
                
              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body"> 
							@if(Session::has('success'))
							 <div class="success-msg">
								{{ Session::get('success') }}
							 </div>
							 @endif 
							  @if(Session::has('error'))
								   <div class="success-msg">
									  {{ Session::get('error') }}
							    	</div> 
							 @endif
							 
							 <div class="msg_sec"></div>
						
    							<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                     
               
                      
                    </tr>
                    </tbody>
                  </table>
                </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
                  <th>Service</th>
                  <th>Amount</th>
									<th>Date</th>
                  <th>Next Billing Date</th>
                  <th>Action</th>
								  </tr>
								  </thead>

								  @foreach($subscription as $sub)
								  <tr>
                    <td>
                       {{get_service_name($sub->service_id)}}
                    </td>
									<td> €{{get_service_amount($sub->service_id)}}</td>									
								 <td> {{ date('d F, Y h:i:s',strtotime($sub->created_on)) }}</td> 
                  <td><?php
              if($sub->next_billing_date!=""){
                  $arr=explode('/',$sub->next_billing_date);
                   $s = $arr[1].'/'.$arr[0].'/'.$arr[2];

                  $date = strtotime($s);
                  echo date('d F, Y ', $date);
                  }
              ?></td>                 
                 <td class="action_links"> 
                 
                    <a href="{{url('cancel-subscription/'.$sub->service_id)}}" data-toggle="tooltip" data-original-title="Cancel subscription"><i class="fa fa-times"></i></a> </td>

								  </tr>
								  @endforeach
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>
	  
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
	
<style>
#myModal {
    padding-right: 45%;
}
</style>
    @stop 