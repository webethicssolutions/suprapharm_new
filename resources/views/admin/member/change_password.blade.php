@extends('admin.layouts.master')
@section('pageTitle', 'Change Password')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.user_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.member_sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->
        <section class="content">

<div class="bottom">
<div class="container">

<div class="row">

<div class="col-md-8 col-md-offset-2" style="box-shadow: 0 0 40px 0 rgba(0,0,0,0.15)">

<div class="panel-default">
<div class="panel-body">
	 @if(Session::has('success'))
     <div class="success-msg">
       {{Session::get('success')}}
     </div>
     @endif
     @if(Session::has('error'))
     <div class="error-msg">
       {{Session::get('error')}}
     </div>
     @endif
					<h2>Change Password</h2>
					{{ Form::open(array('url' => '/update_password', 'method' => 'post','class'=>'profile','enctype'=>'multipart/form-data')) }}

					<div class="form-group">
							{{ Form::label('Old Password') }}
							{{ Form::password('old_password',array('id'=>'admin_old_password','class'=>'form-control','placeholder'=>'Old Password')) }}
							<span class="error" id="admin_old_password_error" >  {{ $errors->first('old_password')  }} </span>
					</div>
					<div class="form-group">
							{{ Form::label('New Password') }}
							{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
							<span class="error" >  {{ $errors->first('password')  }} </span>
					</div>
					<div class="form-group">
							{{ Form::label('Confirm Password') }}
							{{ Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Confirm Password')) }}
							<span class="error" >  {{ $errors->first('password_confirmation')  }} </span>
					</div>
					<div class="sign-up-btn ">
							<!-- <input name="login" class="loginmodal-submit" id="change_password_admin" value="Update" type="submit"> -->
							<input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Update" type="submit">
					 </div>
 		  {{ Form::close() }}
</div>
</div>
</div>
</div>
</div>
</div>


@endsection
