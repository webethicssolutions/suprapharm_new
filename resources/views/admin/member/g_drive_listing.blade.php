@extends('admin.layouts.master')
@section('pageTitle', 'Services')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.user_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.member_sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  {{--- @include('admin.common.breadcrumb') ----}}
        <!-- Main content -->
        <section class="content">
          <div class="row">
            	<div class="col-lg-12">
            		<div class="box box-primary folder-box">
            			 @foreach ($results as $file) 
            			 @if($file['mimeType']=='application/vnd.google-apps.folder')
							  <div class="service_blk text-center">
	  
        <a href="/drive-folder/{{$file['id']}}">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4>{{ $file['name'] }}</h4></span>
        </a>
	  
	 


<!-- <div class="prc_button"><a class="pricing_button" href="https://suprapharm.webethics.online/category-files/1">View Files</a></div> -->
				</div>
				@endif
	  @endforeach
	   	
	   				</div>
              		  <div class="box box-primary">

                          <!-- /.box-header -->
                          <div class="box-body">
							
			
					
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th>Sr No.</th>
									<th>Title</th>
									<!-- <th>ID</th>
									<th>File Type</th>   
									<th>Assigned Categories</th>  -->
									<th>Action</th>
								  </tr>
								  </thead>
									<?php $i=1; 
									/* echo "<pre>";
									print_r($results);
									die; */
									?>
								  @foreach ($results as $file) 
								  @if($file['mimeType']!='application/vnd.google-apps.folder')
								  <tr> 
								  	
									<td >{{$i}}</td>
									<td> {{ $file['name'] }}</td>
									<!-- <td>{{ $file['id'] }} </td> 
									<td>{{ $file['mimeType'] }} </td> 
									<td>{{ get_file_categories($file['id'].'_'.$file['name'] ) }} </td>  -->
									<td class="action_links"> 
                 
                    <a href="/public/uploads/Google-Drive/{{$file['id'].'_'.$file['name']}}" data-toggle="tooltip" data-original-title="View" target="_blank"><i class="fa fa-tasks"></i></a>
                         <a href="/public/uploads/Google-Drive/{{$file['id'].'_'.$file['name']}}" data-toggle="tooltip" data-original-title="Download" class="dwnld-file" download><i class="fa fa-download" ></i></a> 
                       </td>
									
								  </tr>
									<?php  $i++; ?>
									@endif
								  @endforeach
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col 
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		
    </div>

<style>
#fltr-rw{
	display: none;
}
.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
	 background-color: "" !important; 
}
.glyphicon 
{
	top:0px !important;
display: table-cell !important;
	}
</style>
    @stop
