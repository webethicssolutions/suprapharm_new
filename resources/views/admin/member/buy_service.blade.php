@extends('admin.layouts.master')
@section('pageTitle', 'Make Payment')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.user_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.member_sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->
        <section class="content">

<div class="bottom">
<div class="container">

<div class="row">

<div class="col-md-8 col-md-offset-2" style="box-shadow: 0 0 40px 0 rgba(0,0,0,0.15)">

<div class="panel-default">
<div class="panel-body">
	 @if(Session::has('success'))
     <div class="success-msg">
       {{Session::get('success')}}
     </div>
     @endif
     @if(Session::has('error'))
     <div class="error-msg">
       {{Session::get('error')}}
     </div>
     @endif
     			@if(count($cards)>0)

     			<h2>Use Saved Cards</h2>
     			{{ Form::open(array('url' => '/make-payment-save-card', 'method' => 'post','class'=>'profile','enctype'=>'multipart/form-data')) }}
					<input type="hidden" name="service" value="{{$id}}">
     			<select name="existing_card" id="existing-crd" class="form-control">
     					<option value="0">Select Card</option>
     				@foreach($cards as $card)
     					<option value="{{$card->card_index}}">{{$card->card_number}}</option>
     					@endforeach;
     			</select>
     			<span class="error" >  {{ $errors->first('existing_card')  }} </span>
     			<div class="sign-up-btn ">
							<!-- <input name="login" class="loginmodal-submit" id="change_password_admin" value="Update" type="submit"> -->
							<input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Update" type="submit">
					 </div>
 		  {{ Form::close() }}
     			@endif
					<h2>Card Details</h2>
					{{ Form::open(array('url' => '/make-payment', 'method' => 'post','class'=>'profile','enctype'=>'multipart/form-data')) }}
					<input type="hidden" name="service" value="{{$id}}">
					<div class="form-group">
						<div class="col-md-6">
							{{ Form::label('Card Number') }}
							{{ Form::text('card_number','',array('id'=>'cardNumber','class'=>'form-control','placeholder'=>'Card Number','maxlength'=>"19")) }}
							<span class="error" id="admin_old_password_error" >  {{ $errors->first('card_number')  }} </span>
					</div>
					<div class="col-md-6">
							{{ Form::label('CVV') }}
							{{ Form::text('cvv','',array('id'=>'admin_old_password','class'=>'form-control','placeholder'=>'CVV','maxlength'=>"3")) }}
							<span class="error" id="admin_old_password_error" >  {{ $errors->first('cvv')  }} </span>
					</div>
				</div>
					<div class="form-group">
						<div class="col-md-6">
							{{ Form::label('Card Type') }}
							<select name="card_type" id="cardType" class="form-control">
			<option value="CB">CB</option>
			<option value="VISA">VISA</option>
			<option value="MASTERCARD">MASTERCARD</option>
			<option value="AMEX">AMEX</option>
			<option value="FNAC">FNAC</option>
			<option value="PRINTEMPS">PRINTEMPS</option>
			<option value="SURCOUF">SURCOUF</option>
			<option value="KANGOUROU">KANGOUROU</option>
			<option value="AURORE">AURORE</option>
			<option value="SOFINCO">SOFINCO</option>
			<option value="DINERS">DINERS</option>
			<option value="COFINOGA">COFINOGA</option>
			<option value="CYRILLUS">CYRILLUS</option>
			<option value="PASS">PASS</option>
			<option value="CBPASS">CBPASS</option>
			<option value="JCB">JCB</option>
			<option value="MANDARINE">MANDARINE</option>
			<option value="OKSHOPPING">OKSHOPPING</option>
			<option value="PAYPAL">PAYPAL</option>
			<option value="CASINO">CASINO</option>
			<option value="CDGP">CDGP</option>
			<option value="MCVISA">MCVISA</option>
			<option value="IDEAL">IDEAL</option>
			<option value="ELV">ELV</option>
			<option value="CHEQUE">CHEQUE</option>
			<option value="1EURO.COM">1EURO.COM</option>
			<option value="NETELLER">NETELLER</option>
			<option value="PAYLIB">PAYLIB</option>
			<option value="SWITCH">SWITCH</option>
			<option value="MASTERCARDPASS">MASTERCARDPASS</option>
			<option value="PAYFAIR">PAYFAIR</option>
			<option value="MAESTRO">MAESTRO</option>
			<option value="INTERNET+">INTERNET+</option>
			<option value="PAYSAFECARD">PAYSAFECARD</option>
			<option value="VISAPREPAID">VISAPREPAID</option>
			<option value="AMEX-REC BILLING">AMEX-REC BILLING</option>
			<option value="AMEX-ONE CLICK">AMEX-ONE CLICK</option>
			<option value="YANDEX">YANDEX</option>
			<option value="TICKETSURF">TICKETSURF</option>
			<option value="WEXPAY">WEXPAY</option>
			<option value="NEOSURF">NEOSURF</option>
			<option value="MAXICHEQUE">MAXICHEQUE</option>
			<option value="EMONEO">EMONEO</option>
			<option value="BCMC">BCMC</option>
			<option value="PINPAID">PINPAID</option>
			<option value="SDD">SDD</option>
			<option value="ILLICADO">ILLICADO</option>
			<option value="CADEAU_ACCORD">CADEAU_ACCORD</option>
			<option value="3XONEY">3XONEY</option>
			<option value="3XONEY_SF">3XONEY_SF</option>
			<option value="4XONEY">4XONEY</option>
			<option value="4XONEY_SF">4XONEY_SF</option>
			<option value="DISCOVER">DISCOVER</option>
			<option value="UKASH">UKASH</option>
			<option value="TOTALGR">TOTALGR</option>
			<option value="MONEYCLIC">MONEYCLIC</option>
			<option value="MASTERPASS">MASTERPASS</option>
			<option value="PRZELEWY24">PRZELEWY24</option>
			<option value="SOFORT">SOFORT</option>
			<option value="GIROPAY">GIROPAY</option>
			<option value="YAPITAL">YAPITAL</option>
			<option value="LYDIA">LYDIA</option>
			<option value="ANCV">ANCV</option>
			<option value="3XCB">3XCB</option>
			<option value="VME">VME</option>
			<option value="SACARTE">SACARTE</option>
			<option value="SKRILL(MONEYBOOKERS)">SKRILL(MONEYBOOKERS)</option>
			<option value="LEETCHI">LEETCHI</option>
			<option value="BUYSTER">BUYSTER</option>
		</select>
							<span class="error" >  {{ $errors->first('card_type')  }} </span>
					</div>
					<div class="col-md-6">
							{{ Form::label('Expiry Date') }}
							{{ Form::text('expiry_date','',array('id'=>'expiryDD','class'=>'form-control','placeholder'=>'MM/YY','maxlength'=>"5")) }}
							<span class="error" >  {{ $errors->first('expiry_date')  }} </span>
					</div>
					</div>
					<h2>Billing Address</h2>
					<div class="form-group">
						<div class="col-md-6">
							{{ Form::label('Street Number') }}
							{{ Form::text('street_number',$address,array('id'=>'admin_old_password','class'=>'form-control','placeholder'=>'Street Number')) }}
							<span class="error" id="admin_old_password_error" >  {{ $errors->first('street_number')  }} </span>
					</div>
					<div class="col-md-6">
							{{ Form::label('Area') }}
							{{ Form::text('area',$area,array('id'=>'admin_old_password','class'=>'form-control','placeholder'=>'Area')) }}
							<span class="error" id="admin_old_password_error" >  {{ $errors->first('area')  }} </span>
					</div>
				</div>
					<div class="form-group">
						<div class="col-md-6">
							{{ Form::label('City') }}
							{{ Form::text('city',$city,array('class'=>'form-control','placeholder'=>'City')) }}
							<span class="error" >  {{ $errors->first('city')  }} </span>
					</div>
					<div class="col-md-6">
							{{ Form::label('Zip Code') }}
							{{ Form::text('zip_code',$pin,array('class'=>'form-control','placeholder'=>'Zip Code')) }}
							<span class="error" >  {{ $errors->first('zip_code')  }} </span>
					</div>
					</div>
					<div class="sign-up-btn ">
							<!-- <input name="login" class="loginmodal-submit" id="change_password_admin" value="Update" type="submit"> -->
							<input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Update" type="submit">
					 </div>
 		  {{ Form::close() }}
</div>
</div>
</div>
</div>
</div>
</div>


@endsection
