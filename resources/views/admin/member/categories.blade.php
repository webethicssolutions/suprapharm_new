@extends('admin.layouts.master')
@section('pageTitle', 'Services')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.user_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.member_sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				 @if(Session::has('success'))
     <div class="success-msg">
       {{Session::get('success')}}
     </div>
     @endif
     @if(Session::has('error'))
     <div class="error-msg">
       {{Session::get('error')}}
     </div>
     @endif
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary folder-box">
						@foreach($categories as $cat)
	  <div class="service_blk text-center">
	  
        <a href="{{url('category-files/'.$cat->id)}}">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4>{{$cat->category_name}}</h4></span>
        </a>
	  
	 


<!-- <div class="prc_button"><a class="pricing_button" href="{{url('category-files/'.$cat->id)}}">View Files</a></div> -->
				</div>
	   @endforeach
				</div>
			</div>
		</div>
	</div>
</section>


 @stop
 
 