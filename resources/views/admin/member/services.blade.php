@extends('admin.layouts.master')
@section('pageTitle', 'Services')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.user_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.member_sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				 @if(Session::has('success'))
     <div class="success-msg">
       {{Session::get('success')}}
     </div>
     @endif
     @if(Session::has('error'))
     <div class="error-msg">
       {{Session::get('error')}}
     </div>
     @endif
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary">
						@foreach($services as $service)
	  <div class="col-md-4 col-lg-4 service_blk text-center">
	  	<div class="services">
	  	<h2>{{$service->title}}</h2>
	  <ul class="list-unstyled mt-3 mb-4 planlist">
						
							<?php $desc=explode(',',$service->description);?>
							@foreach($desc as $des)
							<li>{{$des}}</li>
						@endforeach
								
						</ul>
<h4 class="my-0">€{{$service->amount}} <sub>/ month</sub></h4>
@if(in_array($service->id,$subs_arr))
 <div class="prc_button"><a class="pricing_button" href="{{url('cancel-subscription/'.$service->id)}}">Cancel Subscription</a></div>
<!-- <div class="prc_button"><a class="pricing_button" href="{{url('cancel-service/'.$service->id)}}">Cancel Subscription</a></div> -->
@else
<div class="prc_button"><a class="pricing_button" href="{{url('buy-service/'.$service->id)}}">Make a payment</a></div>
@endif					</div></div>
	   @endforeach
				</div>
			</div>
		</div>
	</d0iv>
</section>


 @stop
 
 