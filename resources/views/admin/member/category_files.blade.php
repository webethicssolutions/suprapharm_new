@extends('admin.layouts.master')
@section('pageTitle', 'Subscriptions')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.user_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.member_sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  {{--- @include('admin.common.breadcrumb') ----}}
        <!-- Main content -->
        <section class="content">
        	<div class ="table-title">
                
              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body"> 
							@if(Session::has('success'))
							 <div class="success-msg">
								{{ Session::get('success') }}
							 </div>
							 @endif 
							  @if(Session::has('error'))
								   <div class="success-msg">
									  {{ Session::get('error') }}
							    	</div> 
							 @endif
							 
							 <div class="msg_sec"></div>
						
    							<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                     
               
                      
                    </tr>
                    </tbody>
                  </table>
                </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
                  <th>Sr No.</th>
                  <th>Name</th>
									<th>Action</th>
                  
								  </tr>
								  </thead>
                  <?php $i=1;?>
								  @foreach($files as $file)
								  <tr>
                    <td>{{$i}} </td>
									<td><?php $arr=explode('_',$file->image_id);
                  echo $arr[count($arr)-1];?></td>									
							
                        
                 <td class="action_links"> 
                 
                    <a href="/public/uploads/Google-Drive/{{$file->image_id}}" data-toggle="tooltip" data-original-title="View" target="_blank"><i class="fa fa-tasks"></i></a>
                         <a href="/public/uploads/Google-Drive/{{$file->image_id}}" data-toggle="tooltip" data-original-title="Download" class="dwnld-file" download><i class="fa fa-download" ></i></a> 
                       </td>

								  </tr>
                  <?php $i++;?>
								  @endforeach
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->

@if(count($sub_category)>0)
        <section class="content usr-contnt"> 
      <div class="row"> 
       <div class="title col-sm-12" style="margin-bottom:30px">
                <h2>Sub Categories</h2>
              </div>
       
        <div class="col-lg-12"> 
          <div class="box box-primary folder-box">
            @foreach($sub_category as $cat)
    <div class="service_blk text-center">
    
    
   
  <a href="{{url('sub-category-files/'.$cat->id)}}">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4>{{$cat->sub_category_name}}</h4></span>
        </a>
    


        </div>
     @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
@endif
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>
	  
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
	
<style>
#myModal {
    padding-right: 45%;
}
</style>
    @stop 