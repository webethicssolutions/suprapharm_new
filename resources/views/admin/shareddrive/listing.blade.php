@extends('admin.layouts.master')
@section('pageTitle', 'Services')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  {{--- @include('admin.common.breadcrumb') ----}}
        <!-- Main content -->
        <section class="content">
          <div class="row">
            	<div class="col-lg-12">
            		<div class="box box-primary folder-box">
            			 @foreach ($results as $file) 
            			 @if($file['mimeType']=='application/vnd.google-apps.folder')
							  <div class="service_blk text-center">
	  
        <a href="/admin/drive-folder/{{$file['id']}}">
          <span class="glyphicon glyphicon-folder-close" style="font-size:100px;"> <h4>{{ $file['name'] }}</h4></span>
        </a>
	  
	 


<!-- <div class="prc_button"><a class="pricing_button" href="https://suprapharm.webethics.online/category-files/1">View Files</a></div> -->
				</div>
				@endif
	  @endforeach
	   	
	   				</div>
              		  <div class="box box-primary">

                          <!-- /.box-header -->
                          <div class="box-body">
							@if(Session::has('success'))
							 <div class="success-msg">
								{{ Session::get('success') }}
							 </div>
							 @endif
							  @if(Session::has('error'))
								   <div class="success-msg">
									  {{ Session::get('error') }}
							    	</div>
							 @endif
				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
							 <div class="msg_sec"></div>
							 <!--input type="checkbox" id="check-all"/>Check All</input-->
							 <div class="row">
							 	<div id="fltr-rw1">
							 <div class=" col-md-5">
								<div class="form-group">
									<select name="category" id="parent_category" class="form-control fltr-select">
										<option value=''>Select Parent Category</option>
									@foreach($categories as $cat)
									<option value="{{$cat->id}}">{{$cat->category_name}}</option>
									@endforeach
									</select>
								</div>
							</div>	
							<div class=" col-md-5">
								<div class="form-group">
									<select name="sub_category" id="sub_category" class="form-control fltr-select">
									<option value=''>Select Parent Category First</option>
									</select> 
								</div>
							</div>
							<div class=" col-md-2">
								<div class="form-group">
									<button id="add-files" class="btn btn-primary fltr-select">Save</button>
								</div>
							 </div>
							  </div>
							   </div>
							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
									<th> <input type="checkbox" id="check-all"/></input></th>
									<th>Title</th>
									<th>ID</th>
									<th>File Type</th>   
									<th>Assigned Categories</th> 
								  </tr>
								  </thead>
									<?php $i=1; 
									/* echo "<pre>";
									print_r($results);
									die; */
									?>
								  @foreach ($results as $file) 
								  @if($file['mimeType']!='application/vnd.google-apps.folder')
								  <tr> 
								  	
									<td ><input type="checkbox" value="{{ $file['id'] }}_{{ $file['name'] }}" class="file-check"></td>
									<td> {{ $file['name'] }}</td>
									<td>{{ $file['id'] }} </td> 
									<td>{{ $file['mimeType'] }} </td> 
									<td>{{ get_file_categories($file['id'].'_'.$file['name'] ) }} </td> 
									
									
								  </tr>
									<?php  $i++; ?>
									@endif
								  @endforeach
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col 
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		
    </div>

<style>
#fltr-rw{
	display: none;
}
.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
	 background-color: "" !important; 
}
.glyphicon 
{
	top:0px !important;
display: table-cell !important;
	}
</style>
    @stop
