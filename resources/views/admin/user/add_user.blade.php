@extends('admin.layouts.master')
@section('pageTitle', 'Add User')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							@if(Session::has('error'))
								<div class="error-msg">
								   {{Session::get('error')}}
								</div>
					        @endif
							<div class ="col-sm-12 user_profile" style="margin-bottom:30px">
								<h2 >Add New User</h2>
							</div>
							
							{{ Form::open(array('url' => 'admin/user/admin_save_user', 'method' => 'post','class'=>'profile form-horizontal','enctype'=>'multipart/form-data')) }}
							<div class="form-group col-md-12">
								<div class="row"> 
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('First Name') }}
											{{ Form::text('first_name','',array('class'=>'form-control','placeholder'=>'First Name')) }}
											<span class="error"> {{ $errors->first('first_name')  }} </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Last Name') }}
											{{ Form::text('last_name','',array('class'=>'form-control','placeholder'=>'Last Name')) }}
											<span class="error"> {{ $errors->first('last_name')  }} </span>
										</div>  

										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Username') }}
											{{ Form::text('username','',array('class'=>'form-control','placeholder'=>'First Name')) }}
											<span class="error"> {{ $errors->first('username')  }} </span>
										</div> 
										

										<div class="clearfix"></div>
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Email') }}
											{{ Form::text('email','',array('id'=>'user_email','class'=>'form-control','placeholder'=>'Email')) }}
											<span class="error"> {{ $errors->first('email') }} </span>
										</div>
										<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Password') }}
												{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
												<span class="error" >  {{ $errors->first('password')  }} </span>
										</div>
										
										<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Gender') }}
												<select class="form-control" name="gender">
                   								<option disabled="disabled">Select</option>
	                                    		<option value="male">Male</option>
	                                    		<option value="female">Female</option>
	                                    		<option value="other">Other</option>
	                                          
	                                          </select> 
										</div>
									<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Date Of Birth') }}
												{{ Form::date('dob','',array('class'=>'form-control','placeholder'=>'Date of Birth')) }}
												<span class="error" >  {{ $errors->first('dob')  }} </span>
										</div>
										
										<div class="clearfix"></div>
										
										 																
									</div>
								
								</div>
							</div>  
								
							<div class="col-md-12"> 
								 <div class="sign-up-btn ">
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Add User" type="submit">
									 <a href="{{url('admin/users')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>			
								  {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- <script>
jQuery(document).ready(function(){ 
	jQuery(".profile").submit(function(e){
		var company_name    = jQuery("#company_name").val().trim();
		var company_number  = jQuery("#company_number").val().trim();
		var user_email      = jQuery("#user_email").val().trim();
		var contact_phone   = jQuery("#contact_phone").val().trim(); 
		
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		   
		if(company_name == ""){ 
			jQuery("#company_name").addClass("error_block"); 
			jQuery("#company_number").removeClass("error_block");  
			jQuery("#contact_phone").removeClass("error_block"); 
			jQuery("#user_email").removeClass("error_block"); 
		}else if(company_number == ""){ 
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").addClass("error_block"); 
			jQuery("#user_email").removeClass("error_block"); 
			jQuery("#contact_phone").removeClass("error_block"); 
		}else if(user_email == ""){  
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").removeClass("error_block"); 
			jQuery("#user_email").addClass("error_block");   
			jQuery("#contact_phone").removeClass("error_block");  
		}else if(user_email != "" && !emailReg.test( user_email )){ 
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").removeClass("error_block"); 
			jQuery("#user_email").addClass("error_block");   
			jQuery("#contact_phone").removeClass("error_block");  
		}else if(contact_phone == ""){    
			jQuery("#company_name").removeClass("error_block"); 
			jQuery("#company_number").removeClass("error_block"); 
			jQuery("#user_email").removeClass("error_block");  
			jQuery("#contact_phone").addClass("error_block"); 
		}else{ 
			return true;
		}
		
		e.preventDefault();
	});
})
</script> -->
 @stop
 
 