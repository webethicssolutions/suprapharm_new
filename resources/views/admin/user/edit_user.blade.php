@extends('admin.layouts.master')
@section('pageTitle', 'Edit User')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							<div class ="col-sm-12 user_profile" style="margin-bottom:30px">
								<h2 >User Profile</h2>
							</div>
							
							{{ Form::open(array('url' => 'admin/user/admin_user_edit', 'method' => 'post','class'=>'profile form-horizontal','enctype'=>'multipart/form-data')) }}
							 
							@foreach($result as $user_data)
							<div class="form-group col-md-12">
								<div class="row"> 
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('First Name') }}
											{{ Form::text('first_name', $user_data->first_name ,array('class'=>'form-control','placeholder'=>'')) }}
											<span class="error"> {{ $errors->first('first_name')  }} </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Last Name') }}
											{{ Form::text('last_name', $user_data->last_name ,array('class'=>'form-control','placeholder'=>'')) }}
											<span class="error"> {{ $errors->first('last_name')  }} </span>
										</div> 
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Username') }}
											{{ Form::text('username', $user_data->username ,array('class'=>'form-control','placeholder'=>'','readonly' => 'true')) }}
											<span class="error"> {{ $errors->first('username')  }} </span>
										</div> 
										
										
										<div class="clearfix"></div>
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Email') }}
											{{ Form::text('email',$user_data->email,array('class'=>'form-control','placeholder'=>'','readonly' => 'true')) }}
											<span class="error"> {{ $errors->first('email') }} </span>
										</div>
										

										<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Gender') }}
												<select class="form-control" name="gender">
                   								<option disabled="disabled">Select</option>
	                                    		<option value="male" @if($user_data->gender=='male'){{'selected'}}@endif>Male</option>
	                                    		<option value="female" @if($user_data->gender=='female'){{'selected'}}@endif>Female</option>
	                                    		<option value="other" @if($user_data->gender=='other'){{'selected'}}@endif>Other</option>
	                                          
	                                          </select> 
										</div>
									<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Date Of Birth') }}
												{{ Form::date('dob',$user_data->dob,array('class'=>'form-control','placeholder'=>'Date of Birth')) }}
												<span class="error" >  {{ $errors->first('dob')  }} </span>
										</div>
										
										<div class="clearfix"></div>
										
										
									
																		 																
									</div>
								
								</div>
							</div> 
							
								
							<div class="col-md-12"> 
								 <div class="sign-up-btn ">
									<input type="hidden" value="{{$user_data->id}}" name="user_edit_id" id="user_edit_id" >
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Update" type="submit">
									 <a href="{{url('admin/users')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
							@endforeach			
								  {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

    @stop
