@extends('admin.layouts.master')
@section('pageTitle', 'User details')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  {{--- @include('admin.common.breadcrumb') ----}}
        <!-- Main content -->
			<div class="dtable_custom_controls">
                   <table id="filterStatus" cellspacing="5" cellpadding="5" border="0" style="display:inline-block;">
                    <tbody><tr>
                      <td><a href="{{url('admin/user/adduser')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Add User</a></td>


                    </tr>
                    </tbody>
                  </table>
                </div>
        <section class="content">
        	<div class ="table-title">

              </div>
          <div class="row">
            	<div class="col-lg-12">
              		  <div class="box box-primary">
                          <!-- /.box-header -->
                          <div class="box-body">
						<!--	@if(Session::has('success'))
							 <div class="success-msg">
								{{ Session::get('success') }}
							 </div>
							 @endif
							  @if(Session::has('error'))
								   <div class="success-msg">
									  {{ Session::get('error') }}
							    	</div>
							 @endif

							 <div class="msg_sec"></div>-->


							  <div class="table-responsive">
								<table id="users" class="table table-bordered table-striped">
								  <thead>
								  <tr>
                  <th>Full Name</th>
                  <th>Username</th>
									<th>Email</th>

                  <th>Status</th>
                  <!--<th>Gender</th>
                  <th>Date of birth</th>
                  <th>Registred On</th>-->
                  <th>Action</th>
								  </tr>
								  </thead>

								  @foreach($user_details as $key=>$user)
								  <tr>
                    <td>{{ $user->first_name }} {{ $user->last_name }}</td>
									<td><?php

                     echo $user->username;
                   ?></td>
									<td>{{ $user->email }}</td>


									<td><label class="switch">
											@if($user->status == "enable")
												<input class="usr_status" id="on_off" data-val="{{ $user->id }}" checked="checked" type="checkbox">
											@else
												<input class="usr_status" id="on_off1" data-val="{{ $user->id }}" type="checkbox">
											@endif
											<span class="slider round"></span>
										</label>
									</td>
                 <!--<td>{{ $user->gender }}</td>
                  <td>{{ $user->dob }}</td>
                  <td>
                  <?php $date = explode(" ", $user->modified_on);

                               $timestamp = strtotime($date[0]);
                        ?>
                           {{date('d M Y', $timestamp)}} {{ $date[1] }}
                  </td>-->
									<td class="action_links">
                   <!--  <a href ="javascript:void(0)" title="manage"><i class="fa fa-tasks"></i></a> -->
                   <!-- <a href ="{{url('admin/user/manage')}}/{{$user->id}}" target="_blank" title="manage"><i class="fa fa-tasks"></i>Manage</a> -->
										<a href ="{{url('admin/user/edit')}}/{{$user->id}}" data-toggle="tooltip" data-original-title="Edit" ><i class="fa fa-pencil"></i></a>
										<a href ="javascript:void(0)"  data-target="#myModal" data-toggle="tooltip" data-original-title="Delete" onclick="confirm_delete('{{url('admin/deleteuser')}}/{{$user->id}}')" ><i class="fa fa-trash-o"></i></a>
										  <a href ="{{url('admin/user/manage')}}/{{$user->id}}" data-toggle="tooltip" data-original-title="Manage User" target="_blank"><i class="fa fa-tasks"></i></a>  
                      <a href ="{{url('admin/user/user-services')}}/{{$user->id}}" data-toggle="tooltip" data-original-title="User subscriptions" ><i class="fa fa-external-link" aria-hidden="true"></i></a>
									</td>
								  </tr>
								  @endforeach
								</table>
							  </div>
                          </div>
                    </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>

		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

         </div>
         <div class="modal-body">
			<h4 class="modal-title" id="myModalLabel">Are you Sure! Do you want to delete?</h4>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary" id="delete">Yes</button>
         </div>
      </div>
   </div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<script type="text/javascript">
$(".usr_status").change(function(){

        $(".success-msg").hide();
        $(".erorr-msg").hide();
        if($(this).data('val')!=""){
            var u_id = $(this).data('val');
            var usr_tokn = $('input[name="_token"]').val();

            var ajax_url = "";
            var ustatus = "";
            if($(this).prop("checked") == true){
                ajax_url = "/admin/user/enableuser/"+u_id;
                ustatus = "Enabled";
            }else{
                ajax_url = "/admin/user/disableuser/"+u_id;
                ustatus = "Disabled";
            }

            $.ajax({
                type: "POST",
                url: ajax_url,
                data:{'_token':usr_tokn},
                success: function (response) {
                    console.log('response '+response);
                    if(response == "success"){
                        $(".msg_sec").html('<div class="success-msg">User Status '+ustatus+' Successfully.</div>')
                    }else{
                        $(".msg_sec").html('<div class="success-msg">Something went wrong.</div>')
                    }

                    $(".msg_sec").show();
                }
            });
        }
    });
</script>
<style>
#myModal {
    padding-right: 45%;
}
</style>
    @stop
