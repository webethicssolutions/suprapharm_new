@extends('admin.layouts.master')
@section('pageTitle', 'Add User')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt"> 
			<div class="row"> 
				<!--a href="#" data-toggle="modal" data-target="#signup-next" data-dismiss="modal" class="loginmodal-submit">Next</a-->
				<div class="col-lg-12"> 
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							@if(Session::has('error'))
								<div class="error-msg">
								   {{Session::get('error')}}
								</div>
					        @endif
							<div class ="col-sm-12 user_profile" style="margin-bottom:30px">
								<h2 >Add New Promotion</h2>
							</div>
							
							{{ Form::open(array('url' => 'admin/save_promotion', 'method' => 'post','class'=>'profile form-horizontal','enctype'=>'multipart/form-data')) }}
							<div class="form-group col-md-12">
								<div class="row"> 
									<div class="col-md-8 col-xs-12">
										<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Select Image') }}
											{{ Form::file('image[]',array('class'=>'form-control','multiple'=>true,)) }}
											

											<span class="error"> {{ $errors->first('image')  }} </span>
										</div> 
										<div class="clearfix"></div>														
									</div>
								
								</div>
							</div>  
								
							<div class="col-md-12"> 
								 <div class="sign-up-btn ">
									 <input class="loginmodal-submit btn btn-primary" id="profile_back" value="Submit" type="submit">
									 <a href="{{url('admin/promotions')}}" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>			
								  {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<style>
#myModal {
    padding-right: 45%;
}
</style>
 @stop
 
 