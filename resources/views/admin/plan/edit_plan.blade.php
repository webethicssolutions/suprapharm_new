@extends('admin.layouts.master')
@section('pageTitle', 'Edit Service')
@section('content')
    <div class="wrapper">
      <!-- Main Header -->
     @include('admin.common.admin_header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.common.sidebar')
        @include('admin.common.confirm')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       @include('admin.common.breadcrumb')
        <!-- Main content -->

		<section class="content usr-contnt">
			<div class="row">
				<div class="col-lg-12">
					<div class="box box-primary">
						<div class="box-body">
							@if(Session::has('success'))
								<div class="success-msg">
								   {{Session::get('success')}}
								</div>
					        @endif
							@if(Session::has('error'))
								<div class="error-msg">
								   {{Session::get('error')}}
								</div>
					        @endif
							<div class="title" style="margin-bottom:30px">
								<h2 >Update Service</h2>
							</div>

							{{ Form::open(array('url' => 'admin/plan/update', 'method' => 'post','class'=>'promo_form form-horizontal')) }}
							<div class="form-group col-md-12">
								<div class="row">
									@foreach($plans as $data)
										<div class="col-md-8 row col-xs-12">
											<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Title') }}
												{{ Form::text('title', $data->title, array('id'=>'couponName','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('title') }} </span>
											</div> 
											<div class="col-md-12 col-xs-12 field">
												{{ Form::label('Amount') }}
												{{ Form::text('amount', $data->amount, array('id'=>'couponName','class'=>'form-control')) }}
												<span class="error"> {{ $errors->first('amount') }} </span>
											</div> 
											<div class="col-md-12 col-xs-12 field">
											{{ Form::label('Description (Comma Seprated)') }}
											<textarea name="description" placeholder="Description" class='form-control'>{{$data->description}}</textarea>
											<span class="error"> {{ $errors->first('description') }} </span>
										</div> 
											
											 
											<div class="clearfix"></div>
											<input name="plan_id" type="hidden" value="{{$data->id}}" />
										</div> 
									@endforeach
								</div>
							</div>

							<div class="form-group col-md-12">
								 <div class="sign-up-btn "> 
									 <input name="login" class="loginmodal-submit btn btn-primary" id="profile_update" value="Submit" type="submit">
									 <a href="{{url('admin/plans')}}" name="back" class="loginmodal-submit btn btn-primary" id="profile_back" value="Back" type="submit">Back</a>
								</div>
							</div>
								  {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> 
    <script>
        $(document).ready(function(){  
			$("#discount").change(function(){ 
				var selectedOpt1 = $(this).val();  
				if(selectedOpt1 == ""){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else if(selectedOpt1 == "percent"){
					$(".prcnt_sec").removeClass('hide');
					$(".amount_sec").addClass('hide');
				}else{
					$(".prcnt_sec").addClass('hide');
					$(".amount_sec").removeClass('hide');
				}
			}); 
		});
    </script>

 @stop
