<?php 
 $currentAction = \Route::currentRouteAction();
    list($controller, $method) = explode('@', $currentAction); 
    $controller = preg_replace('/.*\\\/', '', $controller);  
    pr($method,1);
    ?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>

    <!-- Sidebar Menu -->
    
    <ul class="sidebar-menu"> 
     
      
  
         <li class="@if($method=='services'){{'active'}}@endif"><a href="{{url('services')}}"><i class="fa fa-ticket text-aqua"></i> <span>Services</span></a></li>
        <li class="@if($method=='my_subscription'){{'active'}}@endif"><a href="{{url('subscription')}}"><i class="fa fa-ticket text-aqua"></i> <span>Abonnements</span></a></li>
         <li class="@if($method=='user_transaction'){{'active'}}@endif"><a href="{{url('transactions-details')}}"><i class="fa fa-ticket text-aqua"></i> <span>Paiements</span></a></li>

         <li class="@if($method=='flyer_tool'){{'active'}}@endif"><a href="{{url('flyer-tool')}}" target="_blank"><i class="fa fa-ticket text-aqua"></i> <span>Création Affiche</span></a></li>
     
     <!--   <li class="@if($method=='categories' || $method =='category_files'){{'active'}}@endif"><a href="{{url('categories')}}"><i class="fa fa-ticket text-aqua"></i> <span>Fichiers</span></a></li> -->
       <li class="@if($method=='shared_google_drive' || $method =='google_drive_by_folderID'){{'active'}}@endif"><a href="{{url('shareddrive')}}"><i class="fa fa-ticket text-aqua"></i> <span>Fichiers</span></a></li>
       <li><a href="{{url('https://avantages.suprapharm.fr/application/25/login')}}" target="_blank"><i class="fa fa-ticket text-aqua"></i> <span>Carte de fidélité</span></a></li>
       <li><a href="{{url('http://groupements.astera.coop/PUB/USR201.aspx')}}" target="_blank"><i class="fa fa-ticket text-aqua"></i> <span>Achats Astera</span></a></li>
	   <li><a href="{{url('https://www.pcmad.fr/-/grand-public/identification')}}" target="_blank"><i class="fa fa-ticket text-aqua"></i> <span>Oxypharm</span></a></li>
	    <li><a href="{{url('https://cdp.astera.coop/PUB/USR101.aspx')}}" target="_blank"><i class="fa fa-ticket text-aqua"></i> <span>Centrale Pharmaciens</span></a></li>
       
    
    </ul> 
  </section>
  <!-- /.sidebar -->
</aside>
