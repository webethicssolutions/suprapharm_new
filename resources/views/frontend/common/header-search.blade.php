<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="INSPIRO" />
    <meta name="description" content="Suprapharm">
    <!-- Document title -->
    <title>Suprapharm @yield('pageTitle')</title>
    <!-- Stylesheets & Fonts --><link href="{{ url('/assets/frontend/new/css/plugins.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="icon" href="{{ url('/assets/frontend/new/images/favicon-supra1.png')}}" type="image/gif" sizes="16x16">
    <link href="{{ url('/assets/frontend/new/css/style.css')}}" rel="stylesheet">
    <link href="{{ url('/assets/frontend/new/css/responsive.css')}}" rel="stylesheet">
	<link href="{{ url('/assets/frontend/new/css/slick-theme.css')}}" rel="stylesheet">
	<link href="{{ url('/assets/frontend/new/css/slick.css')}}" rel="stylesheet">
	</head>
<body>


    <!-- Body Inner -->    <div class="body-inner">


        <!-- Topbar -->
        <div id="topbar" class="topbar-fullwidth d-none d-xl-block d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="top-menu">
                            <li><a href="tel:0155217076"><i class="fa fa-phone"></i> 01 55 21 70 76</a></li>
                            <li><a href="mailto:contact@suprapharm.fr"><i class="fa fa-envelope"></i> contact@suprapharm.fr</a></li>
							<li><i class="fa fa-map-marker-alt"></i>Address: 108 rue Marius Aufan, 92300 Levallois Perret</li>
                        </ul>
                    </div>
                    <div class="col-md-4 d-none d-sm-block">
                        <div class="social-icons social-icons-colored-hover">
                            <ul class="top-link">
                                <li><a href="/search">Acheter en ligne</a></li>
                            </ul>
                            <ul>
                                <li class="social-facebook"><a href="https://www.facebook.com/Suprapharm/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-twitter"><a href="https://twitter.com/suprapharmgroup" target="_blank"><i class="fab fa-twitter"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Topbar -->

        <!-- Header -->
        <header id="header" data-fullwidth="true">
            <div class="header-inner">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="{{ url('/')}}" class="logo" data-src-dark="images/logo-supra1.png">
                            <img src="{{ url('/assets/frontend/new/images/logo-supra1.png')}}" alt="Polo Logo">
                        </a>
                    </div>
                    <!--End: Logo-->

                                        <!-- Search -->
                    <div id="search">
                        <div id="search-logo"><img src="{{ url('/assets/frontend/new/images/logo-supra1.png')}}" alt="Polo Logo"></div>
                        <button id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i
                                class="fa fa-times
"></i></button>
                        <form class="search-form" action="search-results-page.html" method="get">
                            <input class="form-control" name="q" type="search" placeholder="Search..."
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
                            <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                        </form>


                    </div>
                    <!-- end: search -->

                    <!--Header Extras-->
                    <div class="header-extras">
                        <ul>
                                                        <li>
                                <!--search icon-->
                                <a id="btn-search" href="#"> <i class="fa fa-search"></i></a>
                                <!--end: search icon-->
                            </li>

                        </ul>
                    </div>
                    <!--end: Header Extras-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                              @if(url()->current() == 'https://suprapharm.webethics.online/vous-%C3%AAtes-pharmacien' || url()->current() == 'https://suprapharm.webethics.online/vous-%C3%AAtes-pharmacien/nos-services' || url()->current() == 'https://suprapharm.webethics.online/vous-%C3%AAtes-pharmacien/installation-help')
                            
                                    <li> <a href='{{ url('/vous-êtes-pharmacien/nos-services')}}'>NOS SERVICES</a></li>
                                    <li> <a href='{{ url('/vous-êtes-pharmacien/installation-help')}}'>Aide à l'installation</a></li>   
                                   
                              
                              @else 
                                    <li> <a href='{{ url('/vous-êtes-pharmacien')}}' target='_blank'>Vous êtes Pharmacien</a></li>
                                    
                                    <li style='display:none;'> <a href='{{ url('/nos-services')}}'>NOS SERVICES</a></li>
                                    <li style='display:none;'> <a href='{{ url('/about_us')}}'>C’EST QUOI?</a></li>
                                    <li style='display:none;'> <a href='{{ url('/nos-services')}}'>NOS SERVICES</a></li>
                                    <li style='display:none;'> <a href='{{ url('/installation-help')}}'>Aide à l'installation</a></li>
                                    <li> <a href='{{ url('/cartes-des-pharmacies')}}'>CARTES DES PHARMACIES</a></li>
                                    <li><a href='{{ url('/contact-us')}}'>CONTACT</a></li>
                                    <li>
                                    <a href='{{ url('/admin/login')}}' target='_blank'>Connexion</a>
                                    </li> 
                                    
                              @endif
                                    


                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->
    <!-- Search Bar -->
    
			<div class="bg-darkgrey">
				<div class="container">
				  <div class="result-searchform">
					<div class="searchform hidden-print">
					  <form action="/search" method="GET" id="searchForm" role="form" data-disable="false" novalidate="novalidate" class="fv-form fv-form-bootstrap">
						<input type="hidden" id="slat" name="slat" value=""/>
						<input type="hidden" id="slng" name="slng" value=""/>
						<button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
						<div class="row row-no-padding">
						  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-12 col-sm-offset-0 searchform-margin"></div>
						  <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12 col-sm-offset-0 searchform-margin">
							<a href="javascript:void(0);" class="btn btn-primary searchform-geoloc btn-block lf_geoloc">
							  <em class="glyphicon glyphicon-map-marker"></em> <span class="hidden-sm">Géolocalisez moi</span>
							</a>
						  </div>
						  <div class="col-sm-1 col-xs-12 col-sm-offset-0 searchform-margin text-center">
							<div class="searchform-or">
							  ou
							</div>
						  </div>
						  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-sm-offset-0 searchform-margin searchform-padding text-left">
							<div class="form-group">
							  <div class="typeahead-container">
								<span class="typeahead-query">
								  <label for="query" class="sr-only">Recherche par mots-clés</label>                        
                  <input type="text" id="query" name="query" class="form-control searchform-query" placeholder="Paris, 92230, nom magasin..." value="@isset($search){{$search}}@endisset" required="" />
								</span>
							  </div>
							</div>
						  </div>
						  
						  <div class="col-md-3 col-sm-3 col-xs-12 col-sm-offset-0 searchform-margin">
						  	<button type="submit" class="btn btn-primary btn-block searchform-submit" title="Rechercher" disabled><em class="glyphicon glyphicon-search" aria-hidden="true"></em> <span class="hidden-sm">Rechercher</span></button>
						  </div>
						</div>
					  </form>
					</div>
				  </div>
				</div>
			</div>
		<!-- Search Bar End -->
