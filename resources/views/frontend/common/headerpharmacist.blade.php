<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="INSPIRO" />
    <meta name="description" content="Themeforest Template Polo">
    <!-- Document title -->
    <title>Suprapharm12 - @yield('pageTitle')</title>
    <!-- Stylesheets & Fonts --><link href="{{ url('assets/frontend/new/css/plugins.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="icon" href="{{ url('assets/frontend/new/images/favicon-supra1.png')}}" type="image/gif" sizes="16x16">
    <link href="{{ url('assets/frontend/new/css/style.css')}}" rel="stylesheet">
    <link href="{{ url('assets/frontend/new/css/responsive.css')}}" rel="stylesheet">
	<link href="{{ url('assets/frontend/new/css/slick-theme.css')}}" rel="stylesheet">
	<link href="{{ url('assets/frontend/new/css/slick.css')}}" rel="stylesheet">
	</head>
<body>


    <!-- Body Inner -->    <div class="body-inner">


        <!-- Topbar -->
        <div id="topbar" class="topbar-fullwidth d-none d-xl-block d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="top-menu">
                            <li><a href="tel:0155217076"><i class="fa fa-phone"></i> 01 55 21 70 76</a></li>
                            <li><a href="mailto:contact@suprapharm.fr"><i class="fa fa-envelope"></i> contact@suprapharm.fr</a></li>
							<li><i class="fa fa-map-marker-alt"></i>Address: 108 rue Marius Aufan, 92300 Levallois Perret</li>
                        </ul>
                    </div>
                    <div class="col-md-4 d-none d-sm-block">
                        <div class="social-icons social-icons-colored-hover">
                            <ul>
                                <li class="social-facebook"><a href="https://www.facebook.com/Suprapharm/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-twitter"><a href="https://twitter.com/suprapharmgroup" target="_blank"><i class="fab fa-twitter"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Topbar -->

        <!-- Header -->
        <header id="header" data-fullwidth="true">
            <div class="header-inner">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="{{ url('/')}}" class="logo" data-src-dark="images/logo-supra1.png">
                            <img src="{{ url('assets/frontend/new/images/logo-supra1.png')}}" alt="Polo Logo">
                        </a>
                    </div>
                    <!--End: Logo-->

                                        <!-- Search -->
                    <div id="search">
                        <div id="search-logo"><img src="{{ url('assets/frontend/new/images/logo-supra1.png')}}" alt="Polo Logo"></div>
                        <button id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i
                                class="fa fa-times
"></i></button>
                        <form class="search-form" action="search-results-page.html" method="get">
                            <input class="form-control" name="q" type="search" placeholder="Search..."
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
                            <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                        </form>


                    </div>
                    <!-- end: search -->

                    <!--Header Extras-->
                    <div class="header-extras">
                        <ul>
                                                        <li>
                                <!--search icon-->
                                <a id="btn-search" href="#"> <i class="fa fa-search"></i></a>
                                <!--end: search icon-->
                            </li>

                        </ul>
                    </div>
                    <!--end: Header Extras-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                   
                                     <li> <a href="{{ url('/nos-services')}}">NOS SERVICES</a></li>
                                    <li> <a href="{{ url('/installation-help')}}">Aide à l'installation</a></li>
                                    <li><a href="{{ url('/contact-us')}}">CONTACT</a></li>
                                  <!--  <li> <a href="{{ url('/member-access')}}">Accès membres</a></li>-->


                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->
