@foreach($pharmacy as $data)
<div class="col-lg-4 col-md-6 pharmacydiv blogbox" id="load-{{$data->id ?? ''}}">
<div class="pharmacyblk" id="element">
  <div class="pharmacyimgwrap">
    <img src="{{ url('assets/frontend/new/images/esnseigne.jpg')}}">
  </div>
  <div class="pharmacy_info">
<div class="">
<h6 class="pharmacy_title"><a href="#">{{ $data->pharmacy ?? '' }}</a></h6>

</div>

<div class="pharmacy_item_data">

<p title="Address"><i class="fa fa-map-marker-alt"></i>
<span>{{ $data->address ?? ''}}</span></p>
<p title="CP"><i class="fa fa-book-medical"></i>
<span>{{ $data->cp ?? ''}}</span></p>
<p title="Ville"><i class="fa fa-home"></i>
<span>{{ $data->city ?? ''}}</span></p>

</div>

</div>
<div class="pharmacy-bottom">
<p>Visit Here : <a href="http://pharmacie.pharmao.fr/pharmacie-thiers.html" target="_blank">pharmacie.pharmao.fr</a>
</div>
</div>
</div>

@endforeach
