<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Article;
use App\models\Slide;
use DB;
use Input;
use Validator;
use Auth;
use Redirect;
use Session;
use Response;
use Storage;
use Image;
use URL;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use View;


class HomeController extends Controller{

public function login(){
	return view("frontend.layouts.login");
}

public function cards_of_pharmacies(){
	return view("frontend.layouts.map");
}

/*
* Get pharmacy result by ajax
*/
public function loadDataAjax(Request $request){

		$output = '';
	  $id = $request->id;
  	$pharmacy = DB::table('pharmacy')->where('id','>',$id)->limit(6)->get();
		$lastId = $id;
		if(count($pharmacy)){
			foreach($pharmacy as $ph){
				$lastId = $ph->id;
			}
		}
			$view = view("frontend.common.list",compact('pharmacy'))->render();
      return response()->json(['html'=>$view,'lastId'=>$lastId]);
}



public function pharmacysearch(Request $request)
{
if($request->ajax())
{
			$output="";
			$pharmacy=DB::table('pharmacy')->where('address','LIKE','%'.$request->search."%")->get();
			$countPharmacy = count($pharmacy);
			if($pharmacy)
			{
					$data = array();
					foreach($pharmacy as $pharmy){
						$lat = $pharmy['latitude '];
						$log = $pharmy['longitude'];
						if(empty($lat) && empty($log)){
							//calculate lat anf long

							 $latlong    =   $this->get_lat_long($location); // create a function with the name "get_lat_long" given as below
							 $map        =   explode(',' ,$latlong);
							 $mapLat         =   $map[0];
							 $mapLong    =   $map[1];
							}
							$address = $pharmy['address']
					}
					$view = view("frontend.common.list",compact('pharmacy'))->render();
					return response()->json(['html'=>$view,'countPharmacy'=>$countPharmacy],'address'=>$address);
			}
			else
			{
				$msg='There is no pharmacy available on this address';
				return response()->json(['html'=>$msg]);
			}
}
}

// function to get  the address
public function get_lat_long($address){
    $address = str_replace(" ", "+", $address);
    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
    $json = json_decode($json);

    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    return $lat.','.$long;
}




/*
* Get list of pharmacies
*/
public function list_of_pharmacies(){
	$pharmacy = DB::table('pharmacy')->paginate(6);

	$lastId = 0;
	foreach($pharmacy as $ph){
		$lastId = $ph->id;
	}
	return view("frontend.layouts.list_pharmacies",compact('pharmacy','lastId'));
}

/*
* Return About Us page view
*/
public function about_us(){
	return view("frontend.layouts.about_us");
}
public function ourservices(){
	return view("frontend.layouts.master");
}
public function what_is_this(){
		$magzine = DB::table('images')->where('status','=','enable')->get();
		$promotion = DB::table('promotions')->where('status','=','enable')->get();
	    return view("frontend.layouts.home",compact('magzine','promotion'));
}
public function contact_us(){
	return view("frontend.layouts.contact");
}
public function services(){
	return view("frontend.layouts.services");
}


}
