 @include('frontend.common.headernew')
  <!-- REVOLUTION SLIDER -->
<div id="slider">
            <div id="rev_slider_21_1_wrapper" class="inspiro-slider slider-halfscreen arrows-large arrows-creative dots-creative" data-alias="Polo Design Studio" style="background-color:transparent;padding:0px;">
               <div class="banner-container">
                  <!-- START REVOLUTION SLIDER 5.1 fullscreen mode -->
                  <div id="rev_slider_21_1" class="rev_slider" style="display:none;" data-version="5.1">
                     <ul>
                        <!-- SLIDE  -->
						<li data-transition="fade" data-slotamount="10" data-speed="100"  data-masterspeed="1000" class="slide2">
                           <!-- MAIN IMAGE -->
                           <img src="{{ url('assets/frontend/new/images/slider1.jpg')}}" alt="" width="1680" height="1050" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="0" class="rev-slidebg" data-no-retina>
                           <!-- LAYERS -->
                           <!-- LAYER NR. 3 -->
                           <div class="tp-caption tp-resizeme rs-parallaxlevel-1" id="slide-98-layer-4" data-x="['left','left','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-10','0','-10','-10']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"  data-start="800" data-responsive_offset="on" style="z-index: 7;"><img src="{{ url('assets/frontend/new/images/girl.png')}}" alt=""
                              data-ww="['280px','280px','280px','240px']" data-hh="['auto','auto','auto','auto']" data-no-retina>
                           </div>
                           <!-- LAYER NR. 13 -->
                           <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_title" id="slide-98-layer-20" data-x="['right','right','center','right']" data-hoffset="['9','9','10','0']" data-y="['center','center','top','top']" data-voffset="['-10','-50','20','30']" data-fontsize="['80','80','56','50']" data-lineheight="['78','78','58','50']" data-width="['637','637','537','465']" data-height="['186','186','186','190']" data-whitespace="normal" data-transform_idle="o:1;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="800" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 17; min-width: 637px; max-width: 800px; max-width: 186px; max-width: 186px; white-space: normal; font-size: 80px; line-height: 78px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">0€ cotisation + 0 contrat = 100%
                           </div>
                           <!-- LAYER NR. 14 -->
                           <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_para" id="slide-98-layer-21" data-x="['right','right','center','center']" data-hoffset="['16','16','23','0']" data-y="['center','center','top','top']" data-voffset="['80','50','150','150']" data-fontsize="['26','26','20','20']" data-lineheight="['26','26','20','20']" data-width="['637','637','465','465']" data-height="['none','none','125','125']" data-whitespace="normal" data-transform_idle="o:1;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 18; min-width: 637px; max-width: 637px; white-space: normal; font-size: 26px; line-height: 26px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">conditions, services, communication
                           </div>
                        </li>
                        <li data-transition="fade" data-slotamount="10" data-speed="100"  data-masterspeed="1000" class="slide2">
                           <!-- MAIN IMAGE -->
                           <img src="{{ url('assets/frontend/new/images/slider1.jpg')}}" alt="" width="1680" height="1050" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="0" class="rev-slidebg" data-no-retina>
                           <!-- LAYERS -->
                           <!-- LAYER NR. 3 -->
                           <div class="tp-caption tp-resizeme rs-parallaxlevel-1" id="slide-98-layer-4" data-x="['left','left','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','-10','-10']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"  data-start="800" data-responsive_offset="on" style="z-index: 7;"><img src="{{ url('assets/frontend/new/images/girl.png')}}" alt=""
                              data-ww="['280px','280px','280px','240px']" data-hh="['auto','auto','auto','auto']" data-no-retina>
                           </div>
                           <!-- LAYER NR. 13 -->
                           <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_title" id="slide-98-layer-20" data-x="['right','right','center','right']" data-hoffset="['9','9','10','0']" data-y="['center','center','top','top']" data-voffset="['-10','20','40','40']" data-fontsize="['80','80','56','50']" data-lineheight="['78','78','58','50']" data-width="['637','637','537','465']" data-height="['186','186','186','190']" data-whitespace="normal" data-transform_idle="o:1;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="800" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 17; min-width: 637px; max-width: 800px; max-width: 186px; max-width: 186px; white-space: normal; font-size: 80px; line-height: 78px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">100% Services
                           </div>
                           <!-- LAYER NR. 14 -->
                           <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_para" id="slide-98-layer-21" data-x="['right','right','center','center']" data-hoffset="['16','16','23','0']" data-y="['center','center','top','top']" data-voffset="['80','30','120','130']" data-fontsize="['26','26','20','20']" data-lineheight="['26','26','20','20']" data-width="['637','637','465','465']" data-height="['none','none','125','125']" data-whitespace="normal" data-transform_idle="o:1;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 18; min-width: 637px; max-width: 637px; white-space: normal; font-size: 26px; line-height: 26px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">Une offre complete pour simplifier notre quotidien et reduire nos couts de structure
                           </div>
                        </li>
                        <li data-transition="fade" data-slotamount="10" data-speed="100"  data-masterspeed="1000" class="slide3">
                           <!-- MAIN IMAGE -->
                           <img src="{{ url('assets/frontend/new/images/slider1.jpg')}}" alt="" width="1680" height="1050" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="0" class="rev-slidebg" data-no-retina>
                           <!-- LAYERS -->
                           <!-- LAYER NR. 3 -->
                           <div class="tp-caption tp-resizeme rs-parallaxlevel-1" id="slide-98-layer-4" data-x="['left','left','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','-10','-10']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"  data-start="800" data-responsive_offset="on" style="z-index: 7;"><img src="{{ url('assets/frontend/new/images/girl.png')}}" alt=""
                              data-ww="['280px','280px','280px','240px']" data-hh="['auto','auto','auto','auto']" data-no-retina>
                           </div>
                           <!-- LAYER NR. 13 -->
                           <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_title" id="slide-98-layer-20" data-x="['right','right','center','right']" data-hoffset="['9','9','10','0']" data-y="['center','center','top','top']" data-voffset="['-10','-50','20','30']" data-fontsize="['80','80','56','50']" data-lineheight="['78','78','58','50']" data-width="['637','637','537','465']" data-height="['186','186','186','190']" data-whitespace="normal" data-transform_idle="o:1;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="800" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 17; min-width: 637px; max-width: 800px; max-width: 186px; max-width: 186px; white-space: normal; font-size: 80px; line-height: 78px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">100% conditions commerciales
                           </div>
                           <!-- LAYER NR. 14 -->
                           <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_para" id="slide-98-layer-21" data-x="['right','right','center','center']" data-hoffset="['16','16','23','0']" data-y="['center','center','top','top']" data-voffset="['80','50','150','150']" data-fontsize="['26','26','20','20']" data-lineheight="['26','26','20','20']" data-width="['637','637','465','465']" data-height="['none','none','125','125']" data-whitespace="normal" data-transform_idle="o:1;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 18; min-width: 637px; max-width: 637px; white-space: normal; font-size: 26px; line-height: 26px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">+ de 70 accords labos et prestataires
                           </div>
                        </li>
                        <li data-transition="fade" data-slotamount="10" data-speed="100"  data-masterspeed="1000" class="slide4">
                           <!-- MAIN IMAGE -->
                           <img src="{{ url('assets/frontend/new/images/slider1.jpg')}}" alt="" width="1680" height="1050" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="0" class="rev-slidebg" data-no-retina>
                           <!-- LAYERS -->
                           <!-- LAYER NR. 3 -->
                           <div class="tp-caption tp-resizeme rs-parallaxlevel-1" id="slide-98-layer-4" data-x="['left','left','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','-10','-10']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"  data-start="800" data-responsive_offset="on" style="z-index: 7;"><img src="{{ url('assets/frontend/new/images/girl.png')}}" alt=""
                              data-ww="['280px','280px','280px','240px']" data-hh="['auto','auto','auto','auto']" data-no-retina>
                           </div>
                           <!-- LAYER NR. 13 -->
                           <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_title" id="slide-98-layer-20" data-x="['right','right','center','right']" data-hoffset="['9','9','10','0']" data-y="['center','center','top','top']" data-voffset="['-10','-50','20','30']" data-fontsize="['80','80','56','50']" data-lineheight="['78','78','58','50']" data-width="['637','637','537','465']" data-height="['186','186','186','190']" data-whitespace="normal" data-transform_idle="o:1;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="800" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 17; min-width: 637px; max-width: 800px; max-width: 186px; max-width: 186px; white-space: normal; font-size: 80px; line-height: 78px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">0€ cotisation + 0 contrat = 100%
                           </div>
                           <!-- LAYER NR. 14 -->
                          <div class="tp-caption medium_light_black text-center tp-resizeme rs-parallaxlevel-1 slidecontent_para" id="slide-98-layer-21" data-x="['right','right','center','center']" data-hoffset="['16','16','23','0']" data-y="['center','center','top','top']" data-voffset="['80','50','150','150']" data-fontsize="['26','26','20','20']" data-lineheight="['26','26','20','20']" data-width="['637','637','465','465']" data-height="['none','none','125','125']" data-whitespace="normal" data-transform_idle="o:1;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 18; min-width: 637px; max-width: 637px; white-space: normal; font-size: 26px; line-height: 26px; font-weight: 100; color: rgba(0, 0, 0, 1.00);text-align:center;padding:0px 0px 0px 0px;border-color:rgba(255, 214, 88, 1.00);">conditions, services, communication
                           </div>
                        </li>
                     </ul>
                     <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                  </div>
               </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
         </div>
         <!-- END REVOLUTION SLIDER -->
 

        </div>
        <section class="service-section">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-4 col-md-12 servicesec">
                     <h2><i class="fa fa-arrow-up"></i><span>Click & Collect</span></h2>
                     <div class="greeboxdiv">
                        <div class="greebox">
                           <div class="greenblk">
                              <span>Je Prepare ma commande </span><br/>Avec <span class="cgreen">Mon appli</span> ou <span class="cgreen">Mon ordi</span> & <br/>
                              <strong class="boldtext">Je Recupere Mes Achats</strong><br/><strong>En Pharmacie</strong>
                           </div>
                        </div>
                     </div>
                     <div class="servicebtn text-right">
                        <a href='{{ url('/cartes-des-pharmacies')}}'>Je decouvre le services<i class="fa fa-caret-right"></i></a>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-12 servicesec">
                     <h2><i class="fa fa-file"></i><span>Scan Ordonnance</span></h2>
                     <div class="greeboxdiv">
                        <div class="greebox">
                           <div class="greenblk">
                              <span>J'ENVOIE Mon ordo Avec</span><br/>Avec <span class="cgreen">Mon appli</span> ou <span class="cgreen">Mon ordi</span><br/>
                              <strong class="boldtext">Elle sera prete</strong><br/><strong>En Pharmacie</strong>
                           </div>
                        </div>
                     </div>
                     <div class="servicebtn text-right">
                        <a href='{{ url('/cartes-des-pharmacies')}}'>Je decouvre le services<i class="fa fa-caret-right"></i></a>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-12 servicesec">
                     <h2><i class="fa fa-bicycle"></i><span>Livraison</span></h2>
                     <div class="greeboxdiv">
                        <div class="greebox">
                           <div class="greenblk">
                              <span>Faites Vous Livrer </span><br/>
                              <strong class="boldtext">Vos Medicaments</strong><br/><strong>A Domicile 7J/J!</strong>
                           </div>
                        </div>
                     </div>
                     <div class="servicebtn text-right">
                        <a href='{{ url('/cartes-des-pharmacies')}}'>Je decouvre le services<i class="fa fa-caret-right"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </section>
        <!-- END REVOLUTION SLIDER -->


        <!-- MISSION & VISSION
        <section class="box-fancy section-fullwidth text-light pt-0 section1 pb-0">
            <div class="row">
                <div class="col-lg-4 col-md-4 text-left sec1blk">
				<a href="#"><img src="{{ url('assets/frontend/new/images/img1.jpg')}}">
				<div class="overlay_bg">
                    <h2>Click and Collect</h2>
				</div>
				</a>
                </div>

                <div class="col-lg-4 col-md-4 text-left sec1blk">
                 <a href="#"><img src="{{ url('assets/frontend/new/images/img2.jpg')}}">
					<div class="overlay_bg">
						<h2>Scan Ordonance</h2>
					</div></a>
                </div>
				<div class="col-lg-4 col-md-4 text-left sec1blk">
                  <a href="#"><img src="{{ url('assets/frontend/new/images/img3.jpg')}}">
				  <div class="overlay_bg">
                    <h2>Delivery Service</h2>
				  </div>
				  </a>
                </div>
            </div>
        </section>
         end: MISSION & VISSION -->

        <!-- WHAT WE DO -->
<section class="pb-5">
      <div class="container-fluid">

                <div class="heading-text heading-section m-b-40">
                    <h2>Promotions du Mois</h2>
                </div>
				 <div  class="promotionimages">
				 <div class="row">


@foreach($promotion as $key=>$promotion)
<div class="col-md-3 promotion-grid-box">
    <div class="image">
        <a href="#"><img src="{{url('public/images/admin/images', $promotion->name) }}"></a>
    </div>
</div>
@endforeach

</div>
</div>
            </div>
        </section>
        <!-- END WHAT WE DO -->
        <!-- WHAT WE DO -->
        <section class="grey-background magazine_sec">
            <div class="container-fluid greybg-container">

	<div class="row">
				<div class="col-lg-4 slider-right borderdiv">
            <h2 class="magtitle">Magazine</h2>
				 <div  class="similar-slider">
<div class="category-grid-box-1 grid-classifieds">
    <div class="image">
        <a href="#"><img src="{{ url('assets/frontend/new/images/proslider1.jpg')}}"></a>
    </div>
</div>

@foreach($magzine as $key=>$magzine)
<div class="category-grid-box-1 grid-classifieds">
    <div class="image">
        <a href="#"><img src="{{url('public/images/admin/images', $magzine->name) }}"></a>
    </div>
</div>
@endforeach

</div>
<div class="col-sm-12 text-center mt-4">
     <a href="JavaScript:void(0);" class="btn btn-outline btndiv"><span>Télécharger le magazine</span></a>
    </div>
</div>
<div class="col-lg-4 mag_imgsec borderdiv">
<h2 class="magtitle">Carte de fidélité</h2>
<div  class="magimg">

<div class="image">
        <a href="#"><img src="{{ url('assets/frontend/new/images/proslider1.jpg')}}"></a>
    </div>
</div>
<div class="col-sm-12 text-center mt-4">
          <a href="{{url('https://avantages.suprapharm.fr/application/25/login')}}" target="_blank" class="btn btn-outline btndiv"><span>Accéder au service</span></a>
       </div>
</div>
<div class="col-lg-4 mag_imgsec borderdiv">
<h2 class="magtitle">Renouvellement Ordonnance</h2>
<div  class="magimg">

<div class="image">
        <a href="#"><img src="{{ url('assets/frontend/new/images/proslider1.jpg')}}"></a>
    </div>
</div>
</div>

</div>
            </div>
        </section>
        <!-- END WHAT WE DO -->
                <!-- Blog -->
				<section class="pt-0">
				<div class="container-fluid">
				<div class="heading-text heading-section m-b-40">
                    <h2>Des articles</h2>
                </div>
                <!-- Blog -->
					 
                
				<div id="blog" class="grid-layout post-4-columns m-b-30" data-item="post-item" data-stagger="10">
				<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
					<script type="text/javascript">
						$(document).ready(function () {
							$.ajax({
								type: 'GET',
								url: 'https://suprapharm.webethics.online/blogs/wp-json/wp/v2/posts?_embed&per_page=4',

								success: function (data) {
									var posts_html = '';
									
									
									 
									$.each(data, function (index, post) {
										
										posts_html += '<div class="post-item border">';
										 posts_html += '<div class="post-image"><a href="'+ post.link +'"><img alt="" src="'+post._embedded["wp:featuredmedia"][0].source_url+'"></a>';
										  
															
							             posts_html += '<span class="post-meta-category"><a href="'+post._embedded["wp:term"][0][0].link+'">'+post._embedded["wp:term"][0][0].name+'</a>';
								  
										 posts_html += '</span></div>';
										 posts_html += '<div class="post-item-wrap"><div class="post-item-description">  <span class="post-meta-date"><i class="fa fa-calendar-o"></i>'+ formatDate(post.date)+'</span><h2><a href="' + post.link +'">'+ post.title.rendered +'</a></h2> <p>' + post.excerpt.rendered +'</p><a href="'+ post.link +'" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>';
										 posts_html += '</div> </div>';
										 posts_html += '  </div>';
										
									});
									
									$('#blog').html(posts_html);
								},
								error: function (request, status, error) {
									alert(error);
								}
							});
						})
						function formatDate(date) {
										
										
								       var month = new Array();
										month[0] = "Jan";
										month[1] = "Feb";
										month[2] = "Mar";
										month[3] = "Apr";
										month[4] = "May";
										month[5] = "Jun";
										month[6] = "Jul";
										month[7] = "Aug";
										month[8] = "Sept";
										month[9] = "Oct";
										month[10] = "Nov";
										month[11] = "Dec";
										var d = new Date(date),
						            mont = '' + month[d.getMonth()],
									
									
									day = '' + d.getDate(),
									year = d.getFullYear();

								if (day.length < 2) 
									day = '0' + day;

								return [mont, day, year].join(', ');
							}
						;
					</script>

                    
                    
                </div>
				<div class="row">
				<div class="col-sm-12 text-center mt-4">
				<a href="{{ url('/blogs')}}" class="btn btn-outline btndiv"><span>Voir plus</span></a>
				</div>
				</div>
				</div>
                <!-- end: Blog -->
				</section>
                <!-- end: Blog -->
@include('frontend.common.footernew')
