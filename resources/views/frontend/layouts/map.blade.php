 @include('frontend.common.headernew')
<!--about-content-start-->
        <!-- Inspiro Slider -->
        <div id="slider" class="inspiro-slider innerbanner">
            <!-- Slide 2 -->
            <div class="slide background-overlay-dark kenburns" style="background-image:url('assets/frontend/new/images/office.jpg');">
                <div class="container">
                    <div class="slide-captions text-center text-light">
                        <!-- Captions -->
                        <h1>Cartes des pharmacies</h1>
                        <!-- end: Captions -->
                    </div>
                </div>
            </div>
            <!-- end: Slide 2 -->

        </div>
        <!--end: Inspiro Slider -->
<!--map-section-start-->
<section class="section py-0">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5661875.706410286!2d-2.434900432556539!3d46.139041211913174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd54a02933785731%3A0x6bfd3f96c747d9f7!2sFrance!5e0!3m2!1sen!2sin!4v1574417845594!5m2!1sen!2sin" width="100%" height="600px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</section>
<!--map-section-end-->
 @include('frontend.common.footernew')
