@include('frontend.common.headernew')
 <!-- REVOLUTION SLIDER -->
        <!-- Inspiro Slider -->
        <div id="slider" class="inspiro-slider innerbanner">
            <!-- Slide 2 -->
            <div class="slide background-overlay-dark kenburns" style="background-image:url('assets/frontend/new/images/office.jpg');">
                <div class="container">
                    <div class="slide-captions text-center text-light">
                        <!-- Captions -->
                        <h1>Contact</h1>
                        <!-- end: Captions -->
                    </div>
                </div>
            </div>
            <!-- end: Slide 2 -->

        </div>
        <!--end: Inspiro Slider -->


<section class="contactsection">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 contactinformation">
                        <h3 class="text-uppercase">Get In Touch</h3>
                        <p>The most happiest time of the day!. Suspendisse condimentum porttitor cursus. Duis nec nulla turpis. Nulla lacinia laoreet odio, non lacinia nisl malesuada vel. Aenean malesuada fermentum bibendum.</p>


                        <div class="row m-t-30">
                            <div class="col-lg-12 adressinfo">
			  <p><i class="fa fa-map-marker-alt"></i><span>108 rue Marius Aufan 92300 Levallois Perret</span></p>
			  <p><a href="tel:0155217076"><i class="fa fa-phone"></i><span>01 55 21 70 76</span></a></p>
			  <p><a href="mailto:contact@suprapharm.fr" target="_blank"><i class="fa fa-envelope"></i><span>contact@suprapharm.fr</span></a></p>

                            </div>
                        </div>
                        <div class="social-icons m-t-30 social-icons-colored">
                            <ul>
                                <li class="social-facebook"><a href="https://www.facebook.com/Suprapharm/"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-twitter"><a href="https://twitter.com/suprapharmgroup"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
    
               </div>



                    <div class="col-lg-6 contactform">

                    <div class="alert alert-success contact-success d-none"></div>
                    
                       {{ Form::open(array('url' => '', 'id' => 'contact_form','method' => 'post','enctype'=>'multipart/form-data')) }}

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="name">Name</label>
                                    <input type="text" aria-required="true" name="name" id="name" class="form-control required name" placeholder="Enter your Name" value="{{ old('name', isset($request) ? $request->name : '') }}">
                                    
                                    <span style="color:red;" class="error" id="name-error" >  {{ $errors->first('name')  }} </span>
                                  

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" aria-required="true" name="email" id="email" class="form-control required email" placeholder="Enter your Email">
                                    <span style="color:red;" class="error" id="email-error" >  {{ $errors->first('email')  }} </span>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="subject">Your Subject</label>
                                    <input type="text" name="subject" id="subject" class="form-control required" placeholder="Subject...">
                                    <span style="color:red;" class="error" id="subject-error" >  {{ $errors->first('subject')  }} </span>
                                </div>
                            </div>   <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea type="text" name="message" rows="5" id="message" class="form-control required" placeholder="Enter your Message"></textarea>
                                    <span style="color:red;" class="error" id="message-error" >  {{ $errors->first('message')  }} </span>

                                </div>
                            <button class="btn" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Send message</button>
                           
                            {{ Form::close() }}


                    </div>

                </div>
            </div>
</section>



        <!-- Footer -->
@include('frontend.common.footernew')
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
    <script src="https://homologation-payment.payline.com/scripts/widget-min.js"></script>
   <link href="https://homologation-payment.payline.com/styles/widget-min.css" rel="stylesheet" />
   <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
                                var tpj = jQuery;
                                var revapi21;
                                tpj(document).ready(function() {
                                tpj(document).on('click', '#form-submit', function(e) {
                                e.preventDefault();
                                $('.error').html("");
                                var postData = new FormData($("#contact_form")[0]);
                                $.ajax({
                                type:'POST',
                                url:'/send-email1',
                                processData: false,
                                contentType: false,
                                data : postData,
                                success:function(data){
                                console.log(data);
                                //var data = JSON.parse(data);
                                console.log(data.msg);
                                if(data.status == 'success') {
                                    //$('.success-msg').show();
                                    $('.contact-success').html(data.msg).removeClass('d-none');
                                    $("#contact_form")[0].reset();
                                    setTimeout(function(){ $('.contact-success').html('').addClass('d-none'); }, 2000);
                                
                                } 
                                },
                                error: function( json )
      { console.log(json);
          if(json.status == 422) {
              var errors = json.responseJSON;
              //console.log(errors);
              //console.log(errors);
              $.each(errors, function (key, value) {
               // console.log(key);
                  $('#'+key+'-error').html(value);
              });
          }
          else{
            alert(json['msg'])
          }
      }
                               
                                });
                                })
                                
                                            if ($("#rev_slider_21_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_21_1");
            } else {
                revapi21 = $("#rev_slider_21_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/plugins/revolution/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        onHoverStop: "off",
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [868, 768, 960, 720],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 700,
                        levels: [2, 6, 10, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51, 55],
                        type: "mouse",
                        disable_onmobile: "on"
                    },
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

    </script>
<script type="text/javascript">
      $(document).ready( function() {
  $('#deletesuccess').delay(3000).animate({opacity:1},3000);
  // $('#deletesuccess').delay(3000).fadeOut();
	// $('#deletesuccess').animate({opacity:"1"},{duration:3000}, function(){
   // $(this).css('opacity','0');
// });
      });
    </script>
</body>

</html>
