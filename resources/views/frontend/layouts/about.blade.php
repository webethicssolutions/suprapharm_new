 @include('frontend.common.headernew')

 <!--about-content-start-->
        <!-- Inspiro Slider -->
        <div id="slider" class="inspiro-slider innerbanner">
            <!-- Slide 2 -->
            <div class="slide background-overlay-dark kenburns" style="background-image:url('assets/frontend/new/images/office.jpg');">
                <div class="container">
                    <div class="slide-captions text-center text-light">
                        <!-- Captions -->
                        <h1>À propos de nous</h1>
                        <!-- end: Captions -->
                    </div>
                </div>
            </div>
            <!-- end: Slide 2 -->

        </div>
        <!--end: Inspiro Slider -->

<section>
	<div class="container">
		<div class="row text-center center-title">
			<div class="col-lg-12">
				<div class="heading-text heading-section">
					<h2>C'est Quoi?</h2>
				</div>
			</div>
			<div class="col-lg-12">
                <div class="row">
                <div class="col-lg-12 innercontent"><p>Créé en 2014 par des pharmaciens titulaires, nous sommes un réseau national de 70 pharmacies pour un Chiffre d’affaires de 200 M€ Nos valeurs : partage, transparence et indépendance Notre objectif : l’union pour simplifier notre quotidien, assurer notre développement et préserver notre indépendance</p>

 </div>


</div></div>

		</div>
	</div>
</section>


<section class="box-fancy section-fullwidth text-light p-b-0 pt-0 aboutsection">
		<div class="row">
			<div style="background-color:rgba(70, 171, 63,1)" class="col-lg-6 col-md-6 aboutblks">
				<h1 class="text-lg text-uppercase"><i class="fa fa-chart-line"></i></h1>
                <h3>Notre business Model</h3>
				<span>Nous obtenons des budgets des départements marketing et communication des fournisseurs pour faire vivre le groupement. Toutes les conditions et RFA sont directement versées aux adhérents. Rien ne transite par le groupement.</span>
			</div>

			<div style="background-color:rgba(70, 171, 63,0.9)" class="col-lg-6 col-md-6 aboutblks">
				<h1 class="text-lg text-uppercase"><i class="fa fa-award"></i></h1>
                <h3>Pour qui ?</h3>
				<span>Je veux rester indépendant Je veux maîtriser mes achats, pas d’envois automatiques Je veux une enseigne valorisante Je veux augmenter ma marge et mes ventes Je veux avoir une communication dynamique Je veux des services pour simplifier mon quotidien Je veux des échanges entre titulaires</span>
			</div>

		</div>
</section>

<section class="section py-0">
		<div class="">
			<div class="row justify-content-between gutter-vr-30px">
				<div class="col-lg-6 nopadding">
					<div class="image-block">
						<img src="{{ url('assets/frontend/new/images/iMac-Template.jpg')}}" alt="" class="img-fluid">
					</div>
				</div><!-- .col -->
				<div class="col-lg-6 d-flex align-items-center nopadding">
					<div class="text-block heading-text heading-section">
						<h2>Comment ça marche ?</h2>
						<p class="lead">Rassurer avec une enseigne commune et un Concept Store valorisant + d’entrées via une communication extérieure différenciante et percutante : Coverings et campagnes de communication + de ventes : Magazine, animation linéaire, formation collaborateurs, service de merchandising, étude de rentabilité linéaire + de fidélisation : un concept store accueillant et différenciant, coaching équipes, carte de fidélité, produits exclusifs + de marge sans se stocker : centrale de négociations fournisseurs, marques propres + de facilité du quotidien : conseils en organisation, application de gestion du merchandising et des opérations commerciales - couts : centrale de négociation prestataires Aide aux bonnes pratiques et conseils ordinaux

</p>
					</div><!-- .text-block  -->
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->
	</section>
<section class="section py-0">
		<div class="">
			<div class="row justify-content-between gutter-vr-30px reverserow">
				<div class="col-lg-6 d-flex align-items-center nopadding">
					<div class="text-block heading-text heading-section">
						<h2>Nos avantages</h2>
						<p class="lead">Plus de 70 laboratoires et prestataires Conditions max sans se stocker Envois automatiques limités les Rfa ne transitent pas par le groupement mais sont directement versées au pharmacien Plan trade annuel pour faciliter vos achats

</p>
					</div><!-- .text-block  -->
				</div><!-- .col -->
								<div class="col-lg-6 nopadding">
					<div class="image-block">
						<img src="{{ url('assets/frontend/new/images/besoins3.png')}}" alt="" class="img-fluid">
					</div>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->
	</section>



 @include('frontend.common.footernew')
