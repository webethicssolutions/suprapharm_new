@extends('frontend.layouts.master')
@section('pageTitle', 'FAQ')
@section('pageDescription', 'This is FAQ page meta description')
@section('content')
@include('frontend.common.header')
<div class="sub-navigation">
<div class="navbar tabbed">
      <ul class="menu">
        
        <li class=""><a href="{{url('billing')}}">Billing</a></li>
        <li class=""><a href="{{url('subscription')}}">Subscriptions</a></li>
        <li class=""><a href="{{url('account-settings')}}">Account settings</a></li>
        <li class="active"><a href="{{url('help')}}">FAQ</a></li>
        
    </ul>
</div>
</div>
<div class="box-b-wrap center-panel">
<div class="box-c-knowledge-base-dashboard">
<div class="box-c-dashboard-header">
<div class="box-c-dashboard-header-wrap">
<h1>
How can we help?</h1>
<div class="box-c-search">

<form class="search-wide formtastic" action="{{url('/search')}}" accept-charset="UTF-8" data-remote="true" method="post"><input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input id="kb-search" name="search" placeholder="Enter a question, keyword or topic..." required >
</form>
</div>
</div>
</div>

<div class="help-desc-dsply">
@foreach($description_help as $desc)

<h2 class="text-info">
{{ $desc->help_title }}
</h2>

{{ $desc->help_description }}
@endforeach
</div>
</div>
</div>

@include('frontend.common.footer')
 @stop