@extends('frontend.layouts.master')
@section('pageTitle', 'Billing')
@section('pageDescription', 'This is Billing page meta description')
@section('content')
@include('frontend.common.header')
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
      
        <li class="active"><a href="{{url('billing')}}">Billing</a></li>
        <li class=""><a href="{{url('subscription')}}">Subscriptions</a></li>
        <li class=""><a href="{{url('account-settings')}}">Account settings</a></li>
       <!--  <li><a href="{{url('help')}}">FAQ</a></li> -->
       
    </ul>
</div>
</div>

<div class="page container">

    <!-- <div id="content-payment-methods" class="content content-settings">

        <div class="panel panel-pad">

            <h1>Billing</h1>
            
            <div class="panel-content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Payment method</th>
                            <th>Cardholder name</th>
                            <th>Expires</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="payment-method">
                                <span class="cc-number">
                                    <img class="cc-brand" src="{{ url('resources/assets/frontend/images/card-mastercard.svg')}}">
                                    <span class="cc-mask">••••</span>
                                    <span class="cc-digits">9988</span>
                                    <span class="bullet"></span>
                                    <span class="cc-expiry">01 / 2021</span>
                                </span>
                            </td>
                            <td class="cc-name">
                                <span class="cc-name">Christian Gal</span>
                            </td>
                            <td class="actions">
                                <a href="#" class="btn remove-btn">
                                    <i class="fa fa-times" aria-hidden="true"></i> <span>Remove card</span>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        
    </div> -->

    <div id="content-transactions" class="content content-settings">

        <div class="panel panel-pad">

            <h1>Transaction history</h1>

            <div class="panel-content">
			<div class="table-responsive">
                <table class="table">
                    <thead class="">
                        <tr>
                            <th>Date</th>
                            <th>Username</th>
                            <th>Description</th>
                            <th>Payment method</th>
                            <th class="right">Total</th>
                        </tr>
                    </thead>
                    <tbody>



  @foreach($billing as $bill)
                        <tr>
                          
                            <td class="trans-date">
                                <div class="date">
                                    
                                        <?php $date = explode(" ", $bill->created_on);
                                        $timestamp = strtotime($date[0]);
?>


                                 <a target="_blank" href="{{url('pay_detail')}}/{{ $bill->transaction_id }}">{{date('M d, Y', $timestamp)}}</a>
                                    
                                </div>
                            </td>
                            <td class="trans-sub ">
                                                           {{$bill->insta_username}}
                            </td>
                            <td class="trans-desc ">
                                                        
                            </td>
                            <td class="payment-method ">
                                                            <span class="cc-number">
                                                                <!-- <img class="cc-brand" src="{{ url('resources/assets/frontend/images/card-mastercard.svg')}}"> -->
                                                                <span class="cc-digits">Stripe</span>
                                                            </span>
                            </td>
                            <td class="trans-amount right">
                                <div class="total">
                                    {{$bill->amount}} {{$bill->currency_code}}
                                </div>

                            </td>
                        </tr>
@endforeach
                    </tbody>
                </table>
            </div>
            </div>

        </div>
    </div>
</div>       
@include('frontend.common.footer')   
 @stop