@extends('frontend.layouts.master')
@section('pageTitle', 'Subscription')
@section('pageDescription', 'This is Subscription page meta description')
@section('content')
@include('frontend.common.header')

<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
     
        <li class=""><a href="{{url('billing')}}">Billing</a></li>
        <li class="active"><a href="{{url('subscription')}}">Subscriptions</a></li>
        <li class=""><a href="{{url('account-settings')}}">Account settings</a></li>
       <!--  <li><a href="{{url('help')}}">FAQ</a></li> -->
        
    </ul>
</div>
</div>

<div class="page container subscription-page">
    <div id="content-subs" class="content content-settings">
        <div class="panel panel-pad">

            <h1>Subscriptions</h1>

            <div class="panel-content">
			<div class="table-responsive">
                <table class="table">
                    <thead class="hidden-xs hidden-sm">
                        <tr>
                            <th>Username</th>
                            <th>Subscription</th>
                             <th>Amount</th>
                            <th>Next billing date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
            </div>
            </div>
        
            
        </div>
    </div>

</div>






            


@include('frontend.common.footer')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php 
// setcookie("autolove_payment",'success', time() + 30);
if(isset($_COOKIE['autolove_payment']))
  {?>

<script type="text/javascript">
  jQuery(document).ready(function(){
    


    var pay="<?php echo $_COOKIE['autolove_payment'];?>";
   // alert(pay);
    jQuery('#payment-success').modal('show');
    jQuery('#payment-success').css('margin-top','12%');
    jQuery('.icon > img').css('height','55px');
  });
</script>
<?php }
else {
?>

<?php } ?>

<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('.manage-btn').click(function(){
		var click_id=jQuery(this).attr('click-id');
		var current=jQuery(this).attr('cureent-id');
		var sub_id=jQuery(this).attr('sub-id');
        var cus_id=jQuery(this).attr('cus-id');
		var status=jQuery(this).attr('status');
        var typ=jQuery(this).attr('typ');
		//alert(click_id);
		var img=jQuery('#pic_'+click_id).attr('src');
		var name=jQuery('#name_'+click_id).html();
		 var subscription_id=jQuery(this).attr('sub-id');
		 jQuery('#agreement_id').val(subscription_id);
		 
         if(typ=='ony-l')
         {

            jQuery('.onlyy-l').show();
            jQuery('.both-lf').hide();
         }
          if(typ=='lik+fo')
         {
            jQuery('.onlyy-l').hide();
            jQuery('.both-lf').show();
         }
	 jQuery('#custom3').val(sub_id);
		$(".upgrade input:radio").each(function(){
		
		var radio_value = $(this).val();

		if(current==radio_value){
			jQuery(this).prop("checked", true);
		}
																																																																																																																			
        });
   
    if(status=='active'){
    jQuery('#manage-active').show();   
     jQuery('#manage-cancel').hide();
    jQuery('#cancel-plan').show();
    }else{
    jQuery('#active_sub_id').val(sub_id);
    jQuery('#active_cus_id').val(cus_id); 

    jQuery('#manage-active').hide(); 
     jQuery('#manage-cancel').show();  
    jQuery('#cancel-plan').hide();
    
    }

    jQuery('#mng-pop-img > img').attr('src',img);
    jQuery('.instaname-name').html(name);
    jQuery('#cancel-plan').attr('data-id',sub_id)
     jQuery('#cancel-plan').attr('cus-id',cus_id)
    $('#manageplan').modal('show');
    });
     
    $(".upgrade input:radio").change(function() {
     var price=jQuery(".upgrade input[name='subscription']:checked").attr('data-p');
   //alert(price);
   jQuery('#price1').val(price);
    var sub_id=jQuery(this).val();
	
    var name=jQuery('.instaname-name').text();
  var uid=jQuery('#uid').val();
   var custom1= sub_id;
   var custom2= name;
   jQuery('#custom1').val(custom1);
   jQuery('#custom2').val(custom2);
   
     jQuery('#btn-nxt2').prop('disabled', false );
 
   });  


jQuery('#cancel-plan').click(function(){
   
 var img=jQuery('#mng-pop-img > img').attr('src');
    $('#manageplan').modal('hide');
     jQuery('#mng-pop-img1 > img').attr('src',img);
    var id=jQuery(this).attr('data-id');
    var cus_id=jQuery(this).attr('cus-id');

    jQuery('#cancel_sub_id').val(id);
    jQuery('#cancel_cus_id').val(cus_id);

jQuery('#cancelModal').modal('show');
})
});

   
</script>
 @stop