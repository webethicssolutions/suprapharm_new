@extends('frontend.layouts.master')
@section('pageTitle', 'Home')
@section('pageDescription', 'This is home page meta description')
@section('content')
@foreach($pay_detail as $detail)

<div class="page container">
    <div class="pay-header">
    <h1>Autolove</h1>
</div>
    <div id="content-transactions" class="content content-settings">
        
        <div class="panel panel-pad">
            <h1>Transaction Detail:-</h1>
            <div class="panel-content table-responsive">
                <table class="table">
                    <thead class="">
                       <tr>
                        <td>
                            Customer<br>
                            <b>{{$detail->username}}<br>
                            {{$detail->email}}</b>
                        </td>
                        <td></td>
                        <td>
                           <span>Payment method</span><br>    
                         <span><b>Stripe</b></span>
                               
                        </td>
                        <td class="right"><span>Transaction id</span><br>    
                         <span><b>{{$detail->transaction_id}}</b></span>
                               
                        </td>

                        </tr>
                        <tr>
                            <th>DATE</th>
                            <th>DESCRIPTION</th>
                            <th>SERVICE PERIOD</th>
                            <th class="right">AMOUNT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="trans-date">
                                <div class="date"> 
									<?php 
										$date = explode(" ", $detail->created_on);
										$timestamp = strtotime($date[0]); 
									?>

									{{date('M d, Y', $timestamp)}} 
                                </div>
                            </td>
                            
                            <td class="trans-desc "><span>Autolove Subscription</span><br>{{ App\Helpers\Helper::get_plan_like($detail->subscription_id) }} 
                                                        
                            </td>
                            <td><?php echo "From " .date('M d, Y', $timestamp)." - Till ".  date('M d, Y', strtotime('+1 month', $timestamp)); ?>

                            </td>
                            <td class="trans-amount right">
                                <div class="total">
                                    {{$detail->currency_code}} {{$detail->amount}}
									@if(isset($invoice_details['discount']['coupon']))
										 ( {{ 'Coupon Applied:' }} {{$invoice_details['discount']['coupon']['id']}}, 
									
										@if(isset($invoice_details['discount']['coupon']['amount_off']))
											{{ 'FLAT' }} {{$invoice_details['discount']['coupon']['amount_off']/100}} Discount)
										@endif
										
										@if(isset($invoice_details['discount']['coupon']['percent_off']))
											{{ 'FLAT' }} {{$invoice_details['discount']['coupon']['percent_off']}}% Discount)
										@endif
									@endif
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td><b>Total Paid</b>
                            </td>
                             <td> </td>
                              <td> </td>
                            <td class="trans-amount right">
                                <div class="total"><b> 
									@if($invoice_details['amount_paid'])
										 {{$invoice_details['amount_paid']/100}} {{$invoice_details['currency']}}
									@else	
										 {{$detail->amount}} {{$detail->currency_code}}
									@endif
                                </b></div>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@endforeach  
@include('frontend.common.footer')        
 @stop