@extends('frontend.layouts.master')
@section('pageTitle', 'Account Settings')
@section('pageDescription', 'This is Account Settings page meta description')
@section('content')
@include('frontend.common.header')
<div class="sub-navigation">
<div class="navbar tabbed">
    <ul class="menu">
		
			<li class=""><a href="{{url('billing')}}">Billing</a></li>
			<li class=""><a href="{{url('subscription')}}">Subscriptions</a></li>
			<li class="active"><a href="{{url('account-settings')}}">Account settings</a></li>
		<!-- 	<li><a href="{{url('help')}}">FAQ</a></li> -->
			
    </ul>
</div>
</div>


<div class="page container">

    <div id="content-account" class="content content-settings">

        <div class="panel panel-pad">

            <div class="panel-content">
                <hr class="lg">

            <h1>Account settings</h1>
  @if(Session::has('error'))
          <span class="error">
                    {{Session::get('error')}}
            </span>
             
                @endif
            @if(Session::has('success'))
               <p style="color:green">
                    {{Session::get('success')}}
               </p>
              @else
             
                @endif
                <form method="post" action="{{url('update_email')}}" class="form typeUnlock">
                   
            {{ csrf_field() }}
                   <div class="form-title">Account email</div>
                    <div class="form-group">
                        <div class="input-group">
                            <input id="update_email" name="email" type="email" class="form-control" value="{{$email}}" placeholder="Email address" required="">
                        {{ $errors->first('email')  }}
                        </div>
                    </div>
                    <input id="save_email" type="submit" class="btn btn-green mx" value="Save email" disabled>
                </form>

                <hr class="lg">

                <form method="post" action="{{url('update_password')}}" class="form typeUnlock">
               
            {{ csrf_field() }}
                  
                    <div class="form-title">Change password</div>
                    <div class="form-group">
                        <div class="input-group">
                            <input id="currentPassword" name="old_password" type="password" class="form-control" placeholder="Current password" required="" min="">
                            {{ $errors->first('old_passowrd')  }}
                        </div>
                        <div class="input-group">
                            <input id="newPassword" name="password" type="password" class="form-control placeholder-shown" placeholder="New password" required="">
                            {{ $errors->first('password')  }}
                        </div>
                        <div class="input-group">
                            <input id="confirmPassword" name="password_confirmation" type="password" class="form-control placeholder-shown" placeholder="Confirm new password" required="">
                             {{ $errors->first('password_confirmation')  }}
                        </div>
                    </div>
                    <input type="submit" class="btn btn-green mx" value="Save password">
                </form>
            </div>

        </div>
    </div>

</div>
@include('frontend.common.footer')
 @stop