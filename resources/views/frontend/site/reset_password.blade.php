@extends('frontend.layouts.master')
@section('pageTitle', 'Home')
@section('pageDescription', 'This is home page meta description')
@section('content')
@include('frontend.common.header')
<div class="container full-height">
    <div class="row h-100 align-items-center">
  <div class="col-md-12"> 
<div class="login-form">
        
        <div class="form_container">
          <div class="form-title">Reset Password</div>
             @if(Session::has('error'))
          <span class="error">
                    {{Session::get('error')}}
            </span>
             
                @endif
  @if(!$flag)
          <form method="POST" id="payment-form"  action="{{url('/save-reset-password')}}">
           
            {{ csrf_field() }}
          
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" name="password" class="form-control input_pass" value="" placeholder="password">
               <span class="error"> {{ $errors->first('password')  }}</span>
            </div>
              <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="password"  name="password_confirmation" class="form-control input_user" value="" placeholder="Confirm password">
               <span class="error"> {{ $errors->first('password_confirmation')  }}</span>
            </div>
            @if($token !='' )
                <input name="password_token"  value="{{$token}}" type="hidden">
                @endif
                <input name="email"  value="{{$email}}" type="hidden">
            <input type="Submit" name="button" class="btn login_btn" value="Submit">
          </form>
           @else
             <h3 style="color:red"> Your Link has been expired. </h3> 

             @endif
        </div>
        
        
      </div>
      </div>      
      </div>      
    </div>  
      @include('frontend.common.footer')
 @stop