@extends('frontend.layouts.master')
@section('pageTitle', 'Home')
@section('pageDescription', 'This is home page meta description')
@section('content')
@include('frontend.common.header')
<div class="container full-height">
    <div class="row h-100 align-items-center">
  <div class="col-md-12"> 
<div class="login-form">
        
        <div class="form_container">
          <div class="form-title">Forgot password</div>

          <form method="POST" id="payment-form"  action="{{url('/forget-password-email')}}">
            @if(Session::has('error'))
          <span class="error">
                    {{Session::get('error')}}
            </span>
             
                @endif
            @if(Session::has('success'))
               <p style="color:green">
                    {{Session::get('success')}}
               </p>
              @else
             
                @endif
            {{ csrf_field() }}
            <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="text"  name="email" class="form-control input_user" value="" placeholder="Email">
               <span class="error"> {{ $errors->first('email')  }}</span>
            </div>
           
            
            <input type="Submit" name="button" class="btn login_btn" value="Submit">
          </form>
        </div>
        
        
      </div>
      </div>      
      </div>      
    </div>  
     @include('frontend.common.footer') 
 @stop