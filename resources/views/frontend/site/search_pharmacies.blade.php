@include('frontend.common.header-search')


<?php

$pharmacy = $pharmacy->toArray();
//echo "<pre>"; print_r($pharmacy); echo "</pre>"; 
if (array_key_exists('data', $pharmacy)){
  $pharmacy_arr = $pharmacy['data'];
  $per_page = $pharmacy['per_page'];
  $current_page = $pharmacy['current_page'];
  $last_page = $pharmacy['last_page'];
  $total = $pharmacy['total'];
}
else {
  $pharmacy_arr = $pharmacy;
  $total = count($pharmacy_arr);
}

?>
<div class="result">
  <div class="result-top">
    <div class="container">
      <h1 class="result-top-title title h3">Résultats pour votre recherche</h1>
      <div class="result-top-subtitle"><span><?php echo $total; ?></span> pharmacies disponibles</div>
    </div>
  </div>

  <div class="bg-white" id="result-section">
    <div class="row row-no-padding margin-bottom m-0">
      <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 hidden-xs col pull-right hidden-print p-0">
        <div id="map"></div>
      </div>
      <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 p-0">
        <div class="result-content">
          <h2 class="result-content-postype">Pharmacies</h2>
          @php
            
          if(isset($current_page)) {
			      $i = $per_page * ($current_page - 1) ;
          }
          else{
            $i = 0;
          }
          $j = $i+1;
          
          @endphp
          @foreach( $pharmacy_arr as $pharmacy_data)
          <div class="row row-no-padding result-content-pos bg-white has-page m-0" data-url="">
            <div class="has-page">
              @isset($pharmacy_data->distance)
                <div class="result-content-pos-distance medium">{{ number_format((float)$pharmacy_data->distance, 2, '.', '') }} km</div>
              @endisset
              <div class="result-content-pos-column-left">
                <div class="result-content-pos-marker">
                  <div class="result-content-pos-marker-number" aria-hidden="true">{{ $j }}</div>
                </div>
              </div>
              <div class="result-content-pos-column-middle">
                <a href="/pharmacy/{{ $pharmacy_data->id }}-{{ str_slug($pharmacy_data->name) }}" class="result-content-pos-link has-page">
                  <h3 class="h4 result-content-pos-name">{{ $pharmacy_data->pharmacy }}</h3>
                </a>
                <address class="result-content-pos-address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                  <div itemprop="streetAddress">
                    {{ $pharmacy_data->address }}
                  </div>
                  <div>
                    <span itemprop="addressLocality">{{ $pharmacy_data->city }}</span>, <span itemprop="addressRegion"></span> <span itemprop="postalCode">{{ $pharmacy_data->cp }}</span>
                  </div>
                </address>
                <!--<img class="result-content-pos-program" src="https://d1nuj3f2uuhf3u.cloudfront.net/smart_tag_parameters/7559/original.png?1492766899" alt="{{ $pharmacy_data->name }}">-->
                <div class="btn btn-primary result-content-pos-showphone hidden-print " data-pos-id="{{ $i }}">Voir le n° de téléphone</div>
                <div class="result-content-pos-phone print-show hide" data-pos-id="{{ $i }}">
                  <em class="fa fa-phone"></em>
                  <span itemprop="telephone" class="bold">{{ $pharmacy_data->phone }} </span>
                  <span class="result-content-pos-phone-comment small"></span>
                </div>
              </div>
            </div>
            
            <a href="tel:0322206969" class="result-content-pos-phone btn btn-default hidden-sm hidden-md hidden-lg print-hide hide" data-pos-id="{{ $i }}">
            <em class="fa fa-phone"></em>
            <span itemprop="telephone" class="bold">03 22 20 69 69 </span>
            <span class="result-content-pos-phone-comment small"></span>
            </a>
            <div class="result-content-pos-column-right hidden-print lf_poslink_links">
              <div class="result-content-pos-locate hidden-xs text-center locate" data-marker-id="{{ $i }}" aria-hidden="true">
              </div>
            </div>
          </div>
          @php
          $i++;
          $j++;
          @endphp
          @endforeach
          @if($total == 0 )
            <div class="text-center no-result">No result found!</div>
          @endif
        </div>
      </div>
    </div>
    @isset($current_page)
    <div class="text-center col-md-4 col-sm-6 col-xs-12 hidden-print">
        <ul class="result-pagination pagination" id="lf_pagination">
        @for ($j = 1; $j <= $last_page; $j++)
          <?php
            $half_total_links = floor($last_page / 2);
            $from = $current_page - $half_total_links;
            $to = $current_page + $half_total_links;
            if ($current_page < $half_total_links) {
                $to += $half_total_links - $current_page;
            }
            if ($last_page - $current_page < $half_total_links) {
                $from -= $half_total_links - ($last_page - $current_page) - 1;
            }
            
          ?>
          @if ($from <= $j && $j <= $to)
            <li class="result-pagination-list {{ ($current_page == $j) ? 'active' : '' }}">
              <a href="search?page={{ $j }}" class="pagination-number">{{ $j }}</a>
            </li>
          @endif
        @endfor
        </ul>
    </div>
    @endisset
  </div>
</div>


       <!-- Footer -->
  @include('frontend.common.footer-search')
       <!-- end: Footer -->
  </div>
  <!-- end: Body Inner -->
  <!-- Scroll top -->
  <a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
  <!--Plugins-->
   
   
  <script>
    var map,infowindow,marker;
    var slat,slog;
    var myLatLng = {lat: 46.227638, lng: 2.213749000000007};

    var pharmacies = <?php echo json_encode($pharmacy_all); ?>;
    var activeInfoWindow;

    initMap();
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 6
      });
      
      var markers1 = [];
      var locations = [];
      $.each(pharmacies, function(index) {
        var latpos = [];
        var latLong = {lat: this.latitude, lng: this.longitude};
        
        var contentString = '<div id="content-window">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h2 class="window-heading">'+this.pharmacy+'</h2>'+
        '<div id="bodyContent">'+
        '<p>'+this.address+'<br>'+this.city+', '+this.cp+'</p>'+
        '<p>';
        if(this.phone) {
          contentString += '<strong><i class="fa fa-phone"></i> '+this.phone+'</strong><br><strong>';
        }
        if(this.url) {
          contentString += '<a href="'+this.url+'" target="_blank"><i class="fa fa-search"></i> Voir la fiche détaillée</a></strong></p>';
        }
        else {
          
        }
        '</div>'+
        '</div>';
        latpos.push(latLong);
        latpos.push(contentString);
        locations.push(latpos);
      });
    
      var clusterStyles = [
        {
            height: 56,
            url: '/assets/frontend/new/images/cluster.png',
            width: 40,
            textSize: 0.001,
        },
      ];
      var infowindow = new google.maps.InfoWindow();
      var markers = locations.map(function(location, i) {
        var label = (i+1).toString();
        
        var marker =  new google.maps.Marker({
          position: location[0],
          icon: {
            url: '/assets/frontend/new/images/marker.png',        
            labelOrigin: { x: 20, y: 20}
          },
          label: {
            text: label,
            fontSize: '15px',
            color: '#ffffff'
          }
        });

        marker.addListener('click', function() {
          if (activeInfoWindow) { activeInfoWindow.close(); }
          infowindow.setContent(location[1]);
          infowindow.open(map, marker);
          map.setZoom(14);
          map.setCenter(this.position);
          activeInfoWindow = infowindow;
        });

        return marker;

      });

      var markerCluster = new MarkerClusterer(map, markers,
      {
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
        styles: clusterStyles
      });

      $('.result-content-pos-locate').click(function() {
        var index = $(this).data('marker-id');
        google.maps.event.trigger(markers[index], 'click');
        $('html, body').animate({
          scrollTop: $("#result-section").offset().top-100
        }, 500);
      });

      var input = document.getElementById('query');
      var options = {
        componentRestrictions: {country: "fr"}
      };
      var autocomplete = new google.maps.places.Autocomplete(input, options);
      autocomplete.bindTo('bounds', map);
      
      autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        var slatitude = place.geometry.location.lat();
        var slongitude = place.geometry.location.lng();
        $('.searchform-submit').attr('disabled', false);
        $('#slat').val(slatitude);
        $('#slng').val(slongitude);
      });
    }

    $(document).on('click','.result-content-pos-showphone',function(){
      $(this).hide();
      $(this).next('.result-content-pos-phone').show();
    });

   </script>
