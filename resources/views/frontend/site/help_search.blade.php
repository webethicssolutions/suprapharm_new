@extends('frontend.layouts.master')
@section('pageTitle', 'FAQ')
@section('pageDescription', 'This is FAQ page meta description')
@section('content')
@include('frontend.common.header')

@if(Session::get('user_id') != '')
<div class="sub-navigation">
<div class="navbar tabbed">
     <ul class="menu">
           
        <li class=""><a href="{{url('billing')}}">Billing</a></li>
        <li class=""><a href="{{url('subscription')}}">Subscriptions</a></li>
        <li class=""><a href="{{url('account-settings')}}">Account settings</a></li>
        <li class="active"><a href="{{url('help')}}">FAQ</a></li>
     
    </ul>
</div>
</div>
@endif 

<div class="box-b-wrap center-panel">
<div class="box-c-knowledge-base-dashboard">
<div class="box-c-dashboard-header">
<div class="box-c-dashboard-header-wrap">
<h1>
How can we help?</h1>
<div class="box-c-search">

<form class="search-wide formtastic" action="{{url('/search')}}" accept-charset="UTF-8" data-remote="true" method="post"><input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input id="kb-search" name="search" value="<?php if(isset($search_error)) {echo $search_error;} else {echo $search_data;}?>" required >
</form>
</div>
</div>
</div>

 
        
@if(isset($search_data))
<div class="help-desc-dsply  pt-3">
<center><h3 class="text-info">Search result for {{ $search_data }}</h3></center>
</div>
<div class="faq-section">
	@foreach($search_all as $key=>$search)
	<div class="help-desc-dsply">
		<a class="help_title" href="#">{{ $search->help_title }}</a>
		<div class="help_description" style="display: none;">{{ $search->help_description }}</div>
	</div>
	@endforeach
</div>

@endif

@if(isset($search_error))
<div class="help-desc-dsply pt-3">
<center><h3 class="text-info">Search result for {{ $search_error }}</h3>
<br><h4 class="text-danger">Your search returned no matches.</h4>

<h5>Suggestions:-</h5>
->Try different keywords.<br>
->Try more general keywords.
</center>
</div>
@endif
</div>
</div>

@include('frontend.common.footer')
 @stop