@extends('frontend.layouts.master')
@section('pageTitle', 'FAQ')
@section('pageDescription', 'This is FAQ page meta description')
@section('content')
@include('frontend.common.header')
<div class="sub-navigation">
<div class="navbar tabbed">
     <ul class="menu">
         
        <li class=""><a href="{{url('billing')}}">Billing</a></li>
        <li class=""><a href="{{url('subscription')}}">Subscriptions</a></li>
        <li class=""><a href="{{url('account-settings')}}">Account settings</a></li>
        <li class="active"><a href="{{url('help')}}">FAQ</a></li>
     
    </ul>
</div>
</div>
<div class="box-b-wrap center-panel">
<div class="box-c-knowledge-base-dashboard">
<div class="box-c-dashboard-header">
<div class="box-c-dashboard-header-wrap">
<h1>
How can we help?</h1>
<div class="box-c-search">

<form class="search-wide formtastic" action="{{url('/search')}}" accept-charset="UTF-8" data-remote="true" method="post"><input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input id="kb-search" name="search" placeholder="Enter a question, keyword or topic..." required >
</form>
</div>
</div>
</div>



<div class="help-desc-dsply">

</div>
@foreach($view_all as $view)
<div class="help-desc-dsply">
    <a class="help_title" href="#">{{ $view->help_title }}</a>
    <div class="help_description" style="display: none;">{{ $view->help_description }}</div>
</div>
@endforeach
</div>
</div>
@include('frontend.common.footer')
@stop