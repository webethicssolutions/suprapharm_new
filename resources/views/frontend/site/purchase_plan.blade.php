@extends('frontend.layouts.master')
@section('pageTitle', 'Update Plan')
@section('pageDescription', 'This is Update page meta description')
@section('content')
@include('frontend.common.header')
<?php 
// define('PAYMENT_ENVIRONMENT','SANDOBX');
// define('PAYPAL_URL','https://www.sandbox.paypal.com/cgi-bin/webscr');
// define('PAYPAL_MERCHANT','pathcodertest@gmail.com');


// define('CURRENCY','USD');
// define('PAYPAL_RETURN_URL','https://autolove.co/return_url');
// define('CANCEL_URL','https://autolove.co/');
// define('PAYPAL_NOTIFICATION_URL','https://autolove.co/ipn');



?>
<div id="create-account" class="content content-checkout">
    <div class="panel panel-pad panel-left">
        <div class="summary">
            <!--- div class="summary-toggle">
                <div>
                    <img class="menu" src="/assets/img/dash/v1/menu.svg">
                    <a href="#">Show summary</a>
                </div>
                <div>$17.00</div>
            </div ---->
@if(Session::has('success'))
                <div class="success-msg">
                   {{Session::get('success')}}
                </div>
                  @endif
            <div class="summary-content">
                <table class="summary-table">
                    <thead class="hidden-xs hidden-sm">
                        <tr>
                            <th class="title" colspan="2">Order summary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span class="icon">
                                    <img src="{{ url('resources/assets/frontend/images/heartbeat.gif')}}">
                                </span>
                                <span style="margin-left:10px;">
                                    {{$like}} @if($type=='L')
          {{"Like Per Post"}}
          @else
          {{'Likes+Followers'}}
@endif @if($id2!=0)<br>+{{$like2}} {{'Followers'}}@endif<!-- <em class="sub">/ post</em> --><br>
                                    <a href="{{url('/')}}">Change</a>
                                </span>
                            </td>
                            <td><span>{{$price}} / mo</span></td>
                        </tr>
                      
                        <tr class="subtotal" style="display: none">
                            <td>
                                <div>Subtotal</div>
                                <div class="subtotal-prorated" style="display: none">Prorate Adj.</div>
                                <div class="subtotal-promo" style="display: none">Discount <span class="subtotal-promo-code"></span> <a href="#" class="clear"></a></div>
                            </td>
                            <td>
                                <div>$<span class="subtotal-amount">{{$price}}</span></div>
                                <div class="subtotal-prorated" style="display: none">- $<span class="subtotal-discount">NaN</span></div>
                                <div class="subtotal-promo" style="display: none">- $<span class="subtotal-discount">NaN</span></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total today</th>
                            <th>$<span class="total">{{$price}}</span></th>
                        </tr>
                    </tfoot>
                </table>
                <div class="summary-features hidden-xs hidden-sm">
                    <ul>
                        <li>Real likes from real people</li>
                        <li>Dynamic and natural delivery</li>
                        <li>Completely automatic</li>
                        <li>Safe and secure</li>
                        <li>Cancel anytime</li>
                    </ul>
                </div>
            </div>
        </div>    </div>
    <div class="panel panel-pad panel-right">
       @if(Session::has('error'))
          <span class="error">
                    {{Session::get('error')}}
            </span>
             
                @endif
            @if(Session::has('success'))
               <p style="color:green">
                    {{Session::get('success')}}
               </p>
              @else
             
                @endif
      <!-- {{ Form::open(array('url' => '/paypal', 'method' => 'post')) }} -->
	  <form class="w3-container w3-display-middle w3-card-4 " method="POST" id="regsiter_form" action="/purchase-plan-with-card">
		{{ csrf_field() }}
        <div class="form checkout-form">
			<input type="hidden" name="custom" value="{{$custom}}">
			<input type="hidden" name="customer_id" value="{{$customer_id}}">
			<div class="billing-info"  id="billing-details">
          
              <input type="hidden" name="stripe_plan_id" value="{{$stripe_plan_id}}" id="stripe_plan_id">
                <input type="hidden" name="stripe_plan_id2" value="{{$stripe_plan_id2}}" id="stripe_plan_id2">
			    <div class="form-group-header">
					<span class="sub-title">Used Save Cards</span>
				</div>
			  <div class="form-group" id="saved_card">
				
					<div class="input-group input-dropdown">
						
						<div class="dropdown">
							<a href="javascript:void(0)" class="user-billing-info selected_info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<?php if($card_details){ 
									
									?>
									
									Select a Card
									
								<?php }else{
									
									
								} ?>
							
							</a>
							
							<input type="hidden" name="card_id" id="card_id" value="<?php echo  $card_details[0]->id;?>">
							
							
							
							<ul class="dropdown-menu">
								<?php if($card_details){ 
									foreach($card_details as $key=>$value){
									?>
										<li>
											<a href="javascript:void(0)" class="user-billing-info selected" data-value="<?php echo  $value->id; ?>">
												<img class="cc-brand" src="{{ url('resources/assets/frontend/images/card-mastercard.svg')}}">
												<span class="cc-number">
													<span class="cc-mask">••••</span>
													<span class="cc-digits"><?php echo substr($value->card_number,-4);?></span>
												</span>
												<span class="cc-name"><?php echo $value->card_name;?></span>
												<span class="cc-exp"><?php echo  $value->month;?> / <?php echo  $value->year;?></span>
												<img class="cc-selected" src="{{ url('resources/assets/frontend/images/check-green.svg')}}"> 
												
											</a>
											
										</li>
								<?php } } ?>
							</ul>
						</div>
					</div>
					 <div class="input-group validation-group" id="cvv_id" style="display:none">
						
						<input id="cardCode" type="password" name="cardCode" autocomplete="off" class="form-control placeholder-shown mask" placeholder="CVV/CVC" aria-label="CVV/CVC" pattern="[0-9]*" maxlength="4" inputmode="numeric" required=""> 
						<label for="card-code"><span class="placeholder">CVV/CVC</span></label>
						<span class="validation-message required">Please enter a CVV/CVC code</span>
						<span class="validation-message invalid">CVV/CVC code is invalid</span>
						<span id="cardCode_error" class="error"> {{ $errors->first('cardCode')  }}   </span>
						
					</div>
					 <button type="submit" class="btn btn-success btn-block btn-submit-cc mx" data-payment="card" data-saved="false" data-setup="subscription" id="update-plan">Confirm &amp; Pay<span class="loading" style="display:none;"><img src="{{ url('resources/assets/frontend/images/load.gif')}}"></span></button>
				</div>	
				</div>
			</div>
		</form>		
		<form class="w3-container w3-display-middle w3-card-4 " method="POST" id="regsiter_form" action="/purchase-plan-save">
			{{ csrf_field() }}
			<div class="form checkout-form">
				<input type="hidden" name="custom" value="{{$custom}}">
				<input type="hidden" name="customer_id" value="{{$customer_id}}">
				
			
				<div class="billing-info"  id="billing-details">
			  
					<input type="hidden" name="stripe_plan_id" value="{{$stripe_plan_id}}" id="stripe_plan_id">
                       <input type="hidden" name="stripe_plan_id2" value="{{$stripe_plan_id2}}" id="stripe_plan_id2">
				<div class="form-group" id="new_card">
				  <div class="form-group-header">
					
                        <span class="sub-title">Pay with credit / debit card</span>
                        <div class="billing-features">
                            <div class="billing-feature"><span class="icon"><img src="/assets/img/moneyback.svg"></span><span class="message"><em data-toggle="tooltip" title="" data-original-title="If for any reason you are not satisfied with our service within 30 days of signing up, contact us for a full refund - no questions asked.">30 day money back guarantee</em></span></div>
                            <div class="billing-feature"><span class="icon"><img src="/assets/img/cancel-anytime.svg"></span><span class="message">Cancel anytime</span></div>
                        </div>        <!-- <span class="sub-title-info card-list">
                            <span class="card card-visa"></span>
                            <span class="card card-mastercard"></span>
                            <span class="card card-discover"></span>
                            <span class="card card-amex"></span>
                        </span> -->
                    </div>
                    <div class="input-group input-group-number validation-group">
                        <input id="cardNumber" type="tel" name="cardNumber" class="form-control placeholder-shown" placeholder="Card number" aria-label="Card number" required="">
                        <label for="card-number"><span class="placeholder">Card number</span></label>
                        <span class="icon icon-lock"></span>
                        <span class="validation-message required">Please enter a card number</span>
                        <span class="validation-message invalid">Card number is invalid</span>
                         <span id="cardNumber_error" class="error"> {{ $errors->first('cardNumber')  }}   </span>

                      
                    </div>
                    <div class="input-group input-group-name validation-group">
                        <input id="cardName" type="text" name="cardName" value="" class="form-control" placeholder="Cardholder name" aria-label="Cardholder name" required="">
                        <label for="card-name"><span class="placeholder">Cardholder name</span></label>
                        <span class="validation-message required">Please enter a cardholder name</span>
                        <span class="validation-message invalid">Cardholder name is invalid</span>
                           <span id="cardName_error" class="error"> {{ $errors->first('cardName')  }}   </span>

                    </div>
                    <div class="input-group input-select input-group-month validation-group">
                        <select id="cardMonth" name="cardMonth" class="form-control" placeholder="Month" aria-label="Month" required="">
                            <option value="" disabled="" selected="" hidden=""></option>
                            <option value="01" selected="">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                        <label for="card-month"><span class="placeholder">Month</span></label>
                        <span class="validation-message required">Please enter a month</span>
                            <span id="cardMonth_error" class="error"> {{ $errors->first('cardMonth')  }}   </span>
                    </div>
                    <div class="input-group input-select input-group-year validation-group">
                        <select id="cardYear" name="cardYear" class="form-control" placeholder="Year" aria-label="Year" required="">
                            <option value="" disabled="" selected="" hidden=""></option>
							<?php 
								$currentyear = date('Y');
								$yearEnd =intval($currentyear)+15;   
									for($year = $currentyear;$year<=$yearEnd;$year++){ ?>
									<option value="<?php echo $year;?>"<?php if($currentyear == $year){echo 'selected=""';} ?>><?php echo $year;?></option>
								<?php } ?>
							 </select>
						</select>
                        <label for="card-year"><span class="placeholder">Year</span> <span class="validation-message"></span></label>
                        <span class="validation-message required">Please enter a year</span>
                        <span id="cardYear_error" class="error"> {{ $errors->first('cardYear')  }}   </span>
                    </div>
                    <div class="input-group input-group-code validation-group">
                        <input id="cardCode" type="password" name="cardCode" autocomplete="off" class="form-control placeholder-shown mask" placeholder="CVV/CVC" aria-label="CVV/CVC" pattern="[0-9]*" maxlength="4" inputmode="numeric" required=""> 
                        <label for="card-code"><span class="placeholder">CVV/CVC</span></label>
                        <span class="validation-message required">Please enter a CVV/CVC code</span>
                        <span class="validation-message invalid">CVV/CVC code is invalid</span>
                          <span id="cardCode_error" class="error"> {{ $errors->first('cardCode')  }}   </span>
                    </div>
                </div>
                <hr>
				<?php 
				
				if(!empty($billing_address)){
					
					$country  = $billing_address->country;
					$address  = $billing_address->address;
					$address1  = $billing_address->address1;
					$city  = $billing_address->city;
					$state  = $billing_address->state;
					$pincode  = $billing_address->pincode;
				}else{
					$country  = '';
					$address  = '';
					$address1  = '';
					$city  = '';
					$state  = '';
					$pincode  = '';
					
				}  ?>
				
				<?php $countries  =  array('US'=>'United States','AR'=>'Argentina','AU'=>'Australia','BR'=>'Brazil','CA'=>'Canada','CO'=>'Colombia','HR'=>'Croatia','DK'=>'Denmark','FR'=>'France','DE'=>'Germany','IN'=>'India','ID'=>'Indonesia','IL'=>'Israel','IT'=>'Italy','JP'=>'Japan','LU'=>'Luxembourg','MX'=>'Mexico','NZ'=>'New Zealand','NO'=>'Norway','RU'=>'Russia','SG'=>'Singapore','ZA'=>'South Africa','ES'=>'Spain','SE'=>'Sweden','CH'=>'Switzerland','AE'=>'United Arab Emirates','GB'=>'United Kingdom'); ?>
                <div class="form-group">
                    <div class="form-group-header">
                        <span class="sub-title">Billing address</span>
                    </div>
                    <div class="input-group input-select validation-group input-group-country">
                        <select id="billCountry" name="billCountry" class="form-control" placeholder="Country" aria-label="Country" required="">
                            <option value="" disabled="" selected="" hidden=""></option>
                            <?php foreach($countries as $key=>$value){ ?>
								<option value="<?php echo $key;?>" <?php if($key == $country){echo 'selected="selected"';}?>><?php echo $value;?></option>
							<?php }?>
							
						</select>
                        <label for="bill-country"><span class="placeholder">Country</span></label>
                        <span class="validation-message required">Please enter a country</span>

                          <span id="billCountry_error" class="error"> {{ $errors->first('billCountry')  }}   </span>
                    </div>
                    <div class="input-group input-group-address validation-group">
                        <input id="billAddress1" type="text" name="billAddress1" class="form-control" placeholder="Street Address" aria-label="Street Address" data-default="test" value="{{$address}}" required="">
                        <label for="bill-address"><span class="placeholder">Address line 1</span></label>
                        <span class="validation-message required">Please enter an address</span>
                           <span id="billAddress1_error" class="error"> {{ $errors->first('billAddress1')  }}   </span>
                    </div>
                    <div class="input-group input-group-address2">
                        <input id="billAddress2" type="text" name="billAddress2" class="form-control placeholder-shown" placeholder="Apartment, suite, etc. (optional)" aria-label="Apartment, suite, etc. (optional)" value="{{$address1}}" data-default="">
                        <label for="bill-address2"><span class="placeholder">Address line 2</span></label>
                           <span id="billAddress2_error" class="error"> {{ $errors->first('billAddress2')  }}   </span>
                    </div>
                    <div class="input-group input-group-city validation-group">
                        <input id="billCity" type="text" name="billCity" class="form-control" placeholder="City" aria-label="City" data-default="test" required="" value="{{$city}}"> 
                        <label for="bill-city"><span class="placeholder">City</span></label>
                        <span class="validation-message required">Please enter a city/town</span>
                         <span id="billCity_error" class="error"> {{ $errors->first('billCity')  }}   </span>
                    </div>
                    <div class="input-group input-select input-group-state validation-group hidden">
                         <input id="state" type="text" name="state" class="form-control placeholder-shown" data-default=""  placeholder="State" value="{{$state}}">
                        <label for="bill-state"><span class="placeholder">State</span></label>
                        <span class="validation-message required">Please enter a state</span>
                        <span id="state_error" class="error"> {{ $errors->first('state')  }}   </span>
                    </div>

                    <div class="input-group input-group-zip validation-group">
                        <input id="billZip" type="text" name="billZip" class="form-control" value="{{$pincode}}" placeholder="Zip Code" aria-label="Zip Code" data-default="160059" required="">
                        <label for="bill-zip"><span class="placeholder">Postal code</span></label>
                        <span class="validation-message required">Please enter a zip/postal code</span>
                            <span id="billZip_error" class="error"> {{ $errors->first('billZip')  }}   </span>
                    </div>
                </div>   
                <button type="submit"  name="saved_card"  class="btn btn-success btn-block btn-submit-cc mx" data-payment="card" data-saved="false" data-setup="subscription" id="update-plan">Confirm &amp; Pay<span class="loading" style="display:none;"><img src="{{ url('resources/assets/frontend/images/load.gif')}}"></span></button>
                 </div>

          </form>
        <!--   {{ Form::close() }} -->
        </div>    </div>
</div>
@include('frontend.common.footer')
<style>
.icon img {
  border-radius: 50px !important;
  height: 40px !important;
  width: 40px;
  margin-top: -7px !important;
}
.loading img {

   height: 25px !important;
   float: none;
   position: absolute;
   right: 0;
   top: -25px;

}
.loading {

   width: 100%;
   position: relative;

}
</style>
<script>


</script>
 @stop