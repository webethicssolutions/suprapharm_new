@extends('frontend.layouts.master')
@section('pageTitle', 'Home')
@section('pageDescription', 'This is home page meta description')
@section('content')
@include('frontend.common.header')

	<div class="main-banner">
	
		
		<div class="container"> 
			<div class="row align-items-center">
				<div class="col-12  animate-bx">
				<h1 class="display-5">Lorem Ipsum<br>Ipsum</h1>
				<div class="sh-heading" id="heading-animated-igwqI4rhLS">
					<h1 class="display-5 sh-heading-content size-xxxl sh-heading-animated-content">
					
						<span class="sh-heading-animated-typed"></span>
					</h1>
				</div>				
				<p class="lead mb-5 mt-2 mt-sm-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


				<div class="button">
				<a href="#pricing" class="price-sec subs">Subscribe</a>
				</div>				
                </div>
			
				
			</div>
		</div>	
		
	
		
	</div>
	
<div id="about">
<div class="special-bg">
<div class="graphic-sec">
	<div class="container">
		<div class="row align-items-center">
		
			<div class="col-lg-6 hm-lft-img">
				<img src="{{ url('resources/assets/frontend/images/about.jpg')}}">
			</div>		
			<div class="col-lg-6">
				<h2 class="title">Lorem Ipsum</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br>
</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				<p class="notice">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				<div class="col-12 bnr_btn">
				<div class="button">
			
				<a href="javascript:void(0)" class="price-sec">Subscribe</a>
				</div>
				
				</div>
			</div>
			
		</div>	
	</div>
	
</div>
<div id="features" class="container icon-info-wrapper services">
        <div class="row">
		
          <div class="col-md-4 text-center mb-5 mb-md-0">
          <img class="img-fluid" src="{{ url('resources/assets/frontend/images/aeroplane-.png')}}">

            <h3 class="mt-4 mb-2">Service A</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="col-md-4 text-center mb-5 mb-md-0">
		 <img class="img-fluid" src="{{ url('resources/assets/frontend/images/idea.png')}}">

            <h3 class="mt-4 mb-2">Service B</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="col-md-4 text-center">
		<img class="img-fluid" src="{{ url('resources/assets/frontend/images/power.png')}}">

            <h3 class="mt-4 mb-2">Service C</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
		  <div class="col-md-12 bdr"></div>
            <div class="col-md-4 text-center mb-5 mb-md-0">
          <img class="img-fluid" src="{{ url('resources/assets/frontend/images/smartphone.png')}}">

            <h3 class="mt-4 mb-2">Service D</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="col-md-4 text-center mb-5 mb-md-0">
		 <img class="img-fluid" src="{{ url('resources/assets/frontend/images/innovation.png')}}">

            <h3 class="mt-4 mb-2">Service E</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="col-md-4 text-center">
		<img class="img-fluid" src="{{ url('resources/assets/frontend/images/data-transfer.png')}}">

            <h3 class="mt-4 mb-2">Service F</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>		  
        </div>
</div>


</div>
<div>
<div class="client-reviews">
<div class="container">
<div class="row align-items-center">
	<div class="col-md-12">
<div class="review-slider">
  <div>
 		<div class="heading-bar">
			<div class="title">WHY OUR CLIENTS LOVE US?</div>
		</div> 
		
		<div class="testi">
			<div>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
			<div>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div> 
			<div>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div> 
			<div>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>

			<div>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div> 
			<div>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div> 
			
	  </div>
		<div class="col-12 bnr_btn">
				<div class="button">
				<a href="#pricing" class="price-sec">Subscribe</a>
				</div>
				</div>
  </div>
  <div>
</div>	
</div>
</div>
</div>	
</div>
</div>
</div>
<div class="pb-5 pricing-wrap">
<div class="form-wrapper pricing-table-sec">
<div class="container">
<div class="row">
	<div class="col-12">
 <h1  id="pricing" class="text-center">Subscription Plans</h1>
</div>	 
<div class="col-12 mt-5">
	<div class="card-deck mb-3 text-center"> 
		<div class="plan-slider"> 
			@foreach($plans as $plan)
				
				<div>
					<div class="card mb-4">
					<span></span>
					<span></span>
					<span></span>
					<span></span>			
					<div class="card-header">
						<h3 class="mt-0 mb-3">Most Popular</h3> 
						<div>
					
						</div>	
					  <h4 class="my-0">{{$plan->title}}</h4>				
					</div>
					  <div class="card-body">
						<h1 class="card-title pricing-card-title pricing__value pricing__value--show"></h1> 
						<ul class="list-unstyled mt-3 mb-4 planlist">
						
							<?php $desc=explode(',',$plan->description);?>
							@foreach($desc as $des)
							<li>{{$des}}</li>
						@endforeach
								
						</ul>
						<h4 class="my-0">${{$plan->amount}} <sub>/ month</sub></h4>
						<div class="prc_button"><a class="pricing_button" href="{{url('register')}}/{{$plan->id}}">Make a payment</a></div>
					  </div>
					</div>
				</div>
				
			@endforeach
			
  
	</div>

	</div>
		  
	  


</div>	
	</div>	
 
	  

 </div>
 </div>
 </div>	 </div>	

@include('frontend.common.footer')
 @stop