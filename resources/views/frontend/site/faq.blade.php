@extends('frontend.layouts.master')
@section('pageTitle', 'FAQ')
@section('pageDescription', 'This is FAQ page meta description')
@section('content')
@include('frontend.common.header')


<link rel="stylesheet" href="{{ url('resources/assets/faq_accordin/css/reset.css')}}"> <!-- CSS reset -->
<link rel="stylesheet" href="{{ url('resources/assets/faq_accordin/css/style.css')}}"> <!-- Resource style -->
<script src="{{ url('resources/assets/faq_accordin/js/modernizr.js')}}"></script> 

<div class="page container">

    
    <div id="content-transactions" class="content content-settings">

        <div class="panel panel-pad">
  <h1>FAQ</h1>
   <section class="cd-faq">
        <ul id="mobile" class="cd-faq-group">
            <li>
                <a class="cd-faq-trigger" href="#0">Why should I use AutoLove.co?</a>
                  <div class="cd-faq-content">
                    <p>We are a small team of experts with years of experience in Instagram Social Media Marketing and Influencers field. We only offer services we believe in and make sense.Trying our services is a decision you won’t regret making. ♠</p>
                </div> <!-- cd-faq-content -->
            </li>
                 <li>
                   <a class="cd-faq-trigger" href="#0">How long for service activation?</a>
                       <div class="cd-faq-content">
                    <p>Usually instant, should take no longer however than 30min after your sign-up in case of delays.</p>
                      </div> <!-- cd-faq-content -->
                    </li>
            <li>
                            <a class="cd-faq-trigger" href="#0">How many photos or videos may I post/upload each day?</a>
            <div class="cd-faq-content">
                                <p>Currently we support Unlimited posts per day.
            (Please make sure to follow our guidelines, Terms & Conditions)</p>
                            </div> <!-- cd-faq-content -->
                        </li>
                        <li>
                            <a class="cd-faq-trigger" href="#0">Real likes, really?</a>
                            <div class="cd-faq-content">
                                <p>We do not use illegitimate bots to deliver you likes which all show up a second after your new post/upload. All our likes are from other real Instagram users which start to arrive moments after your new post/upload and are usually delivered within 2-4 hoursbased on subscription size chosen. 1 Sunday, April 7, 2019</p>
                            </div> <!-- cd-faq-content -->
                        </li>
                           <li>
                            <a class="cd-faq-trigger" href="#0">How do your followers get delivered?</a>
                             <div class="cd-faq-content">
                                <p> Our followers are spread throughout the month based on subscription size chosen. 1,000 followers/month would be between 30-35 followers per day. Followers arrive throughout the day and not all at once.</p>
                            </div> <!-- cd-faq-content -->
                          </li>
                             <li>
                               <a class="cd-faq-trigger" href="#0"> Do you offer manual likes?</a>
                                   <div class="cd-faq-content">
                                <p> Not yet. We are currently in the works to release such feature in the very near future.</p>
                                   </div> <!-- cd-faq-content -->
                             </li>
                                <li>
                                 <a class="cd-faq-trigger" href="#0"> Why does my account have to be set Public?</a>
                                   <div class="cd-faq-content">
                                <p> If your account is set to Private, new users can’t see your content. Only your followers can. For those reasons your account must be set Public so new users can like your content and follow you too. :)</p>
                                    </div> <!-- cd-faq-content -->
                                 </li>
                              <li>
                                <a class="cd-faq-trigger" href="#0"> Do I need to provide you with my password?</a>
                                  <div class="cd-faq-content">
                                 <p> No. No password is required for AutoLove.co services.</p>
                                  </div> <!-- cd-faq-content -->
                              </li>
                              <li>
                                 <a class="cd-faq-trigger" href="#0"> Will my account get banned?</a>
                                  <div class="cd-faq-content">
                                <p> No. None of our clients accounts got banned due to our services.</p>
                                     </div> <!-- cd-faq-content -->
                              </li>
                          <li>
                            <a class="cd-faq-trigger" href="#0"> How do I cancel my subscription?</a>
                                 <div class="cd-faq-content">
                                <p> Once you’re logged in, there is an unsubscribe button at our Subscriptions tab for each account of yours.</p>
                                </div> <!-- cd-faq-content -->
                         </li>
                         <li>
                            <a class="cd-faq-trigger" href="#0"> What is your refund policy?</a>
                          <div class="cd-faq-content">
                            <p> We currently only refund orders if services do not start at all within 3 business days aka 72hours. With any issues, please contact support.</p>
                                    </div> <!-- cd-faq-content -->
                                </li>
                            </ul> 
                        </section>
            <div class="panel-content"> 
            </div>

        </div>
    </div>
</div> 
@include('frontend.common.footer')
<script src="{{ url('resources/assets/faq_accordin/js/jquery-2.1.1.js')}}"></script>
<script src="{{ url('resources/assets/faq_accordin/js/jquery.mobile.custom.min.js')}}"></script>
<script src="{{ url('resources/assets/faq_accordin/js/main.js')}}"></script>          
 @stop