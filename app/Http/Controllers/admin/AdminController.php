<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\User;
use DB;
use Input;
use Validator;
use Auth;
use Redirect;
use Excel;
use Response;
use Session;
use Image;
use Google_Client;
use Google_Service_Drive;
use Stripe;
use Google\Spreadsheet\ServiceRequestFactory;
use Google\Spreadsheet\DefaultServiceRequest;

class AdminController extends Controller
{


	public function all_subscriptions()
{
	$subscription = DB::table('subscriptions')->where('status','=','active')->orderBy('id','desc')->get();
		return view("admin.subscription.subscriptions",compact('subscription'));
}
public function all_images()
{

		$images = DB::table('images')->orderBy('id', 'desc')->get();
		return view("admin.images.index",compact('images'));
}
public function promotion()
{

		$promotions = DB::table('promotions')->orderBy('id', 'desc')->get();
		return view("admin.promotion",compact('promotions'));
}
public function pharmacy()
{

		$pharmacy = DB::table('pharmacy')->orderBy('id', 'desc')->get();
		return view("admin.pharmacy.index",compact('pharmacy'));
}
public function payment_details($id)
{

	$transactions = DB::table('transactions')->where('payment_id','=',$id)->orderBy('id','desc')->get();

		return view("admin.user.transactions",compact('transactions'));
}

public function user_services($id)
{

		$subscription = DB::table('subscriptions')->where('user_id','=',$id)->where('status','=','active')->orderBy('id','desc')->get();

		return view("admin.user.user_services",compact('subscription','id'));
}
public function index(){

     //$user_id =  Session::get('admin_user_id');
			//return view('admin.login.users');

			///$user_details = User::find($user_id);
      // $user_details = User::where('user_type', '!=', 'admin')->get();
	// return view("admin.login.users",compact('user_details'));
      // return view("admin.login.users");

	  // $details= DB::table('email')->get();
	  // return view("admin.email.email",compact('details'));
	 	return Redirect::intended('/admin/users');

}
 public function payline_config(){
 	$mer_id='';
 	$acc_key=
 	$merchant_id = DB::table('settings')->where('meta_key','=','payline_merchant_id')->get();
 	$access_key = DB::table('settings')->where('meta_key','=','payline_access_key')->get();
		$mer_id=$merchant_id[0]->meta_value;
		$acc_key=$access_key[0]->meta_value;
	return view('admin.payline.payline_config',compact('mer_id','acc_key'));
	}

	public function save_payline(Request $request){
		$rules = array(
			'merchant_id' => 'required',
			'access_key' => 'required',
			);
		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/payline')->withErrors($validator)->withInput();
		}else{

            $data = array('meta_value'=>$request->input('merchant_id'));
            $data1 = array('meta_value'=>$request->input('access_key'));
            DB::table('settings')->where('meta_key','=','payline_merchant_id')->update($data);
             DB::table('settings')->where('meta_key','=','payline_access_key')->update($data1);
	    Session::flash('success', 'Data saved successfully');

		return redirect('admin/payline');
		}
	}
	/* Admin login View */
  public function login(){
			if(Session::get('admin_user_id')){
			 		return Redirect::intended('/admin');
			 }
			 elseif(Session::get('user_id')){
					return Redirect::intended('/services');
					//return view("frontend.layouts.login");
			 }
			 else{
			 		return view('admin.login.login');
			 }
	}


	/* Check admin Login*/
	function checklogin(Request $request){
				//print_r($request->all());
				$rules = array('email'    => 'required|email',
								'password' => 'required'
							  );

				$validator = Validator::make($request->input(), $rules);

				if ($validator->fails()) {
						return redirect('/')->withErrors($validator)->withInput($request->except('password'));
				} else {

					 $result = DB::table('users')
						->where('email', '=', $request->get('email'))
						->where('password', '=', md5($request->get('password')))
						->first();
					  if(isset($result) && !empty($result)){

						if($result->status == 'disable')
						{
							Session::flash('error', "Your Account is disabled ");
							return Redirect::to('/');

						}
					  	if($result->user_type=='admin'){
                            $name = $result->username;

							Session::put('admin_user_id', $result->id);
							Session::put('admin_user_name',$name);
							Session::put('user_type','admin');
						  return Redirect::intended('/admin');
						}
						else{

							$name = $result->username;
							Session::put('user_id', $result->id);
							Session::put('user_name',$name);
							Session::put('user_type','user');
							return Redirect::intended('/services');
						}
					  }else{

							Session::flash('error', 'Please fill correct email/password.');
							return Redirect::to('admin/login');
					  }

				  }

	   }
/*
function checklogin(Request $request){

				$rules = array('email'    => 'required|email',
								'password' => 'required'
							  );
				$validator = Validator::make($request->input(), $rules);

				if ($validator->fails()) {
						return redirect('/')->withErrors($validator)->withInput($request->except('password'));
				} else {

					 $result = DB::table('users')
						->where('email', '=', $request->get('email'))
						->where('password', '=', md5($request->get('password')))
						->get();

					if(!empty($result)){
					if($result->status == 'disable')
					{
					Session::flash('error', "Your Account is disabled");
					return Redirect::to('/');
					die;
					}
					  	if($result[0]->user_type=='admin'){
                            $name = $result[0]->username;
							Session::put('admin_user_id', $result[0]->id);
							Session::put('admin_user_name',$name);
							Session::put('user_type','admin');
						  return Redirect::intended('/admin');
						}
						else{
							$name = $result[0]->username;
							Session::put('user_id', $result[0]->id);
							Session::put('user_name',$name);
							Session::put('user_type','user');
							return Redirect::intended('/member');
						}
					  }else{
							Session::flash('error', 'Please fill correct email/password.');

							return Redirect::to('admin/login');
					  }

				  }

	   }
  /* Admin Logout function */
	public function logout()
	{

			Session::put('admin_user_id', '');
			Session::put('admin_user_name', '');
			Session::put('user_id', '');
			Session::put('user_name', '');
			Session::put('user_type','');
			//return redirect()->action('admin\AdminController@index');
			return redirect('/admin/login');
	}
	// public function user_status($id='',$status='')
	// {

	// 	if($status)
	// 	{
	// 		DB::table('users')
 //            ->where('id', '=',$id)
 //            ->update(['status' => $status]);

	// 	}
	// 	else
	// 	{
	// 		DB::table('users')
 //            ->where('id', '=',$id)
 //            ->update(['status' => $status]);
	// 	}

	// }
	public function admin_change_password()
		{

				return view('admin.login.change_password_admin');

		}
		public function change_password_admin(Request $request)
		{

				$user_id = Session::get('admin_user_id');

						$rules = [
						'password'    => 'min:6|confirmed|required',
						'password_confirmation' => 'min:6|required',
						'old_password' => 'required'
				  	];
					$messages = ['password.regex' => "Your password must contain 1 lower case character 1 upper case character one number"];
				  $validator = Validator::make($request->input(),$rules,$messages);
						if ($validator->fails())
								return redirect('admin/change-password-admin')->withErrors($validator)->withInput();
						else{

							$new_password = $request->get('password');
							$old_password = $request->get('old_password');

							$result = DB::table('users')
						 	->where('id', '=', $user_id)
						 	->where('password', '=', md5($old_password))
						 	->get();

						 	if(!$result->isEmpty()){

						 		$data = array('password'=>md5($new_password));
							    DB::table('users')->where('id', '=', $user_id)
									->update($data);

						 		Session::flash('success', 'Password changed successfully !');
						 		return redirect('admin/change-password-admin');


						 		}else{

						 			Session::flash('error', 'Your current password does not matches with the password you provided. Please try again.');
						 			return redirect('admin/change-password-admin');
						 		}
							//  $date = date('Y-m-d H:i:s');
							//  $data = array('password'=>md5($request->input('password')),'updated_at'=>$date,);
							// DB::table('users')->where('id', '=', $user_id)
						 //  ->update($data);
							//  Session::flash('success', 'Your Password has been Updated.');
						 // 	return back();
		}
	}
	// public function admin_check_old_password(Request $request)
	// {

	// 		 $user_id = Session::get('admin_user_id');
	// 		 $password = md5($request->input('old_password'));
	// 		 $user = DB::table('users')->where('id','=',$user_id)->where('password','=',$password)->get();
	// 					if(count($user)>0)
	// 					{
	// 							return Response::json(array(
	// 						 'success' => true,
	// 						 'errors' => ''
	// 						 ), 200);
	// 					}
	// 					else
	// 					{
	// 						return Response::json(array(
	// 					 'success' => false,
	// 					 'errors' => ''
	// 					 ), 200);
	// 					}
	// 	}

//***************email template functionality****************************//

/*
* Import CSV functionality
*/
public function import_csv(Request $request)
{
/*	echo'<pre>';print_r($request->import_file);die;
        $request->validate([
            'import_file' => 'required'
        ]);
*/
	        $path = $request->file('import_file')->getRealPath();
	    		$data = array_map('str_getcsv', file($path));
	        if(count($data)){
            foreach ($data as $key => $value) {
						//	echo'<pre>';print_r($value[2]);die;
							$result = [];
							if(isset($value[0]))
							{
								$result['name'] = $value[0];
							}
							if(isset($value[1]))
							{
								$result['date_of_entry'] = $value[1];
							}
							if(isset($value[2]))
							{
								$result['cip'] = $value[2];
							}
							if(isset($value[3]))
							{
								$result['pharmacy'] = $value[3];
							}
							if(isset($value[4]))
							{
								$result['address'] = $value[4];
							}
							if(isset($value[5]))
							{
								$result['cp'] = $value[5];
							}
							if(isset($value[6]))
							{
								$result['city'] = $value[6];
							}
							DB::table('pharmacy')->insert($result);
            }
        }
        return back()->with('success', 'Insert Record successfully.');
}




	public function email_template(){

	  $details= DB::table('email')->where('id', '=' , 17)->get();
	  return view("admin.email.email",compact('details'));
	}


public function email_template_edit($id){

	$result = DB::table('email')->where('id', '=' , $id)->get();
	return view('admin.email.edit_email' , compact('result'));
}

	public function email_template_update(Request $request){


		$email_id = $request->input('email_id');
		$rules = array(
			    'subject'		 => 'required',
			    'description'      => 'required',
			    );

	  $validator = Validator::make($request->input(),$rules);
	  if ($validator->fails()){
	    return redirect('admin/email/edit/'.$email_id)->withErrors($validator)->withInput();
	  }
	  else{

	  	  $subject = $request->input('subject');
	      $description = $request->input('description');
	      $data = array('subject'=>$subject,'description'=>$description);
	      DB::table('email')->where('id', '=', $email_id)
				->update($data);
		Session::flash('success', 'Email Template has been Updated.');
		return redirect('admin/email/edit/'.$email_id);

	    }
	}

	public function new_email_template(){

	  return view('admin.email.add_email_template');
	}

	public function email_template_add(Request $request){

	  $rules = array(
			    'title'		 => 'required|unique:email,title',
			    'subject'		 => 'required',
			    'description'      => 'required',
			    );

	  $validator = Validator::make($request->input(),$rules);
	  if ($validator->fails()){
	    return redirect('admin/email/new')->withErrors($validator)->withInput();
	  }
	  else{

	    $title = $request->input('title');
	    $subject = $request->input('subject');
	    $description = $request->input('description');
	    $data = array('title'=>$title,'subject'=>$subject,'description'=>$description);
	     DB::table('email')->insert($data);
	    Session::flash('success', 'Template Added successfully !');
	    return back();
	  }
	}

	public function delete_template($id){

	    DB::table('email')->where('id', '=', $id)->delete();

	    Session::flash('success', 'Template deleted successfully.');
	    return Redirect::to('/admin/email');
	}



//*************************Plans functionality*****************//

	public function plans(){
		// display user list
		$plans = DB::table('plans')->get();

		return view("admin.plan.plans",compact('plans'));

	}
	public function add_plan(){


		return view("admin.plan.add_plan");

	}
	public function add_pharmacy(){


		return view("admin.pharmacy.create");

	}
	public function edit_plan($id){
		// display user list
		$plans = DB::table('plans')->where('id','=',$id)->get();

		return view("admin.plan.edit_plan",compact('plans'));

	}
	public function edit_pharmacy($id){
		// display user list
		$pharmacy = DB::table('pharmacy')->where('id','=',$id)->get();

		return view("admin.pharmacy.edit",compact('pharmacy'));

	}
	public function save_edit_plan(Request $request){
		// display user list
		$rules = array(
			'title' => 'required',
			'amount' => 'required|numeric',
			'description' => 'required',

			);
		$id=$request->input('plan_id');
		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/plan/edit/'.$id)->withErrors($validator)->withInput();
		}else{
		$date = date('Y-m-d H:i:s');

            $data = array('title'=>$request->input('title'),'amount'=>$request->input('amount'),'description'=>$request->input('description'));
            DB::table('plans')->where('id', '=', $id)->update($data);
	    Session::flash('success', 'Data saved successfully');

		return redirect('admin/plan/edit/'.$id);
		}
	}
	public function update_pharmacy(Request $request){

		$rules = array(
			'name' => 'required',
			'address' => 'required',
			'lati' => 'required',
			'longi' => 'required',
			'city' => 'required',
			'pharmacy' => 'required',
			);
			$customMessages = array(
        'lati.required' => 'Please select a valid address',
				'longi.required' => 'Please select a valid address'

    );
	  $id=$request->input('id');
		$validator = Validator::make($request->input(), $rules, $customMessages);
		if ($validator->fails()) {
			return redirect('admin/pharmacy/edit/'.$id)->withErrors($validator)->withInput();
		}else{
		$timing = [];
		$timing['monday'] = $request->input('monday');
		$timing['tuesday'] = $request->input('tuesday');
		$timing['wednesday'] = $request->input('wednesday');
		$timing['thursday'] = $request->input('thursday');
		$timing['friday'] = $request->input('friday');
		$timing['saturday'] = $request->input('saturday');
		$timing['sunday'] = $request->input('sunday');
		$timing = serialize($timing);

if(!empty($request->profile))
 {
      $check = DB::table('pharmacy')->where('id',$id)->pluck('profile');
			$filename = public_path("/images/admin/pharmacy/$check");
			if (file_exists($filename)) {
					$unlinkimage = public_path("/images/admin/pharmacy/$filename");
					unlink($unlinkimage);
		  } // get previous image from folder
			$date = date('Y-m-d H:i:s');
			$newfile_name = preg_replace('/\s+/', '', $request->file('profile')->getClientOriginalName());
			$image = time().'.'.$newfile_name;
			$data = array('name'=>$request->input('name'),'address'=>$request->input('address'),'latitude'=>$request->input('lati'),'longitude'=>$request->input('longi'),'city'=>$request->input('city'),'pharmacy'=>$request->input('pharmacy'),'phone'=>$request->input('phone'),'description'=>$request->input('description'),'timing'=>$timing,'profile'=>$image,'updated_at'=>$date,);
			$request->profile->move(public_path('/images/admin/pharmacy'), $image);
			DB::table('pharmacy')->where('id', '=', $id)->update($data);

			Session::flash('success', 'Data saved successfully');
			return redirect('admin/pharmacy');
		}
		$date = date('Y-m-d H:i:s');
		$data = array('name'=>$request->input('name'),'address'=>$request->input('address'),'latitude'=>$request->input('lati'),'longitude'=>$request->input('longi'),'city'=>$request->input('city'),'pharmacy'=>$request->input('pharmacy'),'phone'=>$request->input('phone'),'description'=>$request->input('description'),'timing'=>$timing,'updated_at'=>$date,);
		DB::table('pharmacy')->where('id', '=', $id)->update($data);
		Session::flash('success', 'Data saved successfully');
		return redirect('admin/pharmacy');
		}
	}
	public function save_plan(Request $request){
		// display user list
		$rules = array(
			'title' => 'required',
			'amount' => 'required|numeric',
			'description' => 'required',
			);

		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/plan/add')->withErrors($validator)->withInput();
		}else{
		$date = date('Y-m-d H:i:s');
		$id=$request->input('plan_id');
            $data = array('title'=>$request->input('title'),'amount'=>$request->input('amount'),'description'=>$request->input('description'),'created_on'=>$date);
            DB::table('plans')->insert($data);
	    Session::flash('success', 'Data saved successfully');

		return redirect('admin/plans');
		}
	}
	public function save_pharmacy(Request $request){
		// display user list
		$rules = array(
			'name' => 'required',
			'address' => 'required',
			'cp' => 'required|numeric',
			'cip' => 'required|numeric',
			'lati' => 'required',
			'longi' => 'required',
			'city' => 'required',
			'pharmacy' => 'required',
			'profile' => 'mimes:jpeg,jpg,png,gif','max:1000',
			'url' => 'required',
			);
			$customMessages = array(
				'lati.required' => 'Please select a valid address',
				'longi.required' => 'Please select a valid address'

		);

		$validator = Validator::make($request->input(), $rules, $customMessages);
		if ($validator->fails()) {
			return redirect('admin/pharmacy/create')->withErrors($validator)->withInput();
		}
		else{
			if(!empty($request->profile))
         	{
				$timing = [];
				$timing['monday'] = $request->input('monday');
				$timing['tuesday'] = $request->input('tuesday');
				$timing['wednesday'] = $request->input('wednesday');
				$timing['thursday'] = $request->input('thursday');
				$timing['friday'] = $request->input('friday');
				$timing['saturday'] = $request->input('saturday');
				$timing['sunday'] = $request->input('sunday');
				$timing = serialize($timing);
				$date = date('Y-m-d h:i:s');
				$newfile_name = preg_replace('/\s+/', '', $request->file('profile')->getClientOriginalName());
				$image = time().'.'.$newfile_name;
				$data = array('name'=>$request->input('name'),'address'=>$request->input('address'),'city'=>$request->input('city'),'cip'=>$request->input('cip'),'longitude'=>$request->input('longi'),'latitude'=>$request->input('lati'),'cp'=>$request->input('cp'),'url'=>$request->input('url'),'phone'=>$request->input('phone'),'date_of_entry'=>$date,'pharmacy'=>$request->input('pharmacy'),'description'=>$request->input('description'),'timing'=>$timing,'profile'=>$image,'created_at'=>$date);
				$request->profile->move(public_path('/images/admin/pharmacy'), $image);
				DB::table('pharmacy')->insert($data);
				Session::flash('success', 'Data saved successfully');
				return redirect('admin/pharmacy');
			}
					
			$date = date('Y-m-d h:i:s');
			$data = array('name'=>$request->input('name'),'address'=>$request->input('address'),'city'=>$request->input('city'),'cip'=>$request->input('cip'),'cp'=>$request->input('cp'),'url'=>$request->input('url'),'phone'=>$request->input('phone'),'date_of_entry'=>$date,'pharmacy'=>$request->input('pharmacy'),'description'=>$request->input('description'),'timing'=>$timing,'created_at'=>$date);
			DB::table('pharmacy')->insert($data);
			Session::flash('success', 'Data saved successfully');
			return redirect('admin/pharmacy');


		}
	}
	/*public function save_pharmacy(Request $request){
		// display user list
		$rules = array(
			'name' => 'required|string',
			'city' => 'required|string',

			);

		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/plan/add')->withErrors($validator)->withInput();
		}else{
		$date = date('Y-m-d H:i:s');
		$id=$request->input('plan_id');
						$data = array('title'=>$request->input('title'),'amount'=>$request->input('amount'),'description'=>$request->input('description'),'created_on'=>$date);
						DB::table('plans')->insert($data);
			Session::flash('success', 'Data saved successfully');

		return redirect('admin/plans');
		}
	}*/
	public function delete_plan($id){
		//delete email template
	    DB::table('plans')->where('id', '=', $id)->delete();

	    Session::flash('success', 'Plan deleted successfully.');
	    return Redirect::to('/admin/plans');
	}
	public function delete_pharmacy($id){
		//delete email template
			DB::table('pharmacy')->where('id', '=', $id)->delete();

			Session::flash('success', 'Pharmacy deleted successfully.');
			return Redirect::to('/admin/pharmacy');
	}


public function transactions(){
	$transactions = DB::table('transactions')->orderBy('id','desc')->get();

		return view("admin.transaction.transactions",compact('transactions'));

	}

//*************************Users functionality*****************//
public function user_manage($id){
		$services = DB::table('plans')->get();
		$result = DB::table('users')
		->where('id', '=', $id)->get();
		$name = $result[0]->username;
		   Session::put('user_id', $result[0]->id);
			Session::put('user_name',$name);
			Session::put('user_type','user');

		// $subscription = DB::table('subscriptions')
		// ->where('user_id', '=', $id)->get();
		// if(count($subscription)>0)
		// {
		// 	Session::put('subscription_id',$subscription[0]->service_id);
		// }
		return Redirect::intended('/services');

	}

	public function users_details(){
		// display user list
		$user_details = User::where('user_type', '!=', 'admin')->orderBy('ID','desc')->get();

		return view("admin.user.user_details",compact('user_details'));

	}

	public function editUser($id){
		// edit user profile
		$result = DB::table('users')->where('id', '=' , $id)->get();
		//fetch countries name from db

		return view('admin.user.edit_user' , compact('result'));

	}


 public function admin_user_edit(Request $request){

		if($request->input('user_edit_id') != ""){
			$user_id = $request->input('user_edit_id');
		}
		//check validation user field
		$rules = array(
			'first_name'=>'required',
			'last_name'=>'required',
			'gender'=>'required',
			'dob'=>'required',
			);
		$type=0;

		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/user/edit/'.$user_id)->withErrors($validator)->withInput();
		}else{
			$first_name = $request->input('first_name');
			$last_name = $request->input('last_name');
			$username = $request->input('username');
			$email = $request->input('email');
			$password = $request->input('password');
			$gender = $request->input('gender');
			$dob = $request->input('dob');
			$date = date('Y-m-d H:i:s');

			// insert new user in db by admin
			$data = array('first_name'=>$first_name,'last_name'=>$last_name,'username'=>$username,'email'=>$email,'gender'=>$gender,'dob'=>$dob,'created_on'=>$date,'user_type'=>'user','status'=>'enable');
        	DB::table('users')->where('id', '=', $user_id)
			->update($data);
			Session::flash('success', 'User profile has been Updated.');
			return redirect('admin/user/edit/'.$user_id);
		}

	}
public function delete_image($id){
		//delete email template
		$data = DB::table('images')->find($id);
		$image = $data->name;
		if(!empty($image))
		{
		$image_delete = storage_path('app/public/images/admin/images/'.$image);
		@unlink($image_delete);
	    DB::table('images')->where('id', '=', $id)->delete();
		Session::flash('success', 'Image deleted successfully.');
		return redirect('admin/all-images');
		}
		else
		{
		DB::table('images')->where('id', '=', $id)->delete();
		Session::flash('success', 'Image deleted successfully.');
		return redirect('admin/all-images');
		}
		/*
	    DB::table('images')->where('id', '=', $id)->delete();

	    Session::flash('success', 'Image deleted successfully.');
			return redirect('admin/all-images');
*/	}
public function delete_promotion($id){
		//delete email template
		$data = DB::table('promotions')->find($id);
		$image = $data->name;
		if(!empty($image))
		{
		echo $image_delete = storage_path('app/public/images/admin/images/'.$image);
		@unlink($image_delete);
	    DB::table('promotions')->where('id', '=', $id)->delete();
		Session::flash('success', 'Image deleted successfully.');
		return redirect('admin/promotions');
		}
		else
		{
		DB::table('promotions')->where('id', '=', $id)->delete();
		Session::flash('success', 'Image deleted successfully.');
		return redirect('admin/promotions');
		}
		/*
	    DB::table('images')->where('id', '=', $id)->delete();

	    Session::flash('success', 'Image deleted successfully.');
			return redirect('admin/all-images');
*/	}

	public function delete_user($id){
		//delete email template
	    DB::table('users')->where('id', '=', $id)->delete();

	    Session::flash('success', 'User deleted successfully.');
	    return Redirect::to('/admin/users');
	}
public function enableuser($id=''){
		if($id!=''){
			// $result = DB::table('users')->where('id', '=', $id)->first();

			DB::table('users')->where('id', '=', $id)->update(['status'=>'enable']);
			echo "success";die;
		}else{
			echo "error";die;
		}
	}
public function enableimage($id=''){
		if($id!=''){
			// $result = DB::table('users')->where('id', '=', $id)->first();

			DB::table('images')->where('id', '=', $id)->update(['status'=>'enable']);
			echo "success";die;
		}else{
			echo "error";die;
		}
	}
public function disableimage($id=''){
		if($id!=''){
			// $result = DB::table('users')->where('id', '=', $id)->first();

			DB::table('images')->where('id', '=', $id)->update(['status'=>'disable']);
			echo "success";die;
		}else{
			echo "error";die;
		}
	}
public function enablepromotion($id=''){
		if($id!=''){
			// $result = DB::table('users')->where('id', '=', $id)->first();

			DB::table('promotions')->where('id', '=', $id)->update(['status'=>'enable']);
			echo "success";die;
		}else{
			echo "error";die;
		}
	}
public function disablepromotion($id=''){
		if($id!=''){
			// $result = DB::table('users')->where('id', '=', $id)->first();

			DB::table('promotions')->where('id', '=', $id)->update(['status'=>'disable']);
			echo "success";die;
		}else{
			echo "error";die;
		}
	}

public function disableuser($id=''){

		/* if($id!=''){
			DB::table('users')->where('id', '=', $id)->update(['user_status'=>'disable']);
			Session::flash('success', 'User updated Successfully.');
		}
		return back(); */

		if($id!=''){
			DB::table('users')->where('id', '=', $id)->update(['status'=>'disable']);
			echo "success";die;
		}else{
			echo "error";die;
		}
	}



	public function addUser(){

		return view('admin.user.add_user');
	}
	public function addImage(){
		return view('admin.images.add_image');
	}
	public function addpromotion(){
		return view('admin.add_promotion');
	}


	public function admin_save_user(Request $request){
		//check validation of add user field
		$rules = array(
			'first_name'=>'required',
			'last_name'=>'required',
			'username'=>'required|unique:users',
			'email'=>'required|email|unique:users',
			'password'=>'required|min:6',
			'gender'=>'required',
			'dob'=>'required',
			);


		$validator = Validator::make($request->input(),$rules);
		if ($validator->fails()){
			return redirect('admin/user/adduser/')->withErrors($validator)->withInput();
		}
		else{


			$first_name = $request->input('first_name');
			$last_name = $request->input('last_name');
			$username = $request->input('username');
			$email = $request->input('email');
			$password = $request->input('password');
			$gender = $request->input('gender');
			$dob = $request->input('dob');
			$date = date('Y-m-d H:i:s');

			// insert new user in db by admin
			$data = array('first_name'=>$first_name,'last_name'=>$last_name,'username'=>$username,'email'=>$email, 'password'=>md5($password),'gender'=>$gender,'dob'=>$dob,'created_on'=>$date,'user_type'=>'user','status'=>'enable');
			DB::table('users')->insert($data);
			//$last_id=DB::getPdo()->lastInsertId();

			// send email to user (account details)
   //    		$username = $email;
			// $password = $password;
			// $from = 'pathcodertest@gmail.com';
			// $fromname = '';
			// $to = $request->input('email');
			// $result = DB::table('email')->where('id', '=' , '2' )->get();
			// $subject = $result[0]->subject;
   //    		$message_body = $result[0]->description;
   //    		$list = Array
   //            (
   //               '[name]' => $name,
   //               '[username]' => $username,
   //               '[password]' => $password,
   //            );

   //    		$find = array_keys($list);
   //    		$replace = array_values($list);
   //    		$message = str_ireplace($find, $replace, $message_body);

			// $mail = $this->send_email($to, $subject, $message, $from, $fromname);
			Session::flash('success', 'User Added Successfully.');
			return redirect('/admin/users');
		}


	}

 public function deleteMultiple(Request $request){
		$ids=$request->ids;
		DB::table('images')->whereIn('id',explode(",",$ids))->delete();
	    	return Response::json(array(
				'success' => true,
				'errors' => ''
		 ), 200);

    }
	 public function deleteMultiplepromotion(Request $request){
	 	 $ids=$request->ids;
		DB::table('promotions')->whereIn('id',explode(",",$ids))->delete();
		return Response::json(array(
				'success' => true,
				'errors' => ''
		 ), 200);

    }


public function save_image(Request $request)
{
	// echo'<pre>';print_r($request->all());die;
    $rule = array(
			'image.*'=>'mimes:jpeg,jpg,png,gif|required',
			);
			if($request->file('image') == null){
				$rule = array(
					'image'=>'mimes:jpeg,jpg,png,gif|required',
					);
			}elseif(count($request->file('image')) == 1){
				$rule = array(
					'image.*'=>'mimes:jpeg,jpg,png,gif|required',
					);
			}

		$validator = Validator::make($request->all(),$rule);
		if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();

		 }
		else{
						$image = $request->file('image');
						foreach ($image as $key=>$files){
						$destinationPath = base_path().'/public/images/admin/images/';
						$Image = time().".".$files->getClientOriginalName().".".$files->extension();
						$large_image_path = $destinationPath.$Image;
						Image::make($files)->resize(500, 300)->save($large_image_path);
		    		$date = time();
						$data[$key] = array('name'=>$Image,'created_at'=>$date);
            }
            $check =DB::table('images')->insert($data);

        }

	    return redirect('admin/all-images');

}/*{
		//check validation of add user field
	//echo '<pre>'; print_r($request->all());die;
	// $newfile_name = preg_replace('/\s+/', '', $request->input('image'));
      //$image = time().'.'.$newfile_name;
	  //echo $image;die;
	  $rule = array(
			'image'=>'mimes:jpeg,jpg,png,gif|required|max:1000',
			);

		$validator = Validator::make($request->all(),$rule);
		if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
		 }
		else{

		foreach($request->file('image') as $file){
            $newfile_name = preg_replace('/\s+/', '', $file->extension());
            $image_name = time().'.'.$newfile_name;
	  	    $date = date('Y-m-d H:i:s');
			$data = array('name'=>$image_name,'created_at'=>$date);
			DB::table('images')->insert($data);
			$request->file('image')->move(public_path('/images/admin/images'), $image_name);
			Session::flash('success', 'Image Added Successfully.');
			return redirect('admin/all-images');
			}

		}

	}*/

public function save_promotion(Request $request){

	$rule = array(
		'image.*'=>'mimes:jpeg,jpg,png,gif|required',
		);
		if($request->file('image') == null){
			$rule = array(
				'image'=>'mimes:jpeg,jpg,png,gif|required',
				);
		}elseif(count($request->file('image')) == 1){
			$rule = array(
				'image.*'=>'mimes:jpeg,jpg,png,gif|required',
				);
		}

	$validator = Validator::make($request->all(),$rule);
	if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();

	 }
		else{

			$image = $request->file('image');

			//dd($image);

			foreach ($image as $key=>$files){
				$destinationPath = base_path().'/public/images/admin/images/';
				$Image = time().".".$files->getClientOriginalName().".".$files->extension();
				$large_image_path = $destinationPath.$Image;
				Image::make($files)->resize(500, 300)->save($large_image_path);
				$date = time();
				$data[$key] = array('name'=>$Image,'created_at'=>$date);
			}

			$check =DB::table('promotions')->insert($data);

		}
		return redirect('admin/promotions');

	/*  $rule = array(
			'image'=>'mimes:jpeg,jpg,png,gif|required|max:1000',
			);

		$validator = Validator::make($request->all(),$rule);
		if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
		 }
		else{
        $newfile_name = preg_replace('/\s+/', '', $request->file('image')->extension());
        $image_name = time().'.'.$newfile_name;

	  	$date = date('Y-m-d H:i:s');


			// insert new user in db by admin
			$data = array('name'=>$image_name,'created_at'=>$date);
			DB::table('promotions')->insert($data);
			$request->file('image')->move(public_path('/images/admin/images'), $image_name);

			Session::flash('success', 'Promotion Added Successfully.');
			return redirect('admin/promotions');

		}
*/
	}

	//***************** end promocode functionality *****************//



// public function useredit($id='')
// 	{
// 		$user_data = DB::table('users')
// 	 	->where('id', '=', $id)
// 		->where('user_type', '=', 'user')
// 	 	->get();
// 		if(count($user_data)>0){
// 			   Session::put('user_id', $user_data[0]->id);
// 			   Session::put('user_type','user');
// 			 }
// 					// pr($user_data);die;
// 		 return view('admin.login.edit_user' , compact('user_data'));
// 	}
// public function admin_edit_user(Request $request)
// {
// 	/*echo "<pre>";
// 	print_r($request->input());
// 	print_r($request->file());die;*/

// 	$user_id = $request->input('id');
// $phoneRegex = "/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})?$/";
// 			$user = DB::table('users')->where('id','=',$user_id)->get();
// 			$rules = array('firstname'    => 'required',
// 							 'lastname'    => 'required',
// 							 'gender'    => 'required',
// 							 'city'    => 'required',
// 							 'state'    => 'required',
// 							 'zip'    => 'required',
// 							 'phone'    => ['required', "regex:$phoneRegex"],

// 							);

// 		$validator = Validator::make($request->input(),$rules);
// 			if ($validator->fails())
// 			return back()->withErrors($validator)->withInput();
// 			else{
// 				  $date = date('Y-m-d H:i:s');
// 				 $data = array('first_name'=>$request->input('firstname'),'last_name'=>$request->input('lastname'),'gender'=>$request->input('gender'),'city'=>$request->input('city'),'state'=>$request->input('state'),'zip'=>$request->input('zip'),'phone'=>$request->input('phone'),'occupation'=>$request->input('occupation'),'updated_at'=>$date,);
// 				 DB::table('users')->where('id', '=', $user_id)
// 				 ->update($data);
// 	}
// 		if($request->hasFile('image')){
//            $this->old_pic_remove($user_id);

// 		 $media =$request->file('image');

// 				 $destinationPath = storage_path().'/app/public/uploads/users/'.$user_id.'/';
// 				 $filename = microtime().'.'.$media->getClientOriginalExtension();
// 				 if (!file_exists($destinationPath))
// 				 {
// 						 mkdir($destinationPath, 0777, true);
// 						 chmod($destinationPath,0777);
// 					}
// 				 $large_image_path = $destinationPath.$filename ;
// 				 Image::make($media)->resize(200, 200)->save($large_image_path);
// 				 DB::table('users')->where('id', '=', $user_id)
// 				 ->update(['image'=>$filename]);

//         }
//   Session::flash('success', 'Your profile  has been Updated.');
// 		 return back();
// 		 }
//  public function change_user_password()
// {

// 	return view('admin.login.change_password');

// }
// public function change_password_user(Request $request)
// {
// 		$user_id = Session::get('user_id');
// 		$rules = [
// 		'password'    => 'min:6|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@]).*$/|confirmed|required',
// 		'password_confirmation' => 'min:6|required'
// 		];
// 	 $messages = ['password.regex' => "Your password must contain 1 lower case character 1 upper case character one number",
//  										'password_confirmation.required'=> "Confirm password is a required feild",
// 										'password_confirmation.min'=> "Confirm password must atleast contain six characters"];
// 	 $validator = Validator::make($request->input(),$rules,$messages);
// 		 		if ($validator->fails())
// 				 return back()->withErrors($validator);
// 		 	 	else{
// 						$date = date('Y-m-d H:i:s');
// 						$data = array('password'=>md5($request->input('password')),'updated_at'=>$date,);
// 						DB::table('users')->where('id', '=', $user_id)
// 						->update($data);
// 						Session::flash('success', 'Your Password has been Updated.');
// 			 return back();
// 			}

// 		}
// 		public function old_pic_remove($user_id='')
// 	{
// 			$data = DB::table('users')->find($user_id);
// 			$image = $data->image;
// 			$image_thumb = storage_path('app/public/uploads/users/'.$user_id.'/'.$image);
// 			@unlink($image_thumb);
// 		  return true;
// 	}

/*** Share google doc ****/

	public function shared_google_drive(){
// 		$files = glob(public_path()."/uploads/Google-Drive/*"); // get all file names
// 		foreach($files as $file){ // iterate files
//   	if(is_file($file))
//     unlink($file); // delete file
// }
//die;

		$categories=DB::table('categories')->get();
		 $client = new Google_Client();
		 putenv('GOOGLE_APPLICATION_CREDENTIALS='.public_path().'/client_secret.json');
		 $client->useApplicationDefaultCredentials();
		 $client->setScopes(['https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/spreadsheets','https://spreadsheets.google.com/feeds','https://docs.google.com/feeds','https://www.googleapis.com/auth/drive.file','https://www.googleapis.com/auth/drive.readonly','https://www.googleapis.com/auth/drive.metadata.readonly']);

		$drive = new Google_Service_Drive($client);
		$query = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
        $resultss = $drive->files->listFiles();

		$resultsss = $resultss->getFiles();
		
		$folderId = "18ouvTE-Bim-DHpeoTc83_RHQOELoFXfY";
		$optParams = array(
			'pageSize' => 10,
			'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
			'q' => "'".$folderId."' in parents"
        );
		 $results = $drive->files->listFiles($optParams);

		//  echo "<pre>";
		//  print_r($results); 
		// print_r($resultssss); 
		// die;



		 foreach ($resultsss as $file) {

		if($file->mimeType!='application/vnd.google-apps.folder'){

        $fileId = $file->id;


		$response = $drive->files->get($file->id, array('alt' => 'media'));



if (!file_exists(public_path()."/uploads/Google-Drive/".$file->id."_".$file->name))   
{


		// Open file handle for output.

		$outHandle = fopen(public_path()."/uploads/Google-Drive/".$file->id."_".$file->name, "w+");

		// Until we have reached the EOF, read 1024 bytes at a time and write to the output file handle.

		while (!$response->getBody()->eof()) {

		fwrite($outHandle, $response->getBody()->read(1024));
		}

		// Close output file handle.

		fclose($outHandle);
		//echo "Done.\n";
	}
		}
		}


		// print_r($content);
		// $target_dir = public_path()."/uploads/Google-Drive";


		// echo move_uploaded_file($response, $target_dir);

		// die;
// 		$retrieved_files = array();
// 		foreach($resultss->files as $file)
// 		{
// 			array_push($retrieved_files, array(
// 				'filename'  =>  $file->name,
// 				'fileid'    =>  $file->id,
// 				'file'      =>  $file
// 			));
// 		}
// 		/* echo "<pre>";
// 		print_r($retrieved_files);
// 		die; */
// $get_specific_file = array_search('example_file_name',
//     array_column($retrieved_files, 'filename'));

		return view('admin.shareddrive.listing', compact('results','categories'));
	}



public function google_drive_by_folderID($id){


		$categories=DB::table('categories')->get();
		 $client = new Google_Client();
		 putenv('GOOGLE_APPLICATION_CREDENTIALS='.public_path().'/client_secret.json');
		 $client->useApplicationDefaultCredentials();
		 $client->setScopes(['https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/spreadsheets','https://spreadsheets.google.com/feeds','https://docs.google.com/feeds','https://www.googleapis.com/auth/drive.file','https://www.googleapis.com/auth/drive.readonly','https://www.googleapis.com/auth/drive.metadata.readonly']);

		$drive = new Google_Service_Drive($client);
		$query = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
        $resultss = $drive->files->listFiles();

		$results = $resultss->getFiles();
		
		$folderId = $id;
		$optParams = array(
			'pageSize' => 10,
			'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
			'q' => "'".$folderId."' in parents"
        );
		 $results = $drive->files->listFiles($optParams);

		


	

		return view('admin.shareddrive.listing', compact('results','categories'));
	}

	/********** Create share google folder **************/
	function create_google_folder() {
		$client = new Google_Client();
		 putenv('GOOGLE_APPLICATION_CREDENTIALS='.public_path().'/client_secret.json');
		 $client->useApplicationDefaultCredentials();
		 $client->setScopes(['https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/spreadsheets','https://spreadsheets.google.com/feeds','https://docs.google.com/feeds','https://www.googleapis.com/auth/drive.file','https://www.googleapis.com/auth/drive.readonly','https://www.googleapis.com/auth/drive.metadata.readonly']);

		$drive = new Google_Service_Drive($client);
		$query = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
        $results = $drive->files->listFiles();
        if (count($results->getFiles()) == 0) {
            print "No files found.\n";
        } else {
            print "Files:\n";
            foreach ($results->getFiles() as $file) {
                dump($file->getName(), $file->getID());
            }
        }
		// Insert a file
		$file = new \Google_Service_Drive_DriveFile();
		$file->setTitle ( 'My document' );
		$file->setDescription ( 'A test document' );
		$file->setMimeType ( 'text/plain' );

		//$data = file_get_contents ( 'document.txt' );

		$createdFile = $service->files->create( $file, array (
				'data' => $data,
				'mimeType' => 'text/plain'
		) );

		echo $createdFile;


	}


	public function add_category()
	{
		$categories=DB::table('categories')->get();
		return view('admin.category.add_category',compact('categories'));
	}

	public function save_category(Request $request)
	{
		$rules = array(
			'category_name'=>'required|unique:categories'

			);


		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/category/add')->withErrors($validator)->withInput();
		}else{
			$date = date('Y-m-d H:i:s');
			$data=array('category_name'=>$request->input('category_name'),'created_on'=>$date);
			DB::table('categories')->insert($data);
			$last_id=DB::getPdo()->lastInsertId();
			if($request->input('sub_category')!='')
			{
				$date = date('Y-m-d H:i:s');
			$data1=array('category_id'=>$last_id,'sub_category_name'=>$request->input('sub_category'),'created_on'=>$date);
			DB::table('sub_categories')->insert($data1);

			}
			Session::flash('success', 'Data saved successfully');
			return redirect('admin/category/add');
		}
	}

	public function edit_category($id)
	{

		$cat_name='';
		$category=DB::table('categories')->where('id','=',$id)->get();
		$cat_name=$category[0]->category_name;
		return view('admin.category.edit_category',compact('cat_name','id'));

	}

	public function edit_save_category(Request $request)
	{
		$rules = array(
			'category_name'=>'required|unique:categories'

			);


		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/category/edit/'.$request->input('cat_id'))->withErrors($validator)->withInput();
		}else{
		$data=array('category_name'=>$request->input('category_name'));
		$category=DB::table('categories')->where('id','=',$request->input('cat_id'))->update($data);
			Session::flash('success', 'Data saved successfully');
		return redirect('admin/category/edit/'.$request->input('cat_id'));
	}
	}

public function delete_category($id)
	{

		DB::table('categories')->where('id','=',$id)->delete();
		DB::table('sub_categories')->where('category_id','=',$id)->delete();
		Session::flash('success', 'Category deleted successfully');
		return redirect('admin/category/add');
	}


		public function edit_sub_category($id)
	{
		$sub_cat_name='';
		$sub_category=DB::table('sub_categories')->where('id','=',$id)->get();
		$sub_cat_name=$sub_category[0]->sub_category_name;
		return view('admin.category.edit_sub_category',compact('sub_cat_name','id'));
	}

	public function edit_save_sub_category(Request $request)
	{
		$rules = array(
			'sub_category_name'=>'required'

			);


		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/sub-category/edit/'.$request->input('sub_cat_id'))->withErrors($validator)->withInput();
		}else{
		$data=array('sub_category_name'=>$request->input('sub_category_name'));
		$category=DB::table('sub_categories')->where('id','=',$request->input('sub_cat_id'))->update($data);
			Session::flash('success', 'Data saved successfully');
		return redirect('admin/sub-category/edit/'.$request->input('sub_cat_id'));
	}
	}

public function delete_sub_category($id)
	{


		DB::table('sub_categories')->where('id','=',$id)->delete();
		Session::flash('success', 'Category deleted successfully');
		return redirect('admin/category/add-sub');
	}

	public function add_sub_category()
	{
		$categories=DB::table('categories')->get();
		$sub_categories=DB::table('sub_categories')->get();
		return view('admin.category.add_sub_category',compact('categories','sub_categories'));
	}

	public function save_sub_category(Request $request)
	{
		$rules = array(
			'category'=>'required',
			'sub_category'=>'required'

			);


		$validator = Validator::make($request->input(), $rules);
		if ($validator->fails()) {
			return redirect('admin/category/add-sub')->withErrors($validator)->withInput();
		}
		else
		{

			$exist=DB::table('sub_categories')->where('category_id',$request->input('category'))->where('sub_category_name',$request->input('sub_category'))->get();
			if(count($exist)>0)
			{
				 $validator->getMessageBag()->add('sub_category', 'This Sub category already exist under this parent catefory.');
				 return redirect('admin/category/add-sub')->withErrors($validator)->withInput();
			}
			else
			{
				$date = date('Y-m-d H:i:s');
				$data1=array('category_id'=>$request->input('category'),'sub_category_name'=>$request->input('sub_category'),'created_on'=>$date);
				DB::table('sub_categories')->insert($data1);
				Session::flash('success', 'Data saved successfully');
				return redirect('admin/category/add-sub');
			}
		}
	}


	public function get_sub_category(Request $request)
	{

			$sub_categories=DB::table('sub_categories')->where('category_id',$request->input('value'))->pluck("sub_category_name","id");
            return response()->json($sub_categories);



	}


	public function selected_category(Request $request)
	{


			$total=array();

			$all_images=DB::table('google_drive')->where('category_id','=',$request->input('value'))->where('subcategory_id','=',$request->input('sub_cat'))->get();

            foreach($all_images as $img)
            {
            	$total[]=$img->image_id;
            }

			return $total;

	}
	public function save_images_to_category(Request $request)
	{
		// echo '<pre>';
		// print_r($request->input());
		// echo '</pre>';
		// die;
		DB::table('google_drive')->where('category_id','=',$request->input('cat_id'))->where('subcategory_id','=',$request->input('sub_cat_id'))->whereIn('image_id',$request->input('every'))->delete();
		foreach($request->input('img_id') as $img) {
			$exist=DB::table('google_drive')->where('image_id','=',$img)->where('category_id','=',$request->input('cat_id'))->where('subcategory_id','=',$request->input('sub_cat_id'))->get();
			if(count($exist)<1)
			{
		$data1=array('image_id'=>$img,'category_id'=>$request->input('cat_id'),'subcategory_id'=>$request->input('sub_cat_id'));
		$google_drive = DB::table('google_drive')->insert($data1);
		}

		}

	}


}
