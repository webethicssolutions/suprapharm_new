<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\ContactRequest;
use App\User;
use App\Article;
use App\models\Slide;
use DB;
use Input;
use Validator;
use Auth;
use Redirect;
use Session;
use Response;
use Storage;
use Image;
use URL;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use View;


class HomeController extends Controller{

public function login(){
	return view("frontend.layouts.login");
}

public function cards_of_pharmacies(){
	return view("frontend.layouts.map");
}

/*
* Get pharmacy result by ajax
*/
public function loadDataAjax(Request $request){

		$output = '';
	  $id = $request->id;
  	$pharmacy = DB::table('pharmacy')->where('id','>',$id)->limit(6)->get();
		$lastId = $id;
		if(count($pharmacy)){
			foreach($pharmacy as $ph){
				$lastId = $ph->id;
			}
		}
			$view = view("frontend.common.list",compact('pharmacy'))->render();
      return response()->json(['html'=>$view,'lastId'=>$lastId]);
}


// function to get  the address
function get_lat_long($address){

    $address = str_replace(" ", "+", $address);
		$data = [];
		$data['lat'] = '';
		$data['log'] = '';
    $json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCmByayAiEvKG4MdveNZZ2v5nEPbwFz6I8&address='.$address.'&sensor=false');
    $json = json_decode($json);
		//dd($json);
		if(isset($json->results) && isset($json->results[0]) && isset($json->results[0]->geometry)){
	    $data['lat'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
	    $data['log'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		}
    return $data;
}

public function iterativeTable(){
	$pharmacy = DB::table('pharmacy')->get();
	foreach ($pharmacy as $key => $phmy) {
		$phmy = get_object_vars($phmy);
		if(empty($phmy['latitude']) && empty($phmy['longitude'])){
			$result = $this->get_lat_long($phmy['address']);
			if(!empty($result['lat']) && !empty($result['log'])){
				$data = array('latitude'=>$result['lat'],'longitude'=>$result['log']);
				DB::table('pharmacy')->where('id','=',$phmy['id'])->update($data);
			}
		}
	}
}



public function pharmacysearch(Request $request)
{
if($request->ajax())
{
			$output="";
			//serach lat and long $request->search
			if(!empty($request->search)){
				$addressResp = $this->get_lat_long($request->search);
				if(!empty($addressResp['lat']) && !empty($addressResp['log'])){
					$slatitude  = $addressResp['lat'];
				  $slongitude = $addressResp['log'];
					$DISTANCE_KILOMETERS = 10;
					$raw_query = "SELECT * FROM ( SELECT *, ( ( ( acos( sin(( $slatitude* pi() / 180)) * sin(( `latitude` * pi() / 180)) + cos(( $slatitude * pi() /180 )) * cos(( `latitude` * pi() / 180)) * cos((( $slongitude - `longitude`) * pi()/180))) ) * 180/pi() ) * 60 * 1.1515 * 1.609344 ) as distance FROM pharmacy ) pharmacy WHERE distance <= $DISTANCE_KILOMETERS LIMIT 15 ";

					$pharmacy = DB::select( DB::raw($raw_query));

				}
			}

			if(!isset($pharmacy) || (isset($pharmacy) && count($pharmacy) == 0)){
				$pharmacy=DB::table('pharmacy')->where('address','LIKE','%'.$request->search."%")->get();
			}

			//$pharmacy=DB::table('pharmacy')->where('address','LIKE','%'.$request->search."%")->get();
			$data = [];$key = 0;$view = '';
			foreach($pharmacy as $phmy)
			{
							$phmy = get_object_vars($phmy);
					    $address=$phmy['address'];
							$data[$key]['name'] = $phmy['pharmacy'];
							$data[$key]['address'] = $phmy['address'];
							if(empty($phmy['latitude']) && empty($phmy['longitude'])){
					       $locationResp= $this->get_lat_long($phmy['address']);
								 if(!empty($locationResp['lat']) && !empty($locationResp['log'])){
									 $data[$key]['latitude'] = $locationResp['lat'];
 									 $data[$key]['longitude'] = $locationResp['log'];
								 }
								}

								if(!isset($data[$key]['latitude']) && !isset($data[$key]['longitude']))
								{
									$data[$key]['latitude'] = $phmy['latitude'];
									$data[$key]['longitude'] = $phmy['longitude'];
								}

								$key++;
				    }
						$view = view("frontend.common.list",compact('pharmacy'))->render();
						return response()->json(['html'=>$view,'data'=>$data]);
}

			/*$countPharmacy = count($pharmacy);
			if($pharmacy)
			{
					$view = view("frontend.common.list",compact('pharmacy'))->render();
					return response()->json(['html'=>$view,'countPharmacy'=>$countPharmacy]);
			}
			else
			{
				$msg='There is no pharmacy available on this address';
				return response()->json(['html'=>$msg]);
			}*/
}



// function to get  the address





/*
* Get list of pharmacies
*/
public function list_of_pharmacies(){
	$pharmacy = DB::table('pharmacy')->paginate(6);

	$lastId = 0;
	foreach($pharmacy as $ph){
		$lastId = $ph->id;
	}
	return view("frontend.layouts.list_pharmacies",compact('pharmacy','lastId'));
}

/*
* Return About Us page view
*/
public function about_us(){
	return view("frontend.layouts.about_us");
}
public function ourservices(){
	return view("frontend.layouts.master");
}
public function what_is_this(){
		$magzine = DB::table('images')->where('status','=','enable')->get();
		$promotion = DB::table('promotions')->where('status','=','enable')->get();
	    return view("frontend.layouts.home",compact('magzine','promotion'));
}
public function contact_us(){
	return view("frontend.layouts.contact");
}
public function services(){
	return view("frontend.layouts.services");
}
public function newservices(){
	return view("frontend.layouts.newservices");
}

public function pharmacist(){
	return view("frontend.layouts.pharmacist");
}
public function installation_help(){
	return view("frontend.layouts.installation");
}
public function contact_usnew(){
	return view("frontend.layouts.contacttest");
}
public function home(){
	
	return view("frontend.layouts.homenew");
}
/*
* contact testing
*/
public function contact_email(ContactRequest $request){
	//check contact form fields validation

 
		//send email from user to admin (contact-email).
		$name = $request->input('name');
		$email = $request->input('email');
		$subj = $request->input('subject');
		$msg = $request->input('message');
		$fromname = "supraform contact email";
		$to = 'arunsharma.webethics';
		$from = $email;
    
	 
   
			// Session::flash('success', 'Thank you for contacting us, we will get back to you soon.!!');
				//   return Redirect::to('contact-usnew');
			//	 return json_encode(['status'=>'success', 'msg'=>'Thank you for contacting us, we will get back to you soon.!!']);  
			 return response(['status'=>'success', 'msg'=>'Thank you for contacting us, we will get back to you soon.!!']);  
				  // return Redirect::back();
			
	
	}
}
