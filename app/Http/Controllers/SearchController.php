<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\ContactRequest;
//use App\User;
//use App\Article;
//use App\models\Slide;
use DB;
use Input;
//use Validator;
use Auth;
use Redirect;
use Session;
use Response;
use Storage;
use Image;
use URL;
use View;

class SearchController extends Controller {
    public function index(){
        if(isset($_GET['slat'])) {
            $search = trim($_GET['query']);
            $lat = $_GET['slat'];
            $long = $_GET['slng'];             

        $pharmacy = DB::table('pharmacy')
        ->select('*', DB::raw(sprintf(
            '(6371 * acos(cos(radians(%1$.7f)) * cos(radians(latitude)) * cos(radians(longitude) - radians(%2$.7f)) + sin(radians(%1$.7f)) * sin(radians(latitude)))) AS distance',
            $lat,
            $long
        )))
        ->having('distance', '<', 20)
        ->orderBy('distance', 'asc')
        ->get();        
        $pharmacy_all = $pharmacy;
        return view("frontend.site.search_pharmacies",compact('pharmacy', 'pharmacy_all', 'search'));
        }
        else {
            $pharmacy = DB::table('pharmacy')->paginate(20);
            $pharmacy_all = DB::table('pharmacy')->get()->toArray();
            return view("frontend.site.search_pharmacies",compact('pharmacy', 'pharmacy_all'));
        }
        
    }
	public function detail($id)
	{
		$id = explode("-",$id);
		$id = $id[0];
		$search_data= DB::table('pharmacy')->where('id','=',$id)->get()->toArray();
		$search_data = $search_data[0];
		return view("frontend.site.serach_pharmacies_detail",compact('search_data'));
	}
}