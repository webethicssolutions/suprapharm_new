<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\ContactRequest;
use App\User;
use App\Article;
use App\models\Slide;
use DB;
use Input;
use Validator;
use Auth;
use Redirect;
use Session;
use Response;
use Storage;
use Image;
use URL;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use View;
use App\Helpers\Helper as Helper;
use Stripe;
use Payline\PaylineSDK;
use Monolog\Logger;
use Payline\Wallet;
use Google\Spreadsheet\ServiceRequestFactory;
use Google\Spreadsheet\DefaultServiceRequest;
use Google_Service_Drive;
use Google_Client;

class SiteController extends Controller{


public function get_flyer_template(Request $request)
{


 	$data=DB::table('flyer_template')->where('id','=',$request->input('id'))->where('user_id','=',Session::get('user_id'))->get();
 	$all_data=array('product_id'=>$data[0]->product_id,'title'=>$data[0]->title,'name'=>$data[0]->name,'price'=>$data[0]->price,'size'=>$data[0]->size,'image'=>$data[0]->image,'image1'=>$data[0]->image1,'image2'=>$data[0]->image2,'image_pos'=>$data[0]->image_pos);
 	return Response::json(array(
            'success' => true,
            'data' => $all_data
            ), 200);
 	 //return redirect('/flyer-tool');

}

public function save_flyer_template(Request $request)
{
	$date = date('Y-m-d H:i:s');
			$data=array('user_id'=>Session::get('user_id'),'template_name'=>$request->input('template_name'),'product_id'=>$request->input('product_id'),'title'=>$request->input('main_title'),'name'=>$request->input('product_name'),'image'=>$request->input('image_name'),'price'=>$request->input('product_price'),'size'=>$request->input('paper_size'),'image1'=>$request->input('offr_img'),'image2'=>$request->input('offr_img2'),'image_pos'=>$request->input('offr_img_class'),'created_on'=>$date);
			DB::table('flyer_template')->insert($data);
			     Session::flash('success', 'Data Saved successfully.');
 	 //return redirect('/flyer-tool');

}

public function get_products($cip,$token){
	  $curl = curl_init();
	  curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://ws2.pharmao.fr/api/v1/products",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => false,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>"{\n\t\"pharmacy_cip\": \"$cip\"}",
	  CURLOPT_HTTPHEADER => array(
		"Content-Type: application/json",
		"Authorization: Bearer $token"
	  ),
	));
	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
	  return  $err;
	} else {
		//echo $response;die;
		  $products_array  = json_decode($response);
		  $result = array();
		  if(@count($products_array->data)>0){
			  $result['data']= $products_array->data;
			  $result['error']='';
		  } else{
			    if(@$products_array->data){
			    $result['data']= $products_array->data;
				$result['error']='';
				}
			    else{
				$result['data']=  '';
				$result['error']= $products_array->error;;
				}
		  }
       return $result;
	}
}

/* Get Product details by ID and CIP */
public function get_product_detail($prdocu_id,$cip,$token){

	  $url = "http://ws2.pharmao.fr/api/v1/products/".$prdocu_id .'/'.$cip ;
	  $curl = curl_init();
	  curl_setopt_array($curl, array(
	  CURLOPT_URL => "$url",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => false,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"Content-Type: application/json",
		"Authorization: Bearer $token"
	  ),
	 ));
	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);
	if ($err) {
	  return  $err;
	} else {
		  $products_array  = json_decode($response);
		  return $products_array->data;

	}
}
public function flyer_ajax(){
if(Session::get('user_id')=='')
	{
		 return Response::json(array(
            'success' => true,
            'redirect' => 'yes'
            ), 200);

	}
	else{
$token = $this->flyer_token();
$CIP=2255336;
	// $CIP =  $config['CIP'];
 //  	$token =  $config['token'];
  $path = 'product/';
  if(isset($_POST['action']) && $_POST['action']=='show_detail'){
		  if(isset($_POST['product'])&& $_POST['product']!=''){
			  $prdocu_id = $_POST['product'];
			  //$prdocu_id = 5232;
			  $c_ip = $CIP;
			  //Get Product details
		  if($prdocu_id!=''){
				$product_data = $this->get_product_detail($prdocu_id,$c_ip,$token);
			  }
			 // Get Tamplate content
			  $get_template =  file_get_contents('template.html');
			  $LOGO = $product_data->photo;
			  			  // Main title
			  $MAIN_TITLE = 'Promo Du Mois';
			  $FONT_SIZE_MAIN_TITLE = '60px';
			  $PRODUCT_NMAE = $product_data->name_medicament;
			  $FONT_SIZE_PRODUCT_NAME = '50px';
			 // echo "oldprice".$product_data->old_price;
			  // $old_price = $product_data->old_price;
			  $old_price = $product_data->old_price;
			  $show_old_price_strike='';
			  if($old_price != 0){
				  $exp_old = explode('.',$old_price);
				  $OLD_PRICE= array_shift($exp_old); //Old price before decimal
				  $OLD_PRICE_DEC =  end($exp_old);  //Old price after decimal
				  $show_old_price_strike= '<span class="o_price">'.$OLD_PRICE.'<sub>€'.$OLD_PRICE_DEC. '</sub></span>';
			  }
			  else
			  {
				if($_POST['oldprice'] != ''){
					$old_price = $_POST['oldprice'];
					$exp_old = explode('.',$old_price);
					$OLD_PRICE= array_shift($exp_old); //Old price before decimal
					$OLD_PRICE_DEC =  end($exp_old);  //Old price after decimal
					$show_old_price_strike= '<span class="o_price">'.$OLD_PRICE.'<sub>€'.$OLD_PRICE_DEC .'</sub></span>';
				}
			  }
			  $new_price = $product_data->new_price;
			  $exp_new = explode('.',$new_price);
			  $NEW_PRICE= array_shift($exp_new);  //New price before decimal
			  $NEW_PRICE_DEC =  end($exp_new);    //New price after decimal
			   if($NEW_PRICE_DEC!='') $NEW_PRICE_DEC= '<span class="dollarsign">€</span>'.end($exp_new);
			  //Font Size
			   $font_size ='22vw';
			 // echo $font_size;
			   $url =  $LOGO ;
			   $exp_img =  explode('/',$url);
			   $image_name = end($exp_img);
			   $img_name = $path.$image_name;
			   $offertext = $_POST['offertext'];
			   if($offertext !="")
			  {
				  $offertext = '<span>'.$offertext.'</span>';
			  }
			  
				
			  // Function to write image into file
			  file_put_contents($img_name, file_get_contents($url));
			  // Find an Replace the the content
			  $array_replace_from =array("[LOGO]","[NEW_PRICE]","[NEW_PRICE_DEC]","[OLD_PRICE]","[PRODUCT_NMAE]","[FONT_SIZE]","[FONT_SIZE_PRODUCT_NAME]","[MAIN_TITLE]","[FONT_SIZE_MAIN_TITLE]","[OFFER_TEXT]");
			  $array_replace_to = array($img_name,$NEW_PRICE,$NEW_PRICE_DEC,$show_old_price_strike,$PRODUCT_NMAE,$font_size,$FONT_SIZE_PRODUCT_NAME,$MAIN_TITLE,$FONT_SIZE_MAIN_TITLE,$offertext);
			  echo  $final_template = str_replace($array_replace_from,$array_replace_to,$get_template);
			 die;
		  }
   }
	/* delete image from server that is store un product folder  */
       if(isset($_POST['action']) && $_POST['action']=='delete_img'){
			 $path = $_POST['image_path'];
			 @unlink($path);
			 echo "ok";

	 }




// Update Product information and replace with template
	   if(isset($_POST['action']) && $_POST['action']=='update_product'){
			//echo "<pre>";print_r($_POST); 
			 $product_name=  $_POST['product_name'];
			 $product_price=  $_POST['product_price'];
			if(isset($_POST['image_name']) != ""){
			 $image_name	=  $_POST['image_name'];
			}
			else
			{
				$image_name = "";
			}
			 $main_title	=  $_POST['main_title'];
			 $old_price	=  $_POST['oldprice'];
			 $offer_img	=  $_POST['offr_img'];
			 $offer_img2	=  $_POST['offr_img2'];
			 $offr_img_class=$_POST['offr_img_class'];
			 $offertext=$_POST['offertext'];
			 if($offertext !="")
			  {
				  $offertext = '<span>'.$offertext.'</span>';
			  }
			 // Get Tamplate content
				$class_offer_img = "";
				if($offer_img ==''){
					$class_offer_img = "offer1-notfound";
				}
				if($offer_img2 ==''){
					$class_offer_img = "offer2-notfound";
				}
				if($offer_img =='' && $offer_img2 ==''){
					$class_offer_img = "offer-notfound";
				}
				
			  if($offer_img!='' || $offer_img2!=''){
			  	if($_POST['paper_size']=='a4multi' ){
				 $get_template =  file_get_contents('templateA4.html');
				 
			   }else{
				 $get_template =  file_get_contents('offer_template1.html');
			   }
			   
			  }
			  else{
			 if($_POST['paper_size']=='a4multi' ){
				 $get_template =  file_get_contents('templateA4.html');
			 }else{
			 $get_template =  file_get_contents('template.html');
			 }
			 
			}
			$LOGO = $image_name;
			  // Main title
			 $MAIN_TITLE=  $main_title;
			 $FONT_SIZE_MAIN_TITLE = '60px';
			  // Product Name
			  $PRODUCT_NMAE = $product_name;
			  $FONT_SIZE_PRODUCT_NAME = '50px';
			  $show_old_price_strike='';
			  if($old_price!=""){
				  $exp_old = explode('.',$old_price);
				  $OLD_PRICE= array_shift($exp_old); //Old price before decimal
				  $OLD_PRICE_DEC =  end($exp_old);  //Old price after decimal
				  $show_old_price_strike= '<span class="o_price">'.$OLD_PRICE.'<sub>€'.$OLD_PRICE_DEC .'</sub></span>';
			  }
			  

			  $new_price = $product_price;
			  $exp_new = explode('.',$new_price);
			  $NEW_PRICE= array_shift($exp_new);  //New price before decimal
			  $NEW_PRICE_DEC =  end($exp_new);    //New price after decimal
			  if($NEW_PRICE_DEC!='') $NEW_PRICE_DEC= '<span class="dollarsign">€</span>'.end($exp_new);

			  //Font Size
			   $font_size ='22vw';
			   if($LOGO != ""){
			   $url =  $LOGO ;
			   $exp_img =  explode('/',$url);
			   $image_name = end($exp_img);
			   $img_name = $path.$image_name;

			  // Function to write image into file
			  file_put_contents($img_name, file_get_contents($url));
			   }
			   else
			   {
				   $img_name = "";
			   }

			  // Find an Replace the the content
			  $array_replace_from =array("[LOGO]","[NEW_PRICE]","[NEW_PRICE_DEC]","[OLD_PRICE]","[PRODUCT_NMAE]","[FONT_SIZE]","[FONT_SIZE_PRODUCT_NAME]","[MAIN_TITLE]","[FONT_SIZE_MAIN_TITLE]","[OFFER_IMG]","[OFFER_IMG2]","[OFFER_IMG_CLASS]","[OFFER_TEXT]","[CLASS_OFFER_IMG]");
			  $array_replace_to = array($img_name,$NEW_PRICE,$NEW_PRICE_DEC,$show_old_price_strike,$PRODUCT_NMAE,$font_size,$FONT_SIZE_PRODUCT_NAME,$MAIN_TITLE,$FONT_SIZE_MAIN_TITLE,$offer_img,$offer_img2,$offr_img_class,$offertext,$class_offer_img);
			  //Show template
			  echo  $final_template = str_replace($array_replace_from,$array_replace_to,$get_template);
			  die;

   }

// Save token if expire from the form
 if(isset($_POST['action']) && $_POST['action']=='token_save'){
	touch('token.txt');
	chmod('token.txt',0777);
	file_put_contents('token.txt',trim($_POST['token']));
 }


 // Change Size from dropdown
 if(isset($_POST['action']) && $_POST['action']=='a4multi'){
	  $offer_img	=  $_POST['offr_img'];
	  $offer_img2	=  $_POST['offr_img2'];
	 if($offer_img!='' ||  $offer_img2!=''){

			  if($_POST['paper_size']=='a4multi' ){
				 $get_template =  file_get_contents('templateA4.html');
			 }else{
				$get_template =  file_get_contents('offer_template1.html');
			 }
			  }
			  else{

	$paper_size = $_POST['paper_size'];
	if($paper_size == 'a4' || $paper_size == 'a5'){
		$get_template =  file_get_contents('template.html');
	}
	if($paper_size == 'a4multi'){
		$get_template =  file_get_contents('templateA4.html');
	}
}
	$main_title = $_POST['template_main_title'];
	$new_price = $_POST['new_price'];
	$new_price_dec = $_POST['new_price_dec'];
	$product_name = $_POST['product_name1'];
	$image_name = $_POST['image_name'];
	$old_price = $_POST['oldprice'];
	$show_old_price_strike='';
	 $offer_img	=  $_POST['offr_img'];
	  $offer_img2	=  $_POST['offr_img2'];
	  $offertext=$_POST['offertext'];
	  if($offertext !="")
	  {
		  $offertext = '<span>'.$offertext.'</span>';
	  }
			  $offr_img_class=$_POST['offr_img_class'];
	 if($old_price!=0){
		
			$exp_old = explode('.',$old_price);
			
			$OLD_PRICE= array_shift($exp_old); //Old price before decimal
			$OLD_PRICE_DEC =  end($exp_old);  //Old price after decimal
			$show_old_price_strike= '<span class="o_price">'.$OLD_PRICE.'<sub>€'.$OLD_PRICE_DEC .'</sub></span>';
			
	  }
	 
	  $new_price_dec = str_replace("€","",$new_price_dec);
	  $NEW_PRICE_DEC= '<span class="dollarsign">€</span>'.$new_price_dec;
	  //echo $NEW_PRICE_DEC;

     $array_replace_from =array("[LOGO]","[NEW_PRICE]","[NEW_PRICE_DEC]","[PRODUCT_NMAE]","[MAIN_TITLE]","[OLD_PRICE]","[OFFER_IMG]","[OFFER_IMG2]","[OFFER_IMG_CLASS]","[OFFER_TEXT]");
	 $array_replace_to = array($image_name,$new_price,$NEW_PRICE_DEC,$product_name,$main_title,$show_old_price_strike,$offer_img,$offer_img2,$offr_img_class,$offertext);
			  //Show template
	echo  $final_template = str_replace($array_replace_from,$array_replace_to,$get_template);
	die;

 		}
	}
}

//Contact form controller

public function contact_email(ContactRequest $request){
 //check contact form fields validation

 
	 //send email from user to admin (contact-email).
	 $name = $request->input('name');
	 $email = $request->input('email');
	 $subj = $request->input('subject');
	 $msg = $request->input('message');
	 $fromname = "supraform contact email";
	 $to = 'arunsharma.webethics';
	 $from = $email;

   $result = DB::table('email')->where('id', '=' , '17' )->get();
   $subject = $result[0]->subject;
   $message_body = $result[0]->description;
	 $list = Array(
					 '[name]' => $name,
					 '[email]' => $email,
					 '[subject]' => $subj,
					 '[message]' => $msg,
				 );
	    $find = array_keys($list);
			$replace = array_values($list);
			$message = str_ireplace($find, $replace, $message_body);
	    //$mail = $this->send_email_($to, $subject, $message, $from, $fromname);
			$mail = $this->send_email_($to, $subject,$message, $from, $fromname);
			return response(['status'=>'success', 'msg'=>'Thank you for contacting us, we will get back to you soon.!!']);  

		  //Session::flash('success', 'Thank you for contacting us, we will get back to you soon.!!');
				//return Redirect::to('contact-us');
		 // return json_encode(['status'=>'success']);  
				//return Redirect::back();
 

}

public function send_email_($to,$subject,$message,$from,$fromname){

		try {
		$mail = new PHPMailer();
		$mail->isSMTP(); // tell to use smtp
		$mail->CharSet = "utf-8"; // set charset to utf8
		$mail->Host = "webethicssolutions.com";
		$mail->SMTPAuth = true;
		$mail->Port = 587;
		$mail->Username = "php@webethicssolutions.com";
		$mail->Password = "el*cBt#TuRH^";
		$mail->From = "contact@suprfarm.fr";
		//$mail->From = "test@gmail.com";
		$mail->FromName = $fromname;
        $mail->AddAddress('pathcodertest@gmail.com ');
		$mail->IsHTML(true);
		$mail->Subject = $subject.' From '.'S-'.rand();
		$mail->Body = $message;
		//$mail->addReplyTo(‘examle@examle.net’, ‘Information’);
		//$mail->addBCC(‘examle@examle.net’);
		//$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
		$mail->SMTPOptions= array(
		'ssl' => array(
		'verify_peer' => false,
		'verify_peer_name' => false,
		'allow_self_signed' => true
		)
		);

		$mail->send();
		return true ;
		} catch (phpmailerException $e) {
		dd($e);
		} catch (Exception $e) {
		dd($e);
		}
		 return false ;
	 }




   public function verify($token)
		{
			$data = array('register_token'=>'');
			DB::table('users')->where('register_token',$token)->update($data);
			Session::flash('success', 'Your Account has been verified.');
			return redirect('login');

		}


public function flyer_tool()
{
	$dir    = public_path()."/uploads/flyer/1";
	$files1= scandir($dir);

	$dir2    = public_path()."/uploads/flyer/2";
	$files2= scandir($dir2);

	$templates=DB::table('flyer_template')->where('user_id','=', Session::get('user_id'))->get();
	$result = DB::table('users')->where('id', '=' , Session::get('user_id'))->get();
	
	$user_id=Session::get('user_id');
		$subscription = DB::table('subscriptions')->where('user_id', '=', Session::get('user_id'))->get();

		if(count($subscription)>0)
		{

		$token = $this->flyer_token();
		$cip=2255336;
		$prdocuts=$this->get_products($cip,$token);
		// echo "<pre>";
		// print_r($prdocuts);die;
		return view("admin.member.flyer",compact('prdocuts','files1','files2','templates','result'));
		}
		else
		{
		return redirect('/services');
		}
}

public function user_tranaction()
{
		$transactions = DB::table('transactions')->where('user_id','=',Session::get('user_id'))->orderBy('id','desc')->get();
		//echo 'yes';die;
		return view("admin.member.transactions",compact('transactions'));
}


public function save_payment_data()
	{
		$merchant_id = DB::table('settings')->where('meta_key','=','payline_merchant_id')->get();
 		$access_key = DB::table('settings')->where('meta_key','=','payline_access_key')->get();
		$mer_id=$merchant_id[0]->meta_value;
		$acc_key=$access_key[0]->meta_value;

			$merchant_id =$mer_id;
			$access_key = $acc_key;

			$proxy_host = '';
			$proxy_port = '';
			$proxy_login = '';
			$proxy_password = '';
			$environment = 'HOMO';

			$paylineSDK = new PaylineSDK($merchant_id, $access_key, $proxy_host, $proxy_port, $proxy_login, $proxy_password, $environment, $pathLog = null, $logLevel = Logger::INFO, $externalLogger = null, $defaultTimezone = "Asia/Dili");

		$subscription = DB::table('subscriptions')->where('status','=','active')->get();
		if(count($subscription)>0)
		{


			foreach($subscription as $sub)
			{

				$array['contractNumber'] = 1234567;
				$array['paymentRecordId'] = $sub->payment_id;


				$response = $paylineSDK->getPaymentRecord($array);
				// echo '<pre>';
				// print_r($response);
				// echo '</pre>';
				//die;

				if($response['result']['shortMessage']=='ACCEPTED' && $response['isDisabled']==0)
				{
					foreach($response['billingRecordList']['billingRecord'] as $bill)
					{
						if($bill['rank']>$sub->rank && $bill['status']==1)
						{
							if(isset($bill['result']) && $bill['result']['shortMessage'] == 'ACCEPTED')
							{
								$amount=$bill['amount']/100;
								$data=array('user_id'=>$sub->user_id,'service_id'=>$sub->service_id,'transaction_id'=>$bill['transaction']['id'],'payment_id'=>$sub->payment_id,'amount'=>$amount,'created_on'=>$bill['transaction']['date']);
								DB::table('transactions')->insert($data);
								$data1=array('rank'=>$bill['rank']);
								DB::table('subscriptions')->where('payment_id','=',$sub->payment_id)->update($data1);
								// $bill['result']='';
								// $bill['transaction']='';
							}
						}
					}

					foreach($response['billingRecordList']['billingRecord'] as $bill)
					{

						if($bill['status']==0)
						{

							$nex_date= $bill['date'];
							$data1=array('next_billing_date'=>$nex_date);
							DB::table('subscriptions')->where('payment_id','=',$sub->payment_id)->update($data1);
							break;
						}

					}
				}

			}

		}
	}


/****----------------------------Get subscription payment data----------------**/

public function subscription_payment_detail()
	{

			include('vendor/gocardless/gocardless-pro/lib/loader.php');
			$access_token = 'sandbox_oew80reeCDzUCUF0TmalQaMYDAdDr2BKgR0zmUN6';
			$client = new \GoCardlessPro\Client(array(
			  'access_token' => $access_token,
			  'environment'  => \GoCardlessPro\Environment::SANDBOX
			));

			$data = $client->payments()->list();


				foreach ($data->api_response->body->payments as $key => $value) {


						$payment_id = $value->id;
						$trans=DB::table('transactions')->where('payment_id','=',$payment_id )->get();
						if(count($trans)==0)
						{
						$subscription_id = $value->links->subscription;
						$amount = $value->amount;
						$amount=$amount/100;
						$created_on = $value->charge_date;

					$subscription = DB::table('subscriptions')->where('subscription_id','=',$subscription_id)->where('status','=','active')->get();


					$date = date('Y-m-d H:i:s');
					 $sub_data=array('user_id'=>$subscription[0]->user_id,'service_id'=>$subscription[0]->service_id,'payment_id'=>$payment_id,'transaction_id'=>$payment_id,'amount'=>$amount,'created_on'=>$created_on,'modiefied_on'=>$date);
				 	DB::table('transactions')->insert($sub_data);
				 }
					
				}

	}



public function make_payment_save_card(Request $request)
	{

		$rules = array();

		$validator = Validator::make($request->input(), $rules);
		if($request->input('existing_card')==0)
				{

					 $validator->getMessageBag()->add('existing_card', 'Please select any card.');
					  return redirect('/buy-service/'.$request->input('service'))->withErrors($validator)->withInput();
				}
				else{
 				$user= DB::table('users')->where('id', '=', Session::get('user_id'))->get();
 		$mer_id='';
 		$acc_key=
		$merchant_id = DB::table('settings')->where('meta_key','=','payline_merchant_id')->get();
 		$access_key = DB::table('settings')->where('meta_key','=','payline_access_key')->get();
		$mer_id=$merchant_id[0]->meta_value;
		$acc_key=$access_key[0]->meta_value;

			$merchant_id =$mer_id;
			$access_key = $acc_key;

			$proxy_host = '';
			$proxy_port = '';
			$proxy_login = '';
			$proxy_password = '';
			$environment = 'HOMO';

			$paylineSDK = new PaylineSDK($merchant_id, $access_key, $proxy_host, $proxy_port, $proxy_login, $proxy_password, $environment, $pathLog = null, $logLevel = Logger::INFO, $externalLogger = null, $defaultTimezone = "Asia/Dili");


				$plan = DB::table('plans')->where('id', '=', $request->input('service'))->get();
			// PAYMENT
			$data['payment']['amount'] = $plan[0]->amount;
			$data['payment']['currency'] = 978;
			$data['payment']['action'] = 101;
			$data['payment']['mode'] = 'REC';

			$mydate=getdate(date("U"));
			// ORDER
			$data['order']['ref'] = 'SupraPharm service';
			$data['order']['amount'] = $plan[0]->amount*100;
			$data['order']['currency'] = 978;
			$data['order']['date'] = $mydate['mday'].'/'.$mydate['mon'].'/'.$mydate['year'].' '.$mydate['hours'].':'.$mydate['minutes'];

			$data['scheduledDate'] = $mydate['mday'].'/'.$mydate['mon'].'/'.$mydate['year'].' '.$mydate['hours'].':'.$mydate['minutes'];
			$data['walletId'] = $user[0]->user_wallet;
			$data['cardInd'] = $request->input('existing_card');
			$data['recurring']['billingCycle'] = 40;
			$data['recurring']['amount'] = $plan[0]->amount*100;

			$data['payment']['contractNumber'] = '1234567';
			//$data['returnURL'] = 'http://127.0.0.1:8000/test/return';
			//$data['cancelURL'] = 'http://127.0.0.1:8000/test/cancel';


			$response1 = $paylineSDK->doRecurrentWalletPayment($data);

			if($response1['result']['shortMessage']=='ACCEPTED')
			{
				$date = date('Y-m-d H:i:s');
				$next_date = date("d/m/Y", strtotime(" +1 months"));
				$subscription=array('user_id'=>Session::get('user_id'),'service_id'=>$request->input('service'),'status'=>'active','payment_id'=>$response1['paymentRecordId'],'rank'=>'0','next_billing_date'=>$next_date,'created_on'=>$date);
				DB::table('subscriptions')->insert($subscription);
				 Session::flash('success', 'You has been successfully subscribed to this service.');
			}
			else{
				 Session::flash('error', 'Something went wrong.Please try again.');
			}


  			return redirect('/subscription');
				}

	}


	public function make_payment(Request $request)
	{

		$rules = array(
		'card_number'=>'required',
		'cvv'=>'required',
		'card_type'    => 'required',
		'expiry_date' => 'required',
		'street_number'=>'required',
		'area'=>'required',
		'city'    => 'required',
		'zip_code' => 'required',

		);

		$validator = Validator::make($request->input(), $rules);

		if ($validator->fails())
		{
		return redirect('/buy-service/'.$request->input('service'))->withErrors($validator)->withInput();
		}
		else
		{

			$result = DB::table('users')->where('id', '=', Session::get('user_id'))->get();

			$mer_id='';
 			$acc_key=
			$merchant_id = DB::table('settings')->where('meta_key','=','payline_merchant_id')->get();
 			$access_key = DB::table('settings')->where('meta_key','=','payline_access_key')->get();
			$mer_id=$merchant_id[0]->meta_value;
			$acc_key=$access_key[0]->meta_value;

			$merchant_id =$mer_id;
			$access_key = $acc_key;

			$proxy_host = '';
			$proxy_port = '';
			$proxy_login = '';
			$proxy_password = '';
			$environment = 'HOMO';

			$paylineSDK = new PaylineSDK($merchant_id, $access_key, $proxy_host, $proxy_port, $proxy_login, $proxy_password, $environment, $pathLog = null, $logLevel = Logger::INFO, $externalLogger = null, $defaultTimezone = "Asia/Dili");



			$array['wallet']['walletId']='test_'.Session::get('user_id');
			$array['wallet']['lastName']=$result[0]->last_name;
			$array['wallet']['firstName']=$result[0]->first_name;
			$array['address']['name']=$result[0]->username;
			$array['address']['street1']=$request->input('street_number');
			$array['address']['street2']=$request->input('area');
			$array['address']['cityName']=$request->input('city');
			$array['address']['zipCode']=$request->input('zip_code');
			$array['card']['number']=str_replace("-","",$request->input('card_number'));
			$array['card']['type']=$request->input('card_type');
			$array['card']['expirationDate']=str_replace("/","",$request->input('expiry_date'));
			$array['card']['cvx']=$request->input('cvv');
			$array['contractNumber'] = 1234567;

			$response = $paylineSDK->createWallet($array);



			if($response['result']['shortMessage']!='ACCEPTED')
			{

					if($response['result']['code']=='02521')
				{

					 $validator->getMessageBag()->add('card_number', 'This card already exist in this wallet.');
					  return redirect('/buy-service/'.$request->input('service'))->withErrors($validator)->withInput();
				}
				else{
 					$validator->getMessageBag()->add('card_number', 'Your card details is invalid.');
					  return redirect('/buy-service/'.$request->input('service'))->withErrors($validator)->withInput();

				}
			}
			else
			{
				$save_wallet=array('user_wallet'=>$array['wallet']['walletId']);
				  DB::table('users')->where('id', '=', Session::get('user_id'))->update($save_wallet);
				  $billing=array('user_id'=> Session::get('user_id'),'address'=>$request->input('street_number'),'address1'=>$request->input('area'),'city'=>$request->input('city'),'pincode'=>$request->input('zip_code'));
				  DB::table('billing_address')->insert($billing);
				//Get user all cards
			$card['contractNumber'] = 1234567;
			$card['walletId'] = $array['wallet']['walletId'];
			$card['cardInd'] = '';
			$cards = $paylineSDK->getCards($card);

			if(isset($cards['cardsList']['cards'][0]))
			{
				//for multiple cards
				$arr=$cards['cardsList']['cards'];
			}
			else{
				//for single card
				$arr[]=$cards['cardsList']['cards'];
			}

			$post_card = substr ($request->input('card_number'), -4);
			foreach($arr as $cardd)
			{

				//check card saved or not
				$exist = DB::table('cards')->where('user_id', '=', Session::get('user_id'))->where('card_number','=',$cardd['card']['number'])->get();
				if(count($exist)<1)
				{
					//save new card into DB
					$data=array('user_id'=>Session::get('user_id'),'card_number'=>$cardd['card']['number'],'expire_date'=>$cardd['card']['expirationDate'],'card_type'=>$cardd['card']['type'],'card_index'=>$cardd['cardInd']);
					DB::table('cards')->insert($data);

				}
					$first=$cardd['card']['number'];
					$last_four = substr ($first, -4);
					if($post_card==$last_four)
					{
						$cardindex=$cardd['cardInd'];
					}
			}




			$plan = DB::table('plans')->where('id', '=', $request->input('service'))->get();
			// PAYMENT
			$data['payment']['amount'] = $plan[0]->amount;
			$data['payment']['currency'] = 978;
			$data['payment']['action'] = 101;
			$data['payment']['mode'] = 'REC';

			$mydate=getdate(date("U"));
			// ORDER
			$data['order']['ref'] = 'SupraPharm service';
			$data['order']['amount'] = $plan[0]->amount*100;
			$data['order']['currency'] = 978;
			$data['order']['date'] = $mydate['mday'].'/'.$mydate['mon'].'/'.$mydate['year'].' '.$mydate['hours'].':'.$mydate['minutes'];

			$data['scheduledDate'] = $mydate['mday'].'/'.$mydate['mon'].'/'.$mydate['year'].' '.$mydate['hours'].':'.$mydate['minutes'];
			$data['walletId'] = $array['wallet']['walletId'];
			$data['cardInd'] = $cardindex;
			$data['recurring']['billingCycle'] = 40;
			$data['recurring']['amount'] = $plan[0]->amount*100;

			$data['payment']['contractNumber'] = '1234567';
			//$data['returnURL'] = 'http://127.0.0.1:8000/test/return';
			//$data['cancelURL'] = 'http://127.0.0.1:8000/test/cancel';


			$response1 = $paylineSDK->doRecurrentWalletPayment($data);

			if($response1['result']['shortMessage']=='ACCEPTED')
			{
				$date = date('Y-m-d H:i:s');
				$next_date = date("d/m/Y", strtotime(" +1 months"));


				$subscription=array('user_id'=>Session::get('user_id'),'service_id'=>$request->input('service'),'status'=>'active','payment_id'=>$response1['paymentRecordId'],'rank'=>'0','next_billing_date'=>$next_date,'created_on'=>$date);
				DB::table('subscriptions')->insert($subscription);
				 Session::flash('success', 'You has been successfully subscribed to this service.');
			}
			else{
				 Session::flash('error', 'Something went wrong.Please try again.');
			}


  			return redirect('/subscription');


			// echo '<pre>';

			// print_r($response1);
			// echo '</pre>';
			// die;

		}

	}
}

	public function cardless($id)
{
	// echo URL::to('/test');
	// die;
		include('vendor/gocardless/gocardless-pro/lib/loader.php');
		$access_token = 'sandbox_oew80reeCDzUCUF0TmalQaMYDAdDr2BKgR0zmUN6';
		$client = new \GoCardlessPro\Client(array(
		  'access_token' => $access_token,
		  'environment'  => \GoCardlessPro\Environment::SANDBOX
		));

		$user = DB::table('users')->where('id','=',Session::get('user_id'))->get();
		if($user[0]->customer_id =="" || $user[0]->mandate_id=="")
		{
			$session_token="SESS_".$this->getToken();
			$redirectFlow = $client->redirectFlows()->create([
			"params" => [
			// This will be shown on the payment pages
			"description" => "SupraPharm service",
			// Not the access token
			"session_token" => $session_token,
			"success_redirect_url" =>URL::to('/success-cardless'),
			// Optionally, prefill customer details on the payment page
			"prefilled_customer" => [
			"given_name" => $user[0]->username,
			"family_name" => $user[0]->first_name.' '.$user[0]->last_name,
			"email" => $user[0]->email
			
			]
			]
			]);

			$dta=array('token'=>$redirectFlow->id);
			DB::table('users')->where('id', '=', Session::get('user_id'))->update($dta);
			$date = date('Y-m-d H:i:s');
			$data=array('user_id'=>Session::get('user_id'),'service_id'=>$id,'token'=>$redirectFlow->id,'session_token'=>$session_token,'created_at'=>$date);
			$user=DB::table('cardless_token')->insert($data);
			return redirect($redirectFlow->redirect_url);
		}
		else
		{
				$service = DB::table('plans')->where('id','=',$id)->get();
				$subscription=$client->subscriptions()->create([
	  			   "params" => ["amount" => $service[0]->amount*100,
	               "currency" => "EUR",
	               "name" => $service[0]->title,
	               "interval_unit" => "monthly",
	               "day_of_month" => 1,
	               //"metadata" => ["order_no" => "ABCD1234"],
	               "links" => ["mandate" => $user[0]->mandate_id]]
]);
		$date = date('Y-m-d H:i:s');
		$sub_data=array('user_id'=>Session::get('user_id'),'service_id'=>$id,'status'=>'active','subscription_id'=>$subscription->id,'created_on'=>$date);
		DB::table('subscriptions')->insert($sub_data);
		Session::flash('success', 'You has been successfully subscribed to this service.');
		return redirect('/subscription');
		}


}

public function success_cardless()
{
	include('vendor/gocardless/gocardless-pro/lib/loader.php');
		$access_token = 'sandbox_oew80reeCDzUCUF0TmalQaMYDAdDr2BKgR0zmUN6';
		$client = new \GoCardlessPro\Client(array(
		  'access_token' => $access_token,
		  'environment'  => \GoCardlessPro\Environment::SANDBOX
		));
		$tokens = DB::table('cardless_token')->where('token','=',$_GET['redirect_flow_id'])->get();
		$service = DB::table('plans')->where('id','=',$tokens[0]->service_id)->get();
		$redirectFlow = $client->redirectFlows()->complete($_GET['redirect_flow_id'], 
	  		  ["params" => ["session_token" => $tokens[0]->session_token]]
		);
		$dta = array('customer_id'=>$redirectFlow->links->customer,'mandate_id'=>$redirectFlow->links->mandate);
		DB::table('users')->where('id', '=', Session::get('user_id'))->update($dta);

		$subscription=$client->subscriptions()->create([
  		"params" => ["amount" => $service[0]->amount*100,
               "currency" => "EUR",
               "name" => $service[0]->title,
               "interval_unit" => "monthly",
               "day_of_month" => 1,
               //"metadata" => ["order_no" => "ABCD1234"],
               "links" => ["mandate" => $redirectFlow->links->mandate]]
		]);
		$date = date('Y-m-d H:i:s');
		$sub_data=array('user_id'=>Session::get('user_id'),'service_id'=>$tokens[0]->service_id,'status'=>'active','subscription_id'=>$subscription->id,'created_on'=>$date);
		DB::table('subscriptions')->insert($sub_data);
		$tokens = DB::table('cardless_token')->where('token','=',$_GET['redirect_flow_id'])->delete();
		 Session::flash('success', 'You has been successfully subscribed to this service.');
		 return redirect('/subscription');
}

	/**--------------------------------------Cancel subscription----------------------------------------**/

	public function cancel_subscription($id){

		include('vendor/gocardless/gocardless-pro/lib/loader.php');
		$access_token = 'sandbox_oew80reeCDzUCUF0TmalQaMYDAdDr2BKgR0zmUN6';
		$client = new \GoCardlessPro\Client(array(
		  'access_token' => $access_token,
		  'environment'  => \GoCardlessPro\Environment::SANDBOX
		));

    	$cancel_subscription = DB::table('subscriptions')->where('user_id','=',Session::get('user_id'))->where('service_id','=',$id)->where('status','=','active')->get();
		$client->subscriptions()->cancel($cancel_subscription[0]->subscription_id);

		$data = array('status'=>'deactive');
		$cancel_subscription = DB::table('subscriptions')->where('user_id','=',Session::get('user_id'))->where('service_id','=',$id)->update($data); 

        	if($cancel_subscription){

				 Session::flash('success', ' Your subscription cancel successfully.');
				 return redirect('/services');

			}else{

				 Session::flash('error', 'Subscription not cancel something went wrong.');
				 return redirect('/services');
			}
                                               
		        
	}



	public function buy_service($id)
	{
		$subscription = DB::table('subscriptions')->where('user_id','=',Session::get('user_id'))->where('service_id','=',$id)->where('status','=','active')->get();
		if(count($subscription)>0)
		{
			 Session::flash('success', 'You have already active subscription for this service.');
			 return redirect('/subscription');
		}
		else
		{
		$cards=array();
		$address='';
		$area='';
		$city='';
		$pin='';
		$service = DB::table('plans')->where('id','=',$id)->get();
		$cards=DB::table('cards')->where('user_id','=',Session::get('user_id'))->get();
		$billing=DB::table('billing_address')->where('user_id','=',Session::get('user_id'))->orderby('id','desc')->get();
		if(count($billing)>0){
			$address=$billing[0]->address;
			$area=$billing[0]->address1;
			$city=$billing[0]->city;
			$pin=$billing[0]->pincode;
	}

		return view("admin.member.buy_service",compact('service','id','cards','address','area','city','pin'));
	}
	}

	public function cancel_service($id)
	{
			$mer_id='';
 			$acc_key=
			$merchant_id = DB::table('settings')->where('meta_key','=','payline_merchant_id')->get();
 			$access_key = DB::table('settings')->where('meta_key','=','payline_access_key')->get();
			$mer_id=$merchant_id[0]->meta_value;
			$acc_key=$access_key[0]->meta_value;

			$merchant_id =$mer_id;
			$access_key = $acc_key;

			$proxy_host = '';
			$proxy_port = '';
			$proxy_login = '';
			$proxy_password = '';
			$environment = 'HOMO';

			$paylineSDK = new PaylineSDK($merchant_id, $access_key, $proxy_host, $proxy_port, $proxy_login, $proxy_password, $environment, $pathLog = null, $logLevel = Logger::INFO, $externalLogger = null, $defaultTimezone = "Asia/Dili");

			$subscription = DB::table('subscriptions')->where('user_id','=',Session::get('user_id'))->where('service_id','=',$id)->where('status','=','active')->get();

			$array['contractNumber'] = 1234567;
			$array['paymentRecordId'] = $subscription[0]->payment_id;


			$response = $paylineSDK->disablePaymentRecord($array);
			// echo '<pre>';
			// print_r($response);
			// echo '</pre>';
			// die;

			if($response['result']['shortMessage']=='ACCEPTED')
				{
					$date = date('Y-m-d H:i:s');
					$dta=array('status'=>'deactive',);
					DB::table('subscriptions')->where('id', '=', $subscription[0]->id)->update($dta);
					 Session::flash('success', 'You subscription has been canceled.');
				}
			else{
				 Session::flash('error', 'Something went wrong.Please try again.');
			}


  			return redirect('/subscription');

	}

public function my_subscription()
	{


		$all_arr=array();
		$plans=array();
		$subscription = DB::table('subscriptions')
		->where('user_id','=',Session::get('user_id'))->where('status','=','active')->orderBy('id','desc')->get();
// echo '<pre>';
// print_r($subscription);
// echo '</pre>';
// die;
		// foreach($subscription as $sub){
		// $all_arr[]=$sub->service_id;
		// }
		//$services = DB::table('plans')->whereIn('id',$all_arr)->get();

		return view("admin.member.my_services",compact('subscription'));
	}


public function services()
	{
		$subs_arr=array();

		$subscription = DB::table('subscriptions')->where('user_id','=',Session::get('user_id'))->where('status','=','active')->get();

		foreach($subscription as $sub){
		$subs_arr[]=$sub->service_id;
		}
		$services = DB::table('plans')->get();


		return view("admin.member.services",compact('services','subs_arr'));
	}

public function categories()
	{


		$categories = DB::table('categories')->get();

		return view("admin.member.categories",compact('categories'));
	}

public function category_files($id)
	{


		$files = DB::table('google_drive')->where('category_id','=',$id)->get();

		$sub_category = DB::table('sub_categories')->where('category_id','=',$id)->get();

		return view("admin.member.category_files",compact('files','sub_category'));
	}

	public function sub_category_files($id)
	{


		$files = DB::table('google_drive')->where('subcategory_id','=',$id)->get();

		$sub_category = array();

		return view("admin.member.category_files",compact('files','sub_category'));
	}

public function register_save(Request $request)
    {


        $rules = array(
        'email'=>'required|email|unique:users',
        'username'=>'required|unique:users',
      'password'    => 'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@-_]).*$/|confirmed',
        'password_confirmation' => 'required|min:6'
        );

        $messages = ['password.regex' => "Your password must contain 1 lower case character 1 upper case character one number and One special character."];

        $validator = Validator::make($request->input(), $rules,$messages);

        $errors=$validator->getMessageBag();

        if ($validator->fails())
        {
            return Response::json(array(
            'success' => false,
            'errors' => $errors->toArray()
            ), 200);

        }
        else
        {
             $date = date('Y-m-d H:i:s');
            $data = array('username'=>$request->input('username'),'email'=>$request->input('email'),'password'=>md5($request->input('password')),'status'=>'enable','user_type'=>'user','created_on'=>$date);
            $user=DB::table('users')->insert($data);
            $userid = DB::getPdo()->lastInsertId();

            $userid_subsc=$userid.' '.$request->input('subscription');


            return Response::json(array(
            'success' => true,
            'errors' => '',
            'user_id'=>$userid_subsc
            ), 200);


        }
    }


public function pay_amount(Request $request)
    {


        $rules = array(
        'cardNumber'=>'required',
        'cardName'=>'required',
      'cardMonth'    => 'required',
        'cardYear' => 'required',
          'cardCode'=>'required',
        'billCountry'=>'required',
      'billAddress1'    => 'required',
        'billAddress2' => 'required',
          'billCity'=>'required',
        'state'=>'required',
      'billZip'    => 'required'

        );



        $validator = Validator::make($request->input(), $rules);

        $errors=$validator->getMessageBag();

        if ($validator->fails())
        {
            return Response::json(array(
            'success' => false,
            'errors' => $errors->toArray()
            ), 200);

        }
        else
        {
             $date = date('Y-m-d H:i:s');
             $next = date('Y-m-d H:i:s', strtotime('+1 months'));
             $custom=explode(' ', $request->input('custom'));
            $data = array('user_id'=>$custom[0],'subscription_id'=>$custom[1],'status'=>'enable','subscriber_id'=>'sub_abcd','customer_id'=>'cus_123','plan_id'=>$custom[1],'next_billing_date'=>$next,'created_on'=>$date);
            $user=DB::table('subscriptions')->insert($data);

          	Session::flash('success', 'Thanks for registertation');
            return Response::json(array(
            'success' => true,
            'errors' => '',
            'user_id'=>'',
            ), 200);


        }
    }


	public function index()
	{



		if(Session::get('user_id')){
			// echo Session::get('user_id').'yes';
			// die;
				return redirect('/account-settings');
		}
		else{
		$plans= DB::table('plans')->get();

 		return view("frontend.site.home",compact('plans'));
 	}
	}
/* public function home(){
	return view("frontend.layouts.home");
} */


	/* public function register($id) */
	public function register($id)
	{

		 $name = '';
		 $email = '';

		$plan= DB::table('plans')
		->where('id', '=', $id)
		->get();
		if(isset($plan[0]->id)){

			$price=$plan[0]->amount;
			$title=$plan[0]->title;



			  return view("frontend.site.register",compact('price','id','name','email','title'));

			}



		else{
			  return redirect('/');
		}
	}

	public function login(){
		// echo $user_id=Session::get('user_id'); die;

		if(Session::get('user_id')){
			// echo Session::get('user_id').'yes';
			// die;
				return redirect('/insta');
		}else{
			$email='';
			$password='';
			$checked='';
			/*if(isset($_COOKIE['phrma_email'])){
				$email=$_COOKIE['phrma_email'];
				$password=$_COOKIE['phrma_password'];
				$checked='on';
			} */

			return view("frontend.site.login",compact('email','password','checked'));
		}
	}




	public function help()
	{
		$top_help=DB::table('help')->get();
		$managing_help=DB::table('help')->where('help_categories','managing-your-account')->take(3)->get();
		$additional_help=DB::table('help')->where('help_categories','additional')->take(3)->get();
		$billing_help=DB::table('help')->where('help_categories','billing-subscription')->take(3)->get();
		return view("frontend.site.help",compact('top_help','managing_help','additional_help','billing_help'));
	}

	public function help_description($id)
	{
		$description_help= DB::table('help')->where('id','=',$id)->get();
		return view("frontend.site.help_description",compact('description_help'));
	}

	public function help_categories($id)
	{

		$view_all=DB::table('help')->where('help_categories','=',$id)->get();

		return view("frontend.site.help_categories",compact('view_all'));
	}

	public function help_search(Request $request)
	{

		$search = $request->input('search');
		if($search !='')
		{
			$search_all=DB::table('help')->where('help_title','LIKE','%'.$search.'%')->get();
			if(count($search_all)>0)
			{

				return view("frontend.site.help_search",compact('search_all'))->with('search_data', $search);
			}
			else
			{
				//Session::flash('search_error', $search);
				return view("frontend.site.help_search")->with('search_error', $search);
			}
		}


	}

	public function billing()
	{


		$billing = DB::table('transactions')
					->where('user_id', '=', Session::get('user_id'))
					->get();
		//$billing=array();
		return view("frontend.site.billing",compact('billing'));
	}

	public function pay_detail($id)
	{
		$pay_detail = DB::table('users')
		->join('transactions', 'users.id', '=', 'transactions.user_id')
		->select('users.username','users.email','transactions.insta_username','transactions.subscription_id','transactions.transaction_id','transactions.amount','transactions.currency_code','transactions.created_on')
		->where('transactions.transaction_id','=',$id)->get();

		$invoice_details = array();
		if($id){
			$stripe = Stripe::setApiKey(env('STRIPE_SECRET'));
			$invoice_details = $stripe->invoices()->find($id);
			/* echo "invoice_details <pre>";print_r($invoice_details); die; */
		}

		return view("frontend.site.pay_detail",compact('pay_detail','invoice_details'));

	}

	public function subscription(){
		$result_data='';
		$plans = DB::table('plans')->get();

		$subscription = DB::table('subscriptions')
					->where('user_id', '=', Session::get('user_id'))
					->get();

 		return view("frontend.site.subscription",compact('subscription','plans','result_data'));
	}

   	/* Check user Login */

	public	function check_login(Request $request){
	// print_r($request->input()); die;

	$email = $request->input('email');
	if(trim($email) != ""){
		if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$email)){
			$email_check = DB::table('users')
			->where('username', '=', $email)
			->count();
			/* echo '$result1 '.$result1;die; */
			if($email_check > 0){
				$result1 = DB::table('users')
				->where('username', '=', $email)
				->get();
				$email = $result1[0]->email;
			}
		}
	}

	if($request->input('keep')=='on') {
		 /* setcookie("autolove_email", $request->input('email'), time() + 3600); */
		 setcookie("phrma_email", $email, time() + 3600);
		 setcookie("phrma_password", $request->input('password'), time() + 3600);
	}

	//echo $_COOKIE['autolove_email'];
	/* $rules = array('email' => 'required|email',
		'password' => 'required'
	);
	*/

	if($email == ""){
		$rules = array('email' => 'required|email',
			'password' => 'required'
		);
	}else{
		$rules = array('email' => 'required',
			'password' => 'required'
		);
	}

	$validator = Validator::make($request->input(), $rules);
	if ($validator->fails()) {
		return redirect('/login')->withErrors($validator)->withInput($request->except('password'));
	} else{
		$result = DB::table('users')
		->where('email', '=', $email)
		->where('password', '=', md5($request->get('password')))
		->get();

		if(isset($result[0]->id)){

			if($result[0]->status == 'disable')
				{
					Session::flash('error', "Your Account is disabled ");
					return Redirect::to('login');
					die;
				}
			else{
			$name = $result[0]->username;
			Session::put('user_id', $result[0]->id);
			Session::put('user_name',$name);
			Session::put('user_type','user');

			$subscription = DB::table('subscriptions')
		->where('user_id', '=', $result[0]->id)->get();
			Session::put('subscription_id',$subscription[0]->plan_id);
			return Redirect::intended('/account-settings');
		}
		  }
		else{
			Session::flash('error', 'The username or password you have entered is invalid.');
			return Redirect::to('/login');
		}
	}
}

  public function forget_password(){

	 return view("frontend.site.forget_password");

	}

	public function privacy(){

	 return view("frontend.site.privacy");

	}
	public function terms(){

	 return view("frontend.site.terms");

	}

public function faq(){
	$top_help=DB::table('help')->get();
	$managing_help=DB::table('help')->where('help_categories','managing-your-account')->take(3)->get();
	$additional_help=DB::table('help')->where('help_categories','additional')->take(3)->get();
	$billing_help=DB::table('help')->where('help_categories','billing-subscription')->take(3)->get();
	/* return view("frontend.site.help",compact('top_help','managing_help','additional_help','billing_help')); */

	return view("frontend.site.faq",compact('top_help','managing_help','additional_help','billing_help'));


}

/************    Send email for forgot password   ****************************/
public function forget_password_email(Request $request){

				$rules = array('email'    => 'required|email');
				$validator = Validator::make($request->input(), $rules);
				if ($validator->fails()) {
						return redirect('/forget-password')->withErrors($validator)->withInput($request->except('password'));
				} else {
					 $result = DB::table('users')
					->where('email', '=', $request->get('email'))
				   ->get();

					 if(count($result)>0){
							 $token = $this->getToken();

						     $password_email = DB::table('password_resets')
							->where('email', '=', $request->get('email'))
							->get();
					     	 $time = date('Y-m-d H:i:s');
							 $data = array('email'=>$request->get('email'),'token'=>$token,'created_at'=>$time);
						     if(count($password_email)>0){
								 DB::table('password_resets')->where('email', '=', $request->get('email'))
								->update($data);
							 }else{
							    DB::table('password_resets')->insert($data);
							 }

							  //send email confirmation to user email.

					            $uname = $result[0]->username;
					            $url= url('/reset-password/'.$token);
								$link = $url ;
								//$logo_url = url('frontend/images/logo.png');
					            $to = $request->get('email');
					            $result = DB::table('email')->where('id', '=' , '16' )->get();
					            $subject = $result[0]->subject;
					            $message_body = $result[0]->description;
					            $list = Array
					             (
					                '[name]' => $uname,
					                '[link]' => $link,
					             );

					             $find = array_keys($list);
					             $replace = array_values($list);
					             $message = str_ireplace($find, $replace, $message_body);
					            	$fromname='';
						 		$from = "test@webethicssolutions.com";
					             $mail = $this->send_email($to, $subject, $message, $from, $fromname);




							 // $url= url('/reset-password/'.$token);
							 // $link = $url ;
							 // $message = 'Please Click <a href="'.$link.'">here</a> to reset your password.';
							 // $to = $result[0]->email;
							 // $subject = 'Reset Password';


						 	// $mail=$this->send_email($to,$subject,$message,$from,$fromname);






  						Session::flash('success', 'Please check your email to reset the password.');
						 	return Redirect::to('/forget-password');

					  }else{
						  Session::flash('error', 'The email you have entered is invalid.');
								  return Redirect::to('/forget-password');
					  }
				  }

}
/******************* Open Reset Password Form  *************************/
public function reset_password($token){
	$result = DB::table('password_resets')
	->where('token', '=', $token)
	->get();

	$flag = false;
	if(count($result)>0){
	$email =  $result[0]->email;
	$date =  date('Y-m-d H:i:s');
	$hourdiff = round((strtotime($date) - strtotime($result[0]->created_at))/3600, 1);
	if($hourdiff>24){
	$flag =true;
	return view('frontend.site.reset_password',compact('token','flag','email'));

	} else{

	return view('frontend.site.reset_password',compact('token','flag','email'));

	}
	}else{
	return redirect('/');
	}
}
/**************** Save Password from reset form page  ***********************/
public function save_reset_password (Request $request){

				$rules = array(
							   'password'    => 'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@]).*$/|confirmed',
							   'password_confirmation' => 'required|min:6'

							  );
	            $messages = ['password.regex' => "Your password must contain 1 lower case character 1 upper case character one number"];
				$validator = Validator::make($request->input(), $rules,$messages);

				if ($validator->fails()) {

					 return redirect('/reset_password/'.$request->input('password_token'))->withInput()->withErrors($validator);
				} else {
						 $result = DB::table('users')
						->where('email', '=', $request->get('email'))

						->get();

						 if(count($result)>0){
							  $date = date('Y-m-d H:i:s');
							  $data = array('password'=>md5($request->get('password')),'modified_on'=>$date);
							   DB::table('users')->where('email', '=', $request->get('email'))
									->update($data);
							Session::flash('success', 'Your password has been updated successfully.');
							return redirect('/login');
						 }else{
						   return redirect('/');
						 }
				}
}



/* User Logout function */
	public function logout()
	{

//
			Session::put('user_id','');
			Session::put('user_name','');
			Session::put('user_type','');
			Session::put('subscription_id','');
			Session::flush();
			return redirect('/admin/login');

	}


/****************** Send email **********************/

function send_email($to='',$subject='',$message='',$from='',$fromname=''){

		try {
		$mail = new PHPMailer();
		$mail->isSMTP(); // tell to use smtp
		$mail->CharSet = "utf-8"; // set charset to utf8
		$mail->Host = "webethicssolutions.com";
		$mail->SMTPAuth = true;
		$mail->Port = 587;
		$mail->Username = "php@webethicssolutions.com";
		$mail->Password = "el*cBt#TuRH^";
		//$mail->From = "test@webethicssolutions.com";
		$mail->From = $from;
		$mail->FromName = $fromname;
		$mail->AddAddress($to);
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->addReplyTo(‘examle@examle.net’, ‘Information’);
		//$mail->addBCC(‘examle@examle.net’);
		//$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
		$mail->SMTPOptions= array(
		'ssl' => array(
		'verify_peer' => false,
		'verify_peer_name' => false,
		'allow_self_signed' => true
		)
		);

		$mail->send();
		return true ;
		} catch (phpmailerException $e) {
		dd($e);
		} catch (Exception $e) {
		dd($e);
		}
         return false ;
   }

	/******************** Get rendom number  *******************************/
 public function getToken()
{
	$length =20;
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[rand(0, $max-1)];
    }

    return $token;
}


	public function account_settings()
	{
		$user_id=Session::get('user_id');
		 $result = DB::table('users')
					->where('id', '=', $user_id)->get();
		$email= $result[0]->email;
 		return view("frontend.site.account_settings",compact('email'));
	}


public function update_email(Request $request){
  $user_id=Session::get('user_id');
				$rules = array('email'    => 'required|email|unique:users');
				$validator = Validator::make($request->input(), $rules);
				if ($validator->fails()) {
						return redirect('/account_settings')->withErrors($validator)->withInput($request->except('password'));
				} else {
					 $data = array('email'=>$request->get('email'));
					DB::table('users')->where('id', '=', $user_id)->update($data);
					Session::flash('success', 'Account settings has been updated.');
							return redirect('/account_settings');
				}
			}
 /************** Update password******************/

public function change_password(){
	return view("admin.member.change_password");

}


public function new_password(Request $request){
	$user_id = Session::get('user_id');
	// 	if($request->input('old_password')!='' || $request->input('password')!='' || $request->input('password_confirmation')!=''){
	// 		$rules = [
	// 		'password'    => 'min:6|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@]).*$/|confirmed|required',
	// 		'password_confirmation' => 'min:6|required',
	// 		'old_password' => 'required'
	//   ];
	// 	$messages = ['password.regex' => "Your password must contain 1 lower case character 1 upper case character one number"];
	//   $validator = Validator::make($request->input(),$rules,$messages);
	// 		if ($validator->fails())
	// 				return redirect('/account_settings')->withErrors($validator)->withInput();
	// 		else{
	// 			 $date = date('Y-m-d H:i:s');
	// 			 $data = array('password'=>md5($request->input('password')),'modified_on'=>$date,);
	// 			 DB::table('users')->where('id', '=', $user_id)
	// 		   ->update($data);
	// 			 Session::flash('success', 'Account settings has been updated.');
	// 		 	return redirect('/logout');

	// 		 }

	// }

	//return redirect('/account_settings');


	$rules = array('old_password' => 'required',
		 	'password'    => 'min:6|confirmed|required',
			'password_confirmation' => 'min:6|required',
		 	);
	$id = Session::get('user_id');
	$new_password = $request->get('password');
	$old_password = $request->get('old_password');

	$validator = Validator::make($request->input(), $rules);
	if ($validator->fails())
	{

		return redirect('/change-password')->withErrors($validator)->withInput();
	}
	else{

		  	$result = DB::table('users')
		 	->where('id', '=', $id)
		 	->where('password', '=', md5($old_password))
		 	->get();

		 	if(!$result->isEmpty()){

		 		$data = array('password'=>md5($new_password));
			    DB::table('users')->where('id', '=', $id)
					->update($data);

		 		Session::flash('success', 'Password changed successfully !');
		 		return redirect('/change-password');


		 		}
		 		else{

		 			Session::flash('error', 'Your current password does not matches with the password you provided. Please try again.');
		 			return redirect('/change-password');
		 		}
		 }
}



public function flyer_token()
{
	return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ3ZTQ2MDEwMTE3Y2U4ZmQ3NWQwOTUwZjVhMDExOWQ2MmY4MjJiN2ViZDkxMjg1Mzc0ZjYxNzY5ZDMwYzc1N2Q2NWVhMTZiYmNkYjRjZWExIn0.eyJhdWQiOiIyIiwianRpIjoiNDdlNDYwMTAxMTdjZThmZDc1ZDA5NTBmNWEwMTE5ZDYyZjgyMmI3ZWJkOTEyODUzNzRmNjE3NjlkMzBjNzU3ZDY1ZWExNmJiY2RiNGNlYTEiLCJpYXQiOjE1Njc1MDcyMDgsIm5iZiI6MTU2NzUwNzIwOCwiZXhwIjoxNjE5MzQ3MjA4LCJzdWIiOiIiLCJzY29wZXMiOlsiYWNjZXNzLWxpbWl0ZWQiXX0.ioYLogCW6G0GyQjCt3U1mZPnQPCCE2GGAPstB_oN_LKqpamxxSy-aTRanfSeU_DRA-NGfT9lhCl969-1OVz3GzYFKJa_5V7mHtTvueBqSX-AS8C9N63HXjmK56O7dg7rbE04OAmAHzzxCuWUKAOfYVtH2t9J3emgLCv51cFIawtdBQF1dT-50e0ihGYoCBH26dbjmsMTBwUgymvjt2BQrzAOgFFZpIVi3uF6w142zefRjuHnkng8nrQqn6AcR2AR79cn4L2Lj_XeR9-RjhYMSqRzu3sm2L0L3oqnXFyidth-ChymOU5mh5gKixGyvmkUcNSmelvxiFMJwy3iR-_ctnXp83H1gVKj0nX1m3ftUfbH5cVn3cXzO4nB2zXvSow4Ufhu3TpF-lPUitseUueSEAHrZUSz6DglnzJbdw6tb4Vdkn2NMzOmT08qar_1QkhtPi7t3PzW-yN2-A1E0xI-gdcy67JAtjmFXwnajf10i5iyW0YfuTVD61Yq5mH3D13QLglHCyQStUh7qoQ_xoo2l2R65ShvtJDUhUogajUmkn21C0d2achTONL3nrCIzijCVHsM0Mqz2_rIiy1dxTVEINc33FaE7rmmJynlSkyMprInFwC6XoKjCa0I6PI-qy4JugR_vH1qNgKM_R9onKr4TwMiXmJHHh60zmO6EIE2SAc';
}




public function shared_google_drive(){
// 		$files = glob(public_path()."/uploads/Google-Drive/*"); // get all file names
// 		foreach($files as $file){ // iterate files
//   	if(is_file($file))
//     unlink($file); // delete file
// }
//die;

		$categories=DB::table('categories')->get();
		 $client = new Google_Client();
		 putenv('GOOGLE_APPLICATION_CREDENTIALS='.public_path().'/client_secret.json');
		 $client->useApplicationDefaultCredentials();
		 $client->setScopes(['https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/spreadsheets','https://spreadsheets.google.com/feeds','https://docs.google.com/feeds','https://www.googleapis.com/auth/drive.file','https://www.googleapis.com/auth/drive.readonly','https://www.googleapis.com/auth/drive.metadata.readonly']);

		$drive = new Google_Service_Drive($client);
		$query = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
        $resultss = $drive->files->listFiles();

		$resultsss = $resultss->getFiles();
		
		$folderId = "18ouvTE-Bim-DHpeoTc83_RHQOELoFXfY";
		$optParams = array(
			'pageSize' => 10,
			'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
			'q' => "'".$folderId."' in parents"
        );
		 $results = $drive->files->listFiles($optParams);

		//  echo "<pre>";
		//  print_r($results); 
		// print_r($resultssss); 
		// die;



		 foreach ($resultsss as $file) {

		if($file->mimeType!='application/vnd.google-apps.folder'){

        $fileId = $file->id;


		$response = $drive->files->get($file->id, array('alt' => 'media'));



if (!file_exists(public_path()."/uploads/Google-Drive/".$file->id."_".$file->name))   
{


		// Open file handle for output.

		$outHandle = fopen(public_path()."/uploads/Google-Drive/".$file->id."_".$file->name, "w+");

		// Until we have reached the EOF, read 1024 bytes at a time and write to the output file handle.

		while (!$response->getBody()->eof()) {

		fwrite($outHandle, $response->getBody()->read(1024));
		}

		// Close output file handle.

		fclose($outHandle);
		//echo "Done.\n";
	}
		}
		}


		// print_r($content);
		// $target_dir = public_path()."/uploads/Google-Drive";


		// echo move_uploaded_file($response, $target_dir);

		// die;
// 		$retrieved_files = array();
// 		foreach($resultss->files as $file)
// 		{
// 			array_push($retrieved_files, array(
// 				'filename'  =>  $file->name,
// 				'fileid'    =>  $file->id,
// 				'file'      =>  $file
// 			));
// 		}
// 		/* echo "<pre>";
// 		print_r($retrieved_files);
// 		die; */
// $get_specific_file = array_search('example_file_name',
//     array_column($retrieved_files, 'filename'));

		return view('admin.member.g_drive_listing', compact('results','categories'));
	}



public function google_drive_by_folderID($id){


		$categories=DB::table('categories')->get();
		 $client = new Google_Client();
		 putenv('GOOGLE_APPLICATION_CREDENTIALS='.public_path().'/client_secret.json');
		 $client->useApplicationDefaultCredentials();
		 $client->setScopes(['https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/spreadsheets','https://spreadsheets.google.com/feeds','https://docs.google.com/feeds','https://www.googleapis.com/auth/drive.file','https://www.googleapis.com/auth/drive.readonly','https://www.googleapis.com/auth/drive.metadata.readonly']);

		$drive = new Google_Service_Drive($client);
		$query = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
        $resultss = $drive->files->listFiles();

		$results = $resultss->getFiles();
		
		$folderId = $id;
		$optParams = array(
			'pageSize' => 10,
			'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
			'q' => "'".$folderId."' in parents"
        );
		 $results = $drive->files->listFiles($optParams);

		


	

		return view('admin.member.g_drive_listing', compact('results','categories'));
	}


}
