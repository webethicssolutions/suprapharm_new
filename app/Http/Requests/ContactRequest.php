<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
        'name' => ['required'],
        'subject' => ['required'],
        'message' => ['required'],
        'email' => ['required','regex:/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/'],
       
        ];
       
    }

    public function messages(){
        return [

            'email.regex'=> 'Email should be in the format of abc@mail.com',
            


        ];
       
    }

    
}
