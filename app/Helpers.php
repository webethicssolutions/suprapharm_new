<?php


 function get_service_name($id)
 {
	 $plans = DB::table('plans')->where('id', '=', $id)->get();

       $plan=$plans[0]->title;
    
      return $plan;
}
 function get_service_amount($id)
 {
	 $plans = DB::table('plans')->where('id', '=', $id)->get();

       $amount=$plans[0]->amount;
    
      return $amount;
}
 function get_user_name($id)
 {
	 $name = DB::table('users')->where('id', '=', $id)->get();

       $fullname=$name[0]->first_name.' '.$name[0]->last_name;
    
      return $fullname;
}
function active_fyler_tool(){
	 $subscription = DB::table('subscriptions')->where('user_id', '=', Session::get('user_id'))->where('service_id','=',3)->where('status','=','active')->get();

      if(count($subscription)>0)
      {
      	return 'yes';
	  }
	  else
	  {
		return 'no';
	  }
    
     
}

 function get_file_categories($id)
 {
 	$parent=array();
 	$sub=array();
 	$total='';
 	$pa=array();
 	$su='';
    $all=DB::table('google_drive')->where('image_id','=',$id)->get();
    foreach($all as $al){
    	$parent[]=$al->category_id;
    	$sub[]=$al->subcategory_id;
    }
     $par_all=DB::table('categories')->whereIn('id',$parent)->get();
     foreach($par_all as $p_al){
     	$pa[]=$p_al->category_name;
     }
     $sub_all=DB::table('sub_categories')->whereIn('id',$sub)->get();
     foreach($sub_all as $s_al){
     	$pa[]=$s_al->sub_category_name;
     }
    $List = implode(', ', $pa); 
  
// Display the comma separated list 
return $List;  
 }

?>


