<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Session;
use App\User;	
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
           app('view')->composer('admin.layouts.master', function ($view) {
             $action = app('request')->route()->getAction();

             $controller = class_basename($action['controller']);

             list($controller, $action) = explode('@', $controller);

             $view->with(compact('controller', 'action'));
         });
		 
		 
		  app('view')->composer('frontend.layouts.master', function ($view) {
             $action = app('request')->route()->getAction();
			  $user_data =array();
              if(Session::get('user_id')!==''){
			   $user_id  =  Session::get('user_id');
			   $user_data = User::find($user_id);
			  }
			  
			  // $user_data['firstname'];
			 
              $controller = class_basename($action['controller']);

             list($controller, $action) = explode('@', $controller);

             $view->with(compact('controller', 'action','user_data'));
         });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
  public function register()
    {
        require_once __DIR__ . '/../Http/Helpers.php';
    }
}
