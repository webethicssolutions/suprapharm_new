<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	Route::get('/home', function () {
	    //return view('welcome');
		return view('/home');
		
	
});
	Route::get('/subsciption-payment','SiteController@subscription_payment_detail');
	Route::group(['prefix' => '','middleware' => 'user'], function () {
	Route::get('/buy-service/{id}','SiteController@cardless');
	Route::get('/success-cardless','SiteController@success_cardless');
	Route::get('/cancel-subscription/{id}','SiteController@cancel_subscription');

	//Route::get('/verify/{id}', 'UserController@verify');
	/*Route::get('/help','SiteController@help'); */
	//Route::get('/billing','SiteController@billing');
	Route::post('/update_email', 'SiteController@update_email');
	Route::post('/update_password', 'SiteController@new_password');
	Route::get('/help/{id}','SiteController@help_description');
	Route::get('/help-categories/{id}','SiteController@help_categories');
	/* Route::post('/search', 'SiteController@help_search'); */
	Route::get('/pay_detail/{id}','SiteController@pay_detail');
	//Route::get('/buy-service/{id}','SiteController@buy_service');
	Route::get('/services','SiteController@services');
	Route::get('/change-password','SiteController@change_password');
	Route::post('/make-payment', 'SiteController@make_payment');
	Route::post('/make-payment-save-card', 'SiteController@make_payment_save_card');
	Route::get('/subscription','SiteController@my_subscription');
	Route::get('/cancel-service/{id}', 'SiteController@cancel_service');
	Route::get('/flyer-tool', 'SiteController@flyer_tool');
	Route::get('/categories', 'SiteController@categories');
	Route::get('/category-files/{id}', 'SiteController@category_files');
	Route::get('/sub-category-files/{id}', 'SiteController@sub_category_files');
	Route::get('/transactions-details', 'SiteController@user_tranaction');
	Route::post('/save-flyer-template', 'SiteController@save_flyer_template');
	Route::post('/get-flyer-template', 'SiteController@get_flyer_template');
});
	Route::group(['prefix' => ''], function () {
	Route::post('send-email', 'SiteController@contact_email');
	Route::post('send-contact-email', 'SiteController@submit_contact_email');
	Route::post('/flyer-ajax', 'SiteController@flyer_ajax');
	Route::get('save-payment-data/','SiteController@save_payment_data');
	Route::get('/','HomeController@what_is_this');
	//Route::get('/','admin\AdminController@login');
	Route::get('/privacy','SiteController@privacy');
	Route::get('/terms','SiteController@terms');
	Route::get('/faq','SiteController@faq');
	/* Route::get('/register/{id}','SiteController@register'); */
	Route::get('/register/{id}/{planid?}','SiteController@register');
	Route::post('/register-save', 'SiteController@register_save');
	Route::get('/login','SiteController@login');
	Route::post('/check-login','SiteController@check_login');
	Route::get('/forget-password', 'SiteController@forget_password');
	Route::post('/forget-password-email', 'SiteController@forget_password_email');
	Route::get('/reset-password/{id}', 'SiteController@reset_password');
	Route::post('/save-reset-password', 'SiteController@save_reset_password');
	Route::post('/pay-amount', 'SiteController@pay_amount');
	Route::get('/logout', 'SiteController@logout');
	Route::get('/member-access', 'admin\AdminController@login');
	Route::get('/help','SiteController@help');
	Route::post('/search', 'SiteController@help_search');
	// Route::get('/member-access', 'HomeController@login');
	Route::get('/what-is-this', 'HomeController@what_is_this');
	Route::get('/our-services', 'HomeController@ourservices');
	//Route::get('/cartes-des-pharmacies', 'HomeController@cards_of_pharmacies');
	Route::get('/about_us', 'HomeController@about_us');
	Route::get('/contact-us', 'HomeController@contact_us');
	Route::get('/contact-usnew', 'HomeController@contact_usnew');
	//Route::get('/nos-services', 'HomeController@services');
	// Route::get('/nos-services', 'HomeController@newservices');
	// Route::get('/installation-help', 'HomeController@installation_help');
	Route::get('/vous-êtes-pharmacien', 'HomeController@pharmacist');
	Route::get('/cartes-des-pharmacies', 'HomeController@list_of_pharmacies');
	Route::post('/load-pharmacies','HomeController@loadDataAjax' );
	Route::get('/filter-pharmacies','HomeController@pharmacysearch');
	Route::post('send-email1', 'HomeController@contact_email');
	Route::get('/homenew','HomeController@home');
	Route::get('/search', 'SearchController@index');
	Route::get('/pharmacy/{id}', 'SearchController@detail');

	Route::get('/drive-folder/{id}','SiteController@google_drive_by_folderID');
		Route::get('/shareddrive','SiteController@shared_google_drive');
});


	Route::group(['prefix' => 'vous-êtes-pharmacien'], function (){
	Route::get('/nos-services', 'HomeController@newservices');
	Route::get('/installation-help', 'HomeController@installation_help');
	

});


	Route::group(['prefix' => 'admin'], function (){
	Route::post('checklogin','admin\AdminController@checklogin');
	Route::get('/', 'admin\AdminController@login');

});
