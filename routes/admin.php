<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([ 'prefix' => 'admin','middleware' => 'admin'], function () {
Route::get('user/adduser','admin\AdminController@addUser');
Route::get('images/add_image','admin\AdminController@addImage');
Route::get('add_promotion','admin\AdminController@addpromotion');
Route::get('pharmacy/create','admin\AdminController@add_pharmacy');
Route::POST('magzine/multiple-delete','admin\AdminController@deleteMultiple');
Route::POST('promotion/multiple-delete','admin\AdminController@deleteMultiplepromotion');

Route::post('importcsv', 'admin\AdminController@import_csv');


	Route::post('user/admin_save_user','admin\AdminController@admin_save_user');
	Route::post('images/save_image','admin\AdminController@save_image');

	Route::post('save_promotion','admin\AdminController@save_promotion');
	Route::get('/','admin\AdminController@index');
	Route::get('logout', 'admin\AdminController@logout');
	Route::get ('/change-password-admin','admin\AdminController@admin_change_password');
	Route::post('/password-change-admin','admin\AdminController@change_password_admin');
	Route::get('email', 'admin\AdminController@email_template');
	Route::get('email/edit/{id}', 'admin\AdminController@email_template_edit');
	Route::post('email/update','admin\AdminController@email_template_update');
	Route::get('email/new', 'admin\AdminController@new_email_template');
	Route::post('email/add','admin\AdminController@email_template_add');
	Route::get('delete/template/{id}', 'admin\AdminController@delete_template');

	Route::get('/users', 'admin\AdminController@users_details');
	Route::get('user/edit/{id}', 'admin\AdminController@editUser');
	Route::post('user/admin_user_edit','admin\AdminController@admin_user_edit');
	Route::get('deleteuser/{id}', 'admin\AdminController@delete_user');
	Route::get('deleteimage/{id}', 'admin\AdminController@delete_image');
	Route::get('deletepromotion/{id}', 'admin\AdminController@delete_promotion');


	    Route::post('user/disableuser/{id}', 'admin\AdminController@disableuser');
		Route::get('user/manage/{id}', 'admin\AdminController@user_manage');
		Route::get('user/user-services/{id}', 'admin\AdminController@user_services');
		Route::get('user/payment-details/{id}', 'admin\AdminController@payment_details');
	    Route::post('user/enableuser/{id}', 'admin\AdminController@enableuser');
		Route::post('magzine/enableimage/{id}', 'admin\AdminController@enableimage');
		Route::post('magzine/disableimage/{id}', 'admin\AdminController@disableimage');
		Route::post('promotion/enablepromotion/{id}', 'admin\AdminController@enablepromotion');
		Route::post('promotion/disablepromotion/{id}', 'admin\AdminController@disablepromotion');



		Route::get('/payline', 'admin\AdminController@payline_config');
		Route::post('/payline/save', 'admin\AdminController@save_payline');
		Route::get('/plans', 'admin\AdminController@plans');
		Route::get('/plan/edit/{id}', 'admin\AdminController@edit_plan');
		Route::get('/pharmacy/edit/{id}', 'admin\AdminController@edit_pharmacy');
		Route::post('/plan/update', 'admin\AdminController@save_edit_plan');
		Route::post('/pharmacy/update', 'admin\AdminController@update_pharmacy');
		Route::get('/plan/add', 'admin\AdminController@add_plan');
		Route::post('/plan/save', 'admin\AdminController@save_plan');
		Route::post('/pharmacy/save', 'admin\AdminController@save_pharmacy');
		Route::get('/plan/delete/{id}', 'admin\AdminController@delete_plan');
		Route::get('/pharmacy/delete/{id}', 'admin\AdminController@delete_pharmacy');
		Route::get('/all-subscriptions', 'admin\AdminController@all_subscriptions');
        Route::get('/promotions', 'admin\AdminController@promotion');
				Route::get('/pharmacy', 'admin\AdminController@pharmacy');
	    Route::get('/all-images', 'admin\AdminController@all_images');
		Route::get('/transactions', 'admin\AdminController@transactions');
		Route::get('/drive-folder/{id}','admin\AdminController@google_drive_by_folderID');
		Route::get('/shareddrive','admin\AdminController@shared_google_drive');
		Route::get('/createdrive','admin\AdminController@create_google_folder');

		Route::get('/category/add','admin\AdminController@add_category');
		Route::post('/category/save', 'admin\AdminController@save_category');
		Route::get('/category/edit/{id}','admin\AdminController@edit_category');
		Route::post('/category/edit-save', 'admin\AdminController@edit_save_category');
		Route::get('/category/delete/{id}', 'admin\AdminController@delete_category');

		Route::get('/category/add-sub','admin\AdminController@add_sub_category');
		Route::post('/category/save-sub', 'admin\AdminController@save_sub_category');
		Route::get('/sub-category/edit/{id}','admin\AdminController@edit_sub_category');
		Route::post('/sub-category/edit-save', 'admin\AdminController@edit_save_sub_category');
		Route::get('/sub-category/delete/{id}', 'admin\AdminController@delete_sub_category');

		Route::post('/category/get-sub-categories', 'admin\AdminController@get_sub_category');
		Route::post('/save-images-to-category', 'admin\AdminController@save_images_to_category');

		Route::post('/category/selected-categories', 'admin\AdminController@selected_category');
});

Route::group(['prefix' => 'admin'], function (){

	Route::post('checklogin','admin\AdminController@checklogin');
	Route::get('login', 'admin\AdminController@login');

});


?>
